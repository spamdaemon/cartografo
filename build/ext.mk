# this makefile fragment defines the external libraries
# for the libraries, examples, tests, and binaries
# 
# the following definitions must be output from this file:
# EXT_LIB_SEARCH_PATH : the search paths for libraries
# EXT_LINK_LIBS       : the libraries with which to link (in the proper link order)
# EXT_INC_SEARCH_PATH : the search paths for include files
EXT_LIB_SEARCH_PATH :=
EXT_LINK_LIBS       :=
EXT_INC_SEARCH_PATH :=
EXT_DEFINES := 

EXT_LIB_SEARCH_PATH += $(BASE_DIR)/contrib/shapelib/lib
EXT_LIB_SEARCH_PATH += $(BASE_DIR)/contrib/shapelib/lib64
EXT_LINK_LIBS += -lshp
EXT_INC_SEARCH_PATH += $(BASE_DIR)/contrib/shapelib/include

# LIBKML must be compiled with -frtti!!!
KML_HOME=$(BASE_DIR)/contrib/libkml-1.2.0
EXT_LIB_SEARCH_PATH += ${KML_HOME}/lib64
EXT_INC_SEARCH_PATH += ${KML_HOME}/include
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libkmlxsd.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libkmlengine.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libkmldom.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libkmlconvenience.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libkmlregionator.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libminizip.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/liburiparser.so
EXT_LINK_LIBS +=  ${KML_HOME}/lib64/libkmlbase.so
EXT_DEFINES += HAVE_LIBKML=1

# libkml needs expat
#EXPAT_HOME := /local/expat
EXT_LINK_LIBS       += -lexpat
#EXT_INC_SEARCH_PATH += $(EXPAT_HOME)/include

# this is an optional definition
# EXT_COMPILER_DEFINES : definitions passed to the compiler (without the -D option)
#EXT_CPP_OPTS := compiler options for c++ compilation
#EXT_C_OPTS := compiler options for c compilation
#EXT_FORTRAN_OPTS := compiler options for fortran compilation

EXT_COMPILER_DEFINES := ${EXT_DEFINES}
EXT_CPP_OPTS := 
EXT_C_OPTS := 
EXT_FORTRAN_OPTS := 
