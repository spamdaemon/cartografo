# this makefile generates list of source files for
# binaries, examples, libraries, tests, and modules
# it also produces headers
# The input variables to this file are
# BASE_DIR
# BASE_SOURCE_DIRS
# GEN_LIB_SOURCE_FILES
#
# The output of this stage is:
# TESTS : the list of tests that can be executed
# EXAMPLES : the list of examples that need to be produced
# BINARIES : the list of binaries
# LIB_OBJECT_FILES : the list of objects that need to linked into a library
# OBJECT_DIR : the directory where object files will be stored
# SOURCE_DIRS : the list of source directories
# MAKEFILE_MODULES : the list of modules to be built through recursive MAKE
# SRC_MODULES : the list of modules to be built automatically

# the header source types are those that need not be compiled
# these are normally c/c++ header files and template files
HEADER_TYPES := .h .tpl

# the object types are those that will produce objects when compiled
OBJECT_TYPES := .cpp .c .f

# all objects are placed into this directory
OBJECT_DIR := $(BASE_DIR)/obj

# we need to build the source query: our query will be a simple 
# find command in each directory
SOURCE_QUERY := $(addprefix -o -name \*, $(OBJECT_TYPES) $(HEADER_TYPES))

# since the source query starts with -o, we need to run find with 
# an initial test which has to return false for the file
SOURCE_FILES := $(shell find $(BASE_SOURCE_DIRS) -false $(SOURCE_QUERY) 2> /dev/null )

# find the sources that need to be compiled into object files
OBJECT_SOURCE_FILES := $(filter $(addprefix %,$(OBJECT_TYPES)),$(SOURCE_FILES) $(GEN_LIB_SOURCE_FILES))
HEADER_FILES := $(filter $(addprefix %,$(HEADER_TYPES)),$(SOURCE_FILES) $(GEN_LIB_SOURCE_FILES))

# get the object files for each type that will result during compilation

OBJECT_FILES := $(foreach ext, $(OBJECT_TYPES), \
	$(filter %.o,$(patsubst %$(ext),$(OBJECT_DIR)/%.o,$(OBJECT_SOURCE_FILES))))

LIB_OBJECT_FILES := $(filter $(OBJECT_DIR)/lib-src/%,$(OBJECT_FILES))
LIB_OBJECT_FILES += $(filter $(OBJECT_DIR)/gen-lib-src/%,$(OBJECT_FILES))

TESTLIB_OBJECT_FILES := $(filter $(OBJECT_DIR)/test-lib-src/%,$(OBJECT_FILES))


TEST_OBJECT_FILES := $(filter $(OBJECT_DIR)/tests-src/%.o, $(OBJECT_FILES))
EXAMPLE_OBJECT_FILES := $(filter $(OBJECT_DIR)/examples-src/%.o, $(OBJECT_FILES))
BINARY_OBJECT_FILES := $(filter $(OBJECT_DIR)/bin-src/%.o, $(OBJECT_FILES))

# the tests to be run
TESTS := $(patsubst $(OBJECT_DIR)/tests-src/%.o, $(BASE_DIR)/tests/%, $(TEST_OBJECT_FILES))

# test scripts (only those scripts or programs that are executable)
TEST_SCRIPTS := $(shell find $(BASE_DIR)/test-scripts -type f -perm +g+x,u+x,o+x 2> /dev/null )
TESTS += $(TEST_SCRIPTS)

# any examples
EXAMPLES := $(patsubst $(OBJECT_DIR)/examples-src/%.o, $(BASE_DIR)/examples/%, $(EXAMPLE_OBJECT_FILES))

# the binaries
BINARIES := $(patsubst $(OBJECT_DIR)/bin-src/%.o, $(BASE_DIR)/bin/%, $(BINARY_OBJECT_FILES))

# the list of modules: a module can be defined in one of two ways:
#  any directory under the modules directory that contains a Makefile 
#  any directory under the modules directory that contains the directory module-src
# MAKEFILE_MODULES are modules that whose makefile will simply be invoked as make module
# SRC_MODULES are modules for which can be built
MAKEFILE_MODULES := $(shell find $(BASE_DIR)/modules -name Makefile 2> /dev/null )
MODULES=$(shell for M in $(MAKEFILE_MODULES); do dir="$$(dirname "$${M}")"; echo "$${dir}/lib$$(basename "$$dir").so"; done)

