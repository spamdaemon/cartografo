#include <timber/Application.h>
#include <timber/logging.h>
#include <timber/WeakPointer.h>
#include <timber/ContentType.h>
#include <canopy/time/Time.h>

#include <cartografo/ProjectionDomain.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/AreaBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/Area.h>
#include <cartografo/Region.h>
#include <cartografo/ExtentPyramid.h>
#include <cartografo/Projector2D.h>
#include <cartografo/Projection.h>
#include <cartografo/projection/CylindricalProjection.h>
#include <cartografo/projection/AzimuthalProjection.h>
#include <cartografo/projection/ConicProjection.h>
#include <cartografo/projection/SimpleProjectionDomain.h>
#include <cartografo/projector/IdentityProjector2D.h>

#include <cartografo/geo/nodes.h>
#include <cartografo/map/Map.h>
#include <cartografo/map/Layer.h>
#include <cartografo/map/layers/GraticuleLayer.h>
#include <cartografo/map/kml/KmlLayer.h>
#include <cartografo/map/shapefile/ShapefileLayer.h>
#include <cartografo/map/raster/RasterLayer.h>
#include <cartografo/map/raster/WmsRasterProvider.h>
#include <cartografo/map/raster/OsmRasterProvider.h>
#include <cartografo/map/raster/AsyncRasterProvider.h>
#include <cartografo/map/raster/RasterCache.h>
#include <cartografo/map/nexus/MapContainer.h>

#include <terra/terra.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Extent.h>
#include <terra/Bounds.h>
#include <terra/Geodesic.h>

#include <nexus/Desktop.h>
#include <nexus/Window.h>
#include <nexus/MenuBar.h>
#include <nexus/Menu.h>
#include <nexus/MenuItem.h>

#include <nexus/event/MotionEventListener.h>
#include <nexus/event/MotionEvent.h>
#include <nexus/event/ButtonEventListener.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/CrossingEventListener.h>
#include <nexus/event/CrossingEvent.h>
#include <nexus/event/ComponentEventListener.h>
#include <nexus/event/ComponentEvent.h>
#include <nexus/event/WheelEventListener.h>
#include <nexus/event/WheelEvent.h>
#include <nexus/event/ActionEvent.h>
#include <nexus/event/ActionEventListener.h>
#include <nexus/event/KeyEvent.h>
#include <nexus/event/KeyEventListener.h>

#include <indigo/nodes.h>
#include <indigo/view/ViewPort.h>

#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;
using namespace ::cartografo;
using namespace ::cartografo::projection;
using namespace ::cartografo::projector;
using namespace ::nexus;
using namespace ::nexus::event;

typedef ::cartografo::map::nexus::MapContainer Viewer;
typedef ::indigo::view::ViewPort MapPort;

// i == 49 (don't know, but causes problem)
//  i==12 AUSTRALIA
//  i==7 ANTARCTICA
//  i==222 USA
//  i==175 RUSSIA
//  i==37 CANADA
//  i==72 FRANCE
//  i==100 A SINGLE AREA
//  i==9 ARGENTINA
static const int MIN_SHAPE_INDEX = 0; // 12         WWF(9750 caused an error)
static const int MAX_SHAPE_INDEX = -1; // 15            WWWF(13710)
static const int MIN_PART_INDEX = -0;
static const int MAX_PART_INDEX = -1;
static const double INITIAL_LATITUDE = 0;
static const double INITIAL_LONGITUDE = 0 * 170;

static const size_t GRATICULE_SIZE = 10;
static const double PROJECTION_SCALE = 1000000000.0;
//static const double PROJECTION_SCALE = 10;
static const bool RENDER_AS_GEODESIC = false;
static double AZIMUTH = toRadians(80.);

static bool USE_SPHEROID = false;

static const char* CURRENT_PROJECTION;
static const indigo::AntiAliasingMode DEFAULT_AA_MODE = indigo::AntiAliasingMode::FAST;

static const char* PROJECTION_NAMES[] = {
      "mercator", "identity", "transverse-mercator", "cassini", "miller", "oblique-mercator", "gnomonic",
      "orthographic", "vertical-perspective", "equidistant", "stereographic", "lambert-equal-area", "equidistant",
      "bonne", "polyconic", "epsg:3857", 0 };

static ::std::vector< Reference< Path> > PATH_GEOMETRIES;
static ::std::vector< Reference< Region> > REGION_GEOMETRIES;
static ::std::vector< Reference< ::cartografo::geo::Marker> > MARKERS;

static Pointer< ::indigo::Group> SHAPES_GROUP(new ::indigo::Group());
static GeodeticCoordinates MOUSE_POSITION;
static bool WITH_WMS = false;
static bool WITH_OSM = false;

static bool hasExtension(const string& s, const string& ext)
{
   if (s.length() < ext.length()) {
      return false;
   }
   size_t pos = s.length() - ext.length();
   return s.find(ext.c_str(), pos, ext.length()) == pos;
}

static bool withRaster()
{
   return WITH_WMS || WITH_OSM;
}

static Log logger()
{
   return "ShapeViewer";
}

static vector< string> kmlFiles;

static Reference< GeodeticDatum> getDatum()
{
   Reference< Spheroid> datum = Spheroid::create(Spheroid::WGS_84);
   if (USE_SPHEROID) {
      datum = Spheroid::createSphere(datum->majorAxisLength(), 0, 0, 0);
   }
   // datum = Spheroid::createEllipsoid(datum->majorAxisLength(),.9,0,0,0);
   return GeodeticDatum::create(datum);
}

struct ShapeInfo : public ::indigo::PickInfo
{
      ShapeInfo(const ::std::string& country, Reference< ::indigo::SolidFillNode> f, Reference< ::indigo::StrokeNode> s)
            : _country(country), _fill(f), _stroke(s), _base(f->color()), _highlight(
                  ::indigo::Color(0xad / 255., 0xad / 255.0, 0)), _selected(false)
      {
      }
      ~ShapeInfo()throws()
      {
      }

   public:
      void setSelected(bool selected)
      {
         if (_selected != selected) {
            if (selected) {
               _fill->setColor(_highlight);
            }
            else {
               _fill->setColor(_base);
            }
         }
      }

   private:
      ::std::string _country;
      Reference< ::indigo::SolidFillNode> _fill;
      Reference< ::indigo::StrokeNode> _stroke;
      ::indigo::Color _base, _highlight;
      bool _selected;

};

static Reference< ::cartografo::map::raster::RasterProvider> createWmsRaster()
{
   auto f = ::timber::media::ImageFactory::getBuiltinFactory();
   Reference< ::cartografo::map::raster::WmsRasterProvider> wms(
         ::cartografo::map::raster::WmsRasterProvider::create(f));
   //  wms->setWmsServerUrl("http://demo.cubewerx.com/demo/cubeserv/cubeserv.cgi");
   //  wms->setWmsLayer("Foundation.COASTL_1M");
   wms->setWmsServerUrl("http://osm.omniscale.net/proxy/service");
   wms->setWmsLayer("osm");
   return wms;
}

static Reference< ::cartografo::map::raster::OsmRasterProvider> getOsmRaster()
{
   auto f = ::timber::media::ImageFactory::getBuiltinFactory();
   return ::cartografo::map::raster::OsmRasterProvider::create(f);
}

static Reference< ::cartografo::map::raster::RasterProvider> createOsmRaster()
{
   auto f = ::timber::media::ImageFactory::getBuiltinFactory();

   Reference< ::cartografo::map::raster::RasterProvider> osm(getOsmRaster());

   // create a disk cache
   osm = ::cartografo::map::raster::RasterCache::createDiskCache(osm, f, "data/OSM", 0,
         ::cartografo::map::raster::OsmRasterProvider::getRasterIDPath);
   return osm;
}

static Reference< Projection> createProjection(GeodeticCoordinates pot)
{
   Pointer< Projection> pp;

   LogEntry(logger()).info() << "New Projection : " << CURRENT_PROJECTION << doLog;
   pot.changeDatum(getDatum());
   try {

      if (CURRENT_PROJECTION == 0) {
      }
      else if (strcmp(CURRENT_PROJECTION, "epsg:3857") == 0) {
         auto crs = ProjectedCRS::createEPSG3857();
         ProjectedCRS::EPSG4979Coordinates wgs;
         double ctr[] = { 0, 0 };
         crs->toEPSG4979(ctr, wgs);
         pot = GeodeticCoordinates(wgs);

         Reference< ProjectionDomain> domain(
               new SimpleProjectionDomain(pot,
                     ::terra::Extent(toRadians(-85.), toRadians(-180.) + pot.longitude(), toRadians(85.),
                           toRadians(180.) + pot.longitude())));
         pp = Projection::create(crs, domain);
         LogEntry(logger()).info() << "Setting new projection center " << domain->center() << doLog;
      }
      else if (strcmp(CURRENT_PROJECTION, "mercator") == 0) {
         pp = CylindricalProjection::createMercatorProjection(pot.longitude(), pot.datum());
      }
      else if (strcmp(CURRENT_PROJECTION, "identity") == 0) {
         Reference< Projector2D> PROJECTOR = new IdentityProjector2D(pot);
         pp = PROJECTOR->projection();
      }
      else if (strcmp(CURRENT_PROJECTION, "transverse-mercator") == 0) {
         pp = CylindricalProjection::createTransverseMercatorProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "cassini") == 0) {
         pp = CylindricalProjection::createCassiniProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "miller") == 0) {
         pp = CylindricalProjection::createMillerProjection(pot.longitude(), pot.datum());
      }
      else if (strcmp(CURRENT_PROJECTION, "oblique-mercator") == 0) {
         pp = CylindricalProjection::createObliqueMercatorProjection(pot, toRadians(45.));
      }
      else if (strcmp(CURRENT_PROJECTION, "gnomonic") == 0) {
         pp = AzimuthalProjection::createGnomonicProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "orthographic") == 0) {
         pp = AzimuthalProjection::createOrthographicProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "vertical-perspective") == 0) {
         pp = AzimuthalProjection::createVerticalPerspectiveProjection(pot, 1000 * 38000.0);
      }
      else if (strcmp(CURRENT_PROJECTION, "equidistant") == 0) {
         pp = AzimuthalProjection::createEquidistantProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "stereographic") == 0) {
         pp = AzimuthalProjection::createStereographicProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "lambert-equal-area") == 0) {
         pp = AzimuthalProjection::createLambertEqualAreaProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "equidistant") == 0) {
         pp = AzimuthalProjection::createEquidistantProjection(pot);
      }
      else if (strcmp(CURRENT_PROJECTION, "polyconic") == 0) {
         // there are some artifacts with this projection
         ::terra::Bounds xbounds(
               GeodeticCoordinates(-::terra::PI / 2 + toRadians(.0125),
                     clampLongitude(pot.longitude() - toRadians(89.987)), getDatum()),
               GeodeticCoordinates(::terra::PI / 2 - toRadians(.0125),
                     clampLongitude(pot.longitude() + toRadians(89.987)), getDatum()));
         pp = ConicProjection::createPolyConicProjection(GeodeticCoordinates(0, pot.longitude(), pot.datum()), xbounds);
      }
      else if (strcmp(CURRENT_PROJECTION, "bonne") == 0) {

         // there are some artifacts with this projection
         ::terra::Bounds xbounds(
               GeodeticCoordinates(-::terra::PI / 2 + toRadians(1.), clampLongitude(pot.longitude() - toRadians(179.)),
                     getDatum()),
               GeodeticCoordinates(::terra::PI / 2, clampLongitude(pot.longitude() + toRadians(179.)), getDatum()));
         pp = ConicProjection::createBonneProjection(xbounds.center(), xbounds);
      }
   }
   catch (const ::terra::UnsupportedDatum& ex) {
      LogEntry(logger()).caught(ex) << "Could not create projection for " << CURRENT_PROJECTION << " projection."
            << doLog;
   }

   if (!pp) {
      if (withRaster()) {
         auto crs = ProjectedCRS::createEPSG3857();
         ProjectedCRS::EPSG4979Coordinates wgs;
         double ctr[] = { 0, 0 };
         crs->toEPSG4979(ctr, wgs);
         pot = GeodeticCoordinates(wgs);

         Reference< ProjectionDomain> domain(
               new SimpleProjectionDomain(pot,
                     ::terra::Extent(toRadians(-85.), toRadians(-180.) + pot.longitude(), toRadians(85.),
                           toRadians(180.) + pot.longitude())));
         pp = Projection::create(crs, domain);
      }
      else {
         pp = CylindricalProjection::createMercatorProjection(pot.longitude(), pot.datum());
      }
      //pp = AzimuthalProjection::createOrthographicProjection(pot);
      {
         //         Reference< Projector2D> PROJECTOR = new IdentityProjector2D(pot);
         //        pp = PROJECTOR->projection();
      }
   }
   return pp;
}

static Pointer< Projector2D> createProjector(GeodeticCoordinates pot)
{

   // projections that works work guaranteed only at the equator center
   //pot = GeodeticCoordinates(0,pot.longitude(),pot.datum());
   //  pot = GeodeticCoordinates::fromDegrees(-20.086,-150.1873,pot.datum());
   //  pot = GeodeticCoordinates(-0.00029946047613664995,-4.7035678286274938e-06,pot.datum());
   double maxLat = ::std::min(toRadians(89.75), clampLatitude(pot.latitude() + toRadians(90.)));
   double minLat = ::std::max(toRadians(-89.75), clampLatitude(pot.latitude() - toRadians(90.)));

   ::terra::Bounds bounds(
         GeodeticCoordinates(maxLat, clampLongitude(pot.longitude() - toRadians(179.99)), 0, getDatum()),
         GeodeticCoordinates(minLat, clampLongitude(pot.longitude() + toRadians(179.99)), 0, getDatum()));

   Pointer< Projector2D> PROJECTOR = new IdentityProjector2D(pot); // works!!
   Pointer< Projection> pp = PROJECTOR->projection();
   LogEntry(logger()).info() << "Projection domain extent : " << pp->domain()->region()->extent() << doLog;

   PROJECTOR = Projector2D::create(createProjection(pot));
   return PROJECTOR;
}

static Pointer< Projector2D> createProjector(const ::terra::Bounds& b, size_t w, size_t h, double& scale)
{
   Pointer< Projector2D> proj = createProjector(b.center());
   // determine the scale needed to display in the specified width and height
   scale = w / b.edgeLength(::terra::Bounds::NORTH);
   scale = ::std::min(w / b.edgeLength(::terra::Bounds::SOUTH), scale);
   scale = ::std::min(h / b.edgeLength(::terra::Bounds::EAST), scale);

   LogEntry(logger()).info() << "New center at : " << b.center() << " at 1 : " << (1.0 / scale) << doLog;
   return proj;
}

struct PositionLayer : public ::cartografo::map::Layer
{
      PositionLayer()throws()
      {

      }
      ~PositionLayer()throws()
      {
      }

};

struct AircraftLayer : public ::cartografo::map::Layer
{
      AircraftLayer()throws()
            : _nodes(new ::cartografo::geo::Group())
      {
         setRootNode(_nodes);
         size_t N = 50;
         for (size_t i = 0; i < N; ++i) {
            for (size_t j = 0; j < N; ++j) {
               //_nodes->add(createAircraftIcon(GeodeticCoordinates::fromDegrees(i*180.0/N-90,j*360./N-180,getDatum()),0));
               //_nodes->add(createAircraftIcon(GeodeticCoordinates::fromDegrees(49,i*180.0/N-90,49,getDatum()),0));
            }
         }
         for (double lon = -180; lon < 0; lon += 5) {
            _nodes->add(createAircraftIcon(GeodeticCoordinates::fromDegrees(49, lon, getDatum()), 0));
         }
      }

      ~AircraftLayer()throws()
      {
      }

      static Reference< cartografo::geo::Node> createAircraftIcon(const GeodeticCoordinates& pos, double rot)
      {
         ::indigo::Point pts[30];
         int i = 0;
         pts[i++] = ::indigo::Point(0.523, 0);
         pts[i++] = ::indigo::Point(0.512, 0.035);
         pts[i++] = ::indigo::Point(0.479, 0.058);
         pts[i++] = ::indigo::Point(0.440, 0.067);
         pts[i++] = ::indigo::Point(0.100, 0.067);
         pts[i++] = ::indigo::Point(-0.137, 0.450);
         pts[i++] = ::indigo::Point(-0.218, 0.450);
         pts[i++] = ::indigo::Point(-0.132, 0.170);
         pts[i++] = ::indigo::Point(-0.132, 0.067);
         pts[i++] = ::indigo::Point(-0.315, 0.067);
         pts[i++] = ::indigo::Point(-0.423, 0.202);
         pts[i++] = ::indigo::Point(-0.477, 0.202);
         pts[i++] = ::indigo::Point(-0.477, 0.073);
         pts[i++] = ::indigo::Point(-0.423, 0.024);
         pts[i++] = ::indigo::Point(-0.423, -0.024);
         pts[i++] = ::indigo::Point(-0.477, -0.073);
         pts[i++] = ::indigo::Point(-0.477, -0.202);
         pts[i++] = ::indigo::Point(-0.423, -0.202);
         pts[i++] = ::indigo::Point(-0.315, -0.067);
         pts[i++] = ::indigo::Point(-0.132, -0.067);
         pts[i++] = ::indigo::Point(-0.132, -0.170);
         pts[i++] = ::indigo::Point(-0.218, -0.450);
         pts[i++] = ::indigo::Point(-0.137, -0.450);
         pts[i++] = ::indigo::Point(0.100, -0.067);
         pts[i++] = ::indigo::Point(0.440, -0.067);
         pts[i++] = ::indigo::Point(0.479, -0.058);
         pts[i++] = ::indigo::Point(0.512, -0.035);

         ::indigo::Polygon p(i, pts);

         ::timber::Pointer< ::indigo::Group> grp(new ::indigo::Group());
         ::indigo::AffineTransform2D t(rot, 500000, 0, 0);
         grp->add(new ::indigo::Transform2D(t));
         //    grp->add(getAirplaneRotator());
         grp->add(new ::indigo::SolidFillNode(::indigo::Color(.7, .7, .7, .5)));
         grp->add(new ::indigo::Polygons(p));

         return new ::cartografo::geo::Marker(pos, grp);
      }

   private:
      Reference< ::cartografo::geo::Group> _nodes;
};

struct BackgroundLayer : public ::cartografo::map::Layer
{
      BackgroundLayer(const Reference< GeodeticDatum>& datum)throws()
      {
         Reference< ::cartografo::geo::Group> g = new ::cartografo::geo::Group(true);
         if (true) {
            Reference< ::cartografo::geo::Regions> r = new ::cartografo::geo::Regions();
            r->add(::cartografo::Region::createBoundsRegion(::terra::Bounds(datum)));
            g->add(
                  new ::cartografo::geo::IndigoNode(
                        new ::indigo::SolidFillNode(::indigo::Color(0, 0x80 / 255.0, 0xcd / 255.0, 1.0))));
            g->add(new ::cartografo::geo::IndigoNode(new ::indigo::StrokeNode(::indigo::Color(0, 0, 0), 1.5)));
            g->add(r);
         }
         if (false) {
            Reference< ::cartografo::geo::Regions> r = new ::cartografo::geo::Regions();
            r->add(
                  AreaBuilder::createRadiansCircle(GeodeticCoordinates(-10 * PI / 180, 110 * PI / 180, datum), PI / 2));
            g->add(new ::cartografo::geo::IndigoNode(new ::indigo::SolidFillNode(::indigo::Color(1.0, 0, 0, .25))));
            g->add(r);
         }

         setRootNode(g);
      }

      ~BackgroundLayer()throws()
      {
      }
};

struct DrawingLayer : public ::cartografo::map::Layer
{

      enum Mode
      {
         CROSS, // draw only latitude and longitude graticule at mouse position
         BOUNDS_OUTLINE, // draw the geodetic bounds from the start position to the current mouse
         BOUNDS, // draw the geodetic bounds from the start position to the current mouse
         CIRCLE, // draw a circle from the start position with a certain radius
         CIRCLE_OUTLINE
      // draw a circle from the start position with a certain radius
      };

      DrawingLayer()throws()
            : _regions(new ::cartografo::geo::Regions()), _paths(new ::cartografo::geo::Paths()), _mode(CIRCLE)
      {
         Reference< ::cartografo::geo::Group> G(new ::cartografo::geo::Group());
         Reference< ::cartografo::geo::Group> R(new ::cartografo::geo::Group());

         R->add(new ::cartografo::geo::IndigoNode(new ::indigo::AlphaNode(0.25)));
         R->add(new ::cartografo::geo::IndigoNode(new ::indigo::SolidFillNode(::indigo::Color(.9, .9, .9))));
         R->add(new ::cartografo::geo::IndigoNode(new ::indigo::StrokeNode(::indigo::Color(.9, .9, .9), 1)));
         R->add(_regions);

         G->add(R);
         G->add(new ::cartografo::geo::IndigoNode(new ::indigo::StrokeNode(::indigo::Color(.7, .7, .7), 1)));
         G->add(_paths);

         setRootNode(G);
      }

      ~DrawingLayer()throws()
      {
      }

      bool hasStartPoint() const throws()
      {
         return _start.get();
      }

      void setStartPoint(const GeodeticCoordinates& start)
      {
         _start.reset(new GeodeticCoordinates(start));
         _bounds.reset(0);
      }

      void setCurrentPoint(const GeodeticCoordinates& cur)
      {
         _current.reset(new GeodeticCoordinates(cur));

         _regions->clear();
         _paths->clear();

         if (_mode == CROSS || _start.get() == 0) {
            _pathBuilder.reset();
            _paths->clear();
            _paths->add(
                  PathBuilder::createRadiansCircle(GeodeticCoordinates(::terra::PI / 2, 0, cur.datum()),
                        ::terra::PI / 2 - cur.latitude()));
            _paths->add(PathBuilder::createGeodesicCircle(cur, cur.travelAlongLoxodrome(10000, 0)));
            return;
         }

         switch (_mode) {
            case CIRCLE:
            case CIRCLE_OUTLINE: {
               double dist = _start->geodesicDistance(cur);
               if (dist > 0) {
                  if (_mode == CIRCLE) {
                     Pointer< Region> theRegion = AreaBuilder::createLoxodromicCircle(*_start, dist, 180);
#if 1
                     _regions->add(theRegion);
#else
                     {
                        Reference<ExtentPyramid> pyramid = ExtentPyramid::createRegionPyramid(theRegion,9);
                        vector<Extent> exts;
                        pyramid->gatherLeafExtents(exts);
                        for (size_t i=0;i<exts.size();++i) {
                           _regions->add(Region::createExtentRegion(exts[i]));
                        }
                     }
#endif
                     _bounds.reset(new ::terra::Bounds(_regions->shape(0)->extent().getBounds(cur.datum())));
                  }
                  else {
                     _paths->add(PathBuilder::createLoxodromicCircle(*_start, dist, 180));
                     _bounds.reset(new ::terra::Bounds(_paths->shape(0)->extent().getBounds(cur.datum())));
                  }
               }
               break;
            }
            case BOUNDS_OUTLINE:
            case BOUNDS:
               _bounds.reset(new ::terra::Bounds(*_start));
               *_bounds = _bounds->merge(::terra::Bounds(cur));
            default: {
               double minLat = ::std::min(_start->latitude(), cur.latitude());
               double maxLat = ::std::max(_start->latitude(), cur.latitude());

               double minLon;
               double maxLon;
               if (::std::abs(cur.longitude() - _start->longitude()) < 180) {
                  minLon = ::std::min(_start->longitude(), cur.longitude());
                  maxLon = ::std::max(_start->longitude(), cur.longitude());
               }
               else if (cur.longitude() > _start->longitude()) {
                  minLon = cur.longitude();
                  maxLon = _start->longitude();
               }
               else {
                  maxLon = cur.longitude();
                  minLon = _start->longitude();
               }

               const ::terra::Bounds b(minLat, minLon, maxLat, maxLon, cur.datum());
               if (_mode == BOUNDS_OUTLINE) {
                  //FIXME: why does this cause the polygons to be so weird
                  _paths->add(PathBuilder::createBoundsPath(b, 1, 1));
                  _bounds.reset(new ::terra::Bounds(_paths->shape(0)->extent().getBounds(cur.datum())));
               }
               else {
                  _regions->add(AreaBuilder::createBoundsArea(b, 1, 1));
                  _bounds.reset(new ::terra::Bounds(_regions->shape(0)->extent().getBounds(cur.datum())));
               }
               break;
            }
         }
      }

      bool bounds(::terra::Bounds& b)
      {
         if (_bounds.get() == 0) {
            return false;
         }
         else {
            b = *_bounds;
            return true;
         }
      }

      void clear()
      {
         _bounds.reset(0);
         _start.reset(0);
         _current.reset(0);
         _paths->clear();
         _regions->clear();
      }

   private:
      ::timber::Reference< ::cartografo::geo::Regions> _regions;
      ::timber::Reference< ::cartografo::geo::Paths> _paths;
      Mode _mode;
      AreaBuilder _areaBuilder;
      PathBuilder _pathBuilder;
      ::std::unique_ptr< GeodeticCoordinates> _start;
      ::std::unique_ptr< GeodeticCoordinates> _current;

   private:
      unique_ptr< ::terra::Bounds> _bounds;
};

struct Overlay : public ::cartografo::map::Layer
{
      Overlay()throws()
            : _text(new ::indigo::Text("Hello")), _transform(new ::indigo::Transform2D()), _x(0), _y(0)
      {
         Reference< ::indigo::Group> g(new ::indigo::Group(3, true));
         _transform->setTransform(::indigo::AffineTransform2D(0, 1, 0, 0));
         g->add(new ::indigo::TransformMode(::indigo::TransformMode::RESET));
         g->add(new ::indigo::ViewTransform2D(::indigo::AffineTransform2D(0, 1, 0, 0)));
         g->add(new ::indigo::StrokeNode(::indigo::Color(1, 1, 1), 1));
         g->add(_transform);
         g->add(_text);
         setRootNode(new ::cartografo::geo::IndigoNode(g));
      }

      ~Overlay()throws()
      {
      }

      void notifyViewPortChanged(::cartografo::map::Map& xmap)throws()
      {
         double w = xmap.viewPort().viewWindow().width();
         double h = xmap.viewPort().viewWindow().height();

         // TODO: compute the text width here
         _width = w;
         _x = w - 300;
         _y = h - 30;
         //    ::std::cerr << "New transform" << ::std::endl;
         _transform->setTransform(::indigo::AffineTransform2D(0, 1, _x, _y));
         updateText(xmap.viewPort().horizontalScale());
      }

   private:
      void updateText(double scale)
      {
         ::std::ostringstream out;
         out << "Scale : 1:" << ::std::setw(6) << (int) (1.0 / scale) << " " << ::std::setw(14)
               << ::std::setprecision(6) << _width / scale;
         _text->setText(out.str());
      }

   private:
      Reference< ::indigo::Text> _text;
      Reference< ::indigo::Transform2D> _transform;
      double _x, _y;
      size_t _width;
};

static Pointer< Projector2D> createProjector(double ptLat, double ptLon)
{
   GeodeticCoordinates pot = GeodeticCoordinates::fromDegrees(ptLat, ptLon, 0, getDatum());
   return createProjector(pot);
}

class Listener : public KeyEventListener,
      public MotionEventListener,
      public ButtonEventListener,
      public CrossingEventListener,
      public ComponentEventListener,
      public WheelEventListener
{

   public:
      Listener()throws()
      {
      }

      ~Listener()throws()
      {
         logger().info("Listener destroyed");
      }

      void processKeyEvent(const ::timber::Reference< KeyEvent>& e)throws()
      {
         if (e->id() != KeyEvent::KEY_TYPED) {
            return;
         }
         Reference< Viewer> v(e->source());
         MapPort& vp = v->viewPort();
         double s = vp.horizontalScale();

         try {
            switch (e->key().id()) {
               case KeySymbols::KEY_LEFT:
                  vp.translateViewWindow(-50, 0);
                  break;
               case KeySymbols::KEY_RIGHT:
                  vp.translateViewWindow(50, 0);
                  break;
               case KeySymbols::KEY_UP:
                  vp.translateViewWindow(0, 50);
                  break;
               case KeySymbols::KEY_DOWN:
                  vp.translateViewWindow(0, -50);
                  break;
               case KeySymbols::KEY_HOME: {
                  vp.movePointToPixel(MapPort::Point(0, 0), vp.centerPixel());
                  double defaultScale = getOsmRaster()->getScale(3);
                  vp.resizeBoundsToScale(defaultScale, defaultScale, vp.getPoint(vp.centerPixel()));
               }
                  break;
               case KeySymbols::KEY_PAGE_DOWN:
                  s = checkScale(s * 2, s);
                  vp.resizeBoundsToScale(s, s, vp.getPoint(vp.centerPixel()));
                  break;
               case KeySymbols::KEY_PAGE_UP:
                  s = checkScale(s * .5, s);
                  vp.resizeBoundsToScale(s, s, vp.getPoint(vp.centerPixel()));
                  break;
               default:
                  // ignore key
                  return;
            }
            //   ::std::cerr << *e << ::std::endl;
         }
         catch (const exception& ex) {
            logger().caught("Caught exception", ex);
         }
      }

      void processComponentEvent(const Reference< ComponentEvent>& e)throws()
      {
         if (e->id() == ComponentEvent::COMPONENT_RESIZED) {
            _acquire = true;
         }
      }

      void processCrossingEvent(const Reference< CrossingEvent>& e)throws()
      {
         if (e->id() == CrossingEvent::COMPONENT_EXITED) {
            _acquire = true;
         }
      }

      double checkScale(double s)
      {
         if (s < (1.0 / (1 << 24))) {
            return 1.0 / (1 << 24);
         }
         else if (s >= 1) {
            return .99999;
         }
         else {
            return s;
         }
      }

      double checkScale(double s, double oldScale)
      {
         if (s < (1.0 / (1 << 24))) {
            return oldScale;
         }
         else if (s >= 1) {
            return oldScale;
         }
         return s;
      }

      void processWheelEvent(const Reference< WheelEvent>& e)throws()
      {
         Reference< Viewer> v(e->source());

         // SCALE:
         MapPort& vp = v->viewPort();
         double s = vp.horizontalScale();

         if (e->wheelUp()) {
            s *= .5;
         }
         else {
            s *= 2;
         }
         s = checkScale(s, vp.horizontalScale());

         //  ::std::cerr << "Before Center : " << v->viewPort().centerPoint() << ::std::endl;
         v->setScaleFactor(s);
         //  ::std::cerr << "After Center : " << v->viewPort().centerPoint() << ::std::endl;

         //  ::std::cerr << "Scale factor : " << s << "  " << v->viewPort().horizontalScale() << ::std::endl;
      }

      void processMotionEvent(const Reference< MotionEvent>& e)throws()
      {
         Reference< Viewer> v(e->source());
         Pixel pos = e->pixel();

         GeodeticCoordinates pot;
         if (!v->getGeodeticCoordinates(pos, &pot)) {
            _acquire = true;
            return;
         }
         if (_acquire) {
            _acquire = false;
            _position = e->pixel();
            return;
         }

         InputModifiers im = e->modifiers();

         if (im.isButton3Pressed()) {
            GeodeticCoordinates center;
            v->getGeodeticCoordinates(_position, &center);

            // check if the coordinates are actually in the center of the projection
            // that should also be done in the map
            Reference< ::cartografo::map::Map> M = v->map();
            for (size_t i = 0, sz = M->layerCount(); i != sz; ++i) {
               Reference< ::cartografo::map::Layer> layer = M->layer(i);
               if (M->isEnabled(layer)) {
                  Pointer< DrawingLayer> drawing = layer.tryDynamicCast< DrawingLayer>();
                  if (drawing) {
                     if (drawing->hasStartPoint()) {
                        drawing->setCurrentPoint(center);
                     }
                     else {
                        drawing->setStartPoint(center);
                     }
                     break;
                  }
               }
            }
         }
         else if (im.isButton1Pressed()) {
            // TRANSLATE

            try {
               GeodeticCoordinates center;
               v->getGeodeticCoordinates(_position, &center);
               MOUSE_POSITION = pot;
               double dLon = pot.longitude() - center.longitude();
               double dLat = pot.latitude() - center.latitude();
               center = v->projection()->domain()->center();
               double lat = center.latitude() - dLat;
               double lon = center.longitude() - dLon;
               //lat = pot.latitude();
               //	lon = pot.longitude();
               lon = clampLongitude(lon);
               if (lat > ::terra::PI / 2) {
                  lat = ::terra::PI / 2;
               }
               else if (lat < -::terra::PI / 2) {
                  lat = -::terra::PI / 2;
               }
               LogEntry(logger()).info() << "Center  : " << toDegrees(lat) << ", " << toDegrees(lon) << doLog;
               Pointer< Projector2D> pp = createProjector(GeodeticCoordinates(lat, lon, getDatum()));
               if (pp) {
                  LogEntry(logger()).info() << "Projection bounds " << pp->projection()->domain()->region()->extent()
                        << doLog;
                  v->setProjector(pp);
               }
            }
            catch (...) {
            }
         }
         _position = e->pixel();
      }

      void processButtonEvent(const Reference< ButtonEvent>& e)throws()
      {
         Reference< Viewer> v(e->source());
         InputModifiers im = e->modifiers();
         if (e->id() == ButtonEvent::BUTTON_CLICKED && im.isControlPressed()) {
            Pointer< ::indigo::PickInfo> picked = v->pickNode(e->pixel());
            Pointer< ShapeInfo> sInfo = picked.tryDynamicCast< ShapeInfo>();
            if (sInfo) {
               sInfo->setSelected(true);
            }
            return;
         }

         if (e->id() == ButtonEvent::BUTTON_RELEASED) {
            Reference< ::cartografo::map::Map> M = v->map();
            for (size_t i = 0, sz = M->layerCount(); i != sz; ++i) {
               Reference< ::cartografo::map::Layer> layer = M->layer(i);
               if (M->isEnabled(layer)) {
                  Pointer< DrawingLayer> drawing = layer.tryDynamicCast< DrawingLayer>();
                  if (drawing) {

                     {
                        ::terra::Bounds b;
                        if (drawing->bounds(b)) {
                           // if re-project
                           if (false) {
                              double scale;
                              Pointer< Projector2D> pp = createProjector(b, v->width(), v->height(), scale);
                              if (pp) {
                                 v->setProjector(pp);
                                 v->setScaleFactor(scale);
                              }
                           }
                           else {
                              bool zoomToBounds = true;
                              Pointer< Region> r = Region::createBoundsRegion(b);
                              r = Region::intersect(r, v->projection()->domain()->region());
                              if (r) {
                                 b = r->extent().getBounds(b.datum());
                              }
                              else {
                                 zoomToBounds = false;
                              }
                              MapPort& vp = v->viewPort();
                              ProjectionMapping bottomRight = v->projection()->project(b.southEast());
                              ProjectionMapping topLeft = v->projection()->project(b.northWest());

                              ::tikal::Point2D br, tl;

                              switch (bottomRight.type()) {
                                 case ProjectionMapping::LINE:
                                    br = bottomRight.line().a();
                                    break;
                                 case ProjectionMapping::PATH:
                                    zoomToBounds = false;
                                    break;
                                 case ProjectionMapping::NONE:
                                    zoomToBounds = false;
                                    break;
                                 case ProjectionMapping::POINT:
                                    br = bottomRight.point();
                                    break;
                              }

                              switch (topLeft.type()) {
                                 case ProjectionMapping::LINE:
                                    tl = topLeft.line().a();
                                    break;
                                 case ProjectionMapping::PATH:
                                    zoomToBounds = false;
                                    break;
                                 case ProjectionMapping::NONE:
                                    zoomToBounds = false;
                                    break;
                                 case ProjectionMapping::POINT:
                                    tl = topLeft.point();
                                    break;
                              }

                              if (!zoomToBounds) {
                                 logger().info("Cannot zoom to bounds");
                              }
                              else {
                                 LogEntry(logger()).info() << "New center : " << b << ": " << br << '\n' << '\t' << tl
                                       << " : " << b << doLog;

                                 if (::indigo::view::Bounds::checkBounds(tl.x(), tl.y(), br.x() - tl.x(),
                                       br.y() - tl.y(), 0)) {
                                    vp.setBounds(
                                          ::indigo::view::Bounds(tl.x(), tl.y(), br.x() - tl.x(), br.y() - tl.y()));
                                    double s = checkScale(vp.horizontalScale());
                                    if (s != vp.horizontalScale()) {
                                       v->setScaleFactor(s);
                                    }
                                 }
                                 else {
                                    LogEntry(logger()).warn() << "Not a valid center bounds object : " << tl.x() << ", "
                                          << tl.y() << ", " << br.x() - tl.x() << ", " << br.y() - tl.y() << ", BNDS : "
                                          << b << doLog;
                                 }
                              }
                           }
                        }
                     }

                     drawing->clear();
                  }
               }
            }
         }
         if (e->id() == ButtonEvent::BUTTON_CLICKED) {
            Pixel pos = e->pixel();
            GeodeticCoordinates pot;
            if (v->getGeodeticCoordinates(pos, &pot)) {
               MOUSE_POSITION = pot;
               LogEntry(logger()).info() << "Pixel : " << pos << " --> " << pot << doLog;
               GeodeticCoordinates to;
               if (v->getGeodeticCoordinates(Pixel(pos.x() + 1, pos.y()), &to)) {
                  Geodesic G(pot, to);
                  LogEntry(logger()).info() << "Distance from " << pot << " to " << to << " : " << G.range() << doLog;
               }
               if (e->button() == 1) {
                  if (!pot.equals(v->projection()->domain()->center())) {
                     Pointer< Projector2D> pp = createProjector(pot);
                     if (pp) {
                        v->setProjector(pp);
                     }
                  }
               }
            }
         }
      }

   private:
      GeodeticCoordinates _pot;
      Pixel _position;
      bool _acquire;
};

struct SetProjectionAction : public ActionEventListener
{
      SetProjectionAction(Reference< Viewer> viewer, size_t i)
            : _viewer(viewer), _index(i)
      {
      }
      ~SetProjectionAction()throws()
      {
      }

      void processActionEvent(const Reference< ActionEvent>&)throws()
      {
         Pointer< Viewer> viewer = _viewer.get();
         if (!viewer) {
            // already destroyed
            Log("ShapeViewer").severe("SetProjectAction was called by viewer has been destroyed");
            return;
         }
         CURRENT_PROJECTION = PROJECTION_NAMES[_index];
         GeodeticCoordinates pot = viewer->projection()->domain()->center();
         Pointer< Projector2D> pp = createProjector(pot);
         if (pp) {
            viewer->setProjector(pp);
         }
      }

      /** Use a WeakPointer to prevent reference count cycles */
   private:
      WeakPointer< Viewer> _viewer;

   private:
      size_t _index;
};

static Reference< ::cartografo::map::Map> shapes(const ::std::vector< string>& args)
{
   Reference< ::cartografo::map::Map> res(new ::cartografo::map::Map());
   res->addLayer(new BackgroundLayer(getDatum()));
   Pointer< ::cartografo::map::raster::RasterProvider> raster;
   if (WITH_WMS) {
      raster = createWmsRaster();
   }
   if (WITH_OSM) {
      raster = createOsmRaster();
   }
   if (raster) {
      raster = ::cartografo::map::raster::AsyncRasterProvider::create(raster);
      res->addLayer(::cartografo::map::raster::RasterLayer::create(raster));
   }

   for (string arg : args) {
      auto shapeLayer = ::cartografo::map::shapefile::ShapefileLayer::create(arg, getDatum());
      if (shapeLayer) {
         res->addLayer(shapeLayer);
      }
   }

   for (string arg : kmlFiles) {
      auto kmlLayer = ::cartografo::map::kml::KmlLayer::create(arg, getDatum());
      if (kmlLayer) {
         res->addLayer(kmlLayer);
      }
      else {
         LogEntry(logger()).info() << "Could not create KML layer for " << arg << doLog;
      }
   }

   res->addLayer(new ::cartografo::map::layers::GraticuleLayer(getDatum(), 45));
   res->addLayer(new DrawingLayer());
   res->addLayer(new Overlay());
   //   res->addLayer(new AircraftLayer());
   return res;
}

static Reference< Viewer> viewer()
{
   Pointer< Viewer> xviewer = new Viewer();
   double s = getOsmRaster()->getScale(3);
   xviewer->setScaleFactor(s); // a scale factor of 1 is 1pixel==1m

   xviewer->setSizeHints(SizeHints(Size::MIN, Size::MAX, Size(640, 400)));
   Reference< Listener> l = new Listener();
   xviewer->addMotionEventListener(l);
   xviewer->addButtonEventListener(l);
   xviewer->addCrossingEventListener(l);
   xviewer->addWheelEventListener(l);
   xviewer->addComponentEventListener(l);
   xviewer->addKeyEventListener(l);
   xviewer->setBackgroundColor(::indigo::Color(0x70 / 255.0, 0x70 / 255.0, 0x70 / 255.0));

   return xviewer;
}

static void initDisplay(size_t argc, const char** argv)
{
   WITH_WMS = false;
   WITH_OSM = false;

   vector < string > args;

   for (size_t i = 1; i < argc; ++i) {
      string arg(argv[i]);
      if (arg == "-wms") {
         WITH_WMS = true;
      }
      else if (arg == "-osm") {
         WITH_OSM = true;
      }
      else if (hasExtension(arg, ".kml") || hasExtension(arg, ".kmz")) {
         kmlFiles.push_back(argv[i]);
      }
      else {
         args.push_back(arg);
      }
   }

   try {
      Reference< Viewer> xviewer = viewer();
      Reference< Window> win = Window::create(xviewer);

      Reference< MenuBar> mbar = new MenuBar();
      win->setMenuBar(mbar);

      Reference< Menu> projMenu = mbar->addMenu("Projections");
      for (size_t i = 0; PROJECTION_NAMES[i] != 0; ++i) {
         Reference< MenuItem> item = projMenu->addMenuItem(PROJECTION_NAMES[i]);
         item->addActionEventListener(new SetProjectionAction(xviewer, i));
      }

      win->requestSize(800, 800);
      win->setCloseAction(Window::EXIT);
      win->setVisible(true);
      win->setTitle("GEO Shape Viewer");

      Pointer< Projector2D> pp = createProjector(GeodeticCoordinates(0, 0, getDatum()));
      if (pp) {
         xviewer->setProjector(pp);
      }
      else {
         LogEntry(logger()).warn() << "Could not create projector" << doLog;
      }

      xviewer->setMap(shapes(args));
   }
   catch (const ::std::exception& e) {
      logger().caught("EXCEPTION RAISED : ", e);
   }
   catch (...) {
      logger().severe("Unexpected exception");
   }
}

int main(int argc, const char** argv)
{
   for (int i = 1; i < argc; ++i) {
      const string arg_i(argv[i]);
      if (arg_i == "-gtk") {
         Application::instance().setProperty(Desktop::DESKTOP_PROVIDER,
               "/home/ray/git/nexus/modules/GtkDesktop/libGtkDesktop.so");
      }
      else if (arg_i == "-spheroid") {
         USE_SPHEROID = true;
      }
   }

   try {
      Reference< Spheroid> datum = Spheroid::create(Spheroid::WGS_84);
      if (USE_SPHEROID) {
         datum = Spheroid::createSphere(datum->majorAxisLength(), 0, 0, 0);
      }
      //   datum = Spheroid::createEllipsoid(datum->majorAxisLength(),.5);
      Spheroid::setDefaultSpheroid(datum);
   }
   catch (const ::std::exception& e) {
      logger().caught(e);
      return 1;
   }
   catch (...) {
      logger().caughtUnexpected();
      return 1;
   }

   Log("cartografo.AbstractPath").setLevel(Level::WARN);
   Log("cartografo.projector.SimpleProjector2D").setLevel(Level::WARN);
   Log("cartografo.projector.BaseProjector2D").setLevel(Level::WARN);
   Log("cartografo.projector.IdentityProjector2D").setLevel(Level::WARN);
   Log("cartografo.map.nexus.MapContainer").setLevel(Level::WARN);
   Log("cartografo.map.MapRenderer").setLevel(Level::INFO);
   Log("cartografo.map.Map").setLevel(Level::WARN);
   Log("cartografo.map.Layer").setLevel(Level::WARN);
   Log("cartografo.ExtentPyramid").setLevel(Level::WARN);
   Log("cartografo.kml.KmlNode").setLevel(Level::INFO);
   Log("nexus.Component").setLevel(Level::WARN);

   int retval = 1;
   try {
     Desktop::configure(argc, argv,initDisplay);
      Desktop::waitShutdown();
      PATH_GEOMETRIES.clear();
      REGION_GEOMETRIES.clear();
      MARKERS.clear();
      SHAPES_GROUP->clear();
   }
   catch (const std::exception& e) {
      logger().caught(e);
      retval = 1;
   }
   catch (...) {
      logger().caughtUnexpected();
      retval = 2;
   }

   return retval;
}

