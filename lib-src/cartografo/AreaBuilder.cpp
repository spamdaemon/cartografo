#include <cartografo/AreaBuilder.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/Area.h>
#include <cartografo/PathSegment.h>
#include <cartografo/Projector.h>

#include <terra/Bounds.h>
#include <terra/Extent.h>
#include <terra/Horizon.h>
#include <terra/Geodesic.h>
#include <terra/terra.h>

#include <timber/logging.h>
#include <canopy/arraycopy.h>

#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
   namespace {
      static Log logger()
      {
         return Log("cartografo.AreaBuilder");
      }

      struct ProjectorWrapper : public Projector
      {
            inline ProjectorWrapper(Projector& tp, bool windCW, const Extent& e)
                  : _projector(tp), _windCW(windCW), _extent(e)
            {
            }

            ~ProjectorWrapper()throws()
            {
            }

         public:
            bool beginPath(const Extent* b)throws()
            {
               return _projector.beginOutsidePath(_windCW, &_extent);
            }
            bool beginOutsidePath(bool, const Extent*)throws()
            {
               return false;
            }
            bool beginInsidePath(bool, const Extent*)throws()
            {
               return false;
            }
            bool moveTo(double lat, double lon, double alt, const Extent* bounds)
            {
               return _projector.moveTo(lat, lon, alt, &_extent);
            }
            void moveAlongArcs(size_t n, const Arc* delta, ArcType type, const Extent* e)
            {
               _projector.moveAlongArcs(n, delta, type, &_extent);
            }
            Projector* copy() const throws()
            {
               return new ProjectorWrapper(_projector, _windCW, _extent);
            }

         private:
            Projector& _projector;
            const bool _windCW;
            const Extent _extent;
      };

      struct SimpleArea : public Area
      {
            SimpleArea(Reference< Path> p, const Extent& e, bool windCW)throws()
                  : _outside(p), _extent(e), _cwPath(windCW)
            {
            }

            SimpleArea(Reference< Path> p, vector< Reference< Path> >& ipaths, const Extent& e, bool windCW)throws()
                  : _outside(p), _extent(e), _cwPath(windCW)
            {
               _inside.swap(ipaths);
            }

            ~SimpleArea()throws()
            {
            }

            Extent extent() const throws()
            {
               return _extent;
            }
            bool isCWArea() const throws()
            {
               return _cwPath;
            }
            size_t pathCount() const throws()
            {
               return _inside.size();
            }
            Reference< Path> outsidePath() const throws()
            {
               return _outside;
            }
            Reference< Path> insidePath(size_t i) const throws()
            {
               return _inside.at(i);
            }
            void project(Projector& pp) const
            {
               assert(pathCount() == 0 && "Inside paths not supported yet");
               ProjectorWrapper wrapper(pp, _cwPath, _extent);
               _outside->project(wrapper);
            }

         private:
            const Reference< Path> _outside;
            vector< Reference< Path> > _inside;
            const Extent _extent;
            bool _cwPath;
      };

      static double distanceToNorthPole(const GeodeticCoordinates& pt)
      {
         return pt.datum()->spheroid().meridianArcLength(pt.latitude(), ::terra::PI / 2);
      }

      static double distanceToSouthPole(const GeodeticCoordinates& pt)
      {
         return pt.datum()->spheroid().meridianArcLength(-::terra::PI / 2, pt.latitude());
      }

      struct Pointer< Area> createCircleArea(const GeodeticCoordinates& ctr, int classify, Reference< Path> outside)
      {
         if (classify == 0) {
            return new SimpleArea(outside, outside->extent(), true);
         }
         if (classify == 3) {
            Extent e(outside->extent());

            e.merge(Extent(ctr.latitude(), ctr.longitude()));
            e.merge(Extent(::terra::PI / 2, ctr.longitude()));
            e.merge(Extent(-::terra::PI / 2, ctr.longitude()));
            return new SimpleArea(outside, e, true);
         }
         // create a new path, from the segments of the first one

         double lat = ::terra::PI / 2;
         if (classify & 2) {
            lat = -::terra::PI / 2;
         }

         PathBuilder B;
#if 0
         LogEntry e("cartografo.AreaBuilder");
         e.info() << "Converting circle around pole : " << toDegrees(lat) << " : ";
#endif 
         GeodeticCoordinates last;
         for (size_t i = 0, nSeg = outside->segmentCount(); i <= nSeg; ++i) {
            const PathSegment& seg = outside->segment(i % outside->segmentCount());
            if (i == 0 && seg.type() == PathSegment::MOVE) {
               B.moveTo(seg.endPoint());
            }
            else {
               // if travelling over 180 degrees, then we insert artifical points to move to the boundary
               // at 180 over the north or south pole
               if (::std::abs(last.longitude() - seg.endPoint().longitude()) > ::terra::PI) {

                  // if the last latitude was less than 0, then move west to -180, move to to the pole
                  // then move EAST to 180 degrees along the pole
                  double xlon;
                  PathBuilder::Direction dir;

                  if (last.longitude() < 0) {
                     xlon = -::terra::PI;
                     dir = PathBuilder::EAST;
                  }
                  else {
                     xlon = ::terra::PI;
                     dir = PathBuilder::WEST;
                  }

                  double xlat = (last.latitude() + seg.endPoint().latitude()) / 2;
                  B.loxodromeTo(GeodeticCoordinates(xlat, xlon, last.datum())); // to the -180/+180 graticule
                  B.loxodromeTo(GeodeticCoordinates(lat, xlon, last.datum())); // move to the north pole
                  B.loxodromeTo(GeodeticCoordinates(lat, -xlon, last.datum()), dir); // move the opposite graticule +180/-180
                  B.loxodromeTo(GeodeticCoordinates(xlat, -xlon, last.datum())); // move down towards the expected latitude
               }
               if (seg.type() == PathSegment::GEODESIC) {
                  B.geodesicTo(seg.endPoint());
               }
               else {
                  B.loxodromeTo(seg.endPoint());
               }
            }
            last = seg.endPoint();
#if 0
            e << last;
#endif
         }
#if 0
         e<< doLog;
#endif

         Pointer< Path> path = B.path();
         if (path) {
            return new SimpleArea(path, path->extent(), true);
         }
         else {
            return Pointer< Area>();
         }
      }

      static Pointer< Area> createArea(size_t n, const GeodeticCoordinates* coords, bool loxodromic, bool windCW)
      {
         if (n < 2) {
            return Pointer< Area>();
         }
         AreaBuilder b;
         b.beginOutsidePath(coords[0], windCW);
         for (size_t i = 1; i < n; ++i) {
            if (coords[i] != coords[i - 1]) {
               if (loxodromic) {
                  b.loxodromeTo(coords[i]);
               }
               else {
                  b.geodesicTo(coords[i]);
               }
            }
         }
         if (loxodromic) {
            b.loxodromeTo(coords[0]);
         }
         else {
            b.geodesicTo(coords[0]);
         }
         return b.area();
      }

      static Pointer< Area> createArea(const ::std::vector< GeodeticCoordinates>& coords, bool loxodromic, bool windCW)
      {
         return createArea(coords.size(), &coords[0], loxodromic, windCW);
      }
   }

   AreaBuilder::AreaBuilder()
   throws()
   {
      reset();
   }

   AreaBuilder::~AreaBuilder()
   throws()
   {
   }

   void AreaBuilder::reset()
   throws()
   {
      _builder.reset();
      _needMoveTo = true;
      _area = Pointer< Area>();
   }

   void AreaBuilder::closeArea()
   throws()
   {

      // since we need a move, we can't actually build an area
      if (_needMoveTo) {
         _builder.reset();
         return;
      }
      _needMoveTo = true;

      // close the path
      if (_endPoint != _startPoint) {
         _builder.geodesicTo(_startPoint);
      }

      Pointer< Path> outside = _builder.path();
      _builder.reset();
      if (!outside) {
         return;
      }
      _area = new SimpleArea(outside, outside->extent(), _windCW);
   }

   void AreaBuilder::beginOutsidePath(const GeodeticCoordinates& p, bool windCW)
   throws()
   {
      closeArea();
      _windCW = windCW;
      _needMoveTo = false;
      _builder.moveTo(p);
      _area = nullptr;
   }

   void AreaBuilder::geodesicTo(const GeodeticCoordinates& p)
   throws (::std::exception)
   {
      _builder.geodesicTo(p);
      _area = nullptr;
   }

   void AreaBuilder::loxodromeTo(const GeodeticCoordinates& p, Direction dir)
   throws (::std::exception)
   {
      _builder.loxodromeTo(p, (PathBuilder::Direction) dir);
      _area = nullptr;
   }

   Pointer< Area> AreaBuilder::area()
   throws()
   {
      if (_area != nullptr) {
         return _area;
      }
      closeArea();
      return _area;
   }

   Pointer< Area> AreaBuilder::createLoxodromicArea(const ::std::vector< GeodeticCoordinates>& coords, bool windCW)
   {
      return createArea(coords, true, windCW);
   }

   Pointer< Area> AreaBuilder::createLoxodromicArea(size_t n, const GeodeticCoordinates* coords, bool windCW)
   {
      return createArea(n, coords, false, windCW);
   }

   Pointer< Area> AreaBuilder::createGeodesicArea(size_t n, const GeodeticCoordinates* coords, bool windCW)
   {
      return createArea(n, coords, false, windCW);
   }

   Pointer< Area> AreaBuilder::createGeodesicArea(const ::std::vector< GeodeticCoordinates>& coords, bool windCW)
   {
      return createArea(coords, false, windCW);
   }

   Pointer< Area> AreaBuilder::createBoundsArea(const Bounds& b)
   {
      return createBoundsArea(b, ::terra::PI / 2, ::terra::PI / 2);
   }

   Pointer< Area> AreaBuilder::createBoundsArea(const Bounds& b, double deltaLat, double deltaLon)
   {
      struct xArea : public Area
      {
            xArea(const Bounds& bnds, double dLat, double dLon)throws()
                  : _extent(bnds), _outside(PathBuilder::createBoundsPath(bnds, dLat, dLon))
            {
            }

            ~xArea()throws()
            {
            }

            Extent extent() const throws()
            {
               return _extent;
            }
            bool isCWArea() const throws()
            {
               return true;
            }
            size_t pathCount() const throws()
            {
               return 0;
            }
            Reference< Path> outsidePath() const throws()
            {
               return _outside;
            }
            Reference< Path> insidePath(size_t) const throws()
            {
               return Pointer< Path>();
            }
            void project(Projector& pp) const
            {
               ProjectorWrapper wrapper(pp, true, _extent);
               _outside->project(wrapper);
            }
            bool contains(double lat, double lon) const throws()
            {
               return _extent.contains(lat, lon);
            }

         private:
            const Extent _extent;
            const Reference< Path> _outside;
      };
      Area* tmp = new xArea(b, deltaLat, deltaLon);
      return Pointer< Area>(tmp);
   }

   Pointer< Area> AreaBuilder::createHorizonArea(const GeodeticCoordinates& obs)
   throws()
   {
      Pointer< Path> P = PathBuilder::createHorizonPath(obs);
      if (!P) {
         Log("cartografo.AreaBuilder").warn("Could not create horizon area");
         return Pointer< Area>();
      }

      int classify = 0;
      const Horizon H(obs); // the horizon
      Extent e(P->extent());
      if (distanceToNorthPole(obs) < H.distance(0)) {
         classify |= 1;
      }
      if (distanceToSouthPole(obs) < H.distance(::terra::PI)) {
         classify |= 2;
      }
      LogEntry("cartografo.AreaBuilder").info() << "Creating a simple horizon area " << e << doLog;
      return createCircleArea(obs, classify, P);
   }

   Reference< Area> AreaBuilder::createGeodesicCircle(const GeodeticCoordinates& a, const GeodeticCoordinates& b)
   {
      if (a.equals(b) || a.equals(b.antipodalCoordinates())) {
         throw ::std::invalid_argument("Points are either equal or antipodal; cannot create geodesic circle");
      }

      AreaBuilder B;
      B.beginOutsidePath(a, true);
      B.geodesicTo(b);
      B.geodesicTo(a.antipodalCoordinates());
      B.geodesicTo(b.antipodalCoordinates());
      B.geodesicTo(a);

      return B.area();
   }

   Reference< Area> AreaBuilder::createLoxodromicCircle(const GeodeticCoordinates& ctr, double radius)
   {
      return createLoxodromicCircle(ctr, radius, 360);
   }

   Reference< Area> AreaBuilder::createLoxodromicCircle(const GeodeticCoordinates& ctr, double radius, size_t nPoints)
   {
      if (radius < 0.0) {
         throw invalid_argument("Invalid radius");
      }
      if (nPoints < 3) {
         throw invalid_argument("Not enough points for loxodromic circle");
      }

      Pointer< Path> outside = PathBuilder::createLoxodromicCircle(ctr, radius, nPoints);
      if (!outside) {
         return Pointer< Area>();
      }

      // simple flag:
      //  0: the circle does not enclose a pole
      //  1: the circle encloses the north pole
      //  2: the circle encloses the south pole
      //  3: the circle encloses both poles
      int classify = 0;

      if (distanceToNorthPole(ctr) < radius) {
         classify |= 1;
      }
      if (distanceToSouthPole(ctr) < radius) {
         classify |= 2;
      }
      return createCircleArea(ctr, classify, outside);
   }

   Reference< Area> AreaBuilder::createRadiansCircle(const ::terra::GeodeticCoordinates& ctr, double radRadius)
   {
      return createRadiansCircle(ctr, radRadius, 360);
   }

   Reference< Area> AreaBuilder::createRadiansCircle(const ::terra::GeodeticCoordinates& ctr, double radRadius,
         size_t nPoints)
   {
      if (radRadius < 0.0) {
         throw invalid_argument("Invalid radius");
      }
      if (nPoints < 3) {
         throw invalid_argument("Not enough points for loxodromic circle");
      }

      Pointer< Path> outside = PathBuilder::createRadiansCircle(ctr, radRadius, nPoints);
      if (!outside) {
         return Pointer< Area>();
      }

      // simple flag:
      //  0: the circle does not enclose a pole
      //  1: the circle encloses the north pole
      //  2: the circle encloses the south pole
      //  3: the circle encloses both poles
      int classify = 0;

      if (::std::abs(::terra::PI / 2 - ctr.latitude()) < radRadius) {
         classify |= 1;
      }
      if (::std::abs(ctr.latitude() + ::terra::PI / 2) < radRadius) {
         classify |= 2;
      }
      return createCircleArea(ctr, classify, outside);
   }

   ::timber::Reference< Area> AreaBuilder::toBoundedLoxodromicArea(const ::timber::Reference< Area>& a, double length)
   throws()
   {
      Reference< Path> outside = a->outsidePath()->toBoundedLoxodromicPath(length);
      vector< Reference< Path> > paths;
      for (size_t i = 0; i < a->pathCount(); ++i) {
         paths.push_back(a->insidePath(i)->toBoundedLoxodromicPath(length));
      }
      return new SimpleArea(outside, paths, a->extent(), a->isCWArea());
   }

   ::timber::Reference< Area> AreaBuilder::toLoxodromicArea(const ::timber::Reference< Area>& a, double err)
   throws()
   {
      Reference< Path> outside = a->outsidePath()->toLoxodromicPath(err);
      vector< Reference< Path> > paths;
      for (size_t i = 0; i < a->pathCount(); ++i) {
         paths.push_back(a->insidePath(i)->toLoxodromicPath(err));
      }
      return new SimpleArea(outside, paths, a->extent(), a->isCWArea());
   }

}
