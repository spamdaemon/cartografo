#ifndef _CARTOGRAFO_PROJECTIONREQUEST_H
#define _CARTOGRAFO_PROJECTIONREQUEST_H

#ifndef _CARTOGRAFO_PROJECTIONMAPPING_H
#include <cartografo/ProjectionMapping.h>
#endif

namespace cartografo {
  /**
   * A projection request.
   */
  struct ProjectionRequest {
    /** 
     * Default constructor 
     */
  public:
    inline      ProjectionRequest () throws()
      : lat(0),lon(0),alt(0)
    {}
    
    /** 
     * Create an initialized projection request.
     * @param latitude the latitude in radians
     * @param longitude the longitude in radians
     * @param altitude the altitude in meters
     */
  public:
    inline ProjectionRequest (double latitude, double longitude, double altitude) throws()
      : lat(latitude),lon(longitude),alt(altitude)
    {}
    
    double lat; //< latitude to be projected (in radians)
    double lon; //< longitude to be projected (in radians)
    double alt; //< an altitude in meters
    ProjectionMapping map; //< the resulting mapping of lat,lon
  };
}

#endif

