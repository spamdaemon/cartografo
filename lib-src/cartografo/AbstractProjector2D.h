#ifndef _CARTOGRAFO_ABSTRACTPROJECTOR2D_H
#define _CARTOGRAFO_ABSTRACTPROJECTOR2D_H

#ifndef _CARTOGRAFO_PROJECTOR2D_H
#include <cartografo/Projector2D.h>
#endif

namespace cartografo {
  /** 
   * Instances of a projector are able to project geographic areas and paths
   * into mostly 2D regions and paths.
   */
  class AbstractProjector2D : public Projector2D {
    AbstractProjector2D(const AbstractProjector2D&);
    AbstractProjector2D&operator=(const AbstractProjector2D&);
    
    /** 
     * Create a projector with an ementation
     * @param p the projection
     */
  protected:
    AbstractProjector2D() throws();
    
    /**
     * Destroy this projector.
     */
  protected:
    ~AbstractProjector2D() throws();
    
    /**
     * Get the 2d path that was projected. It is undefined
     * whether or not this method will return the same result
     * for consecutive invocations.
     * @return the path 
     */
  public:
    ::timber::Pointer< ::tikal::Path2D> getPath() throws();
	
    /**
     * Get the most recently started region. It is undefined
     * whether or not this method will return the same result
     * for consecutive invocations.
     * @return a region 
     */
  public:
    ::timber::Pointer< ::tikal::Region2D> getRegion() throws();

    /**
     * Begin an outside path of an area. 
     * @param windCW true if the path winds in CW direction
     * @param bounds optional known bounds for the path
     * @return false 
     */
  public:
    bool beginOutsidePath(bool windCW, const ::terra::Extent* bounds);
      
    /***
     * Begin the inside path of an area. An outside path must 
     * have been begun before creating an inside path. The path
     * contour will be inside the region enclosed by the most 
     * recently started outside path.
     * @param windCW true if the path winds in CW direction
     * @param bounds optional known bounds for the path
     * @return false 
     */
  public:
    bool beginInsidePath(bool windCW, const ::terra::Extent* bounds);

    /**
     * Begin projection of a new path. If path was already begun
     * then finish that path, keep it, and add a new path. 
     * @param bounds optional known bounds for the path
     * @return false 
     */
  public:
    bool beginPath(const ::terra::Extent* bounds);

    /**
     * Does nothing by default.
     * @param lat the start latitude
     * @param lon the start longitude
     * @param alt the start altitude
     * @param bounds optional known bounds for the path being started
     * @return false 
     */
  public:
    bool moveTo(double lat, double lon, double alt, const ::terra::Extent* bounds);
	
    /**
     * Does nothing by default.
     * @param n the number of position deltas
     * @param delta delta positions
     * @param type the arc type
     * @param an optional extent
     */
  public:
    void moveAlongArcs (size_t n, const Arc* delta, ArcType type,const ::terra::Extent* e);
  };
}

#endif
