#include <cartografo/Projector2D.h>
#include <cartografo/Region.h>
#include <cartografo/projector/SimpleProjector2D.h>
#include <cartografo/projector/SimpleProjector2D.h>
#include <tikal/Region2D.h>

namespace cartografo {
   Projector2D::Projector2D()
   throws()
   {}
   Projector2D::~Projector2D()
   throws()
   {}

   ::timber::Pointer< Projector2D> Projector2D::create(const ::timber::Reference< Projection>& p)
   throws()
   {
      return ::cartografo::projector::SimpleProjector2D::create(p);
   }

   ::timber::Pointer< ::tikal::Region2D> Projector2D::projectRegion(Projector2D& p, const Region& r)
throws()
{
   ::std::vector< ::timber::Reference< ::tikal::Region2D> > res;
   r.project(p);
   p.getRegions(res);
   for (size_t i=1;i<res.size();++i) {
      res[0] = res[0]->merge(res[i]);
   }
   ::timber::Pointer< ::tikal::Region2D> result;
   if (!res.empty()) {
      result = res[0];
   }
   return result;
}

}

