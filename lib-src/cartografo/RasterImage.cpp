#include <cartografo/RasterImage.h>
#include <terra/Bounds.h>

using namespace ::timber;

namespace cartografo {

   RasterImage::Bounds::Bounds()
   throws()
   {
   }

   RasterImage::Bounds::~Bounds()
   throws()
   {
   }

   RasterImage::Bounds::Bounds(const ::terra::Bounds& b)
   throws()
   : _topLeft(b.northWest()),_topRight(b.northEast()),_bottomRight(b.southEast()),_bottomLeft(b.southWest())
   {}

   RasterImage::Bounds::Bounds(const Location& tl, const Location& tr, const Location& br, const Location& bl)
   throws()
   : _topLeft(tl),_topRight(tr),_bottomRight(br),_bottomLeft(bl)
   {
      assert (tr.crs()->equals(*tl.crs()));
      assert (br.crs()->equals(*tl.crs()));
      assert (bl.crs()->equals(*tl.crs()));
   }

   bool RasterImage::Bounds::setCRS(const ::timber::Reference< ::terra::CoordinateReferenceSystem>& newCRS)
   throws()
   {
      if (_topLeft.crs()->equals(*newCRS)) {
         return true;
      }

      // transform the first point, if that succeeds we're more likely than not to set the CRS for all the other point
      if (!_topLeft.setCRS(newCRS)) {
         return false;
      }

      // unfortunately, we need to perform the updates on the remaining corners in a way that preserves the current point
      const Location tr(_topRight);
      const Location br(_bottomRight);
      const Location bl(_bottomLeft);

      if (!_topRight.setCRS(newCRS) || !_bottomRight.setCRS(newCRS) || !_bottomLeft.setCRS(newCRS)) {
         _topRight = tr;
         _bottomRight = br;
         _bottomLeft = bl;
         return false;
      }

      return true;
   }

   bool RasterImage::Bounds::operator==(const Bounds& b) const
   throws()
   {
      if (&b == this) {
         return true;
      }
      return _topLeft == b._topLeft && _topRight == b._topRight && _bottomRight == b._bottomRight && _bottomLeft==b._bottomLeft;
   }

   double RasterImage::Bounds::topWidth() const
   throws() {return _topLeft.distance(_topRight);}

   double RasterImage::Bounds::bottomWidth() const
   throws() {return _bottomLeft.distance(_bottomRight);}

   double RasterImage::Bounds::leftHeight() const
   throws() {return _topLeft.distance(_bottomRight);}

   double RasterImage::Bounds::rightHeight() const
   throws() {return _topRight.distance(_bottomRight);}

   RasterImage::RasterImage()
   throws()
   {}

   RasterImage::RasterImage(const Bounds& b)
   throws()
   : _bounds(b)
   {
   }

   RasterImage::RasterImage(const RasterImage& g)
   throws()
   : _bounds(g._bounds),_image(g._image)
   {}

   RasterImage::RasterImage(const Bounds& b, const ImagePtr& i)
   throws()
   : _bounds(b),_image(i)
   {
   }

   RasterImage::~RasterImage()
throws()
{}

}
