#include <cartografo/AbstractPath.h>
#include <cartografo/PathSegment.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionMapping.h>
#include <cartografo/Projector.h>

#include <terra/Geodesic.h>
#include <terra/Loxodrome.h>
#include <terra/Bounds.h>
#include <terra/Extent.h>
#include <terra/terra.h>

#include <timber/logging.h>

#include <cmath>

using namespace ::timber;
using namespace ::terra;
using namespace ::timber::logging;

namespace cartografo {
  namespace {
    static Log logger() { return Log("cartografo.AbstractPath"); }

    // a location error which is roughly equal to 1mm at the equator
    // 1 degree is about 110km, so 1.1 mm is about 1/10,000 degrees.
    static const double LOCATION_ERROR = ::terra::PI/180 / (100.0 * 1000.0);

    static void splitGeodesic (const GeodeticCoordinates& a, const GeodeticCoordinates& b, double err, PathBuilder& builder)
    {
      ::std::vector<GeodeticCoordinates> X;
      X.push_back(b);
      X.push_back(a);
      size_t count = 0;
      while (X.size()>1 && ++count<100) { //FIXME: hardcoded number!!!
	GeodeticCoordinates p = X.back();
	X.pop_back();
	GeodeticCoordinates q = X.back();
	if (q.latitude()==p.latitude() && ::std::abs(q.latitude())== ::terra::PI) {
	  builder.loxodromeTo(q);
	}
	else {
	  const Geodesic  G(p,q);
	  const Loxodrome L(p,q);
	  if (::std::isnan(G.bearing())) {
	    LogEntry(logger()).warn() << " P : " << p << '\n' << "Q : " << q << ::std::endl
				      << " LAT : " << (p.latitude()==q.latitude()) << ::std::endl
				      << " LON : " << (p.longitude()==q.longitude()) << doLog;
	  }
	  assert(!::std::isnan(G.range()));
	  assert(!::std::isnan(G.bearing()));
	  assert(!::std::isnan(L.range()));
	  assert(!::std::isnan(L.bearing()));
	  if (::std::abs(G.range()-L.range()) <= err) {
	    builder.loxodromeTo(q);
	  }
	  else {
	    const Geodesic geo(G.range()/2,G.bearing());
	    const GeodeticCoordinates pq = geo.move(p);
	    const Geodesic  Gp(p,pq);
	    const Geodesic  Gq(pq,q);
	    if (::std::isnan(Gp.bearing()) || ::std::isnan(Gq.bearing())) {
	      LogEntry(logger()).warn() << "P : " << p << '\n' 
					<< "PQ: " << pq << '\n'
					<< "Q : " << q << ::std::endl;
	      
	    }
	    X.push_back(pq);
	    X.push_back(p);
	  }
	}
	assert (count<100);
      }
    }

    static Reference<Path> createLoxodromicPath (const Path& p, double err)
    {
      PathBuilder b;
      GeodeticCoordinates last;
      for (size_t i=0,sz=p.segmentCount();i<sz;++i) {
	const PathSegment& s = p.segment(i);
	if (s.type()==PathSegment::MOVE) {
	  b.moveTo(s.endPoint());
	}
	else if (s.type()==PathSegment::LOXODROME) {
	  b.loxodromeTo(s.endPoint());
	}
	else if (s.type()==PathSegment::GEODESIC) {
	  if (err >0) {
	    splitGeodesic(last,s.endPoint(),err,b);
	  }
	  else {
	    b.loxodromeTo(s.endPoint());
	  }
	}
	else {
	  assert ("Invalid line type"==0);
	}
	last = s.endPoint();
      }
      return b.path();
    }

    static Reference<Path> createBoundedLoxodromicPath (const Path& p, double seglength)
    {
      PathBuilder b;
      GeodeticCoordinates last;
      for (size_t i=0,sz=p.segmentCount();i<sz;++i) {
	const PathSegment& s = p.segment(i);
	if (s.type()==PathSegment::MOVE) {
	  b.moveTo(s.endPoint());
	}
	else if (s.type()==PathSegment::LOXODROME) {
	  const Loxodrome g(last,s.endPoint());
	  //LogEntry(logger()).info() << "L START : " << last << ", range= " << g.range() << ",  az = " << toDegrees(g.bearing()) << doLog;
	  for (double length = seglength;length < g.range(); length += seglength) {
	    const Loxodrome tmp(seglength,g.bearing());
	    const GeodeticCoordinates next = tmp.move(last);
	    b.loxodromeTo(next);
	    last = next;
	    //LogEntry(logger()).info() << "TO : " <<last << doLog;
	  }
	  b.loxodromeTo(s.endPoint());
	  //LogEntry(logger()).info() << "END : " << s.endPoint() << doLog;
	}
	else if (s.type()==PathSegment::GEODESIC) {
	  Geodesic g(last,s.endPoint());
	  //LogEntry(logger()).info() << "G START : " << last << ", range= " << g.range() << ",  az = " << toDegrees(g.bearing()) << doLog;
	  while (seglength < g.range()) {
	    g = Geodesic(seglength,g.bearing());
	    const GeodeticCoordinates next = g.move(last);
	    b.loxodromeTo(next);
	    last = next;
	    //LogEntry(logger()).info() << "TO : " <<last << doLog;
	    g = Geodesic(last,s.endPoint());
	  }
	  //LogEntry(logger()).info()<< "END : " << s.endPoint() << doLog;
	  b.loxodromeTo(s.endPoint());
	}
	else {
	  assert ("Invalid line type"==0);
	}
	last = s.endPoint();
      }
      return b.path();
    }
  }

  constexpr double AbstractPath::DEFAULT_GEODESIC_ERROR;

  AbstractPath::~AbstractPath() throws()
  {}
    
  Reference<Path> AbstractPath::toBoundedLoxodromicPath (double segLength) const throws()
  { return createBoundedLoxodromicPath(*this,segLength); }

  Reference<Path> AbstractPath::toLoxodromicPath (double err) const throws()
  { return createLoxodromicPath(*this,err); }

  void AbstractPath::project(Projector& p) const
  {
    const size_t N = 128;
    Projector::Arc arcs[N];
    Projector::ArcType arcType;
    PathSegment::Type curType = PathSegment::MOVE;

    size_t n = 0;
    const Extent bnds = extent();
    
    if (!p.beginPath(&bnds)) {
      // path won't be projected
      return;
    }

    double lat,lon,alt;
    bool haveMove = false;
    GeodeticCoordinates moveToPoint;
    for (size_t i=0,sz=segmentCount();i<sz;++i) {
      const PathSegment& seg = segment(i);
      if (seg.type()==PathSegment::MOVE) {
	if (n>0) {
	  p.moveAlongArcs(n,arcs,arcType,0);
	  n=0;
	}
	moveToPoint = seg.endPoint();
	lat = moveToPoint.latitude();
	lon = moveToPoint.longitude();
	//LogEntry(logger()).info() << "MOVE-TO " << toDegrees(lat) << ", " << toDegrees(lon) << doLog;
	alt = moveToPoint.height();
	//@todo check bounds of path here
	haveMove = p.moveTo(lat,lon,alt,0);
	curType = seg.type();
      }
      else if (haveMove) {
	if (n==N) {
	  p.moveAlongArcs(n,arcs,arcType,0);
	  n=0;
	}
	  
	if (seg.type()!=curType) {
	  if (n>0) {
	    p.moveAlongArcs(n,arcs,arcType,0);
	    n=0;
	  }
	  // if the arc type changes, then we need to emit all
	  // existing arcs of the previous type
	  switch (seg.type()) {
	  case PathSegment::LOXODROME:
	    arcType = Projector::LOXODROME;
	    break;
	  case PathSegment::GEODESIC:
	    arcType = Projector::GEODESIC;
	    break;
	  default:
	    assert ("Unknown segment type"==0);
	  };
	  curType = seg.type();
	}
	  
	lat = clampLatitude(lat + seg.deltaLatitude());
	  
	lon += seg.deltaLongitude();
	alt += seg.deltaHeight();
	// deal with some round-off errors that might cause the
	// path not to close properly
	if (moveToPoint.equals(seg.endPoint())) {
	  if (::std::abs(lon-moveToPoint.longitude()) < 1E-5) {
	    lat = moveToPoint.latitude();
	    lon = moveToPoint.longitude();
	    alt = moveToPoint.height();
	  }
	}
	assert (bnds.contains(lat,lon,LOCATION_ERROR) || "Invalid path extent"==0);
	//	LogEntry(logger()).info() << "PATH-TO " << toDegrees(lat) << ", " << toDegrees(lon) << doLog;
	arcs[n].endLat = lat;
	arcs[n].endLon = lon;
	arcs[n].endAlt = alt;
	++n;
      }
    }
    if (n>0) {
      p.moveAlongArcs(n,arcs,arcType,0);
      n=0;
    }
    //LogEntry(logger()).info()  << "PATH-DONE" << doLog;
  }
}
