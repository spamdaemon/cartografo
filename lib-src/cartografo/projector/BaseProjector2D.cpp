#include <cartografo/projector/BaseProjector2D.h>
#include <cartografo/Projection.h>
#include <cartografo/Region.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionDomain.h>

#include <terra/terra.h>
#include <terra/Extent.h>

#include <tikal/Region2D.h>
#include <tikal/Path2D.h>
#include <tikal/Region2DOps.h>

#include <timber/logging.h>

#include <cmath>
#include <vector>

#define ENABLE_DEBUG_LOGGING 0

using namespace timber;
using namespace timber::logging;
using namespace terra;
using namespace tikal;

namespace cartografo {
  namespace projector {
    namespace {
      static inline double compareLat (double a, double b)
      { return a==b; }//::std::abs(a-b) < 1E-9; }
      static inline double compareLon (double a, double b)
      { return a==b; } //::std::abs(a-b) < 1E-9; }
      
      static Log logger() { return Log("cartografo.projector.BaseProjector2D"); }

    }

    BaseProjector2D::BaseProjector2D(const Reference<Projection>& proj) throws()
    : _projection(proj), // remember the projection
      _bounds(proj->domain()->region()->extent().getBounds(proj->domain()->center().datum())), // the bounds
      _currentType(NONE), // no current type yet
      _needMoveTo(true), // need to get a call to moveTo() first
      _regionOps(Region2DOps::create()),
      _lastLat(0),_lastLon(0)
    {
      // need to get the projection bounds here
    }
	
    BaseProjector2D::~BaseProjector2D() throws()
    {}
      
    Reference<Projection> BaseProjector2D::projection() const throws()
    { return _projection; }
	
    size_t BaseProjector2D::getPaths(::std::vector< Reference<Path2D> >& result) throws()
    {
      completePath();
      size_t n=_resultPaths.size();
      result.insert(result.end(),_resultPaths.begin(),_resultPaths.end());
      _resultPaths.clear();
      return n;
    }

    size_t BaseProjector2D::getRegions(::std::vector< Reference<Region2D> >& result) throws()
    { 
      completeRegion();
      size_t n=_resultRegions.size();
      result.insert(result.end(),_resultRegions.begin(),_resultRegions.end());
      _resultRegions.clear();
      return n;
    }

    bool BaseProjector2D::beginOutsidePath(bool windCW, const Extent* ext)
    {
#if ENABLE_DEBUG_LOGGING != 0
      logger().debugging("beginOutsidePath");
#endif

      completeRegion();
      _needMoveTo=true;
      if (ext!=0) {
	const Bounds e(ext->getBounds(_bounds.datum()));
	if (!_bounds.intersects(e)) {
	  _currentType = NONE;
	  return false;
	}
      }
      _currentType = OUTSIDE_PATH;
      _windCW = windCW;
      return true; 
    }
      
    bool BaseProjector2D::beginInsidePath(bool windCW, const Extent* ext)
    {
#if ENABLE_DEBUG_LOGGING != 0
      logger().debugging("beginInsidePath");
#endif
      completeArea();
      _needMoveTo=true;
      if (ext!=0) {
	const Bounds e(ext->getBounds(_bounds.datum()));
	if (!_bounds.intersects(e)) {
	  return false;
	}
      }
      if (_currentType!=INSIDE_PATH) {
	if (_currentType!=OUTSIDE_PATH) {
	  _currentType=NONE;
	  return false;
	}
	_currentType = INSIDE_PATH;
      }
      _windCW = windCW;
      return true; 
    }

    bool BaseProjector2D::beginPath(const Extent* ext)
    {
#if ENABLE_DEBUG_LOGGING != 0
      logger().debugging("beginPath");
#endif
     _needMoveTo=true;
      completeRegion();
      if (ext!=0) {
	const Bounds e(ext->getBounds(_bounds.datum()));
	if (!_bounds.intersects(e)) {
	  _currentType = NONE;
	  return false;
	}
      }
      _currentType = PATH;
      return true; 
    }

    bool BaseProjector2D::moveTo(double lat, double lon, double alt, const Extent* ext)
    {
      if (ext!=0) {
	//FIXME: does ext have to be translated or this intersection ok
	const Bounds e(ext->getBounds(_bounds.datum()));
	if (!_bounds.intersects(e)) {
	  return false;
	}
      }
      switch (_currentType) {
      case OUTSIDE_PATH:
	completeRegion();
	break;
      case INSIDE_PATH:
	completeArea();
	break;
      case PATH:
	completePath();
	break;
      default:
	flush();
	return false;
      }

#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "Move to " << toDegrees(lat) << ", " << toDegrees(lon) << doLog;
#endif
      insertStartPoint(lat,lon,alt);
      _lastLat = lat;
      _lastLon = lon;
      _lastAlt = alt;
      _needMoveTo = false;
      return true;
    }	

    void BaseProjector2D::moveAlongArcs (size_t n, const Arc* delta, ArcType type, const Extent* )
    {
      if (_currentType==NONE || _needMoveTo) {
	return;
      }
      switch(type) {
      case GEODESIC:
	for (const Arc* arc=delta;arc < delta+n;++arc) {
	  if (doArc(arc->endLat,arc->endLon,arc->endAlt)) {
	    _lastLat = arc->endLat;
	    _lastLon = arc->endLon;
	    _lastAlt = arc->endAlt;
	    insertGeodesic(arc->endLat,arc->endLon,arc->endAlt);
	  }
	}
	break;
      case LOXODROME:
	for (const Arc* arc=delta;arc < delta+n;++arc) {
	  if (doArc(arc->endLat,arc->endLon,arc->endAlt)) {
	    _lastLat = arc->endLat;
	    _lastLon = arc->endLon;
	    _lastAlt = arc->endAlt;
	    insertLoxodrome(arc->endLat,arc->endLon,arc->endAlt);
	  }
	}
      }
    }

    bool BaseProjector2D::doArc(double endLat, double endLon, double endAlt) throws()
    {
      // poleStatus will take on 1 of 4 values:
      // 0 : neither the current nor the last point were at a pole
      // 1 : the current point is at a pole, but the last one was not
      // 2 : the current point is not a pole, but the last one was
      // 3 : the current and the last point are at a pole
      int poleStatus = compareLat(::std::abs(endLat),::terra::PI/2) ? 1 : 0;
      // was the last point at a pole
      poleStatus |= compareLat(::std::abs(_lastLat),::terra::PI/2) ? 2 : 0;
      
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "doArc -> " << toDegrees(endLat) << ", " << toDegrees(endLon) << doLog;
#endif

      switch (poleStatus) {
      case 0:
	// not at pole, so do the arc
	return true;
      case 1:
	// now, being at a pole, draw a loxodrome from the last point to the pole
	// insert a loxodrome going from the last position to the pole
	// but don't draw an arc to the actual endpoint.
	insertLoxodrome(endLat,_lastLon,endAlt);
	_lastLat= endLat; // need to keep this up to date
	_lastAlt = endAlt;
	return false;
      case 2:
	// no longer at a pole
	// need to draw a polar arc to the current longitude
	insertPolarArc(_lastLat,endLon,endAlt);
	insertLoxodrome(endLat,endLon,endAlt);
	_lastLat = endLat;
	_lastLon = endLon;
	_lastAlt = endAlt;
	return false;
      case 3:
	// points were at the same pole; the arc endpoint can safely be ignored
	if (compareLat(_lastLat,endLat)) {
	  return false;
	}
	// first, create a circular arc to the end longitude
	// at the last pole, then create a loxodrome to the 
	// other pole.
	if (!compareLon(_lastLon,endLon)) {
	  insertPolarArc(_lastLat,endLon,endAlt);
	}
	insertLoxodrome(endLat,endLon,endAlt);
	// ignore the end point
	_lastLat = endLat;
	_lastLon = endLon;
	_lastAlt = endAlt;
	return false;
      case 4:
	assert(false && "Cannot happen");
	::std::abort();
      };
      return false; // keeps the compiler happy
    }

    void BaseProjector2D::completeArea() throws()
    {
      try {
	Pointer<Region2D> r = createRegion(_windCW);
	if (r) {
	  if (_currentType==INSIDE_PATH && _regions.empty()) {
	    return;
	  }
	  _regions.push_back(r);
	}
      }
      catch (const ::std::exception& e) {
	logger().caught("Could not project region",e);
	flush();
      }
    }

    void BaseProjector2D::completeRegion() throws()
    {
      if (_currentType!=INSIDE_PATH && _currentType!=OUTSIDE_PATH) {
	return;
      }

      completeArea();
      if (_regions.empty()) {
	return;
      }
	
      Pointer<Region2D> result=_regions[0];
      if (_regions.size()>1) {
	Reference<Region2D> inside=_regions[1];
	for (size_t i=2;i<_regions.size();++i) {
	  inside = _regionOps->merge(inside,_regions[i]);
	}
	result = _regionOps->difference(result,inside);
      }
      if (result) {
	_resultRegions.push_back(result);
      }
      _regions.clear();
    }

    void BaseProjector2D::completePath() throws()
    {
      if (_currentType!=PATH) {
	return;
      }
      try {
	Pointer<Path2D> p = createPath();
	if (p) {
	  _resultPaths.push_back(p);
	}
      }
      catch (const ::std::exception& e) {
	logger().caught("Could not project path",e);
	flush();
      }
    }
  }
}
