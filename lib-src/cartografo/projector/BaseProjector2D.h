#ifndef _CARTOGRAFO_PROJECTOR_BASEPROJECTOR2D_H
#define _CARTOGRAFO_PROJECTOR_BASEPROJECTOR2D_H

#ifndef _CARTOGRAFO_PROJECTOR2D_H
#include <cartografo/Projector2D.h>
#endif

#ifndef _TERRA_BOUNDS_H
#include <terra/Bounds.h>
#endif

#include <vector>

namespace tikal {
  class Region2DOps;
  class Region2D;
  class Path2D;
}
namespace cartografo {
  namespace projector {

    /** 
     * The BaseProjector2D is an abstract class that implements the Projector
     * interface in terms of specific functions geared towards the use of 
     * Projector2D.
     */
    class BaseProjector2D : public Projector2D
    {
      BaseProjector2D(const BaseProjector2D&);
      BaseProjector2D&operator=(const BaseProjector2D&);

      /** The projected object types */
    private:
      enum ObjectType {
	NONE, OUTSIDE_PATH,INSIDE_PATH,PATH 
      };
      
      /** 
       * Create a projector for a given projection.
       * @param proj a projection
       */
    protected:
      BaseProjector2D(const ::timber::Reference<Projection>& proj) throws();
      
      /** Destructor. */
    public:
      ~BaseProjector2D() throws();

      /**
       * Implementation of Projector interface.
       * @{
       */
    public:
      size_t getPaths(::std::vector< ::timber::Reference< ::tikal::Path2D> >& result) throws();
      size_t getRegions(::std::vector< ::timber::Reference< ::tikal::Region2D> >& result) throws();
      bool beginOutsidePath(bool, const ::terra::Extent*);
      bool beginInsidePath(bool, const ::terra::Extent*);
      bool beginPath(const ::terra::Extent*);
      bool moveTo(double , double , double , const ::terra::Extent* );
      void moveAlongArcs (size_t , const Arc* , ArcType , const ::terra::Extent* );

      /*@}*/

      /**
       * Get the projection.
       * @return the projection
       */
      ::timber::Reference<Projection> projection() const throws();
      
      /**
       * Insert the start point of an object (region or path) to be projected.
       * @param lat the latitude of a point
       * @param lon the longitude of the point
       * @param alt the altitude of the arc
       */
    protected:
      virtual void insertStartPoint (double lat, double lon, double alt) throws() = 0;
      
      /**
       * Insert an single geodesic arc for projection. The arc starts at the
       * previously inserted point.
       * @param lat the end-latitude of the arc
       * @param lon the end-longitude of the arc
       * @param alt the end-altitude of the arc
       */
    protected:
      virtual void insertGeodesic(double lat, double lon,double alt) throws() = 0;

      /**
       * Insert an single loxodromic arc for projection. The arc starts at the
       * previously inserted point.
       * @param lat the end-latitude of the arc
       * @param lon the end-longitude of the arc
       * @param alt the end-altitude of the arc
       */
    protected:
      virtual void insertLoxodrome(double lat, double lon,double alt) throws() = 0;

      /**
       * Insert a polar arc. A polar arc is a line in the mercator projection
       * at either the north or south pole. On the spheroid it is really a point.
       * @param lat either -PI/2 or PI/2
       * @param lon the longitude value
       */
    protected:
      virtual void insertPolarArc (double lat, double lon,double alt) throws() = 0;

      /**
       * Create a path from the points inserted so far. Once complete,
       * any points or arcs must have been flushed. If this method
       * throws an exception, then flush() is called.
       * 
       * @return a path or null if the path could not be created
       * @throws ::std::exception if the path could not be created
       */
    protected:
      virtual ::timber::Pointer< ::tikal::Path2D> createPath() throws (::std::exception)= 0;
      
      /**
       * Create a single region from the current set of points. Once complete,
       * any points or arcs must have been flushed. The windCW flag must be used to
       * determine if the region's boundary was enumered in clockwise or counter
       * clockwise order.
       * <p>
       * If this method throws an exception, then flush() is called.
       * @param windCW true for clockwise ordering of the region's boundary
       * @return a region or null if it could not be created
       * @throws ::std::exception if the region could not be created
       */
    protected:
      virtual ::timber::Pointer< ::tikal::Region2D> createRegion(bool windCW)  throws (::std::exception) = 0;
      
      /**
       * Flush any coordinates or arcs that that had been inserted. 
       */
    protected:
      virtual void flush() throws() = 0;
      
      /** 
       * Complete the current area.
       */
    private:
      void completeArea() throws();

      /**
       * Complete the current region. This completes the current area
       * and then merges the area with an existing region.
       */
    private:
      void completeRegion() throws();
	
      /**
       * Complete the current path.
       */
    private:
      void completePath() throws();
      
      /**
       * Do an arc arc before rendering the actual arc. If toLat is at the 
       * one of the poles, then special handling is needed because polar locations
       * are really just single points.
       * @param toLat the latitude of the arc's endpoint
       * @param toLon the longitude of the arc's endpoint
       * @param toAlt the altitude of the arc's endpoint
       * @return true if the arc's endpoint should be incorporated, false otherwise
       */
    private:
      bool doArc(double toLat, double toLon, double toAlt) throws();
      
      /** The projection */
    private:
      ::timber::Reference<Projection> _projection;

      /** The bounds */
    private:
      ::terra::Bounds _bounds;

      /** The current object type */
    private:
      ObjectType _currentType;

      /** True if a move-to is needed */
    private:
      bool _needMoveTo;
	
      /** A vector polygons (the first polygon is the external contour, the others all inside contours) */
    private:
      ::std::vector< ::timber::Reference< ::tikal::Region2D> > _regions;

      /** The result regions  (created since the last call to getRegions) */
    private:
      ::std::vector< ::timber::Reference< ::tikal::Region2D> > _resultRegions;

      /** The result paths (created since the last call to getPaths)*/
    private:
      ::std::vector< ::timber::Reference< ::tikal::Path2D> > _resultPaths;
      
      /** The region ops */
    private:
      ::std::unique_ptr< ::tikal::Region2DOps> _regionOps;

      /** The last point */
    private:
      double _lastLat,_lastLon,_lastAlt;
      
      /** True if the current region is winding in clockwise order, false otherwise */
    private:
      bool _windCW;
    };
  }
}
#endif
