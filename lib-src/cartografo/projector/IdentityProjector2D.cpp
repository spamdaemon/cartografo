#include <cartografo/projector/IdentityProjector2D.h>
#include <cartografo/projection/IdentityProjection.h>
#include <cartografo/Projection.h>
#include <cartografo/Region.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionDomain.h>

#include <terra/Bounds.h>
#include <terra/terra.h>
#include <terra/Extent.h>

#include <tikal/BoundingBox2D.h>
#include <tikal/Area2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Path2DBuilder.h>
#include <tikal/VertexOrder.h>
#include <tikal/Region2D.h>
#include <tikal/Path2D.h>
#include <tikal/Point2D.h>
#include <tikal/Region2DOps.h>
#include <tikal/util.h>

#include <timber/logging.h>

#include <cmath>
#include <vector>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;
using namespace ::tikal;
using namespace ::cartografo::projection;

#define ENABLE_DEBUG_LOGGING 0

namespace cartografo {
  namespace projector {
    namespace {
      
      static Log logger() { return Log("cartografo.projector.IdentityProjector2D"); }
    }

    IdentityProjector2D::IdentityProjector2D(const GeodeticCoordinates& ctr) throws()
    : BaseProjector2D(IdentityProjection::create(ctr,Bounds(ctr.datum()))),
      _regionOps(Region2DOps::create()),
      // shift will need to be applied to each point before it is projected
      _latShift(-ctr.latitude()),
      _lonShift(-ctr.longitude()),
      _removeSelfIntersections(false),
      _needClipping(false) // no special clipping is needed
    {
      // create a simple bounding box corresponding to the standard extent
      const BoundingBox2D bbox(-PI,-PI/2,PI,PI/2);
      // create a clockwise polygon which will be used for clipping 
      _projectionRange = Polygon2D::create(bbox,VertexOrder::CW);

      
    }

    IdentityProjector2D::IdentityProjector2D(const Reference<GeodeticDatum>& s) throws()
    : BaseProjector2D(IdentityProjection::create(GeodeticCoordinates(0,0,s),Bounds(s))),
      _regionOps(Region2DOps::create()),
      _latShift(0),_lonShift(0),
      _removeSelfIntersections(false),
      _needClipping(false) // no special clipping is needed
    {
      // create a simple bounding box corresponding to the standard extent
      const BoundingBox2D bbox(-PI,-PI/2,PI,PI/2);
      // create a clockwise polygon which will be used for clipping 
      _projectionRange = Polygon2D::create(bbox,VertexOrder::CW);
    }

    IdentityProjector2D::IdentityProjector2D(const GeodeticCoordinates& ctr, const Reference<Region>& range) throws (::std::exception)
    : BaseProjector2D(IdentityProjection::create(ctr,range->extent().getBounds(ctr.datum()))),
      _regionOps(Region2DOps::create()),
      // shift will need to be applied to each point before it is projected
      _latShift(-ctr.latitude()),
      _lonShift(-ctr.longitude()),
      _removeSelfIntersections(false),
      _needClipping(true)
    {

      // project the "range" region using another instance of this projector
      Reference<IdentityProjector2D> ip(new IdentityProjector2D(ctr));

      range->project(*ip);
      ::std::vector<Reference<Region2D> > regs;
      ip->getRegions(regs);

      // if there are no regions, then something serious was wrong
      if (regs.empty()) {
	logger().bug("Could not project a region using a standard identity projector");
	throw ::std::runtime_error("Could not determine projection range");
      }

      // merge the regions
      Reference<Region2D> r=regs[0];
      for (size_t i=1;i<regs.size();++i) {
	r = _regionOps->merge(r,regs[i]);
      }

      _projectionRange = r;
    }
    IdentityProjector2D::IdentityProjector2D(const IdentityProjector2D& orig) throws()
    : BaseProjector2D(orig.projection()),
      _regionOps(Region2DOps::create()),
      _projectionRange(orig._projectionRange),
    // shift will need to be applied to each point before it is projected
      _latShift(orig._latShift),
      _lonShift(orig._lonShift),
      _removeSelfIntersections(orig._removeSelfIntersections),
      _needClipping(orig._needClipping)
    {
    }

    IdentityProjector2D::~IdentityProjector2D() throws()
    {}

    IdentityProjector2D* IdentityProjector2D::copy() const throws()
    { return new IdentityProjector2D(*this); }

    void IdentityProjector2D::setExpectSelfIntersections (bool selfIntersectionsExpected) throws()
    { _removeSelfIntersections = selfIntersectionsExpected; }
    
    void IdentityProjector2D::flush() throws()
    { _points.clear(); }
    
    void IdentityProjector2D::insertStartPoint (double lat, double lon, double alt) throws()
    { 
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "S " << lat << " " << lon << doLog;
#endif
      lon += _lonShift;
      lat += _latShift; 
      _points.push_back(Point2D(lon,lat)); 
      _pointBounds = BoundingBox2D(lon,lat);
    }
    
    void IdentityProjector2D::insertGeodesic(double lat, double lon,double alt) throws()
    {
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "G " << lat << " " << lon << doLog;
#endif
      lon += _lonShift;
      lat += _latShift; 
      _points.push_back(Point2D(lon,lat)); 

      //FIXME: insert at least 1 more point for the arc, so that a polygon
      // consisting of only that arc is going to be a proper polygon

      _pointBounds.insert(lon,lat);
    }

    void IdentityProjector2D::insertLoxodrome(double lat, double lon,double alt) throws()
    {       
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "L " << lat << " " << lon << doLog;
#endif
      lon += _lonShift;
      lat += _latShift; 
      _points.push_back(Point2D(lon,lat)); 
      _pointBounds.insert(lon,lat);
    }

    void IdentityProjector2D::insertPolarArc (double lat, double lon,double alt) throws()
    {       
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "P " << lat << " " << lon << doLog;
#endif
      lon += _lonShift;
      lat += _latShift; 
      _points.push_back(Point2D(lon,lat)); 
      _pointBounds.insert(lon,lat);
    }

    
    Pointer<Path2D> IdentityProjector2D::createPath() throws(::std::exception)
    {
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "Create path" << doLog;
#endif
      // no points, no problem
      Pointer<Path2D> p;
      if (_points.size()<2) {
	if (!_points.empty()) {
	  LogEntry(logger()).debugging() << "Not enough points for path : " << _points.size() << doLog;
	  _points.clear();
	}
	return p;
      }
      
      // determine if the path represented by the points must be clipped
      bool needClip = _pointBounds.minx() < -PI || _pointBounds.maxx() > PI || _pointBounds.miny() < -PI/2 || _pointBounds.maxy() <= PI/2;

#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "Need to clip path to box " << _pointBounds << " : " << (needClip ? "YES":"NO") << doLog;
#endif

      // if a clip is not needed, then just build a path
      if (!needClip) {
#if ENABLE_DEBUG_LOGGING != 0
	LogEntry(logger()).debugging() << "Not clipping; creating polyline with " << _points.size() << " points" << doLog;
#endif
	p = Path2DBuilder::createPolylinePath(_points);

	// it's possibly that the path must be clipped against a custom region
	needClip = _needClipping;
      }
      else {
	// first, shift the points all the way to east
	// FIXME: we're only shifting in longitude at the moment.

	// determine the shift and offset needed; basically, compute by 
	// how many 2 PI one needs to shift so that the region ends up 
	// FIXME: explain the shift calculation
	double shift = -::std::floor(0.5*(_pointBounds.minx() - PI)/PI);
	double offset = (shift-1) * 2.0 * PI;
	// if an offset is not needed, then don't shift the points
	if (offset!=0.0) {
	  for (::std::vector< Point2D>::iterator i=_points.begin();i!=_points.end();++i) {
	    *i = Point2D(i->x()+offset,i->y());
	  }
	}
	
	assert(_pointBounds.minx()+offset < PI);
	double maxlon = _pointBounds.maxx()+offset;

	// create many concatenated paths
	// and then clip them against the polygon
	while(true) {
	  Reference<Path2D> path = Path2DBuilder::createPolylinePath(_points);
	  p = Path2D::concatenate(path,p);
#if ENABLE_DEBUG_LOGGING != 0
	  LogEntry(logger()).debugging() << "Concatenating path with with " << _points.size() << " points: " << *path << ::std::endl << " result : " << *p << doLog;
#endif
	  
	  maxlon -= 2.0*PI;
	  if (maxlon <= -PI) {
	    break;
	  }
	  // shift the points by 2 PI to the left and create a new polyline path
	  for (::std::vector< Point2D>::iterator i=_points.begin();i!=_points.end();++i) {
	    *i = Point2D(i->x()-(2.0*PI),i->y());
	  }
	}
      }
      // if a clip must still be performed, do so now before returning
      if (needClip) {
	p = clipPath(p,_projectionRange);
#if ENABLE_DEBUG_LOGGING != 0
	if (p) {
	  LogEntry(logger()).debugging() << "Clipped path to projection range " << *p << doLog;
	}
	else {
	  logger().debugging("Path clipped away");
	}
#endif
      }
#if ENABLE_DEBUG_LOGGING != 0
      if (!p) {
	logger().debugging("No path");
      }
#endif      
      _points.clear();
      return p;
    }

    Pointer<Region2D> IdentityProjector2D::createRegion(bool windCW) throws(::std::exception)
    {
      Pointer<Region2D> res;

      // we need at least 2 points for the region (actually 3, but
      // extra points may be added below
      if (_points.size()<2) {
	if (!_points.empty()) {
	  LogEntry(logger()).debugging() << "Too few points to describe a proper region " << _points.size() << doLog;
	  _points.clear();
	}
	return res;
      }
      
      // the first thing to do is to try and close the boundary for the region
      const Point2D first = *_points.begin();
      const Point2D last  = *_points.rbegin();
      
      bool needClip = false;

      if (::std::abs(first.x()-last.x()) > 1E-6 || ::std::abs(first.y()-last.y()) > 1E-6) {
	// if the curve is not closed, then that means that one of the poles will be 
	// included in the curve; need to determine which one
	// in order to compensate, the polygon is extended along the appropriate pole 
	// and closed
	double lat = 2; // any number larger than PI/2 can be used here
	
	// depending on the winding of the region, the region to be extended is the south pole
	// or the north pole
	if ((windCW && first.x() < last.x()) || (!windCW && last.x() < first.x())) {
	  lat = -lat;
	}
	
	// extend the polyon as needed
	_points.push_back(Point2D(last.x(),lat));
	_points.push_back(Point2D(first.x(),lat));

	// update the bounding box as well
	_pointBounds.insert(first.x(),lat);
	
	// the first point is not duplicated, leaving the polygon open; but that's
	// ok, because the first point is implicitly the same as the last point

	// the extension produced points outside the normal clipping region, so a clip is
	// definitely needed
	needClip = true;
      }
      else {
	_points.pop_back();
	
	// need to check if a clip is needed
	needClip = _pointBounds.minx() < -PI || _pointBounds.maxx() > PI || _pointBounds.miny() < -PI/2 || _pointBounds.maxy() <= PI/2;
#if ENABLE_DEBUG_LOGGING != 0
      LogEntry(logger()).debugging() << "Need to clip region to box " << _pointBounds << " : " << (needClip ? "YES":"NO") << doLog;
#endif
      }
      
      // need at least 3 points for a proper polygon
      if (_points.size()<3) {
	logger().debugging("Not enough points for region");
      }

      // FIXME: is this really needed, to compute the vertex order??????
      // should this not be the same as the winding direction?
      const VertexOrder vo = VertexOrder(_points);

      // if a clip is not needed, then just build the region
      if (!needClip) {
	res=Polygon2D::create(_points,vo);

	// it's possibly that the region must be clipped against a custom region
	needClip = _needClipping;
      }
      else {

	// check if the polygon is completely outof range;
	// FIXME: not sure how to handle this yet
	if ((_pointBounds.maxy() < -PI/2 || _pointBounds.miny() > PI/2)) {
	  logger().debugging("Cannot create region; latitude is out of bounds");
	  _points.clear();
	  return res;
	}

	// when building the region, we need to check the min and max longitudes:
	// if they are within the projection's limit, then we're ok

	// compute the shift needed to move the polygon as far right as possible
	double shift = -::std::floor(0.5*(_pointBounds.minx() - PI)/PI);
	double offset = (shift-1) * 2.0 * PI;
	if (offset!=0) {
	  for (::std::vector< Point2D>::iterator i=_points.begin();i!=_points.end();++i) {
	    *i = Point2D(i->x()+offset,i->y());
	  }
	}
	
	assert(_pointBounds.minx()+offset < PI);
	double maxlon = _pointBounds.maxx()+offset;
	while(true) {
	  Pointer<Region2D> poly = Polygon2D::create(_points,vo);
	  
	  // merge the polygon with other polygons already created
	  res = _regionOps->merge(res,poly);
	  
	  // shift to the left and try again
	  maxlon -= 2.0*PI;
	  if (maxlon <= -PI) {
	    break;
	  }
	  for (::std::vector< Point2D>::iterator i=_points.begin();i!=_points.end();++i) {
	    *i = Point2D(i->x()-(2.0*PI),i->y());
	  }
	}
      }
      
      // if desired or advised by the client, normalize the region
      if (_removeSelfIntersections) {
	res = _regionOps->normalize(res);
      }
      
      // if a clip is needed, do so now
      if (needClip) {
	// this is not necessary if the difference operation below will be applied
	res = _regionOps->intersect(_projectionRange,res);
      }
      
      // if the winding direction is not the same as the expected direction, then 
      // the inverse of region must be used
      if (windCW != vo.isCW()) {
	res = _regionOps->difference(_projectionRange,res);
      }
      _points.clear();
#if ENABLE_DEBUG_LOGGING != 0
      if(!res){ 
	logger().debugging("Region clipped away");
      }
      else {
	LogEntry(logger()).debugging() << "Region : " << *res << doLog;
      }
#endif
      return res;
    }
  }
}
