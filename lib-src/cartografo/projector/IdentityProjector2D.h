#ifndef _CARTOGRAFO_PROJECTOR_IDENTITYPROJECTOR2D_H
#define _CARTOGRAFO_PROJECTOR_IDENTITYPROJECTOR2D_H

#ifndef _CARTOGRAFO_PROJECTOR_BASEPROJECTOR2D_H
#include <cartografo/projector/BaseProjector2D.h>
#endif

#ifndef _TERRA_EXTENT_H
#include <terra/Extent.h>
#endif

#include <vector>

namespace terra {
  class GeodeticCoordinates;
  class GeodeticDatum;
}

namespace tikal {
  class Point2D;
  class Region2D;
  class Region2DOps;
}

namespace cartografo {
  class Region;
    
  namespace projector {
    /** 
     * Instances of a projector are able to project geographic areas and paths
     * into mostly 2D regions and paths. The segments between points in this
     * projection are loxodromes. A geodesic arc is always approximated by multiple
     * loxodromes up to a specific precision.
     */
    class IdentityProjector2D : public BaseProjector2D {
      IdentityProjector2D&operator=(const IdentityProjector2D&);

      /**
       * A copy constructor.
       * @param orig the original to be copied
       */
    private:
      IdentityProjector2D(const IdentityProjector2D& orig) throws();

      /** 
       * Create a projector that uses the identity projection over the spheroid
       * centered at the given location.
       * @param ctr the center of the projection
       */
    public:
      IdentityProjector2D(const ::terra::GeodeticCoordinates& ctr) throws();

      /** 
       * Create a projector that uses the identity projection over the specified spheroid.
       * @param sphere a spheroid
       */
    public:
      IdentityProjector2D(const ::timber::Reference< ::terra::GeodeticDatum>& sphere) throws();

      /** 
       * Create a projector that uses the identity projection and constrains the resulting
       * projections to lie within the given range.
       * @param ctr the center of the projection
       * @param range a region that constrains the range of the identity projection
       * @throws ::std::exception if the projector could not be created
       */
    public:
      IdentityProjector2D(const ::terra::GeodeticCoordinates& ctr, const ::timber::Reference<Region>& range) throws (::std::exception);
      
      /**
       * Destroy this projector.
       */
    public:
      ~IdentityProjector2D() throws();

      /**
       * Hint at the projector that regions may have self-intersections.
       * @param selfIntersectionsExpected if true then self-intersections are expected
       */
    public:
      void setExpectSelfIntersections (bool selfIntersectionsExpected) throws();

      /**
       * @name Methods from BaseProjector2D and Projector
       * @{
       */
      void insertStartPoint (double lat, double lon, double alt) throws();
      void insertGeodesic(double lat, double lon,double alt) throws();
      void insertLoxodrome(double lat, double lon,double alt) throws();
      void insertPolarArc (double lat, double lon,double alt) throws();
      void flush() throws();

      ::timber::Pointer< ::tikal::Path2D> createPath() throws (::std::exception);
      ::timber::Pointer< ::tikal::Region2D> createRegion(bool windCW)  throws (::std::exception);

      IdentityProjector2D* copy() const throws();
      
      /*@}*/

      /** The points of the current object to be projected */
    private:
      ::std::vector< ::tikal::Point2D> _points;

      /** A bounding box around the points */
    private:
      ::tikal::BoundingBox2D _pointBounds;

      /** The region ops */
    private:
      const ::std::unique_ptr< ::tikal::Region2DOps> _regionOps;

      /** A clip region */
    private:
      ::timber::Pointer< ::tikal::Region2D> _projectionRange;

      /** The shift constant that moves the center to 0 latitude */
    private:
      const double _latShift;

      /** The shift constant that moves the center to 0 longitude */
    private:
      const double _lonShift;

      /** Remove self intersections */
    private:
      bool _removeSelfIntersections;

      /** 
       * True if clipping with the projection range is always needed because the
       * the clipping range is a custom range
       */
    private:
      const bool _needClipping;
    };
  }
}

#endif
