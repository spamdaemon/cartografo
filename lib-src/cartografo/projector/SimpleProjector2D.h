#ifndef _CARTOGRAFO_PROJECTOR_SIMPLEPROJECTOR2D_H
#define _CARTOGRAFO_PROJECTOR_SIMPLEPROJECTOR2D_H

#ifndef _CARTOGRAFO_ABSTRACTPROJECTOR2D_H
#include <cartografo/AbstractProjector2D.h>
#endif

namespace cartografo {
  namespace projector {
    class IdentityProjector2D;

    /**
     * An abstract projector that projects into 2D regions. Subclasses must only
     * implement projectForward() to use this projector. When projecting,
     * this projector ignores the altitude values.
     */
    class SimpleProjector2D : public AbstractProjector2D {
      /** No copying */
      SimpleProjector2D&operator=(const SimpleProjector2D&);

      /**
       * Constructor.
       * @param p a projection
       * @throws ::std::exception throws an exception if the projector could not be created
       */
    private:
      SimpleProjector2D(const ::timber::Reference<Projection>& p) throws (::std::exception);

      /** A copy constructor 
       * @param orig the original projector
       */
    private:
      SimpleProjector2D(const SimpleProjector2D& orig) throws();
	
      /** Destructor */
    public:
      ~SimpleProjector2D() throws();

      /**
       * Create a new simple 2d projector. An error typically occurs with this
       * call if the there problems with ::tika::Region2DOps.
       * @param p a projector
       * @return a projector or 0 if an error occurred.
       */
    public:
      static ::timber::Pointer<Projector2D> create(const ::timber::Reference<Projection>& p) throws();

      /**
       * @name Implementing methods
       * @{
       */	  
      SimpleProjector2D* copy() const throws();
      ::timber::Reference<Projection> projection() const throws();
      bool beginOutsidePath(bool windCW,const ::terra::Extent* bnds);
      bool beginInsidePath(bool windCW,const ::terra::Extent* bnds);
      bool beginPath(const ::terra::Extent* ext);
      bool moveTo(double lat, double lon, double alt,const ::terra::Extent* bnds);
      void moveAlongArcs (size_t n, const Arc* arcs, ArcType type, const ::terra::Extent* e);
	
      /*@*/

	
    public:
      size_t getRegions(::std::vector< ::timber::Reference< ::tikal::Region2D> >& result) throws();

    public:
      size_t getPaths(::std::vector< ::timber::Reference< ::tikal::Path2D> >& result) throws();
	
      /**
       * A translation in longitude and latitude space 
       * This translation is applied before points are projected, because
       * the projection itself is going to be do apply another difference
       * due to the projection center. 
       * FIXME: redesign the projection code so that we don't have to 
       *        keep shifting things around. The way to do this would be
       *        to keep another internal projection around (0,0) and
       *        call a special projection function that assumes the projection
       *        center to be at (0,0).
       */
    private:
      const double _dx,_dy;

      /** A projection */
    private:
      const ::timber::Reference<Projection> _projection;
      
      /** An identity projector used for clipping to the projection domain */
    private:
      const ::timber::Reference< IdentityProjector2D> _clipper;
      
      /** An array of points */
    private:
      ::std::vector< ::tikal::Point2D> _points;
      
      /** The domain projected into XY */
    private:
      ::std::vector< ::timber::Reference< ::tikal::Region2D> > _domains;

      /** Th result regions */
    private:
      ::std::vector< ::timber::Reference< ::tikal::Region2D> > _regions;

      /** Th result regions */
    private:
      ::std::vector< ::timber::Reference< ::tikal::Path2D> > _paths;
    };
  }
}
#endif
