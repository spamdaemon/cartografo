#include <cartografo/projector/SimpleProjector2D.h>
#include <cartografo/projector/IdentityProjector2D.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionDomain.h>
#include <cartografo/Region.h>

#include <terra/GeodeticCoordinates.h>

#include <tikal/Region2DBuilder.h>
#include <tikal/Path2DBuilder.h>
#include <tikal/Path2D.h>
#include <tikal/Point2D.h>
#include <tikal/Area2D.h>
#include <tikal/Region2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Contour2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/util.h>

#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;
using namespace ::terra;

namespace cartografo {
  namespace {
    static Log logger() { return Log("cartografo.projector.SimpleProjector2D"); }

    static void addPath (const Projection& p, Path2DBuilder& b, vector<Point2D>& pts)
    {
      if (pts.empty()) {
	return;
      }
      
      size_t n = p.projectUnsafe(pts.size(),&pts[0]);
      if (n>0) {
	b.moveTo(pts[0]);
	// a slightly funny loop, but need to skip the first point
	for (vector<Point2D>::const_iterator i= ++pts.begin(),end=pts.begin()+n;i!=end;++i) {
	  b.lineTo(*i);
	}
      }
      pts.clear();
    }

    static void createLoxodrome (Point2D from, Point2D to,vector<Point2D>& pts)
    {
      const double threshold = PI*(5.0/180.0);

      double dx = to.x()-from.x();
      double dy = to.y()-from.y();
      double d2 = dx*dx + dy*dy;
      if (d2 > threshold*threshold) { // FIXME: hardcoded value
	double d = ::std::sqrt(d2);
	dx  /= d;
	dy  /= d;
	for (double i=threshold;i<d;i+=threshold) {
	  pts.push_back(Point2D(from.x() + dx*i,from.y()+dy*i));
	}
      }
      pts.push_back(to);
    }


    static inline void loxodrome(const Point2D& p, vector<Point2D>&  tmpPts)
    {
      createLoxodrome(tmpPts.back(),p,tmpPts);
    }

    static inline void loxodrome(const Contour2D& src,size_t seg, double dx, double dy, vector<Point2D>&  tmpPts)
    {
      Point2D pts[2];
      size_t deg = src.segment(seg,pts);
      assert(deg==1);
      pts[1].translate(dx,dy);
      createLoxodrome(tmpPts.back(),pts[1],tmpPts);
    }


    static Pointer<Path2D> projectPath (const Reference<Path2D>& src, double dx, double dy, const Projection& p,  vector<Point2D>&  tmpPts)
    {
      size_t N = 512;
      tmpPts.reserve(max(N,tmpPts.capacity()));
      tmpPts.clear();
      
      if (src->maxDegree()>1) {
	throw runtime_error("Non-linear paths not supported");
      }

      Path2DBuilder b;
      for (size_t i=0,sz=src->segmentCount();i<sz;++i) {
	Point2D pts[2];
	size_t deg = src->segment(i,pts);
	pts[0].translate(dx,dy);
	assert(deg<=1);
	if (deg==0) {
	  addPath(p,b,tmpPts);
	  tmpPts.push_back(pts[0]);
	}
	else {
	  loxodrome(pts[0],tmpPts);
	}
      }
      // add  the last path 
      addPath(p,b,tmpPts);
      return b.path();
    }
 
    static Pointer<Contour2D> projectContour (const Reference<Contour2D>& src, double dx, double dy, const Projection& p,  vector<Point2D>&  tmpPts)
    {
      size_t N = 512;
      tmpPts.reserve(max(N,tmpPts.capacity()));
      tmpPts.clear();
	  
      if (src->maxDegree()>1) {
	throw runtime_error("Non-polygonal contours are not supported");
      }
      if (src->segmentCount()==0) {
	throw invalid_argument("Contour has no segments");
      }

      Point2D pts[2];
      size_t deg = src->segment(0,pts);
      assert(deg<=1);
      // copy the first point
      pts[0].translate(dx,dy);
      tmpPts.push_back(pts[0]);
      for (size_t i=0,sz=src->segmentCount();i<sz;++i) {
	loxodrome(*src,i,dx,dy,tmpPts);
      }
      size_t n = p.projectUnsafe(tmpPts.size(),&tmpPts[0]);
      try {
	return Polygon2D::create(n,&tmpPts[0]);
      }
      catch (const exception& exc) {
	logger().caught("Could not create polygon",exc);
      }
      return Pointer<Contour2D>();
    }

    static Pointer<Area2D> projectArea (const Reference<Area2D>& src, double dx, double dy, const Projection& p,  vector<Point2D>&  tmpPts)
    {
      Pointer<Contour2D> outside = projectContour(src->outsideContour(),dx,dy,p,tmpPts);
      if (!outside) {
	return Pointer<Area2D>();
      }
      if (src->contourCount()==0) {
	return outside;
      }

      vector< Reference<Contour2D> > holes;
      for (size_t i=0,sz=src->contourCount();i<sz;++i) {
	Pointer<Contour2D> c = src->insideContour(i);
	c = projectContour(c,dx,dy,p,tmpPts);
	if (c) {
	  holes.push_back(c);
	}
      }
      return Area2D::create(outside,holes);
    }

    static Pointer<Region2D> projectRegion2D (const Reference<Region2D>& src, double dx, double dy, const Projection& p, vector<Point2D>&  tmpPts)
    {
      vector< Reference<Area2D> > tmp;
      for (size_t i=0,sz=src->areaCount();i<sz;++i) {
	Pointer<Area2D> a = src->area(i);
	a = projectArea(a,dx,dy,p,tmpPts);
	if (a) {
	  tmp.push_back(a);
	}
      }
      if(tmp.empty()) {
	return Pointer<Region2D>();
      }
      else {
	return Region2D::create(tmp);
      }
    }

  }

  namespace projector {
    SimpleProjector2D::SimpleProjector2D(const Reference<Projection>& p) throws(::std::exception)
    : _dx(p->domain()->center().longitude()),
      _dy(p->domain()->center().latitude()),
      _projection(p),
      _clipper(new IdentityProjector2D(p->domain()->center(),p->domain()->region()))
    {}
    
    SimpleProjector2D::SimpleProjector2D(const SimpleProjector2D& orig) throws()
    : AbstractProjector2D(),
      _dx(orig._dx),
      _dy(orig._dy),
      _projection(orig._projection),
      _clipper(orig._clipper->copy())
    {}
    
    Pointer<Projector2D> SimpleProjector2D::create(const Reference<Projection>& p) throws()
    {
      try {
	return new SimpleProjector2D(p); 
      }
      catch (...) {
      }
      return Pointer<Projector2D>();
    }

    SimpleProjector2D::~SimpleProjector2D() throws()
    {}
    
    SimpleProjector2D* SimpleProjector2D::copy () const throws()
    { return new SimpleProjector2D(*this); }

    Reference<Projection> SimpleProjector2D::projection() const throws()
    { return _projection; }

    bool SimpleProjector2D::beginOutsidePath(bool windCW,const Extent* bnds)
    { return _clipper->beginOutsidePath(windCW,bnds); }
      
    bool SimpleProjector2D::beginInsidePath(bool windCW,const Extent* bnds)
    { return _clipper->beginInsidePath(windCW,bnds);}

    bool SimpleProjector2D::beginPath(const Extent* ext)
    { return _clipper->beginPath(ext);}
      
    bool SimpleProjector2D::moveTo(double lat, double lon, double alt,const Extent* bnds)
    { return _clipper->moveTo(lat,lon,alt,bnds);  }
      
    void SimpleProjector2D::moveAlongArcs (size_t n, const Arc* arcs, ArcType type, const Extent* e)
    { _clipper->moveAlongArcs(n,arcs,type,e); }

    size_t SimpleProjector2D::getRegions(vector< Reference<Region2D> >& result) throws()
    { 
      assert(_regions.size()==0);
      _clipper->getRegions(_regions);
      size_t n=0;
      for (size_t j=0;j<_regions.size();++j) {
	Pointer<Region2D> tmp = projectRegion2D(_regions[j],_dx,_dy,*_projection,_points);
	if (tmp) {
	  result.push_back(tmp);
	  ++n;
	}
      }
      _regions.clear();
      return n;
    }

    size_t SimpleProjector2D::getPaths(vector< Reference<Path2D> >& result) throws()
    { 
      assert(_paths.size()==0);
      _clipper->getPaths(_paths);
      
      size_t n=0;
      for (size_t j=0;j<_paths.size();++j) {
	Pointer<Path2D> tmp = projectPath(_paths[j],_dx,_dy,*_projection,_points);
	if (tmp) {
	  result.push_back(tmp);
	  ++n;
	}
      }
      _paths.clear();
      return n;
    }
    
  }
}

