#include <cartografo/map/Layer.h>
#include <cartografo/map/LayerObserver.h>
#include <cartografo/map/LayerEvent.h>
#include <cartografo/geo/Node.h>
#include <cartografo/geo/NodeEvent.h>
#include <cartografo/geo/NodeGraph.h>
#include <cartografo/geo/NodeObserver.h>

#include <timber/logging.h>
#include <timber/event/Dispatcher.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::cartografo::geo;

namespace {
   static Log logger()
   {
      return Log("cartografo.map.Layer");
   }
}

namespace cartografo {
   namespace map {

      namespace {
         typedef ::timber::event::Dispatcher< LayerObserver*> EventDispatcher;

         struct DispatchFunction
         {
               inline DispatchFunction(const LayerEvent& e)
                     : _event(e)
               {
               }

               inline void operator()(LayerObserver* obs)
               {
                  LogEntry(logger()).info() << "Notifying " << typeid(*obs).name() << doLog;
                  obs->layerChanged(_event);
               }

            private:
               const LayerEvent& _event;
         };
      }

      /**
       * The Nodes class is used to monitor the Layer nodes
       * and dispaches events to registers listeners (node observers)
       */
      class Layer::Nodes : public NodeGraph, public NodeObserver
      {

            /** Default constructor */
         public:
            Nodes()throws() : _layer(0) {}

            /** Destructor */
         public:
            ~Nodes()throws()
            {}

            void setLayer(Layer* layer)throws()
            {  _layer = layer;}

            void nodeChanged(const NodeEvent& e)throws()
            {
               notifyLayerObservers(LayerEvent::NODE_MODIFIED,e.source());
            }

            void setRootNode(Pointer< Node> h)throws()
            {
               if (rootNode()!=h) {
                  NodeGraph::setRootNode(h);
                  notifyLayerObservers(LayerEvent::LAYER_MODIFIED);
               }
            }

            NodeObserver* createObserver(Node&)throws()
            {  return this;}

            /**
             * @name Layer change management interface
             */
            void addObserver(LayerObserver* observer)throws()
            {  _dispatcher.add(observer);}

            void removeObserver(LayerObserver* observer)throws()
            {  _dispatcher.remove(observer);}

            /** Notify the layer's observers */
         public:
            void notifyLayerObservers(const LayerEvent::EventType e, const Pointer< Node>& node = Pointer< Node>())
            {
               logger().debugging("Notifying layer observers");
               // only notify the observer if there is a layer,
               // and that layer has been completely instantiated
               // which means its reference count would be non-zero
               if (_layer != 0 && _layer->refCount() > 0 && !_dispatcher.isEmpty()) {
                  const Reference< Layer> src(_layer);
                  const LayerEvent event(e, src, node);
                  DispatchFunction f(event);
                  logger().debugging("Notifying layer observer");
                  _dispatcher.dispatch(f);
               }
            }

            /** The layer */
         private:
            Layer* _layer;

            /** An event dispatcher */
         private:
            EventDispatcher _dispatcher;
      };

      Layer::Layer()
      throws()
      : _nodes(new Nodes()),_map(0)
      {  _nodes->setLayer(this);}

      Layer::~Layer()
      throws()
      {
         assert(_map==0 && "Layer is not detached");

         // first, clear the layer so that nobody gets notified
         _nodes->setLayer(0);
         // clear the nodes themselves
         _nodes->setRootNode(Pointer<Node>());
      }

      void Layer::setRootNode(const Pointer< Node>& node)
      {
         _nodes->setRootNode(node);
      }

      Pointer< Node> Layer::rootNode() const
      throws()
      {  return _nodes->rootNode();}

      void Layer::fireLayerChanged()
      throws()
      {
         _nodes->notifyLayerObservers(LayerEvent::LAYER_MODIFIED);
      }

      void Layer::addObserver(LayerObserver* observer)
      throws()
      {  _nodes->addObserver(observer);}

      void Layer::removeObserver(LayerObserver* observer)
      throws()
      {  _nodes->removeObserver(observer);}

      void Layer::notifyLayerAttached(Map& map)
      throws(::std::exception)
      {
         if (_map) {
            throw ::std::runtime_error("Layer is already attached to a map");
         }
         _map = &map;
         notifyAttached(map);
      }

      void Layer::notifyLayerDetached(Map& map)
      throws()
      {
         if (_map!= &map) {
            throw ::std::runtime_error("Layer is not attached to the given map");
         }
         _map =0;
         notifyDetached(map);
      }

      void Layer::notifyAttached(Map&)
      throws()
      {}

      void Layer::notifyDetached(Map&)
      throws()
      {}

      void Layer::notifyProjectionChanged(Map&)
      throws()
      {}

      void Layer::notifyViewPortChanged(Map&)
      throws ()
      {}

      void Layer::prepare(Map& map)
throws   ()
   {}

}
}
