#include <cartografo/map/raster/AsyncRasterProvider.h>

#include <terra/terra.h>
#include <timber/logging.h>
#include <timber/scheduler/ThreadPoolScheduler.h>
#include <timber/scheduler/Runnable.h>
#include <timber/scheduler/Task.h>

#include <cmath>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace cartografo {
   namespace map {
      namespace raster {
         namespace {
            static Log logger()
            {
               return Log("cartografo.map.raster.AsyncRasterProvider");
            }
            struct DownloadRasterTask : public ::timber::scheduler::Runnable
            {
                  DownloadRasterTask(const Reference< ::cartografo::map::raster::Raster>& source) :
                     _source(source)
                  {
                  }

                  ~DownloadRasterTask()throws() {}

                  void run()
                  {
                     _source->start();
                  }

               private:
                  const Reference< ::cartografo::map::raster::Raster> _source;
            };

            struct AsyncRaster : public Raster
            {

                  AsyncRaster(const Reference< Raster>& delegate,
                        const ::timber::SharedRef< ::timber::scheduler::Task>& task) :
                     _delegate(delegate), _task(task)
                  {
                  }
                  ~AsyncRaster()throws() {}

                  ::timber::Reference< RasterID> id() const throws() {return _delegate->id();}
                  RasterImage getRasterImage() const throws() {return _delegate->getRasterImage();}
                  State state() const throws() {return _delegate->state();}
                  State start()
                  {
                     // schedule the raster with a small delay, in case it gets cancelled
                     if (!_task->schedule(::std::make_shared<DownloadRasterTask>(_delegate), ::timber::scheduler::Task::Delay(250))) {
                        _delegate->cancel();
                     }
                     return _delegate->state();
                  }

                  State cancel()
                  {
                     logger().info("Canceling image");
                     if (_task) {
                        _task->cancel();
                        _task = nullptr;
                     }
                     return _delegate->cancel();
                  }

               private:
                  const Reference< Raster> _delegate;
                  ::std::shared_ptr< ::timber::scheduler::Task> _task;
            };
         }

         AsyncRasterProvider::AsyncRasterProvider(const ::timber::Reference< RasterProvider>& xdelegate)
         throws()
         :_delegate(xdelegate),
         _scheduler(::timber::scheduler::ThreadPoolScheduler::create(4))
         {
         }

         AsyncRasterProvider::~AsyncRasterProvider()
         throws()
         {
	   logger().debugging("AsyncRasterProvider is being destroyed");
            _scheduler->shutdown();
         }

         Reference< AsyncRasterProvider> AsyncRasterProvider::create(
               const ::timber::Reference< RasterProvider>& xdelegate)
         throws()
         {
            return new AsyncRasterProvider(xdelegate);
         }

         bool AsyncRasterProvider::isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const
         throws()
         {
            return _delegate->isSupportedCRS(crs);
         }

         ::std::vector< ::timber::Reference< RasterID> > AsyncRasterProvider::findRasters(
               const ::terra::Location& corner, ::std::int32_t w, ::std::int32_t h, double scale) const
         throws( ::terra::UnsupportedCRS, ::std::invalid_argument)
         {  return _delegate->findRasters(corner,w,h,scale);}

         ::timber::Pointer< Raster> AsyncRasterProvider::getRaster(const ::timber::Reference< RasterID>& id)
         {
            ::timber::Pointer< Raster> r = _delegate->getRaster(id);
            // return the original raster unless it has been newly created
            if (!r || r->state() != Raster::CREATED || r.tryDynamicCast< AsyncRaster> ()) {
               return r;
            }
            // create an async raster
            return new AsyncRaster(r, _scheduler->newTask());
         }
      }
   }

}
