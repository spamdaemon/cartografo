#include  <cartografo/map/raster/BasicRasterID.h>

namespace cartografo {
   namespace map {
      namespace raster {

         BasicRasterID::BasicRasterID(const RasterImage::Bounds& b, ::std::uint32_t xwidth,
               ::std::uint32_t xheight, double xscale)
         throws(::std::invalid_argument)
         : _bounds(b),_width(xwidth),_height(xheight),_scale(xscale)
         {
            if (xwidth==0 || xheight==0 || xscale<=0) {
               throw ::std::invalid_argument("Invalid size and/or scale");
            }
         }

         BasicRasterID::~BasicRasterID()
         throws()
         {}

         bool BasicRasterID::equals(const RasterID& id) const
         throws()
         {
            const BasicRasterID* other = dynamic_cast<const BasicRasterID*>(&id);
            return other && _bounds==other->_bounds && _width==other->_width && _height == other->_height && _scale==other->_scale;
         }

         const RasterImage::Bounds& BasicRasterID::bounds() const
         throws() {return _bounds;}

         ::std::uint32_t BasicRasterID::width() const
         throws() {return _width;}

         ::std::uint32_t BasicRasterID::height() const
         throws() {return _height;}

         double BasicRasterID::scale() const
throws      () {return _scale;}
   }
}
}
