#include <cartografo/map/raster/RasterLayer.h>
#include <cartografo/map/raster/RasterProvider.h>
#include <cartografo/map/Map.h>
#include <cartografo/geo/Raster.h>
#include <cartografo/Projection.h>
#include <indigo/view/Bounds.h>
#include <timber/logging.h>
#include <terra/terra.h>
#include <nexus/Desktop.h>
#include <nexus/Runnable.h>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace {
   static Log logger()
   {
      return Log("cartografo.map.raster.RasterLayer");
   }

   struct SetRaster : public ::nexus::Runnable, public ::std::enable_shared_from_this<SetRaster>
   {
         SetRaster(const Reference< ::cartografo::map::raster::Raster>& source,
               Reference< ::cartografo::geo::Raster> raster) :
            _source(source), _raster(raster)
         {
         }

         ~SetRaster()throws() {}

      public:
         void run()throws()
         {
            switch(_source->state()) {
               case ::cartografo::map::raster::Raster::CREATED:
               case ::cartografo::map::raster::Raster::IMAGE_IN_PROGRESS:
               ::nexus::Desktop::enqueue(shared_from_this(),250U);
               break;
               case ::cartografo::map::raster::Raster::IMAGE_AVAILABLE:
               _raster->setRaster(_source->getRasterImage());
               break;
               default:
               // failed to get the image
               break;
            };
         }

      private:
         const Reference< ::cartografo::map::raster::Raster> _source;
         Reference< ::cartografo::geo::Raster> _raster;
   };
}

namespace cartografo {
   namespace map {
      namespace raster {

         RasterLayer::RasterLayer(const ::timber::Reference< RasterProvider>& provider)
         throws()
         :
         _rasters(new ::cartografo::geo::Group()),
         _provider(provider)
         {
            Layer::setRootNode(_rasters);
         }

         RasterLayer::~RasterLayer()
         throws()
         {
         }

         Reference< RasterLayer> RasterLayer::create(const ::timber::Reference< RasterProvider>& provider)
         throws()
         {
            return new RasterLayer(provider);
         }

         void RasterLayer::notifyProjectionChanged(Map& map)
         throws()
         {
            LogEntry(logger()).info() << "ProjectionChanged " << map.projection()->epsgCode() << doLog;
            notifyViewPortChanged(map);
        }

         void RasterLayer::RasterLayer::notifyViewPortChanged(Map& map)
throws      ()
      {
         typedef ::indigo::view::ViewPort ViewPort;

         const ViewPort& vp = map.viewPort();
         const ViewPort::Bounds& b = vp.bounds();

         const double bl[] = {b.x(),b.y()};
         const ::terra::Location loc(bl,map.projection());
         ::std::uint32_t w=static_cast< ::std::uint32_t>(vp.viewWindow().width());
         ::std::uint32_t h=static_cast< ::std::uint32_t>(vp.viewWindow().height());

         vector<Reference< ::cartografo::map::raster::RasterID> > ids;
         try {
            LogEntry(logger()).info() << "ViewPortChanged " << map.projection()->epsgCode() << doLog;
            ids = _provider->findRasters(loc,w,h,vp.verticalScale());
         }
         catch (const ::std::exception& e) {
            logger().caught("Could not determine rasters",e);
         }
         // cancel the old rasters and copy those over we've already found
         ::std::vector< ::timber::Reference< ::cartografo::map::raster::Raster> > v;
         ::std::vector< ::timber::Reference< ::cartografo::geo::Node> > nodes;
         for (size_t i=0,sz=_rasterSource.size();i!=sz;++i) {
            const ::timber::Reference< ::cartografo::map::raster::Raster>& r = _rasterSource[i];
            bool found=false;
            for (auto id=ids.begin();id!=ids.end();++id) {
               if (r->id()->equals(**id)) {
                  found = true;
                  break;
               }
            }

            if (found) {
               v.push_back(r);
               nodes.push_back(_rasters->node(i));
            }
            else {
               // cancel the raster
               r->cancel();
            }
         }
         ::std::swap(v,_rasterSource);

         // do the reverse as well
         for (auto id=ids.begin();id!=ids.end();++id) {
            bool found=false;
            for (auto r = _rasterSource.begin();r!=_rasterSource.end();++r) {
               if ((*r)->id()->equals(**id)) {
                  found=true;
                  break;
               }
            }
            if (!found) {
               Pointer< ::cartografo::map::raster::Raster> r = _provider->getRaster(*id);
               if (r) {
                  _rasterSource.push_back(r);
                  Reference< ::cartografo::geo::Raster> R(new ::cartografo::geo::Raster());
                  nodes.push_back(R);

                  ::cartografo::map::raster::Raster::State s = r->start();
                  if (s == ::cartografo::map::raster::Raster::IMAGE_AVAILABLE) {
                     SetRaster(r,R).run();
                  }
                  else {
                     ::nexus::Desktop::enqueue(::std::make_shared<SetRaster>(r,R));
                  }
               }
            }
         }
         _rasters->clear();
         for ( auto node=nodes.begin();node!=nodes.end();++node) {
            _rasters->add(*node);
         }

      }

   }
}
}
