#include <cartografo/map/raster/WmsRasterProvider.h>
#include <cartografo/map/raster/AbstractImageRaster.h>
#include <terra/Location.h>
#include <terra/ProjectedCRS.h>
#include <timber/w3c/URI.h>

#include <timber/logging.h>
#include <terra/terra.h>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::cartografo::map::wms;

namespace cartografo {
   namespace map {
      namespace raster {
         namespace {
            static Log logger()
            {
               return Log("cartografo.map.raster.WmsRasterProvider");
            }

         }

         WmsRasterProvider::WmsRasterProvider(const ::timber::SharedRef< ::timber::media::ImageFactory>& f)
         throws() : _factory(f)
         {
         }

         WmsRasterProvider::~WmsRasterProvider()
         throws()
         {
         }

         Reference< WmsRasterProvider> WmsRasterProvider::create(
               const ::timber::SharedRef< ::timber::media::ImageFactory>& f)
         throws()
         {
            return new WmsRasterProvider(f);
         }

         void WmsRasterProvider::setWmsServerUrl(const string& url)
throws      (invalid_argument)
      {
         _request.setUrl(url);
      }

      void WmsRasterProvider::setWmsLayer(const string& name)
throws   (invalid_argument)
   {
      _request.addLayer(name);
   }
   bool WmsRasterProvider::isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const
   throws()
   {
      if (crs.coordinateCount()!=2) {
         return false;
      }
      if (dynamic_cast<const ::terra::ProjectedCRS*>(&crs)==0) {
         return false;
      }
      return true;
   }

   ::std::vector< ::timber::Reference< RasterID> > WmsRasterProvider::findRasters(const ::terra::Location& corner,
         ::std::int32_t w, ::std::int32_t h, double scale) const
   throws( ::terra::UnsupportedCRS, ::std::invalid_argument)
   {
      if (w==0 || h==0) {
         throw ::std::invalid_argument("Invalid raster bounds");
      }
      if (scale <=0) {
         throw ::std::invalid_argument("Invalid scale");
      }
      if (!isSupportedCRS(*corner.crs())) {
         throw ::terra::UnsupportedCRS(corner.crs());
      }

      // determine the top-left corner
      const double topLeft[] = {::std::min(corner[0],corner[0]+w/scale), ::std::max(corner[1],corner[1]+h/scale)};
      const double botRight[] = {::std::max(corner[0],corner[0]+w/scale), ::std::min(corner[1],corner[1]+h/scale)};
      const double topRight[] = {botRight[0], topLeft[1]};
      const double botLeft[] = {topLeft[0],botRight[1]};

      const ::terra::Location tl(topLeft,corner.crs());
      const ::terra::Location tr(topRight,corner.crs());
      const ::terra::Location br(botRight,corner.crs());
      const ::terra::Location bl(botLeft,corner.crs());
      RasterImage::Bounds b(tl,tr,br,bl);

      ::std::vector< ::timber::Reference< RasterID> > res;
      res.push_back(RasterID::create(b,::std::abs(w),::std::abs(h),scale));
      return res;
   }

   ::timber::Pointer< Raster> WmsRasterProvider::getRaster(const ::timber::Reference< RasterID>& id)
   {
      assert(id->scale() > 0);
      assert(id->width() > 0);
      assert(id->height() > 0);

      Request::BBox box;

      const RasterImage::Bounds& b = id->bounds();
      box.minX = b.topLeft()[0];
      box.maxX = b.topRight()[0];
      box.maxY = b.topLeft()[1];
      box.minY = b.bottomRight()[1];

      _request.setCRS(b.topLeft().crs()->epsgCode().toString());
      _request.setBBox(box);
      _request.setRequest("GetMap");
      _request.setVersion("1.1.1");
      _request.setWidth(id->width());
      _request.setHeight(id->height());
      _request.setTransparent(true);
      _request.setFormat(ContentType::parse("image/png"));

      auto uri = _request.getRequestURI();
      if (!uri) {
         logger().warn("Could not create request URL");
         return ::timber::Pointer< Raster>();
      }
      Level level(Level::DEBUGGING);
      if (logger().isLoggable(level)) {
         LogEntry(logger(), level).stream() << "Request URI " << ::std::endl << *uri << doLog;
      }
      return AbstractImageRaster::create(id, uri, _factory);

   }
}

}
}
