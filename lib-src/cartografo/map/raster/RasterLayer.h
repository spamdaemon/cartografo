#ifndef CARTOGRAFO_MAP_RASTER_RASTERLAYER_H
#define CARTOGRAFO_MAP_RASTER_RASTERLAYER_H

#ifndef _CARTOGRAFO_MAP_LAYER_H
#include <cartografo/map/Layer.h>
#endif

#ifndef _CARTOGRAFO_GEO_RASTER_H
#include <cartografo/geo/Raster.h>
#endif

#ifndef _CARTOGRAFO_GEO_GROUP_H
#include <cartografo/geo/Group.h>
#endif

namespace cartografo {
   namespace map {
      namespace raster {
         class RasterProvider;
         class Raster;

         /**
          * This layer is able to retrieve imagery from a WMS server and render it
          * with the current projection.
          */
         class RasterLayer : public Layer
         {
               /**
                * Default constructor.
                * @param provider the raster provider to use
                */
            private:
               RasterLayer(const ::timber::Reference< RasterProvider>& provider)throws();

               /** Destructor */
            public:
               ~RasterLayer()throws();

               /**
                * Create a new WMS layer.
                * @param provider the raster provider to use
                * @return a WMS layer
                */
            public:
               static ::timber::Reference< RasterLayer> create(const ::timber::Reference< RasterProvider>& provider)throws();

            protected:
               void notifyProjectionChanged(Map& map)throws();

               /**
                * Notify this layer that the viewport has changed.
                * @param map the map whose viewport has changed
                */
            protected:
               void notifyViewPortChanged(Map& map)throws();

               /** The rasters retrieved from the server */
            private:
               ::std::vector< ::timber::Reference< ::cartografo::map::raster::Raster> > _rasterSource;

               /** The image retrieved from the server */
            private:
               ::timber::Reference< ::cartografo::geo::Group> _rasters;

               /** An asynchronous raster provider */
            private:
               ::timber::Reference< ::cartografo::map::raster::RasterProvider> _provider;

         };

      }

   }

}

#endif
