#ifndef _CARTOGRAFO_MAP_RASTER_ABSTRACTRASTER_H
#define _CARTOGRAFO_MAP_RASTER_ABSTRACTRASTER_H

#ifndef _CARTOGRAFO_MAP_RASTER_RASTER_H
#include <cartografo/map/raster/Raster.h>
#endif

#ifndef _CANOPY_MT_MUTEX_H
#include <canopy/mt/Mutex.h>
#endif

namespace cartografo {
   namespace map {
      namespace raster {

         /**
          * This interfaces defines the boundary for a raster image and provides a means to retrieve the image itself.
          */
         class AbstractRaster : public Raster
         {
               /** The image pointer */
            public:
               typedef ::std::shared_ptr< ::timber::media::ImageBuffer> ImagePtr;

               /**
                * Create a raster with the specified id. The state is initialized to CREATED.
                * @param id the raster id.
                *  */
            protected:
               AbstractRaster(const ::timber::Reference< RasterID>& id)throws();

               /**
                * Create a raster with an RASTER_ABORTED state.
                * @return a raster with the state RASTER_ABORTED
                */
            public:
               static ::timber::Reference< Raster> createRasterAborted(const ::timber::Reference< RasterID>& id)throws();

               /** Destructor */
            public:
               ~AbstractRaster()throws();

               /**
                * @name RasterProperties
                * @{
                */

               /**
                * Get the id associated with this raster.
                * @return this raster's id
                */
            public:
               ::timber::Reference< RasterID> id() const throws();

               /**
                * Get the image for this raster.
                * @return the image or null
                */
            public:
               RasterImage getRasterImage() const throws();

               /**
                * Set the image. This method sets the state to AVAILABLE. If another image was previously set, then this
                * method simply returns false.
                * @param image
                * @return a raster state
                */
            protected:
               State setImage(const ::timber::SharedRef< ::timber::media::ImageBuffer>& image)throws();

               /*@}*/

               /**
                * @name RasterState
                * @{
                */

               /**
                * Get the current image state.
                * @return the status of this image.
                */
            public:
               State state() const throws();

               /**
                * Set a new state other than CREATED or IMAGE_AVAILABLE. Use to setImage
                * to set the state to IMAGE_AVAILABLE.
                * @param s a new state
                * @return true if the state was set, false otherwise
                * @pre s!=IMAGE_AVAILABLE
                */
            protected:
               State setState(State s)throws();

               /**
                * Cancel this raster. This method simply calls <tt>setState(RASTER_CANCELED)</tt>.
                */
            public:
               State cancel();

               /*@}*/

               /** The raster id */
            private:
               const ::timber::Reference< RasterID> _id;

               /** The raster */
            private:
               RasterImage _raster;

               /** The image */
            private:
               ImagePtr _image;

               /** The raster state */
            private:
               volatile State _state;

               /** A mutex */
            private:
               ::canopy::mt::Mutex _mutex;
         };
      }
   }
}

#endif
