#include <cartografo/map/raster/OsmRasterProvider.h>
#include <cartografo/map/raster/AbstractImageRaster.h>
#include <cartografo/map/raster/BasicRasterID.h>
#include <terra/terra.h>
#include <terra/ProjectedCRS.h>
#include <timber/logging.h>
#include <timber/w3c/URI.h>

#include <cmath>
#include <sstream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
   namespace map {
      namespace raster {
         namespace {
            static Log logger()
            {
               return Log("cartografo.map.raster.OsmRasterProvider");
            }

            struct TileID : public BasicRasterID
            {
                  ~TileID()throws() {}
                  TileID(const RasterImage::Bounds& b, ::std::uint32_t w, ::std::uint32_t h, double s, uint32_t x,
                        uint32_t y, uint32_t zoom)
                        : BasicRasterID(b, w, h, s), _x(x), _y(y), _zoom(zoom)
                  {
                  }

                  bool equals(const RasterID& id) const throws()
                  {
                     const TileID* other= dynamic_cast<const TileID*>(&id);
                     return other!=0 && other->_x==_x && other->_y == _y && other->_zoom == _zoom;
                  }

                  const uint32_t _x;
                  const uint32_t _y;
                  const uint32_t _zoom;
            };
         }

         const ::std::uint32_t OsmRasterProvider::TILE_SIZE;
         const ::std::uint32_t OsmRasterProvider::DEFAULT_MAX_ZOOM;

         OsmRasterProvider::OsmRasterProvider(const ::timber::SharedRef< ::timber::media::ImageFactory>& f)
         throws()
         : _tileserver("tile.openstreetmap.org"),
         _crs(ProjectedCRS::createEPSG3857()),_tileSize(TILE_SIZE),_maxZoom(DEFAULT_MAX_ZOOM),
         _topLeft(Location(85,-180,CoordinateReferenceSystem::getEPSG4326()).convert(_crs)),
         _bottomRight(Location(-85,180,CoordinateReferenceSystem::getEPSG4326()).convert(_crs)),
         _factory(f)
         {
            double sz = _topLeft[0];
            _bottomRight = Location(-sz,sz,_bottomRight.crs());
            _topLeft = Location(sz,-sz,_topLeft.crs());
            LogEntry(logger()).info() << "OsmRasterProvider: " << _topLeft << ", " << _bottomRight << doLog;
         }

         OsmRasterProvider::~OsmRasterProvider()
         throws()
         {
         }

         Reference< OsmRasterProvider> OsmRasterProvider::create(
               const ::timber::SharedRef< ::timber::media::ImageFactory>& f)
         throws()
         {
            return new OsmRasterProvider(f);
         }

         bool OsmRasterProvider::isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const
         throws()
         {
            return crs.equals(*_crs);
         }
         double OsmRasterProvider::getScale(::std::int32_t zoom) const
         throws()
         {
            const double worldSize = _bottomRight[0]-_topLeft[0];
            return (_tileSize / worldSize) * (1<<zoom);
         }

         ::std::int32_t OsmRasterProvider::getZoom(double scale) const
         throws()
         {
            const double worldSize = _bottomRight[0]-_topLeft[0];

            // the following formula is used:
            // scale = 2**zoom * 256/worldSize
            // so that 2**zoom = (scale*worldSize)/256
            double zoomPow = (worldSize* scale)/_tileSize;

            // get the log base 2 via log10 or just log
            // given 2**log2(x) = x
            // ==> log10(2**log2(x)) = log10(x)
            // ==> log2(x)*log10(2) = log10(x)
            // ==> log2(x) = log10(x)/log10(2)

            // compute the raw zoom level (there's bound to be round-off issues)
            double zoomRaw = ::std::log(zoomPow)/::std::log(2);

            // we need the zoom to be a in integer
            double zoom = ::std::round(zoomRaw);

            LogEntry(logger()).info() << "ZOOM : " << zoomPow << ", " << zoomRaw << " , " << zoom << doLog;

            ::std::int32_t res=-1;// default to invalid zoom
            double eps = 1E-8;

            // make sure the rounded value is close to the real zoom level
            if (::std::abs(zoom-zoomRaw)<eps ) {
               res= static_cast< ::std::int32_t>(zoom);
               if (res < 0) {
                  res = -1;
               }
            }
            // an invalid scale
            return res;
         }

         double OsmRasterProvider::getTileSize(::std::int32_t zoom) const
         throws()
         {
            // width and height are expected to the same, so only compute width
            const double worldSize = _bottomRight[0]-_topLeft[0];

            // the scale
            double scale = (_tileSize* (1<<zoom)) / worldSize;

            // the size of a tile
            double tileWorldSize = _tileSize/scale;

            return tileWorldSize;
         }

         OsmRasterProvider::TileIndex OsmRasterProvider::getTileIndex(double x, double y, ::std::uint32_t zoom) const
         throws()
         {
            TileIndex res(-1,-1);

            if (zoom <= _maxZoom) {

               // the size of a tile
               double sz = getTileSize(zoom);

               // need to invert y, because we need indices with respect to top-left
               double dx = (x - _topLeft[0])/sz;
               double dy = ( _topLeft[1]-y)/sz;

               res.first = static_cast< int32_t>(dx);
               res.second = static_cast< int32_t>(dy);
               LogEntry(logger()).info() << "Getting tile index for " << x << "," << y << " @ " << zoom << " = " << res.first << "," << res.second << " " << sz << doLog;
            }
            return res;
         }

         ::std::vector< ::timber::Reference< RasterID> > OsmRasterProvider::findRasters(const ::terra::Location& corner,
               ::std::int32_t w, ::std::int32_t h, double scale) const
         throws( ::terra::UnsupportedCRS, ::std::invalid_argument)
         {
            if (w==0 || h==0) {
               throw ::std::invalid_argument("Invalid raster bounds");
            }
            if (scale <=0) {
               throw ::std::invalid_argument("Invalid scale");
            }
            if (!isSupportedCRS(*corner.crs())) {
               throw ::terra::UnsupportedCRS(corner.crs());
            }
            ::std::vector< ::timber::Reference< RasterID> > res;

            // determine the zoom level
            int32_t zoom = getZoom(scale);
            LogEntry(logger()).info() << "Converted scale " << scale << " to zoom level " << zoom << doLog;
            if (zoom <0) {
               // FIXME: should we throw a UnsupportedScale exception??
               LogEntry(logger()).info() << "Unsupported scale " << scale << " cannot be converted to a zoom level" << doLog;
               return res;
            }

            if (static_cast<uint32_t>(zoom) > _maxZoom) {
               LogEntry(logger()).info() << "Zoom level " << zoom << " exceeds the maximum zoom; scale is " << scale << doLog;
               return res;
            }

            const int32_t nTiles = 1<<zoom;

            // compute the tile index for the top-left corner
            TileIndex tl = getTileIndex(::std::min(corner[0],corner[0]+w/scale),::std::max(corner[1],corner[1]+h/scale),zoom);
            TileIndex br = getTileIndex(::std::max(corner[0],corner[0]+w/scale),::std::min(corner[1],corner[1]+h/scale),zoom);

            // check if the viewport is entirely outside
            if (br.first < 0 || br.second < 0) {
               return res;
            }
            if (tl.first >= nTiles || tl.second >= nTiles) {
               return res;
            }

            tl.first = ::std::max(tl.first,0);
            tl.second = ::std::max(tl.second,0);
            br.first = ::std::min(br.first,nTiles-1);
            br.second = ::std::min(br.second,nTiles-1);

            LogEntry(logger()).info() << "Retrieving tiles for (" << tl.first << "," << tl.second << ") to (" << br.first << "," << br.second << ")" << doLog;

            double tileSize = getTileSize(zoom);
            for (int32_t x = tl.first;x<=br.first;++x) {
               for (int32_t y = tl.second;y<=br.second;++y) {
                  const Location c_tl(_topLeft[0]+x*tileSize,_topLeft[1]-y*tileSize,_crs);
                  const Location c_br(_topLeft[0]+(x+1)*tileSize,_topLeft[1]-(y+1)*tileSize,_crs);
                  const Location c_tr(c_br[0],c_tl[1],_crs);
                  const Location c_bl(c_tl[0],c_br[1],_crs);

                  const RasterImage::Bounds b(c_tl,c_tr,c_br,c_bl);
                  res.push_back(new TileID(b,_tileSize,_tileSize,scale,x,y,zoom));
               }
            }

            // determine the top-left corner
            return res;
         }

         ::timber::Pointer< Raster> OsmRasterProvider::getRaster(const ::timber::Reference< RasterID>& id)
         {
            assert(id->scale() > 0);
            assert(id->width() > 0);
            assert(id->height() > 0);

            // the three value we need to retrieve the raster
            ::std::uint32_t zoom, x, y;

            Pointer< TileID> tileID = id.tryDynamicCast< TileID>();
            if (tileID) {
               zoom = tileID->_zoom;
               x = tileID->_x;
               y = tileID->_y;
            }
            else {
               // a little bit more complicated
               zoom = getZoom(id->scale());
               if (zoom > _maxZoom) {
                  return ::timber::Pointer< Raster>();
               }

               Location tl = id->bounds().topLeft().convert(_crs);
               TileIndex index = getTileIndex(tl[0], tl[1], zoom);
               if (index.first < 0 || index.second < 0) {
                  return ::timber::Pointer< Raster>();
               }

               const uint32_t nTiles = 1 << zoom;
               x = index.first;
               y = index.second;
               if (x >= nTiles || y >= nTiles) {
                  return ::timber::Pointer< Raster>();
               }
            }
            // create the URL
            ::std::ostringstream out;
            out << "http://" << _tileserver << '/' << zoom << '/' << x << '/' << y << ".png";

            try {
               auto uri = ::timber::w3c::URI::create(out.str());
               return AbstractImageRaster::create(id, uri, _factory);
            }
            catch (const ::std::exception& e) {
               logger().caught("Could not create URI for OSM", e);
            }
            return ::timber::Pointer< Raster>();

         }

         ::std::string OsmRasterProvider::getRasterIDPath(const RasterID& id)
throws      ()
      {
         ::std::ostringstream out;
         const TileID* tile = dynamic_cast<const TileID*>(&id);
         if (tile) {
            const char* sep="/";
            out << tile->_zoom <<
            sep << tile->_x <<
            sep << tile->_y <<
            sep << TILE_SIZE << 'x' << TILE_SIZE << ".png";
         }

         return out.str();
      }

   }}
}
