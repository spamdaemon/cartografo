#ifndef _CARTOGRAFO_MAP_RASTER_BASICRASTERID_H
#define _CARTOGRAFO_MAP_RASTER_BASICRASTERID_H

#ifndef _CARTOGRAFO_MAP_RASTER_RASTERID_H
#include <cartografo/map/raster/RasterID.h>
#endif

#ifndef _TERRA_LOCATION_H
#include <terra/Location.h>
#endif

#include <stdexcept>

namespace cartografo {
   namespace map {
      namespace raster {
         /**
          * This interface defines an ID for a raster.
          */
         class BasicRasterID : public RasterID
         {
               /**
                * Create a basic raster id.
                * @param topLeft the top-left corner of the raster
                * @param width the width of the raster in pixels
                * @param height the height of the raster in pixels
                * @param scale the scale at which the raster is rendered
                * @throws ::std::invalid_argument if width==0 || height ==0
                * @throws ::std::invalid_argument if scale <= 0
                */
            public:
               BasicRasterID(const RasterImage::Bounds& b, ::std::uint32_t width, ::std::uint32_t height,
                     double scale)
               throws( ::std::invalid_argument);

               /** Destructor */
            public:
               ~BasicRasterID()throws();

               /**
                * Compare two raster ids for equality.
                * @param id another raster id
                * @return true if this raster id is equal to the specified raster id
                */
            public:
               bool equals(const RasterID& id) const throws();

               /**
                * Get the location of the top-left corner of the associated raster.
                * @return a location
                */
            public:
               const RasterImage::Bounds& bounds() const throws();
               /**
                * Get the width of the retrieved image.
                * @return the width of the image (never 0)
                */
            public:
               ::std::uint32_t width() const throws();

               /**
                * Get the height of the retrieved image.
                * @return the height of the image (never 0)
                */
            public:
               ::std::uint32_t height() const throws();

               /**
                * Get the scale of the retrieved raster.
                * @return the scale
                */
            public:
               double scale() const throws();

               /** The raster location */
            private:
               const RasterImage::Bounds _bounds;

               /** The width and height */
            private:
               const ::std::uint32_t _width, _height;

               /** The raster scale */
            private:
               const double _scale;
         };
      }
   }
}

#endif
