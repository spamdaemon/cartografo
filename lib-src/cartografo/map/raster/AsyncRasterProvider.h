#ifndef CARTOGRAFO_MAP_RASTER_ASYNCRASTERPROVIDER_H
#define CARTOGRAFO_MAP_RASTER_ASYNCRASTERPROVIDER_H

#ifndef _CARTOGRAFO_MAP_RASTER_RASTERPROVIDER_H
#include <cartografo/map/raster/RasterProvider.h>
#endif

namespace timber {
   namespace scheduler {
      class TaskScheduler;
   }
}

namespace cartografo {
   namespace map {
      namespace raster {

         /**
          * This RasterProvider retrieves raster images asynchronously. It can be used to improve map rendering
          * performance, because loading a raster will no longer block the rendering thread.
          */
         class AsyncRasterProvider : public RasterProvider
         {
               /**
                * Create a new raster provider that retrieves images from the specified delegate.
                * @param delegate the delegate raster provider
                */
            private:
               AsyncRasterProvider(const ::timber::Reference< RasterProvider>& delegate)throws();

               /** Destructor */
            public:
               ~AsyncRasterProvider()throws();

               /**
                * Create a new raster provider that retrieves images from the specified delegate.
                * @param delegate the delegate raster provider
                * @return a raster provider.
                */
            public:
               static ::timber::Reference< AsyncRasterProvider> create(
                     const ::timber::Reference< RasterProvider>& delegate)throws();

            public:
               bool isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const throws();
               ::std::vector< ::timber::Reference< RasterID> > findRasters(const ::terra::Location& corner,
                     ::std::int32_t width, ::std::int32_t height, double scale) const throws( ::terra::UnsupportedCRS, ::std::invalid_argument);
               ::timber::Pointer< Raster> getRaster(const ::timber::Reference< RasterID>& id);

               /** The server request */
            private:
               const ::timber::Reference< RasterProvider> _delegate;

               /** A job queue */
            private:
               ::timber::SharedRef< ::timber::scheduler::TaskScheduler> _scheduler;
         };

      }

   }

}

#endif
