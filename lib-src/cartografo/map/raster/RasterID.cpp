#include  <cartografo/map/raster/RasterID.h>
#include  <cartografo/map/raster/BasicRasterID.h>

namespace cartografo {
   namespace map {
      namespace raster {

         RasterID::RasterID()
         throws()
         {

         }

         RasterID::~RasterID()
         throws ()
         {
         }

         ::timber::Reference< RasterID> RasterID::create(const RasterImage::Bounds& b, ::std::uint32_t xwidth,
               ::std::uint32_t xheight, double xscale)
throws      (::std::invalid_argument)
      {  return new BasicRasterID(b,xwidth,xheight,xscale);}

   }
}
}
