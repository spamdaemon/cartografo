#ifndef _CARTOGRAFO_MAP_RASTER_RASTERID_H
#define _CARTOGRAFO_MAP_RASTER_RASTERID_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _CARTOGRAFO_RASTERIMAGE_H
#include <cartografo/RasterImage.h>
#endif

namespace cartografo {
   namespace map {
      namespace raster {
         /**
          * This interface defines an ID for a raster.
          */
         class RasterID : public ::timber::Counted
         {
               RasterID(const RasterID&);
               RasterID&operator=(const RasterID&);

               /** Default constructor */
            protected:
               RasterID()throws();

               /** Destructor */
            public:
               ~RasterID()throws();

               /**
                * Create a raster id.
                * @param b the bounds of the raster
                * @param width the width of the raster in pixels
                * @param height the height of the raster in pixels
                * @param scale the scale at which the raster is rendered
                * @throws ::std::invalid_argument if width==0 || height ==0
                * @throws ::std::invalid_argument if scale <= 0
                */
            public:
               static ::timber::Reference< RasterID> create(const RasterImage::Bounds& b, ::std::uint32_t width,
                     ::std::uint32_t height, double scale)
               throws( ::std::invalid_argument);

               /**
                * Compare two raster ids for equality.
                * @param id another raster id
                * @return true if this raster id is equal to the specified raster id
                */
            public:
               virtual bool equals(const RasterID& id) const throws() = 0;

               /**
                * Get the location of the top-left corner of the associated raster.
                * @return a location
                */
            public:
               virtual const RasterImage::Bounds& bounds() const throws() = 0;

               /**
                * Get the width of the retrieved image in pixels
                * @return the width of the image (never 0)
                */
            public:
               virtual ::std::uint32_t width() const throws() = 0;

               /**
                * Get the height of the retrieved image in pixels.
                * @return the height of the image (never 0)
                */
            public:
               virtual ::std::uint32_t height() const throws() = 0;

               /**
                * Get the scale of the retrieved raster.
                * @return the scale
                */
            public:
               virtual double scale() const throws() = 0;
         };
      }
   }
}

#endif
