#ifndef _CARTOGRAFO_MAP_RASTER_RASTERCACHE_H
#define _CARTOGRAFO_MAP_RASTER_RASTERCACHE_H

#ifndef _TIMBER_MEDIA_IMAGEFACTORY_H
#include <timber/media/ImageFactory.h>
#endif

#ifndef _CARTOGRAFO_MAP_RASTER_RASTERPROVIDER_H
#include <cartografo/map/raster/RasterProvider.h>
#endif

namespace cartografo {
  namespace map {
    namespace raster {

      /**
       * This class extends the RasterProvider interface by defining
       * a cache for the rasters. A RasterCache always uses a delegate raster
       * provider to obtain the rasters.
       */
      class RasterCache  : public RasterProvider
      {
	/** A cached raster */
      private:
	class CachedRaster;
	   
	/** 
	 * Create a cache for the specified provider.
	 * @param provider the source provider
	 */
      protected:
	RasterCache(const ::timber::Reference<RasterProvider>& provider)throws();
	       
	/** Destructor */
      public:
	~RasterCache()throws();


	/**
	 * Create a simple in-memory cache for rasters. The number of rasters
	 * cached must be provided. If a raster needs to be expunged from the
	 * cache, then least recently used raster is discarded.
	 * @param s a raster cache that provides the actual rasters for this cache
	 * @param n the maximum number of rasters to hold on to
	 * @return a memory cache
	 * @throws invalid_argument if n==0		* @
	 */
      public:
	static ::timber::Reference<RasterCache> createMemoryCache (const ::timber::Reference<RasterProvider>& s, size_t n) throws (::std::invalid_argument);

	/**
	 * Create a simple disk-cache for rasters. The rasters are stored
	 * below the specified directory. If a maximum age is specified, then
	 * rasters that whose creation time is too old, are removed when requested.
	 * @param s a raster cache that provides the real rasters
	 * @param f an image factory used to create images
	 * @param dir the directory under which the cached rasters are stored
	 * @param maxAge the maximum age in seconds for raster or 0 if not specified
	 * @param conv a conversion function to convert a raster id to a string
	 * @throws invalid_argument if dir is not valid directory
	 * @return a raster cache
	 */
      public:
	static ::timber::Reference<RasterCache> createDiskCache (const ::timber::Reference<RasterProvider>& s, const ::timber::SharedRef< ::timber::media::ImageFactory>& f, const ::std::string& dir, size_t maxAge, ::std::string (*conv)(const RasterID&)) throws (::std::invalid_argument);
	       
	/**
	 * @name Delegation
	 * @{
	 */
      public:
	bool isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const throws();
	::std::vector< ::timber::Reference< RasterID> >
	    findRasters(const ::terra::Location& corner, ::std::int32_t width, ::std::int32_t height,
			double scale) const throws( ::terra::UnsupportedCRS, ::std::invalid_argument);
	       
	::timber::Pointer< Raster> getRaster(const ::timber::Reference< RasterID>& id);

	/**
	 * Determine if the raster corresponding to the given id is 
	 * cached. Note that this method does not imply that loadRaster()
	 * will return a non-null pointer.<p>
	 * Implementatin of this method must not modify the state of the cache
	 * in any way.
	 * @param id the id of raster
	 * @return true if the raster is currently cached.
	 */
      public:
	virtual bool isCached(const ::timber::Reference<RasterID>& id) const = 0;

	/**
	 * Load raster by its id. This method is called as part of getRaster().
	 * @param id a raster id
	 * @return a raster or null if the raster isn't cached
	 */
      protected:
	virtual ::timber::Pointer<Raster> loadCachedRaster (const ::timber::Reference<RasterID>& id) = 0;

	/**
	 * Store the specified raster in the cache. This method is called 
	 * as part of getRaster().
	 * @param raster a raster
	 */
      protected:
	virtual void cacheRaster (const ::timber::Reference<Raster>& raster) = 0;
	       
	/**
	 * Get the delegate
	 * @return the delegate
	 */
      protected:
	inline RasterProvider& delegate() const throws() { return *_delegate; }

	/** The actual raster provider */
      private:
	const ::timber::Reference<RasterProvider> _delegate;
      };

    }
  }
}

#endif
