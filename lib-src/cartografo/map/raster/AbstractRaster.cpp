#include <cartografo/map/raster/AbstractRaster.h>
#include <cartografo/map/raster/RasterID.h>
#include <canopy/mt/MutexGuard.h>

namespace cartografo {
   namespace map {
      namespace raster {

         AbstractRaster::AbstractRaster(const ::timber::Reference< RasterID>& xid)
         throws()
         :_id(xid),_state(CREATED)
         {

         }

         AbstractRaster::~AbstractRaster()
         throws()
         {

         }

         ::timber::Reference< RasterID> AbstractRaster::id() const
         throws() {return _id;}

         Raster::State AbstractRaster::state() const
         throws () {

            return _state;
         }

         Raster::State AbstractRaster::setState(State s)
         throws ()
         {
            ::canopy::mt::MutexGuard<> G(_mutex);

            // always allow a state to itself
            if (_state==s) {
               return s;
            }

            // need to use the setImage method
            if (s==IMAGE_AVAILABLE) {
               return _state;
            }

            // can never go back to the initial state
            if (s==CREATED) {
               return _state;
            }

            switch(_state) {
               case CREATED: _state = s; return s;
               case IMAGE_IN_PROGRESS : _state = s; return s;
               case IMAGE_AVAILABLE : return _state;
               case RASTER_ABORTED : return _state;
               case RASTER_CANCELED : return _state;
               case IMAGE_UNAVAILABLE : return _state;
               default: assert(false); return _state;
            }
         }

         RasterImage AbstractRaster::getRasterImage() const
         throws() {
            ImagePtr res;
            _mutex.lock();
            res = _image;
            _mutex.unlock();
            return RasterImage(_id->bounds(),res);
         }

         Raster::State AbstractRaster::setImage(const ::timber::SharedRef< ::timber::media::ImageBuffer>& ximage)
         throws ()
         {
            ::canopy::mt::MutexGuard<> G(_mutex);
            if(_state!= IMAGE_IN_PROGRESS) {
               return _state;
            }
            _image = ximage;
            _state = IMAGE_AVAILABLE;
            return IMAGE_AVAILABLE;
         }

         Raster::State AbstractRaster::cancel()
         {
           return setState(RASTER_CANCELED);
         }

	::timber::Reference<Raster> AbstractRaster::createRasterAborted(const ::timber::Reference< RasterID>& id) throws()
	{
	  struct _  : public AbstractRaster  {
	    _(const ::timber::Reference< RasterID>& xid) : AbstractRaster(xid)
	    {
	      AbstractRaster::_state = RASTER_ABORTED;
	    }
	    ~_() throws() {}

	    State start() { return RASTER_ABORTED; }
	  };

	  AbstractRaster* res(new _(id));
	  return res;
	}

      }
   }
}
