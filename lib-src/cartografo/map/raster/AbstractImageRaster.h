#ifndef _CARTOGRAFO_MAP_RASTER_ABSTRACTIMAGERASTER_H
#define _CARTOGRAFO_MAP_RASTER_ABSTRACTIMAGERASTER_H

#ifndef _CARTOGRAFO_MAP_RASTER_ABSTRACTRASTER_H
#include <cartografo/map/raster/AbstractRaster.h>
#endif

namespace timber {
   namespace w3c {
      class URI;
   }
   namespace media {
      class ImageFactory;
   }
}
namespace cartografo {
   namespace map {
      namespace raster {

         /**
          * This interface provides a simple way to create a raster that when started loads an image.
          */
         class AbstractImageRaster : public AbstractRaster
         {
               /**
                * Create a raster with the specified id. The state is initialized to CREATED.
                * @param id the raster id.
                *  */
            protected:
               AbstractImageRaster(const ::timber::Reference< RasterID>& id)throws();

               /** Destructor */
            public:
               ~AbstractImageRaster()throws();

               /**
                * Create an image raster. The image will be retrieved from the specified URL. Only the
                * <em>png</em> format is supported at this time.
                * @param id the raster id.
                * @param uri a URL
                * @param factory an image factory
                * @return a raster
                */
            public:
               static ::timber::Reference< Raster> create(const ::timber::Reference< RasterID>& rid,
                     const ::timber::SharedRef< ::timber::w3c::URI>& uri,
                     const ::timber::SharedRef< ::timber::media::ImageFactory>& factory)throws();

               /**
                * Create an image raster. The image will be retrieved from specified file path. Only the
                * <em>png</em> format is supported at this time.
                * @param id the raster id.
                * @param path a path to raster file
                * @param factory an image factory
                * @return a raster
                */
            public:
               static ::timber::Reference< Raster> create(const ::timber::Reference< RasterID>& rid,
                     const ::std::string& path, const ::timber::SharedRef< ::timber::media::ImageFactory>& factory)throws();

               /**
                * Start loading the image. This method sets up the raster states as necessary.
                * @return the current state
                */
            public:
               State start();

               /**
                * Load the image.
                * @return the image that was loaded or null if an error occurred
                */
            protected:
               virtual ImagePtr loadImage() = 0;

         };
      }
   }
}

#endif
