#ifndef _CARTOGRAFO_MAP_RASTER_OSMRASTERPROVIDER_H
#define _CARTOGRAFO_MAP_RASTER_OSMRASTERPROVIDER_H

#ifndef _TIMBER_MEDIA_IMAGEFACTORY_H
#include <timber/media/ImageFactory.h>
#endif

#ifndef _CARTOGRAFO_MAP_RASTER_RASTERPROVIDER_H
#include <cartografo/map/raster/RasterProvider.h>
#endif

#ifndef _TERRA_LOCATION_H
#include <terra/Location.h>
#endif

#include <cstdint>

namespace terra {
   class ProjectedCRS;
}

namespace cartografo {
   namespace map {
      namespace raster {

         /**
          * This layer is able to retrieve imagery from a WMS server and render it
          * with the current projection.
          */
         class OsmRasterProvider : public RasterProvider
         {
               /** The default tilesize */
            public:
               static const ::std::uint32_t TILE_SIZE = 256;

               /** The default maximum zoom level */
            public:
               static const ::std::uint32_t DEFAULT_MAX_ZOOM = 17;

               /**
                * A tile index is just a pair of integers; the first one is x, the second is y. Indices
                * may be negative, but they are not considered valid.
                *  */
            public:
               typedef ::std::pair< ::std::int32_t, ::std::int32_t> TileIndex;

               /**
                * Constructor.
                * @param f an image factory
                */
            protected:
               OsmRasterProvider(const ::timber::SharedRef< ::timber::media::ImageFactory>& f)throws();

               /** Destructor */
            public:
               ~OsmRasterProvider()throws();

               /**
                * Create a new OpenStreetMap raster provider.
                * @param f an image factory
                * @return new OpenStreetMap raster provider.
                */
            public:
               static ::timber::Reference< OsmRasterProvider> create(
                     const ::timber::SharedRef< ::timber::media::ImageFactory>& f)throws();

               /**
                * This is a conversion function for rasters to paths. This
                * method is primarily used to support disk-caches.
                * @param id a raster id
                * @return a string representing the id or an empty string on error
                */
            public:
               static ::std::string getRasterIDPath(const RasterID& id)throws();

            public:
               bool isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const throws();
               ::std::vector< ::timber::Reference< RasterID> > findRasters(const ::terra::Location& corner,
                     ::std::int32_t width, ::std::int32_t height, double scale) const throws( ::terra::UnsupportedCRS, ::std::invalid_argument);

               ::timber::Pointer< Raster> getRaster(const ::timber::Reference< RasterID>& id);

               /**
                * Determine the zoom level for a given scale. The zoom level for OSM represents the number
                * of tiles into which visible area is divided. The number of tiles is <tt>2**zoom</tt>. The
                * x and y indices for tiles go from <tt>0 to (2**zoom)-1</tt>.
                * @param s a scale
                * @return the zoom level corresponding to the scale or -1 if the scale is not a proper zoom level
                */
            public:
               ::std::int32_t getZoom(double scale) const throws();

               /**
                * Get the scale at the specified zoom level.
                * @param zoom a zoom level
                * @return the scale
                */
            public:
               double getScale(::std::int32_t zoom) const throws();

               /**
                * Determine the tile width at the specified zoom.
                * @param zoom a zoom level
                * @return the tile size at the zoom
                */
            public:
               double getTileSize(::std::int32_t zoom) const throws();

               /**
                * Compute index of the tile that covers the point (x,y) at the given zoom level. The point
                * is assumed to be in the CRS of this provider.
                * @param x x-coordinate of the point
                * @param y y-coordinate of the point
                * @param zoom the zoom level
                * @return the tile that contains the location at the specified zoom
                */
            private:
               TileIndex getTileIndex(double x, double y, ::std::uint32_t zoom) const throws();

               /** The open-street map tile server */
            private:
               const ::std::string _tileserver;

               /** The coordinate frame */
            private:
               const ::timber::Reference< ::terra::ProjectedCRS> _crs;

               /** The support tile size in pixels */
            private:
               const ::std::uint32_t _tileSize;

               /** The maxmum zoom level */
            private:
               ::std::uint32_t _maxZoom;

               /** The point (-180,85) */
            private:
               ::terra::Location _topLeft;

               /** The point (180,85) */
            private:
               ::terra::Location _bottomRight;

               /** The image factory */
            private:
               const ::timber::SharedRef< ::timber::media::ImageFactory> _factory;
         };

      }

   }

}

#endif
