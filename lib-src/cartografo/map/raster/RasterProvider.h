#ifndef _CARTOGRAFO_MAP_RASTER_RASTERPROVIDER_H
#define _CARTOGRAFO_MAP_RASTER_RASTERPROVIDER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TERRA_UNSUPPORTEDCRS_H
#include <terra/UnsupportedCRS.h>
#endif

#ifndef _CARTOGRAFO_MAP_RASTER_RASTERID_H
#include <cartografo/map/raster/RasterID.h>
#endif

#ifndef _CARTOGRAFO_MAP_RASTER_RASTER_H
#include <cartografo/map/raster/Raster.h>
#endif

#include <vector>

namespace terra {
   class Location;
}
namespace cartografo {
   namespace map {
      namespace raster {

         /**
          * This interface provides a means to obtain rasters that should be displayed in a given region. Obtaining
          * raster images is a 3-step process:
          * <ol>
          * <li>Given an area determine the list of rasters (in the form of an id) that should be retrieved
          * <li>Given a raster id, retrieve that raster.
          * <li>Once the raster has been obtained, start must be invoked to commence image retrieval
          * </ol>
          * Utilizing this 2-step process allows clients to efficiently implement caches.
	  * <p>Implementations of this interface are expected to be threadsafe!
          */
         class RasterProvider : public ::timber::Counted
         {
               /** Default constructor */
            protected:
               RasterProvider()throws();

               /** Destructor */
            public:
               ~RasterProvider()throws();

               /**
                * Determine if the specified CRS is supported. If it is supported, then findRasters will never throw
                * and UnsupportedCRS exception.
                * @param crs a crs
                * @return true if the specified crs is supported
                */
            public:
               virtual bool isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const throws() = 0;

               /**
                * Determine the rasters that this provider can retrieve for the given area. The rasters
                * must be provided in the coordinate reference system of the specified location.<p>
                * Note that width and height are in units of pixels. The width and height of the region in the
                * coordinate reference frame can be obtained by dividing each by the scale.<p>
                * The values of width and height determine which corner of bounds is specified:
                * <ol>
                * <li>if width &gt; > 0 and height &gt; 0, then corner is the bottom-left corner
                * <li>if width &gt; > 0 and height &lt; 0, then corner is the top-left corner
                * <li>if width &gt; < 0 and height &gt; 0, then corner is the bottom-right corner
                * <li>if width &gt; < 0 and height &lt; 0, then corner is the top-right corner
                * </ol>
                * @param bottomRight the bottom right location of the area to be covered
                * @param width the width of the region to be covered by the raster in pixels
                * @param height the height of the region to be covered by the raster in pixels
                * @param scale the scale at which to retrieve the rasters in units of pixel/unit
                * @return a list of raster ids (expected to be unique, but not required)
                * @throws UnsupportedCRS if the coordinate reference system is not supported
                * @throws ::std::invalid_argument if width==0 || height ==0
                * @throws ::std::invalid_argument if scale <= 0
                */
            public:
               virtual ::std::vector< ::timber::Reference< RasterID> >
                     findRasters(const ::terra::Location& corner, ::std::int32_t width, ::std::int32_t height,
                           double scale) const throws( ::terra::UnsupportedCRS, ::std::invalid_argument) = 0;

               /**
                * Get a raster by its id.
                * @param id raster by its id
                * @return a raster or null if there is not such raster
                */
            public:
               virtual ::timber::Pointer< Raster> getRaster(const ::timber::Reference< RasterID>& id) = 0;
         };

      }

   }

}

#endif
