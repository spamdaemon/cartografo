#include <cartografo/map/raster/AbstractImageRaster.h>
#include <timber/logging.h>
#include <timber/w3c/URI.h>
#include <timber/ios/URIInputStream.h>
#include <timber/media/ImageFactory.h>
#include <timber/media/format/Png.h>
#include <canopy/fs/FileSystem.h>

#include <fstream>

using namespace ::timber::logging;

namespace cartografo {
   namespace map {
      namespace raster {
         namespace {
            static Log logger()
            {
               return Log("cartografo.map.raster.AbstractImageRaster");
            }

         }

         AbstractImageRaster::AbstractImageRaster(const ::timber::Reference< RasterID>& xid)
         throws()
         : AbstractRaster(xid)
         {

         }

         AbstractImageRaster::~AbstractImageRaster()
         throws()
         {

         }

         Raster::State AbstractImageRaster::start()
         {
            State s = setState(IMAGE_IN_PROGRESS);
            if (s != IMAGE_IN_PROGRESS) {
               return s;
            }
            try {
               ImagePtr img = loadImage();
               if (img) {
                  return setImage(img);
               }
               else {
                  return setState(IMAGE_UNAVAILABLE);
               }
            }
            catch (const ::std::exception& e) {
               logger().caught("Image retrieval failed", e);
               return setState(RASTER_ABORTED);
            }
         }

         ::timber::Reference< Raster> AbstractImageRaster::create(const ::timber::Reference< RasterID>& rid,
               const ::timber::SharedRef< ::timber::w3c::URI>& uri,
               const ::timber::SharedRef< ::timber::media::ImageFactory>& factory)
         throws ()
         {
            struct _ : public AbstractImageRaster
            {
               _(const ::timber::Reference< RasterID>& xid, const ::timber::SharedRef< ::timber::w3c::URI>& xuri, const ::timber::SharedRef< ::timber::media::ImageFactory>& xfactory)throws() :
               AbstractImageRaster(xid),_uri(xuri),_factory(xfactory) {}

               ~_()throws() {}

               ImagePtr loadImage()
               {
                  LogEntry(logger()).debugging() << "Load Raster image from : " << *_uri << doLog;
                  ::timber::ios::URIInputStream in(_uri);
                  return ::std::dynamic_pointer_cast<timber::media::ImageBuffer>(_factory->read(in));
               }

               private:
               const ::timber::SharedRef< ::timber::w3c::URI> _uri;
               const ::timber::SharedRef< ::timber::media::ImageFactory> _factory;
            };

            AbstractImageRaster* res = new _(rid,uri,factory);
            return res;
         }

         ::timber::Reference< Raster> AbstractImageRaster::create(const ::timber::Reference< RasterID>& rid,
               const ::std::string& path, const ::timber::SharedRef< ::timber::media::ImageFactory>& factory)
throws      ()
      {
         try {
            return create(rid,::timber::w3c::URI::createFile(path),factory);
         }
         catch (const ::std::exception& e) {
            logger().caught("Could not create file URI for " +path,e);
         }
         return AbstractRaster::createRasterAborted(rid);
      }

   }
}
}
