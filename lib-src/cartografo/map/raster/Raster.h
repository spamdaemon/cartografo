#ifndef _CARTOGRAFO_MAP_RASTER_RASTER_H
#define _CARTOGRAFO_MAP_RASTER_RASTER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _CARTOGRAFO_RASTERIMAGE_H
#include <cartografo/RasterImage.h>
#endif

namespace cartografo {
   namespace map {
      namespace raster {
         class RasterID;

         /**
          * This interfaces defines the boundary for a raster image and provides a means to retrieve the image itself.
          */
         class Raster : public ::timber::Counted
         {
               /** A callback that will be invoked once the image has reached a final state. */
               struct Callback
               {

               };

               /** The raster states. The following are valid state transitions:
                * <ul>
                * <li> any state to itself
                * <li> CREATED to any other state
                * <li> IMAGE_IN_PROGRESS to any final state (IMAGE_AVAILABLE,CANCEsLED,ABORTED,UNAVAILABLE)
                * </ul>
                */
            public:
               enum State
               {
                  /** The raster object has been created but no attempt to access the image has been made (initial state) */
                  CREATED,

                  /** The raster is being retrieved */
                  IMAGE_IN_PROGRESS,

                  /** The image is available (final state) */
                  IMAGE_AVAILABLE,

                  /** The raster has been canceled (final state) */
                  RASTER_CANCELED,

                  /** An error has occurred (final state) */
                  RASTER_ABORTED,

                  /**
                   *  The tile is unavailable (for example, it would be outside the
                   *  valid bounds of the coordinate reference system (final state).
                   */
                  IMAGE_UNAVAILABLE
               };

               /** Default constructor */
            protected:
               Raster()throws();

               /** Destructor */
            public:
               ~Raster()throws();

               /**
                * @name RasterProperties
                * @{
                */

               /**
                * Get the id associated with this raster.
                * @return this raster's id
                */
            public:
               virtual ::timber::Reference< RasterID> id() const throws() = 0;

               /**
                * Get the raster. Check the raster's image to see if an image is already available.
                * @return the raster image
                */
            public:
               virtual RasterImage getRasterImage() const throws() = 0;

               /*@}*/

               /**
                * @name RasterState
                * @{
                */

               /**
                * Get the current image state.
                * @return the status of this image.
                */
            public:
               virtual State state() const throws() = 0;

               /**
                * Start retrieving the image for this raster. If the current
                * state is not CREATED, then this method does nothing.
                *
                * @return the new state
                */
            public:
               virtual State start() = 0;

               /**
                * Cancel this raster. If the current state is not CREATED and not IMAGE_IN_PROGRESS,
                * then this method does nothing.
                * @return the new state.
                */
            public:
               virtual State cancel() = 0;

               /*@}*/

         };
      }
   }
}

#endif
