#include <cartografo/map/raster/RasterCache.h>
#include <cartografo/map/raster/AbstractImageRaster.h>
#include <canopy/mt/Mutex.h>
#include <canopy/mt/MutexGuard.h>
#include <canopy/fs/FileSystem.h>
#include <timber/w3c/URI.h>
#include <timber/media/format/Png.h>
#include <timber/logging.h>

#include <deque>
#include <sstream>
#include <fstream>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::canopy::mt;

namespace cartografo {
   namespace map {
      namespace raster {
         namespace {
            static ::std::string raster2File(const RasterID& id)
            {
               ::std::ostringstream out;
               assert("Unimplemented" == 0);
               return out.str();
            }

            static Log logger()
            {
               return "cartografo.map.raster.RasterCache";
            }
         }

         struct RasterCache::CachedRaster : public Raster
         {
               CachedRaster(const Reference< RasterCache>& cache, const Reference< Raster>& r)throws() :
               _cache(cache), _delegate(r) {}
               ~CachedRaster()throws() {}

               ::timber::Reference< RasterID> id() const throws() {return _delegate->id();}
               RasterImage getRasterImage() const throws() {return _delegate->getRasterImage();}
               State state() const throws() {return _delegate->state();}
               State start()
               {
                  State s = _delegate->start();
                  if (s == IMAGE_AVAILABLE) {
                     logger().info("Caching raster");
                     _cache->cacheRaster(_delegate);
                  }
                  else {
                     logger().info("Image not available (yet); not cached");
                  }
                  return s;
               }

               State cancel()
               {
                  return _delegate->cancel();
               }

               const Reference< RasterCache> _cache;
               const Reference< Raster> _delegate;

         };

         RasterCache::RasterCache(const Reference< RasterProvider>& s)
         throws()
         : _delegate(s)
         {

         }

         RasterCache::~RasterCache()
         throws ()
         {
         }

         bool RasterCache::isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const
         throws()
         {  return delegate().isSupportedCRS(crs);}

         ::std::vector< ::timber::Reference< RasterID> >
         RasterCache::findRasters(const ::terra::Location& corner, ::std::int32_t width, ::std::int32_t height,
               double scale) const
         throws( ::terra::UnsupportedCRS, ::std::invalid_argument)
         {  return delegate().findRasters(corner,width,height,scale);}

         ::timber::Pointer< Raster> RasterCache::getRaster(const ::timber::Reference< RasterID>& id)
         {
            ::timber::Pointer< Raster> res;
            try {
               res = loadCachedRaster(id);
            }
            catch (const ::std::exception& e) {
               logger().caught("Could not load a cached raster", e);
            }
            if (!res) {
               res = delegate().getRaster(id);
               if (res) {
                  res = new CachedRaster(toRef< RasterCache> (), res);
               }
            }

            return res;
         }

         Reference< RasterCache> RasterCache::createMemoryCache(const Reference< RasterProvider>& s, size_t n)
         throws (::std::invalid_argument)
         {
            if (n==0) {
               throw ::std::invalid_argument("Invalid tile count");
            }

            struct _ : public RasterCache
            {

                  typedef ::std::deque< Reference< Raster> > Cache;

                  _(const Reference< RasterProvider>& xs, size_t xn) :
                        RasterCache(xs), _max(xn)
                  {
                  }

                  ~_()throws() {}

                  Cache::iterator find(const RasterID& id) const throws()
                  {
                     for (Cache::iterator i=_rasters.begin();i!=_rasters.end();++i) {
                        if ((*i)->id()->equals(id)) {
                           return i;
                        }
                     }
                     return _rasters.end();
                  }

                  bool isCached(const ::timber::Reference< RasterID>& id) const
                  {
                     const MutexGuard< > G(_mutex);
                     return find(*id) != _rasters.end();
                  }

                  Pointer< Raster> loadCachedRaster(const Reference< RasterID>& id)
                  {
                     Pointer< Raster> res;
                     const MutexGuard< > G(_mutex);
                     auto i = find(*id);
                     if (i != _rasters.end()) {
                        res = *i;
                        _rasters.erase(i);
                        _rasters.push_front(res);
                     }
                     return res;
                  }

                  void cacheRaster(const Reference< Raster>& raster)
                  {
                     const MutexGuard< > G(_mutex);
                     auto i = find(*raster->id());
                     // if already cached, then replace the rasters
                     if (i != _rasters.end()) {
                        *i = raster;
                     }

                     // item not cached, so a new one must be added

                     // if the cache is full, then remove the last one
                     if (_rasters.size() == _max) {
                        _rasters.pop_back();
                     }

                     // finally, add the new raster
                     _rasters.push_front(raster);
                  }

                  /** The maximum number of entries in the cache */
               private:
                  const size_t _max;

                  /** A mutex */
               private:
                  Mutex _mutex;

                  /** The rasters by id; for now, this is a simple vector */
               private:
                  mutable Cache _rasters;
            };

            RasterCache* res(new _(s, n));
return         res;

      }

      Reference<RasterCache> RasterCache::createDiskCache (const Reference<RasterProvider>& s,  const ::timber::SharedRef< ::timber::media::ImageFactory>& f, const ::std::string& dir, size_t maxAge, ::std::string (*c)(const RasterID&)) throws (::std::invalid_argument)
      {
         typedef ::std::string (*Raster2File)(const RasterID&);

         struct _ : public RasterCache {

            _(const Reference<RasterProvider>& xs, const ::timber::SharedRef< ::timber::media::ImageFactory>& xf, const ::std::string& xDir, size_t xMaxAge, Raster2File conv )
            : RasterCache(xs),_factory(xf),_dir(xDir),_maxAge(xMaxAge),_raster2File(conv) {}

            ~_() throws() {}

            ::std::string createFile (const RasterID& id) const
            {
               const ::std::string path = (*_raster2File)(id);
               if (path.empty()) {
                  throw ::std::invalid_argument("Cannot convert raster id");
               }
               return _fs.joinPaths(_dir,path);
            }

            bool isCached(const ::timber::Reference<RasterID>& id) const
            {
               try {
                  return _fs.exists(createFile(*id));
               }
               catch (::std::exception& e) {
                  return false;
               }
            }

            Pointer<Raster> loadCachedRaster (const Reference<RasterID>& id)
            {
               try {
                  ::std::string file = createFile(*id);
                  if (_fs.exists(file)) {
                     return AbstractImageRaster::create(id,createFile(*id),_factory);
                  }
               }
               catch (const ::std::exception& e) {
                  logger().caught("Exception loading image raster",e);
               }
               return Pointer<Raster>();
            }

            void cacheRaster (const Reference<Raster>& raster)
            {
               RasterImage image = raster->getRasterImage();
               if (!image.image()) {
                  // there's no image, so cannot store
                  return;
               }
               try {
                  ::std::string file = createFile(*raster->id());

                  // ensure the base directories exist
                  ::std::string basedir = _fs.directoryName(file);
                  _fs.mkdirs(basedir);

                  // create the file itself
                  if (!_fs.createFile(file)) {
                     // file already exists or is being written to
                     return;
                  }

                  ::std::ofstream out(file.c_str());
                  ::timber::media::format::Png::write(*image.image(),out);
               }
               catch (const ::std::exception& e) {
                  // ignore for now
                  logger().caught("Could not store raster as file",e);
               }
            }

            /** An image factory */
            private:
            const ::timber::SharedRef< ::timber::media::ImageFactory> _factory;

            /** The root directory */
            private:
            const ::std::string _dir;

            /** The maximum number of entries in the cache */
            private:
            const size_t _maxAge;

            /** A file system */
            private:
            mutable ::canopy::fs::FileSystem _fs;

            /** The raster to file conversion function */
            private:
            Raster2File _raster2File;
         };

         if (!c) {
            c = raster2File;
         }

         RasterCache* res(new _(s,f,dir,maxAge,c));
         return res;
      }

   }
}
}
