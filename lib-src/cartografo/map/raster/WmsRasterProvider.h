#ifndef CARTOGRAFO_MAP_RASTER_WMSRASTERPROVIDER_H
#define CARTOGRAFO_MAP_RASTER_WMSRASTERPROVIDER_H

#ifndef _TIMBER_MEDIA_IMAGEFACTORY_H
#include <timber/media/ImageFactory.h>
#endif

#ifndef _CARTOGRAFO_MAP_RASTER_RASTERPROVIDER_H
#include <cartografo/map/raster/RasterProvider.h>
#endif

#ifndef _CARTOGRAFO_MAP_WMS_REQUEST_H
#include <cartografo/map/wms/Request.h>
#endif

namespace cartografo {
   namespace map {
      namespace raster {

         /**
          * This layer is able to retrieve imagery from a WMS server and render it
          * with the current projection.
          */
         class WmsRasterProvider : public RasterProvider
         {
               /**
                * Constructor.
                * @param f a factory for images
                */
            protected:
               WmsRasterProvider(const ::timber::SharedRef< ::timber::media::ImageFactory>& f)throws();

               /** Destructor */
            public:
               ~WmsRasterProvider()throws();

               /**
                * Create a new WMS layer.
                * @return a WMS layer
                */
            public:
               static ::timber::Reference< WmsRasterProvider> create(
                     const ::timber::SharedRef< ::timber::media::ImageFactory>& f)throws();

               /**
                * Set the base url for the server from which to retrieve the images.
                * @param url a url
                * @throws invalid_argument the URL is not valid.
                */
            public:
               void setWmsServerUrl(const ::std::string& url)
               throws( ::std::invalid_argument);

               /**
                * Set the name of the layer to be retrieved.
                * @param the name of a single WMS layer
                */
            public:
               void setWmsLayer(const ::std::string& name)
               throws( ::std::invalid_argument);

            public:
               bool isSupportedCRS(const ::terra::CoordinateReferenceSystem& crs) const throws();
               ::std::vector< ::timber::Reference< RasterID> > findRasters(const ::terra::Location& corner,
                     ::std::int32_t width, ::std::int32_t height, double scale) const throws( ::terra::UnsupportedCRS, ::std::invalid_argument);

               ::timber::Pointer< Raster> getRaster(const ::timber::Reference< RasterID>& id);

               /** The server request */
            private:
               ::cartografo::map::wms::Request _request;

               /** The image factory */
            private:
               const ::timber::SharedRef< ::timber::media::ImageFactory> _factory;

         };

      }

   }

}

#endif
