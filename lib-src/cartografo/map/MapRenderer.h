#ifndef _CARTOGRAFO_MAP_MAPRENDERER_H
#define _CARTOGRAFO_MAP_MAPRENDERER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <vector>

namespace indigo {
  class Node;
}

namespace cartografo {
  class Projector2D;
  class ExtentPyramid;

  namespace geo {
    class Node;
  }

  namespace map {
    
    /**
     * The scene graph renderer visits a node recursively 
     * and generates a and indigo scene graph by projecting
     * the shapes. The map renderer is stateful across
     * calls to getMap and will optimize the conversion 
     * process to reduce the number of projections.
     */
    class MapRenderer {

      /** The renderer */
    private:
      class Renderer;

      /**
       * Default rounding precision.
       */
    public:
      static constexpr double DEFAULT_PRECISION = 0.25;

      /**
       * Create a new MapRenderer for a node.
       * @param node the node to which to apply this renderer
       * @param p a projector
      */
    public:
      MapRenderer () throws();
      
      /** Destroy this visitor */
    public:
      ~MapRenderer() throws();

      /**
       * Set the default precision for projected regions.
       * Reducing the precision can avoid degeneracy in
       * the projected regions. Reducing the precision too
       * much may result in missing features in the projection.
       * @param p a precision
       * @pre p>=0
       */
    public:
      void setPrecision (double p) throws();

      /**
       * Get the current precisiom for projected regions.
       * @return the precision
       */
    public:
      double precision() const throws();

      /**
       * Set the root node.
       * @param node the new root node
       */
    public:
      void setRootNode(const ::timber::Pointer< ::cartografo::geo::Node>& node) throws();

      /**
       * Set a new projector. This will trigger a reprojection of the existing nodes.
       * @param proj a new projector
       * @param py an optional extent pyramid 
       */
    public:
      void setProjector (const ::timber::Reference<Projector2D>& proj, const ::timber::Pointer<ExtentPyramid>& py) throws();

      /**
       * Set a new projector. This will trigger a reprojection of the existing nodes.
       * @param proj a new projector
       */
    public:
      void setProjector (const ::timber::Reference<Projector2D>& proj) throws();

      /**
       * Notify this renderer that the specified node has changed.
       * @param node a node
       */
    public:
      void notifyNodeChanged (const ::timber::Reference< ::cartografo::geo::Node>& node) throws();

      /**
       * Get the scene graph from this renderer.
       * @param 
       * @return an indigo node or null if none was built
       */
    public:
      ::timber::Pointer< ::indigo::Node> renderMap () throws();
      
      /** The node */
    private:
      ::timber::Pointer< ::cartografo::geo::Node> _node;

      /** The projector */
    private:
      ::timber::Pointer<Projector2D> _projector;

      /** The renderer */
    private:
      ::std::unique_ptr<Renderer> _renderer;

      /** An extent pyramid */
    private:
      ::timber::Pointer<ExtentPyramid> _pyramid;
    };
  }
}
#endif
