#ifndef _CARTOGRAFO_MAP_SHAPEFILE_SHAPEFILELAYER_H
#define _CARTOGRAFO_MAP_SHAPEFILE_SHAPEFILELAYER_H

#ifndef _CARTOGRAFO_MAP_LAYER_H
#include <cartografo/map/Layer.h>
#endif

#ifndef _CARTOGRAFO_GEO_GROUP_H
#include <cartografo/geo/Group.h>
#endif

#ifndef _TERRA_GEODETICDATUM_H
#include <terra/GeodeticDatum.h>
#endif

namespace cartografo {
   namespace map {
      namespace shapefile {

         /**
          * This layer can be used to display the contents of a single ESRI shapefile.
          */
         class ShapefileLayer : public Layer
         {
               /**
                * Default constructor.
                * @param provider the raster provider to use
                */
            private:
               ShapefileLayer()throws();

               /** Destructor */
            public:
               ~ShapefileLayer()throws();

               /**
                * Open a shapefile and create a layer from it.
                * @param file the path to a shapefile
                * @param datum the datum to be associated with the shapes
                * @return a layer or null if the file could not be opened
                */
            public:
               static ::timber::Pointer<ShapefileLayer> create (const ::std::string& file, const ::timber::Reference< ::terra::GeodeticDatum> & datum) throws();

               /** The shapefile root */
            private:
               ::timber::Reference< ::cartografo::geo::Group> _root;

         };

      }

   }
}

#endif
