#include <cartografo/map/shapefile/ShapefileLayer.h>
#include <cartografo/map/Map.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionDomain.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/AreaBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/Area.h>
#include <cartografo/Region.h>
#include <cartografo/geo/nodes.h>

#include <tikal/Point2D.h>
#include <tikal/Path2D.h>
#include <tikal/Region2DOps.h>
#include <tikal/Region2D.h>
#include <tikal/Area2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Path2DBuilder.h>

#include <indigo/nodes.h>
#include <indigo/view/Bounds.h>
#include <indigo/view/ViewPort.h>

#include <timber/logging.h>
#include <terra/terra.h>
#include <cmath>

// this is shapefile loader
#include <shapefil.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
   namespace map {
      namespace shapefile {

         namespace {
            static const bool RENDER_AS_GEODESIC = false;
            static const indigo::AntiAliasingMode DEFAULT_AA_MODE = indigo::AntiAliasingMode::FAST;

            static Log logger()
            {
               return Log("cartografo.map.raster.ShapefileLayer");
            }

            struct ShapeInfo : public ::indigo::PickInfo
            {
                  ShapeInfo(const ::std::string& country, Reference< ::indigo::SolidFillNode> f,
                        Reference< ::indigo::StrokeNode> s)
                        : _country(country), _fill(f), _stroke(s), _base(f->color()), _highlight(
                              ::indigo::Color(0xad / 255., 0xad / 255.0, 0)), _selected(false)
                  {
                  }
                  ~ShapeInfo()throws()
                  {}

               public:
                  void setSelected(bool selected)
                  {
                     if (_selected != selected) {
                        if (selected) {
                           _fill->setColor(_highlight);
                        }
                        else {
                           _fill->setColor(_base);
                        }
                     }
                  }

               private:
                  ::std::string _country;
                  Reference< ::indigo::SolidFillNode> _fill;
                  Reference< ::indigo::StrokeNode> _stroke;
                  ::indigo::Color _base, _highlight;
                  bool _selected;

            };

            static Pointer< Area> createPartArea(SHPObject* obj, int part, const Reference< GeodeticDatum>& datum)
            {
               int end = (part < obj->nParts - 1) ? obj->panPartStart[part + 1] : obj->nVertices;
               bool first = true;
               ::tikal::Path2DBuilder pBuilder;
               for (int i = obj->panPartStart[part]; i < end; ++i) {
                  //::std::cerr << obj->padfY[i] << "  " << obj->padfX[i] << "  " << obj->padfZ[i] << ::std::endl;
                  double lat = obj->padfY[i];
                  double lon = obj->padfX[i];
                  /*
                   lon += toDegrees(1.1-0.00859933);
                   lat -= toDegrees(0.918922+0.0933276);
                   if (lon < -.5) {
                   continue;
                   }
                   */
                  if (lon > 180 || lon < -180) {
                     LogEntry(logger()).warn() << "Warning: longitude out-of-range " << lon << doLog;
                     lon = lon > 0 ? 180 : -180;
                  }
                  if (first) {
                     first = false;
                     pBuilder.moveTo(lon, lat);
                  }
                  else {
                     pBuilder.lineTo(lon, lat);
                  }
               }
               Pointer< ::tikal::Region2D> reg = ::tikal::Region2D::createMergedPath(pBuilder.path());

               if (reg && reg->areaCount() == 1) {
                  Reference< ::tikal::Contour2D> contour = reg->area(0)->outsideContour();
                  if (contour->maxDegree() == 1) {
                     ::std::vector< ::tikal::Point2D> pts;
                     for (size_t i = 0; i < contour->segmentCount(); ++i) {
                        ::tikal::Point2D pt[2];
                        size_t n = contour->segment(i, pt);
                        assert(n == 1);
                        pts.push_back(pt[0]);
                     }
                     const GeodeticCoordinates firstPoint(
                           GeodeticCoordinates::fromDegrees(pts[0].y(), pts[0].x(), 0, datum));
                     AreaBuilder B;
                     B.beginOutsidePath(firstPoint, true);
                     for (size_t i = 1; i < pts.size(); ++i) {
                        size_t k = contour->vertexOrder().isCW() ? i : (pts.size() - i);
                        const GeodeticCoordinates point(
                              GeodeticCoordinates::fromDegrees(pts[k].y(), pts[k].x(), 0, datum));
                        if (RENDER_AS_GEODESIC) {
                           B.geodesicTo(point);
                        }
                        else {
                           B.loxodromeTo(point);
                        }
                     }
                     if (RENDER_AS_GEODESIC) {
                        B.geodesicTo(firstPoint);
                     }
                     else {
                        B.loxodromeTo(firstPoint);
                     }
                     Pointer< Area> area = B.area();
                     if (area) {
                        // ::std::cerr << "Area Vertex order CW? " << area->isCWArea() << " paths : " << area->pathCount() <<  ::std::endl;
                     }
                     return area;
                  }
               }
               else {
                  logger().warn("TOO many areas");
               }
               return Pointer< Area>();
            }

            static ::timber::Pointer< ::cartografo::geo::Node> createObjectPolygon(SHPObject* obj,
                  const Reference< GeodeticDatum>& datum)
            {
               Reference< ::cartografo::geo::Regions> g(new ::cartografo::geo::Regions());

               //  ::std::cerr << "Number of parts " << obj->nParts << ::std::endl;
               for (int part = 0; part < obj->nParts; ++part) {
                  Pointer< Area> a = createPartArea(obj, part, datum);
                  if (a) {
#if 0
#if 0
                     Reference<ExtentPyramid> pyramid = ExtentPyramid::createRegionPyramid(a,6);
                     vector<Extent> exts;
                     pyramid->gatherLeafExtents(exts);
                     for (size_t i=0;i<exts.size();++i) {
                        g->add(Region::createExtentRegion(exts[i]));
                     }
#else
                     g->add(Region::createExtentRegion(a->extent()));
#endif
#else
                     g->add(a);
#endif
                  }
               }
               // if there's only a single node, then return just that one
               return g;
            }

            static Pointer< ::cartografo::geo::Node> createObjectMarker(SHPObject* obj,
                  const Reference< GeodeticDatum>& datum)
            {
               double lat = 0.5 * (obj->dfYMin + obj->dfYMax);
               double lon = 0.5 * (obj->dfXMin + obj->dfXMax);

               const GeodeticCoordinates point(GeodeticCoordinates::fromDegrees(lat, lon, 0, datum));
               Reference< ::indigo::Group> g(new ::indigo::Group());
               Reference< ::indigo::Points> pts(new ::indigo::Points());
               pts->setPointSize(3);
               pts->add(::indigo::Point(0, 0));
               g->add(new ::indigo::SolidFillNode(::indigo::Color(1.0, .5, .5)));
               g->add(pts);
               return Pointer< ::cartografo::geo::Node>(new ::cartografo::geo::Marker(point, g));
            }

            static void loadShapes(SHPHandle fp, const Reference< GeodeticDatum>& datum,
                  Reference< ::cartografo::geo::Group>& polygons, Reference< ::cartografo::geo::Group>& markers)
            {
               assert(fp && "Shapefile not provided");

               assert(fp != 0);

               const int count = fp->nRecords;
               LogEntry(logger()).info() << "There are " << count << " shape records" << doLog;
               for (int i = 0; i < count; ++i) {
                  if (i > 0 && (i % 20) == 0) {
                     LogEntry(logger()).info() << "Processing shape " << "[ #" << i << "]" << ::std::endl;
                  }
                  SHPObject* obj = SHPReadObject(fp, i);
                  try {
                     switch (obj->nSHPType) {
                        case SHPT_POLYGON:
                        case SHPT_POLYGONM: {
                           LogEntry(logger()).info() << "[ #" << i << "] Processing polygon (" << obj->nSHPType << ')'
                                 << doLog;
                           Pointer< ::cartografo::geo::Node> p = createObjectPolygon(obj, datum);
                           if (p) {
                              polygons->add(p);
                           }
                           break;
                        }
                        case SHPT_POINT: {
                           LogEntry(logger()).info() << "[ #" << i << "] Processing point (" << obj->nSHPType << ')'
                                 << doLog;
                           Pointer< ::cartografo::geo::Node> m = createObjectMarker(obj, datum);
                           if (m) {
                              markers->add(m);
                           }
                           break;
                        }
                        default:
                           LogEntry(logger()).debugging() << "[ #" << i << "] Ignored shape type " << obj->nSHPType
                                 << doLog;
                           break;
                     }
                  }
                  catch (const ::std::exception& e) {
                     LogEntry(logger()).caught(e) << "Unexpected exception when processing shape #" << i << doLog;
                  }
                  SHPDestroyObject(obj);
               }
            }

            Pointer< ::cartografo::geo::Group> createDefaultPolygonStyle(bool fill)
            {
               // create a non-separator root
               Reference< ::cartografo::geo::Group> styleRoot(new ::cartografo::geo::Group(false));
               ::indigo::AntiAliasingMode aaMode = DEFAULT_AA_MODE;

               // don't turn on alpha, it's super slow
               styleRoot->add(new ::cartografo::geo::IndigoNode(new ::indigo::AlphaNode(1)));
               styleRoot->add(new ::cartografo::geo::IndigoNode(new ::indigo::AntiAliasingModeNode(aaMode)));

               Reference< ::indigo::SolidFillNode> F = new ::indigo::SolidFillNode(::indigo::Color(0, 0xae / 255.0, 0));
               Reference< ::indigo::StrokeNode> S = new ::indigo::StrokeNode(::indigo::Color::DARK_GREY,
                     aaMode == ::indigo::AntiAliasingMode::NONE ? 1 : 1);
               styleRoot->add(new ::cartografo::geo::IndigoNode(new ShapeInfo("FOO", F, S)));

               if (fill) {
                  styleRoot->add(new ::cartografo::geo::IndigoNode(F));
               }
               styleRoot->add(new ::cartografo::geo::IndigoNode(S));
               return styleRoot;
            }

         }

         ShapefileLayer::ShapefileLayer()
         throws()
         :
         _root(new ::cartografo::geo::Group())
         {
            Layer::setRootNode(_root);
         }

         ShapefileLayer::~ShapefileLayer()
         throws()
         {
         }

         ::timber::Pointer< ShapefileLayer> ShapefileLayer::create(const ::std::string& file,
               const Reference< GeodeticDatum> & datum)
         throws()
         {
            ::timber::Pointer<ShapefileLayer> layer;
            ::SHPHandle fp = ::SHPOpen(file.c_str(), "r");
            if (!fp) {
               logger().info("Could not open shapefile "+file+" for reading");
               return layer;
            }

            try {
               Reference< ::cartografo::geo::Node> style = createDefaultPolygonStyle(true);
               Reference< ::cartografo::geo::Group> polygons(new ::cartografo::geo::Group());
               Reference< ::cartografo::geo::Group> markers(new ::cartografo::geo::Group());
               loadShapes(fp,datum,polygons,markers);

               layer = new ShapefileLayer();
               layer->_root->add(style);
               layer->_root->add(polygons);
               layer->_root->add(markers);
            }
            catch (const ::std::exception& e) {
               logger().caught("Could not load shapefile : "+file,e);
               layer = ::timber::Pointer<ShapefileLayer>();
            }

            ::SHPClose(fp);

            return layer;
         }

   }}
}
