#ifndef _CARTOGRAFO_MAP_MAP_H
#define _CARTOGRAFO_MAP_MAP_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _CARTOGRAFO_MAP_LAYEROBSERVER_H
#include <cartografo/map/LayerObserver.h>
#endif

#ifndef _TIMBER_EVENT_DISPATCHER_H
#include <timber/event/Dispatcher.h>
#endif

#ifndef _INDIGO_VIEW_VIEWPORT_H
#include <indigo/view/ViewPort.h>
#endif

#include <vector>

namespace cartografo {
   class Projection;
   namespace map {
      class Layer;
      class MapObserver;

      /**
       * This class allows creation of maps defined in terms of primitive
       * shapes arranged in various layers.
       */
      class Map : public ::timber::Counted
      {
            Map(const Map&);
            Map&operator=(const Map&);

            /** Event dispatchers */
         private:
            typedef ::timber::event::Dispatcher< ::timber::Pointer< MapObserver> > Observers;

            /** A layer entry contains information about the state of a layer */
         private:
            class LayerEntry : public LayerObserver
            {

                  /** The map is a friend */
               private:
                  friend class Map;

               public:
                  LayerEntry()throws();

               public:
                  ~LayerEntry()throws();

                  /** The layer has changed */
               protected:
                  void layerChanged(const LayerEvent& e)throws();

                  /** The layer (can be null) */
               private:
                  ::timber::Pointer< Layer> _layer;

                  /** True if the layer is enabled */
               private:
                  bool _enabled;

                  /** The map */
               private:
                  Map* _owner;
            };

            /** The layer entry is a friend */
         private:
            friend class LayerEntry;

            /** The layers */
         private:
            typedef ::std::vector< LayerEntry*> Layers;

            /** Create a map that uses an identity projection centered at (0,0) as the default projection. */
         public:
            Map()throws();

            /** Destructor */
         public:
            ~Map()throws();

            /**
             * Add a map observer.
             * @param observer a map observer
             */
         public:
            void addObserver(const ::timber::Reference< MapObserver> observer)throws();

            /**
             * Remove a map observer.
             * @param observer a map observer
             */
         public:
            void removeObserver(const ::timber::Pointer< MapObserver> observer)throws();

            /**
             * Get the projection currently used.
             * @param p the new projection
             */
         public:
            inline const ::timber::Reference< Projection>& projection() const throws() {return _projection;}

            /**
             * Set the projection for this map. This method notifies each layer of this change to the
             * projection. If the projection has not changed, then the layers may not necessarily be notified.
             * @param p the new projection
             * @param vp the new viewport to go along with the projection
             */
         public:
            virtual void setProjection(const ::timber::Reference< Projection>& p, const ::indigo::view::ViewPort& vp)throws();

            /**
             * Get the current viewport.
             * @return the current viewport for this map.
             */
         public:
            inline const ::indigo::view::ViewPort& viewPort() const throws() {return *_viewPort;}

            /**
             * Set the viewport for this map. The projection remains the same,
             * All layers are notified.
             *
             * @param viewPort the new viewport
             */
         public:
            virtual void setViewPort(const ::indigo::view::ViewPort& viewPort)throws();

            /**
             * Determine if the specified layer is enabled.
             * @param layer a layer
             * @return true if the layer is enabled, false otherwise
             */
         public:
            bool isEnabled(const ::timber::Reference< Layer>& layer) const throws();

            /**
             * Enable or disable a specified layer.
             * @param layer a layer
             * @param enable true to enable the layer, false to disable
             */
         public:
            void setEnabled(const ::timber::Reference< Layer>& layer, bool enable)throws();

            /**
             * Get the number of layers.
             * @return the number of layers
             */
         public:
            inline size_t layerCount() const throws() {return _layers.size();}

            /**
             * Get the layer at the specified index.
             * @param pos the position of the layer
             * @return the layer at the specified position.
             * @pre pos < layerCount()
             */
         public:
            ::timber::Reference< Layer> layer(size_t pos) const throws();

            /**
             * Add a new layer to the top of the map.
             * @param layer a layer
             * @throws ::std::exception if the layer already exists
             */
         public:
            void addLayer(const ::timber::Reference< Layer>& layer)
            throws (::std::exception);

            /**
             * Insert a new layer at the specified position. If the @code pos==layerCount() @endcode,
             * then the layer is added at the top via addLayer.
             * To add a layer at the bottom, set pos = 0
             *
             * @param layer a layer
             * @param pos the position
             * @throws ::std::exception if the layer already exists
             * @throws ::std::exception if pos > layerCount()
             */
         public:
            virtual void insertLayer(const ::timber::Reference< Layer>& layer, size_t pos)
            throws (::std::exception);

            /**
             *  Remove a layer
             * @param layer the layer to be removed
             * @return true if the layer was removed, false if it was not in the map
             */
         public:
            virtual bool removeLayer(const ::timber::Reference< Layer>& layer)throws ();

            /**
             * Remove all layers.
             */
         public:
            void clearLayers()throws();

            /**
             * Prepare all layers for rendering. Note that it is expected that layers
             * generate change notifications in response to this method.
             */
         public:
            void prepare()throws();

            /**
             * Move a layer up or down by the specified number of slots. A negative number
             * means the layer will be moved to the bottom, a positive number means it will be
             * moved towards the top.
             * @param layer a layer
             * @param delta the number of slots to move the layer
             * @return true if the layer was moved, false otherwise
             */
         public:
            virtual bool moveLayer(const ::timber::Reference< Layer>& layer, ::timber::Int delta)throws();

            /**
             * The map has changed and at least a repaint is needed. The layers
             * must be examined to determine if any reprojections are needed
             * as well.
             * @note Subclass that override this method must invoke @code Map::mapChanged() @endcode explicitly.
             */
         protected:
            virtual void fireMapChanged()throws();

            /**
             * Notify observers that this map has changed
             */
         private:
            void notifyObservers()throws();

            /**
             * Notify observers that a layer on this map has changed.
             * @param e the layer event
             */
         private:
            void notifyObservers(const LayerEvent& e)throws();

            /** A list of layer entries (sorted bottom-layer to top-layer) */
         private:
            Layers _layers;

            /** The listeners */
         private:
            Observers _observers;

            /** The current viewport */
         private:
            ::std::unique_ptr< ::indigo::view::ViewPort> _viewPort;

            /** The current projection */
         private:
            ::timber::Reference< Projection> _projection;
      };
   }
}
#endif
