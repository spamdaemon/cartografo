#ifndef _CARTOGRAFO_MAP_MAPEVENT_H
#define _CARTOGRAFO_MAP_MAPEVENT_H

#ifndef _CARTOGRAFO_MAP_MAP_H
#include <cartografo/map/Map.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  namespace map {
    /**
     * This class is created by Scene graph objects.
     * and is passed to MapObserver.
     */
    class MapEvent {
      /** Destroy this event */
    public:
      virtual ~MapEvent () throws();

      /** 
       * Create a new MapEvent 
       * @param g the source graphic object
       */
    public:
      inline MapEvent (const ::timber::Reference<Map>& g) throws()
	: _map(g) {}
      
      /** 
       * Get the source object.
       * @return the source object
       */
    public:
      inline ::timber::Reference<Map> source() const throws()
      { return _map; }

      /** The source object */
    private:
      ::timber::Reference<Map> _map;
    };
  }
}
#endif
