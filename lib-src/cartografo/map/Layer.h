#ifndef _CARTOGRAFO_LAYER_H
#define _CARTOGRAFO_LAYER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  class Projection;
  namespace geo {
    class Node;
  }

  namespace map {
    class Map;
    class LayerObserver;
    class ViewPort;

    /**
     * The layer is typically used to present logically coherent information
     * on Map. A map typically has many different layers. Each layer can only
     * be attached to a single map, but the nodes of the Layer may be shared
     * if possible, across maps.
     * Subclasses of the layer, use setRootNode() to specify what is to be rendered
     * on the layer.
     */
    class Layer : public ::timber::Counted {
      Layer(const Layer&);
      Layer&operator=(const Layer&);

      /** The map is a friend */
    public:
      friend class Map;

      /** The nodes */
    private:
      class Nodes;

      /** The nodes are a friend */
    public:
      friend class Nodes;

      /** Default constructor. */
    protected:
      Layer() throws();
      
      /** Destructor */
    public:
      ~Layer() throws();

      /**
       * This method is invoked on the layer when it is added to the map.
       * @param map the map to which this layer was attached
       * @throws ::std::exception if this map was already attached
       */
    private:
      void notifyLayerAttached (Map& map) throws(::std::exception);

      /**
       * This method is invoked when this layer has been removed from its map
       * @param map the map from which this layer was detached
       * @throws ::std::exception if this map is not attached to the given map
       */
    private:
      void notifyLayerDetached (Map& map) throws();

      /**
       * This method is invoked to inform this layer it was added to a map
       * @param map the map to which this layer was attached
       */
    protected:
      virtual void notifyAttached (Map& map) throws();

      /**
       * This method is invoked to inform this layer that it was removed from a map.
       * @param map the map from which this layer was detached
       */
    protected:
      virtual void notifyDetached(Map& map) throws();

      /**
       * Add a layer observer. This observer is notified whenever
       * the graphics for this layer needs to be repainted and (or) reprojected.
       * @param observer the layer observer
       */
    private:
      void addObserver (LayerObserver* observer) throws();

      /**
       * Remove a layer observer
       * @param observer the observer to be removed
       */
    private:
      void removeObserver (LayerObserver* observer) throws();

      /** 
       * The set the root node for this layer. The layer monitors the node
       * for changes to it or to nodes below. When a change is detected, it 
       * notifies the layer observer.
       * @param node a node or null to clear this layer
       **/
    protected:
      void setRootNode (const ::timber::Pointer< ::cartografo::geo::Node>& node);

      /**
       * Get the root node for this layer
       * @return the root node for this layer
       */
    public:
      ::timber::Pointer< ::cartografo::geo::Node> rootNode() const throws();

      /**
       * The layer has changed. This function can be invoked manually, usually
       * it is invoked whenever the layer's root node detected a change to the
       * tree rooted at the current node.
       * @note This method must invoked from a subclasses if overriden.
       */
    protected:
      void fireLayerChanged() throws();
      
      /** 
       * Notify this layer that a new map projection is in effect. A change to the map
       * projection may also have caused a change to the viewport.
       * @param map the map whose projection and viewport has changed
       */
    protected:
      virtual void notifyProjectionChanged (Map& map) throws();

      /**
       * Notify this layer that the viewport has changed.
       * @param map the map whose viewport has changed
       */
    protected:
      virtual void notifyViewPortChanged (Map& map) throws();

      /**
       * Prepare this layer for rendering. This method is invoked in response to a notification that the
       * layer has been changed. It must complete quickly.
       * @param map the map on which this layer resides
       */
    protected:
      virtual void prepare(Map& map) throws();

      
      /** The node graph for this layer; it holds all the information necessary to render this layer */
    private:
      ::std::unique_ptr<Nodes> _nodes;
      
      /** The current map */
    private:
      Map* _map;
    };
  }
}
#endif
