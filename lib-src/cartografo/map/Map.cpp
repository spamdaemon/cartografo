#include <cartografo/map/Map.h>
#include <cartografo/map/Layer.h>
#include <cartografo/map/LayerObserver.h>
#include <cartografo/map/LayerEvent.h>
#include <cartografo/map/MapObserver.h>
#include <cartografo/map/MapEvent.h>
#include <cartografo/geo/Node.h>
#include <cartografo/geo/NodeVisitor.h>
#include <cartografo/Projection.h>
#include <cartografo/projection/IdentityProjection.h>
#include <terra/GeodeticCoordinates.h>

#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace {
   static Log logger()
   {
      return Log("cartografo.map.Map");
   }
}

namespace cartografo {
   namespace map {

      namespace {
         struct MapChangedDispatchFunction
         {
               inline MapChangedDispatchFunction(const MapEvent& e)
                     : _event(e)
               {
               }

               inline void operator()(Pointer< MapObserver> obs)
               {
                  obs->mapChanged(_event);
               }

            private:
               const MapEvent& _event;
         };

         struct LayerChangedDispatchFunction
         {
               inline LayerChangedDispatchFunction(const LayerEvent& e)
                     : _event(e)
               {
               }

               inline void operator()(Pointer< MapObserver> obs)
               {
                  LogEntry(logger()).debugging() << "Notifying map observer " << typeid(*obs).name() << doLog;
                  obs->layerChanged(_event);
               }

            private:
               const LayerEvent& _event;
         };
      }

      Map::LayerEntry::LayerEntry()
      throws()
      : _enabled(true),
      _owner(0)
      {}

      Map::LayerEntry::~LayerEntry()
      throws()
      {}

      void Map::LayerEntry::layerChanged(const LayerEvent& e)
      throws()
      {
         logger().debugging("Layer changed; need to reconstruct");
         if(_owner) {
            _owner->notifyObservers(e);
         }
         else {
            logger().debugging("No owner");
         }
      }

      Map::Map()
      throws()
      :
      _viewPort(new ::indigo::view::ViewPort()),
      _projection(::cartografo::projection::IdentityProjection::create(::terra::GeodeticCoordinates(0,0)))
      {
      }

      Map::~Map()
      throws()
      {
         logger().info("Destructor");
         Map::clearLayers();
      }

      bool Map::isEnabled(const Reference< Layer>& xlayer) const
      throws()
      {
         for (Layers::const_iterator i=_layers.begin();i!=_layers.end();++i) {
            if ((*i)->_layer == xlayer) {
               return (*i)->_enabled;
            }
         }
         return false;
      }
      void Map::setEnabled(const ::timber::Reference< Layer>& xlayer, bool enable)
      throws()
      {
         for (Layers::iterator i=_layers.begin();i!=_layers.end();++i) {
            if ((*i)->_layer == xlayer) {
               if ((*i)->_enabled != enable) {
                  (*i)->_enabled = enable;
               }
            }
         }
      }

      ::timber::Reference< Layer> Map::layer(size_t pos) const
      throws()
      {
         assert(pos<layerCount());
         return _layers[pos]->_layer;
      }

      void Map::addLayer(const Reference< Layer>& xlayer)
throws   (::std::exception)
   {  insertLayer(xlayer,_layers.size());}

   void Map::insertLayer(const Reference< Layer>& xlayer, size_t pos)
   throws (::std::exception)
   {
      if (pos > _layers.size()) {
         throw ::std::invalid_argument("Invalid position");
      }

      for (Layers::const_iterator i=_layers.begin();i!=_layers.end();++i) {
         if ((*i)->_layer == xlayer) {
            throw ::std::invalid_argument("Layer already added to map");
         }
      }

      // notify the layer that it's been attached; do this before changing
      // any state in this class
      xlayer->notifyLayerAttached(*this);
      xlayer->notifyProjectionChanged(*this);

      ::std::unique_ptr< LayerEntry> e(new LayerEntry());
if   (pos==_layers.size()) {
      _layers.push_back(e.get());
   }
   else {
      _layers.insert(_layers.begin()+pos,e.get());
   }

   e->_layer = xlayer;
   e->_owner = this;
   e->_layer->addObserver(e.get());
   e.release();
   fireMapChanged();
}

void Map::clearLayers()
throws()
{
   const Layers l(_layers);
   for (auto i=l.begin();i!=l.end();++i) {
      removeLayer((*i)->_layer);
   }
}

bool Map::removeLayer(const Reference< Layer>& xlayer)
throws()
{
   for (Layers::iterator i=_layers.begin();i!=_layers.end();++i) {
      if ((*i)->_layer == xlayer) {
         (*i)->_layer->removeObserver(*i);
         delete *i;
         _layers.erase(i);

         xlayer->notifyLayerDetached(*this);
         fireMapChanged();

         return true;
      }
   }
   return false; // no such layer
}

bool Map::moveLayer(const Reference< Layer>& xlayer, Int delta)
throws()
{
   if (delta==0) {
      return false;
   }

   for (Layers::iterator i=_layers.begin();i!=_layers.end();++i) {
      if ((*i)->_layer == xlayer) {
         bool moved=false;
         // keep in mind that the the top-layer is at the end of the vector
         // not the beginning.
         Layers::iterator end = _layers.begin();
         int dPos = -1;
         if (delta>0) {
            end = _layers.end()-1;
            dPos = +1;
         }
         while(i!=end && delta!=0) {
            Layers::iterator j = i+dPos;
            LayerEntry* tmp = *i;
            *i = *j;
            *j = tmp;
            i = j;
            moved=true;
            // reduce delta towards 0
            delta -= dPos;
         }
         if (moved) {
            fireMapChanged();
         }
         return moved;
      }
   }
   return false; // no such layer
}

void Map::prepare()
throws()
{
   logger().debugging("Prepare map");
   vector<Reference<Layer> > L;
   for (Layers::iterator i = _layers.begin();i!=_layers.end();++i) {
      if ((*i)->_layer) {
         L.push_back((*i)->_layer);
      }
   }
   while(!L.empty()) {
      L.back()->prepare(*this);
      L.pop_back();
   }
}

void Map::fireMapChanged()
throws()
{
   logger().debugging("Map changed");
   notifyObservers();
}

void Map::addObserver(const Reference< MapObserver> observer)
throws()
{  _observers.add(observer);}

void Map::removeObserver(const Pointer< MapObserver> observer)
throws()
{  _observers.remove(observer);}

void Map::notifyObservers()
throws()
{
   // only notify the observer if there is a layer,
   // and that layer has been completely instantiated
   // which means its reference count would be non-zero
   if (refCount() > 0 && !_observers.isEmpty()) {
      Reference<Map> src(toRef<Map>());
      MapEvent event(src);
      MapChangedDispatchFunction f(event);
      logger().debugging("Notifying map observers");
      _observers.dispatch(f);
   }
}

void Map::notifyObservers(const LayerEvent& e)
throws()
{
   // only notify the observer if there is a layer,
   // and that layer has been completely instantiated
   // which means its reference count would be non-zero
   if (refCount() > 0 && !_observers.isEmpty()) {
      LayerChangedDispatchFunction f(e);
      logger().debugging("Notifying map observers");
      _observers.dispatch(f);
   }
}

void Map::setProjection(const ::timber::Reference< Projection>& p, const ::indigo::view::ViewPort& vp)
throws()
{
   vector<Reference<Layer> > L;

   _projection = p;
   _viewPort.reset(new ::indigo::view::ViewPort(vp.bounds(),vp.viewWindow()));

   for (Layers::iterator i = _layers.begin();i!=_layers.end();++i) {
      if ((*i)->_layer) {
         L.push_back((*i)->_layer);
      }
   }
   while(!L.empty()) {
      L.back()->notifyProjectionChanged(*this);
      L.pop_back();
   }
}

void Map::setViewPort(const ::indigo::view::ViewPort& vp)
throws()
{
   vector<Reference<Layer> > L;

   _viewPort.reset(new ::indigo::view::ViewPort(vp.bounds(),vp.viewWindow()));

   for (Layers::iterator i = _layers.begin();i!=_layers.end();++i) {
      if ((*i)->_layer) {
         L.push_back((*i)->_layer);
      }
   }
   while(!L.empty()) {
      L.back()->notifyViewPortChanged(*this);
      L.pop_back();
   }
}

}
}
