#ifndef _CARTOGRAFO_GEO_MAPOBSERVER_H
#define _CARTOGRAFO_GEO_MAPOBSERVER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  namespace map {
    class MapEvent;
    class LayerEvent;
  
    /**
     * This class provides the interface by which 
     * changes to a map can be propagated.
     */
    class MapObserver : public ::timber::Counted {
      MapObserver(const MapObserver&);
      MapObserver&operator=(const MapObserver&);


      /** Default constructor */
    protected:
      inline MapObserver() throws() {}

      /** Destroy this observer */
    public:
      ~MapObserver() throws();
      
      /** 
       * A layer was modified. 
       * @param e a layer event
       */
    public:
      virtual void layerChanged (const LayerEvent& e) throws() = 0;
      
      /**
       * A map has changed.
       * @param e a map event
       */
    public:
      virtual void mapChanged (const MapEvent& e) throws() = 0;	
    };
  }
}
#endif
