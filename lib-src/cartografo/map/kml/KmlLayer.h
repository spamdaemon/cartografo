#ifndef _CARTOGRAFO_MAP_KML_KMLAYER_H
#define _CARTOGRAFO_MAP_KML_KMLAYER_H

#ifndef _TIMBER_W3C_URI_H
#include <timber/w3c/URI.h>
#endif

#ifndef _CARTOGRAFO_MAP_LAYER_H
#include <cartografo/map/Layer.h>
#endif

#ifndef _CARTOGRAFO_GEO_GROUP_H
#include <cartografo/geo/Group.h>
#endif

#ifndef _TERRA_GEODETICDATUM_H
#include <terra/GeodeticDatum.h>
#endif

namespace cartografo {
   namespace kml {
      class KmlFactory;
   }

   namespace map {
      namespace kml {

         /**
          * This layer can be used to display the contents of a single ESRI shapefile.
          */
         class KmlLayer : public Layer
         {
               /**
                * Default constructor.
                * @param provider the raster provider to use
                */
            private:
               KmlLayer()throws();

               /** Destructor */
            public:
               ~KmlLayer()throws();

               /**
                * Create a new KML layer containing the contents from the specified KML URI.
                * @param uri the uri of the kmlfile to load (this must be an absolute URI)
                * @param datum the datum to use
                * @param factory the KML factory to use
                * @return a layer or null if the file could not be opened
                */
            public:
               static ::timber::Pointer< KmlLayer> create(const ::timber::SharedRef< ::timber::w3c::URI>& uri, const ::timber::Reference< ::terra::GeodeticDatum> & datum,
                     const ::timber::Reference< ::cartografo::kml::KmlFactory> factory)throws();

               /**
                * Create a new KML layer containing the contents from the specified KML URI.
                * @param uri the uri of the kmlfile to load (this must be an absolute URI)
                * @param datum the datum to use
                * @return a layer or null if the file could not be opened
                */
            public:
               static ::timber::Pointer< KmlLayer> create(const ::timber::SharedRef< ::timber::w3c::URI>& uri, const ::timber::Reference< ::terra::GeodeticDatum> & datum)throws();

               /**
                * Create a new KML layer containing the contents from the specified KML URI.
                * @param uri the URI or filename of a KML file
                * @param datum the datum to use
                * @param factory the KML factory to use
                * @return a layer or null if the file could not be opened
                */
            public:
               static ::timber::Pointer< KmlLayer> create(const ::std::string& uri, const ::timber::Reference< ::terra::GeodeticDatum> & datum,
                     const ::timber::Reference< ::cartografo::kml::KmlFactory> factory)throws();

               /**
                * Create a new KML layer containing the contents from the specified KML URI.
                * @param uri the URI or filename of a KML file
                * @param datum the datum to use
                * @return a layer or null if the file could not be opened
                */
            public:
               static ::timber::Pointer< KmlLayer> create(const std::string& uri, const ::timber::Reference< ::terra::GeodeticDatum> & datum)throws();
         };

      }

   }
}

#endif
