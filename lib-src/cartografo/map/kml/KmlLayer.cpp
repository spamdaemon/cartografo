#include <cartografo/map/kml/KmlLayer.h>
#include <cartografo/kml/KmlFactory.h>
#include <cartografo/kml/KmlNode.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
   namespace map {
      namespace kml {
         namespace {
            static Log logger()
            {
               return Log("cartografo.map.kml.KmlLayer");
            }

            static ::std::shared_ptr< ::timber::w3c::URI> createURI(const ::std::string& kmlURI)
         throws ()
         {
            ::std::shared_ptr< ::timber::w3c::URI> res;
            LogEntry(logger()).debugging() << "Create KML node for \n" << kmlURI << doLog;

            try {
               res = ::timber::w3c::URI::create(kmlURI);
               // if the URI has no scheme then we need to treat it as a file
               if (res->scheme()) {
                  return res;
               }
            }
            catch (const ::std::exception& e) {
               LogEntry(logger()).debugging() << "Cannot create URI for " << kmlURI << "; treating as file" << doLog;
            }

            try {
               res = timber::w3c::URI::createFile(kmlURI);
            }
            catch (const ::std::exception& e) {
               LogEntry(logger()).debugging() << "Could not create file URI for " << kmlURI << doLog;
            }
            return res;
         }
      }

      KmlLayer::KmlLayer()
      throws()
      {}

      KmlLayer::~KmlLayer()
      throws()
      {
      }

      ::timber::Pointer< KmlLayer> KmlLayer::create(const SharedRef< ::timber::w3c::URI>& uri,
            const ::timber::Reference< ::terra::GeodeticDatum> & datum,
            const ::timber::Reference< ::cartografo::kml::KmlFactory> factory)
      throws ()
      {
         ::timber::Pointer< KmlLayer> layer;
         try {

            auto group(new ::cartografo::geo::Group());
            auto kn = factory->createNode(uri,datum);
            group->add(kn);
            layer = new KmlLayer();
            layer->setRootNode(group);
            LogEntry(logger()).info() << "Loaded KML file " << *uri << doLog;
         }
         catch (const ::std::exception& e) {
            LogEntry(logger()).caught(e) << "Failed to create kml node for : " << *uri << doLog;

         }
         return layer;

      }

      ::timber::Pointer< KmlLayer> KmlLayer::create(const SharedRef< ::timber::w3c::URI>& uri,
            const ::timber::Reference< ::terra::GeodeticDatum> & datum)
      throws()
      {
         try {
            return create(uri, datum,::cartografo::kml::KmlFactory::newFactory());
         }
         catch (const ::std::exception& e) {
            LogEntry(logger()).caught(e) << "Could not create KmlFactory" << doLog;
         }
         return ::timber::Pointer< KmlLayer>();
      }

      ::timber::Pointer< KmlLayer> KmlLayer::create(const ::std::string& kmluri,
            const ::timber::Reference< ::terra::GeodeticDatum> & datum,
            const ::timber::Reference< ::cartografo::kml::KmlFactory> factory)
      throws()
      {
         auto uri = createURI(kmluri);
         if (uri) {
            return create(uri,datum,factory);
         }
         else {
            return ::timber::Pointer< KmlLayer>();
         }
      }

      ::timber::Pointer< KmlLayer> KmlLayer::KmlLayer::create(const std::string& kmluri,
            const ::timber::Reference< ::terra::GeodeticDatum> & datum)
   throws ()
   {
      auto uri = createURI(kmluri);
      if (uri) {
         return create(uri,datum);
      }
      else {
         return ::timber::Pointer< KmlLayer>();
      }
   }

}

}
}
