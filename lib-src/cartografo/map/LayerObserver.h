#ifndef _CARTOGRAFO_GEO_LAYEROBSERVER_H
#define _CARTOGRAFO_GEO_LAYEROBSERVER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  namespace map {
    /** A graphic event */
    class LayerEvent;
  
    /**
     * This class provides the interface by which 
     * changes to a layer can be propagated.
     */
    class LayerObserver {
      LayerObserver(const LayerObserver&);
      LayerObserver&operator=(const LayerObserver&);

      /** Default constructor */
    protected:
      inline LayerObserver() throws() {}

      /** Destroy this observer */
    public:
      virtual ~LayerObserver() throws() = 0;
    
      /**
       * A layer has changed.
       * @param e a layer event
       */
    public:
      virtual void layerChanged (const LayerEvent& e) throws() = 0;	
    };
  }
}
#endif
