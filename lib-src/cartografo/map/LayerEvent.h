#ifndef _CARTOGRAFO_MAP_LAYEREVENT_H
#define _CARTOGRAFO_MAP_LAYEREVENT_H

#ifndef _CARTOGRAFO_MAP_LAYER_H
#include <cartografo/map/Layer.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  namespace geo {
    class Node;
  }

  namespace map {
    /**
     * This class is created by Scene graph objects.
     * and is passed to LayerObserver.
     */
    class LayerEvent {

      /** The type of layer event */
    public:
      enum EventType {
	/** 
	 * The Node modified event indicates that a node on the layer was changed.
	 * In this case, the LayerEvent has a valid node() object
	 */
	NODE_MODIFIED, 

	/** 
	 * The layer was modified in some way that cannot be expressed through 
	 * NODE_MODIFIED; this event type is rare.
	 */
	LAYER_MODIFIED,
      };

      /** Destroy this event */
    public:
      virtual ~LayerEvent () throws();

      /** 
       * Create a new LayerEvent 
       * @param t the event type
       * @param l the layer that generated this event
       * @param n an optional node that might have been modified
       */
    public:
      LayerEvent (const EventType& t, const ::timber::Reference<Layer>& g, const ::timber::Pointer< ::cartografo::geo::Node>& node) throws();
      
      /**
       * Get the event type
       * @return the event type
       */
    public:
      inline EventType type() const throws() { return _type; }

      /** 
       * Get the source object.
       * @return the source object
       */
    public:
      inline ::timber::Reference<Layer> source() const throws() { return _layer; }

      /** 
       * Get the node that was modified. This method only returns non-null
       * if the event type indicates NODE_MODIFIED.
       * @return the modified node
       */
    public:
      inline ::timber::Pointer< ::cartografo::geo::Node> node() const throws() { return _node; }

      /** The event type */
    private:
      EventType _type;

      /** The source object */
    private:
      ::timber::Reference<Layer> _layer;

      /** The node */
    private:
      ::timber::Pointer< ::cartografo::geo::Node> _node;
    };
  }
}
#endif
