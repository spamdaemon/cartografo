#ifndef _CARTOGRAFO_MAP_LAYERS_GRATICULELAYER_H
#define _CARTOGRAFO_MAP_LAYERS_GRATICULELAYER_H

#ifndef _CARTOGRAFO_MAP_LAYER_H
#include <cartografo/map/Layer.h>
#endif

#ifndef _CARTOGRAFO_GEO_GROUP_H
#include <cartografo/geo/Group.h>
#endif

#ifndef _TERRA_GEODETICDATUM_H
#include <terra/GeodeticDatum.h>
#endif

namespace cartografo {
   namespace map {
      namespace layers {

         /**
          * This layers drawlines equally spaced lines of latitude and longitude.
          */
         class GraticuleLayer : public Layer
         {
               /** The default graticule spacing in both latitude and longitude */
            public:
               static constexpr double DEFAULT_GRATICULE_SPACING = 45;

               /**
                * Create a new Graticule layer.
                * @param datum the datum in which the graticules are specified
                * @param spacingDeg the spacing of the lines of latitude and longitude in degrees.
                */
            public:
               GraticuleLayer(const ::timber::Reference< ::terra::GeodeticDatum>& datum, double spacingDeg =
                     DEFAULT_GRATICULE_SPACING)throws();

               /** Destructor */
            public:
               ~GraticuleLayer()throws();
         };

      }

   }
}

#endif
