#include <cartografo/map/layers/GraticuleLayer.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/geo/nodes.h>

#include <terra/GeodeticCoordinates.h>
#include <indigo/nodes.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
   namespace map {
      namespace layers {
         namespace {
            static Log logger()
            {
               return Log("cartografo.map.layers.GraticuleLayer");
            }

            static Pointer< ::cartografo::geo::Paths> createMeridians(double spacingDeg,
                  const Reference< GeodeticDatum>& datum)
            {
               const double N = toRadians(spacingDeg);

               PathBuilder pb;
               for (double lon = toRadians(-180.); lon <= toRadians(180.) - N / 2; lon += N) {
                  pb.moveTo(GeodeticCoordinates(-::terra::PI / 2, lon, datum));
                  pb.loxodromeTo(GeodeticCoordinates(0, lon, datum));
                  pb.loxodromeTo(GeodeticCoordinates(::terra::PI / 2, lon, datum));
               }
               return new ::cartografo::geo::Paths(pb.path());
            }

            static Pointer< ::cartografo::geo::Paths> createParallels(double spacingDeg,
                  const Reference< GeodeticDatum>& datum)
            {
               const double N = toRadians(spacingDeg);

               PathBuilder pb;
               double lat = -::terra::PI / 2 + 0.05;
               pb.moveTo(GeodeticCoordinates(lat, -::terra::PI + toRadians(.1), datum));
               pb.loxodromeTo(GeodeticCoordinates(lat, 0, datum));
               pb.loxodromeTo(GeodeticCoordinates(lat, ::terra::PI - toRadians(.10), datum));

               for (lat = -::terra::PI / 2 + N; ::std::abs(lat - ::terra::PI / 2) > N / 2; lat += N) {
                  pb.moveTo(GeodeticCoordinates(lat, -::terra::PI + toRadians(.1), datum));
                  pb.loxodromeTo(GeodeticCoordinates(lat, 0, datum));
                  pb.loxodromeTo(GeodeticCoordinates(lat, ::terra::PI - toRadians(.10), datum));
               }

               lat = ::terra::PI / 2 - .05;
               pb.moveTo(GeodeticCoordinates(lat, -::terra::PI + toRadians(.1), datum));
               pb.loxodromeTo(GeodeticCoordinates(lat, 0, datum));
               pb.loxodromeTo(GeodeticCoordinates(lat, ::terra::PI - toRadians(.10), datum));
               return new ::cartografo::geo::Paths(pb.path());
            }
         }

         GraticuleLayer::GraticuleLayer(const Reference< GeodeticDatum>& datum, double spacingDeg)
         throws()
         {
            Reference< ::cartografo::geo::Group> G(new ::cartografo::geo::Group(true));
            G->add(new ::cartografo::geo::IndigoNode(new ::indigo::StrokeNode(::indigo::Color::LIGHT_GREY, 1)));
            G->add(createMeridians(spacingDeg, datum));
            G->add(createParallels(spacingDeg, datum));

            setRootNode(G);
         }

         GraticuleLayer::~GraticuleLayer()
         throws ()
         {
         }

      }

   }
}
