#include <cartografo/map/MapRenderer.h>
#include <cartografo/geo/nodes.h>
#include <cartografo/geo/NodeEvent.h>
#include <cartografo/geo/NodeObserver.h>
#include <cartografo/geo/NodeVisitor.h>
#include <cartografo/Projector2D.h>
#include <cartografo/ProjectionDomain.h>
#include <cartografo/ProjectionMapping.h>
#include <cartografo/Projection.h>
#include <cartografo/ExtentPyramid.h>

#include <timber/logging.h>

#include <indigo/nodes.h>
#include <indigo/AffineTransform2D.h>

#include <tikal/Region2D.h>
#include <tikal/Area2D.h>
#include <tikal/Path2D.h>

#include <canopy/time/Time.h>

#include <vector>
#include <map>

#define ENABLE_TIMING 0

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;
using namespace ::cartografo::geo;

namespace cartografo {
   namespace map {
      namespace {
         /**
          * The node cache allows us to reuse nodes that have already been processed.
          * Raw pointers can be used because its scope is only the rendering traversal stage
          */
         struct CacheEntry
         {
               inline CacheEntry()throws() : node(0) {}
               ::indigo::Node* node;
         };

         typedef ::std::map< const ::cartografo::geo::Node*, CacheEntry> NodeCache;

         typedef ::std::multimap< Pointer< ::cartografo::Region>, Pointer< ::tikal::Region2D> > RegionCache;
         typedef ::std::multimap< Pointer< ::cartografo::Path>, Pointer< ::tikal::Path2D> > PathCache;

         static Log logger()
         {
            return Log("cartografo.map.MapRenderer");
         }
      }

      constexpr double MapRenderer::DEFAULT_PRECISION;

      struct MapRenderer::Renderer : public NodeVisitor
      {

            Renderer(Projector2D& p, const Pointer< ExtentPyramid>& pyramid)
                  : _precision(DEFAULT_PRECISION), _projector(p), _resetTransformMode(
                        new ::indigo::TransformMode(::indigo::TransformMode::RESET)), _concatTransformMode(
                        new ::indigo::TransformMode(::indigo::TransformMode::CONCATENATE)), _pyramid(pyramid), _logger(
                        logger())
            {
            }

            ~Renderer()throws() {}

            void visit(const IndigoNode& g)throws()
            {
               Pointer< ::indigo::Node> n = g.node();
               if (n) {
                  _outputStack.back()->add(n);
               }
            }

            void visit(const Raster& r)throws()
            {
               const RasterImage& raster = r.raster();
               const RasterImage::ImagePtr& image = raster.image();
               if (!image || image->width()==0 || image->height()==0) {
                  logger().debugging("No image for raster");
                  return;
               }

               Reference< ::indigo::Group> n(new ::indigo::Group(2,false));
               Reference< ::indigo::Node> root(new ::indigo::Separator(n));

               CacheEntry& e = _cache[&r];
               assert(e.node==0);
               e.node = root.unsafePointer();

               RasterImage::Bounds bounds = raster.bounds();
               const double rotation = 0;

               // project the point by setting a new coordinate system
               if (bounds.setCRS(_projector.projection())) {
                  const ::terra::Location& tl = bounds.topLeft();
                  const ::terra::Location& tr = bounds.topRight();
                  const ::terra::Location& bl = bounds.bottomLeft();

                  n->add(new ::indigo::Raster(image,{tl[0],tl[1]},{tr[0],tr[1]},{bl[0],bl[1]}));
               }
               else {
                  logger().debugging("Could not project raster");
               }
               _outputStack.back()->add(root);
            }

            void visit(const Locations& g)throws()
            {
               Reference< ::indigo::Points> n(new ::indigo::Points());

               CacheEntry& e = _cache[&g];
               assert(e.node==0);
               e.node = n.unsafePointer();

               _outputStack.back()->add(n);
               n->setPointSize(g.pointSize());

               ProjectionMapping m;
               Reference<Projection> P = _projector.projection();
               for (size_t i=0;i<g.count();++i) {
                  const Locations::GeodeticCoordinates& p = g.shape(i);
                  P->project(p.latitude(),p.longitude(),m);
                  if (m.type()==ProjectionMapping::POINT) {
                     n->add(::indigo::Point(m.point().x(),m.point().y()));
                  }
               }
            }

            void visit(const Regions& g)throws()
            {
               Reference< ::indigo::Group> n(new ::indigo::Group());

               CacheEntry& e = _cache[&g];
               assert(e.node==0);
               e.node = n.unsafePointer();

               _outputStack.back()->add(n);

               Reference< ::indigo::Regions> regs(new ::indigo::Regions());
               n->add(regs);

               // create a coordinates node
               // project each region
               for (size_t i=g.count();i-- > 0; ) {
                  Pointer<Region> R = g.shape(i);
                  if (!R) {
                     continue;
                  }

                  // if the region was already projected and cached then use that
                  pair<RegionCache::iterator,RegionCache::iterator> pi = _regionCache.equal_range(R.unsafePointer());
                  if (pi.first!=pi.second) {
                     while(pi.first != pi.second) {
                        regs->add(pi.first->second);
                        _xRegionCache.insert(*pi.first); // do not forget to add this
                        ++pi.first;
                     }
                     continue;
                  }
                  else {
#if ENABLE_TIMING == 1
                     ::canopy::time::Time startTime;
#endif
                     Region::Classification c;
                     if (_pyramid) {
                        c = _pyramid->classifyExtent(R->extent());
                     }
                     else {
                        c = _projector.projection()->domain()->region()->classifyRegion(*R);
                     }
                     switch(c) {
                        case Region::OUTSIDE: ++_nOutside; break;
                        case Region::CONTAINED: ++_nInside; break;
                        case Region::POSSIBLE_OVERLAP: ++_nOverlaps; break;
                     }

                     if (c==Region::OUTSIDE) {
                        //	      logger().debugging("Region is outside");
                        continue;
                     }
                     R->project(_projector);

#if ENABLE_TIMING == 1
                     ::canopy::time::Time endTime;

                     if (_logger.isLoggable(Level::DEBUGGING)) {
                        ULong dtimeMS = (endTime.time() - startTime.time())/1000000;
                        if(dtimeMS>100) {
                           LogEntry(_logger).level(Level::DEBUGGING) << "Time to project " << dtimeMS << "ms" << doLog;
                        }
                     }
                     startTime = endTime;
#endif

                     _projector.getRegions(_projRegions);

#if 0
                     LogEntry(_logger).warn() << "Extent classification : " << R->extent() << ": " << c << "; outside ? " << _projRegions.empty() << doLog;
#endif

                     // if there's no projection, the continue; otherwise
                     // determine the regions classification so we know
                     // if we have to deal with boundaries
                     if (_projRegions.empty()) {
                        continue;
                     }
#if ENABLE_TIMING == 1
                     endTime = ::canopy::time::Time();
                     if (_logger.isLoggable(Level::DEBUGGING)) {
                        ULong dtimeMS = (endTime.time() - startTime.time())/1000000;
                        if(dtimeMS>100) { // put a limit on when to report
                           LogEntry(_logger).level(Level::DEBUGGING) << "Time to rebuild " << dtimeMS << "ms" << doLog;
                        }
                     }
#endif	    
                     for (size_t j=0;j<_projRegions.size();++j) {
                        Pointer<Region2D> reg = _projRegions[j];
                        if (!reg) {
                           continue;
                        }
#if ENABLE_TIMING == 1
                        startTime = ::canopy::time::Time();
#endif
                        // we check if the renderer was entirely contained inside the projection's domain; if so, we can be
                        // sure that we don't have to patch up boundary patches; if there was any clipping, then we may have to
                        // deal with patching up polygons that are clipped into 2 separate polygons, but which really should be
                        // a single polygon
                        if (c!=Region::CONTAINED && _precision!=0) {
                           reg = reg->reducePrecision(_precision);
                        }

#if ENABLE_TIMING == 1
                        endTime = ::canopy::time::Time();
                        if (_logger.isLoggable(Level::DEBUGGING)) {
                           ULong dtimeMS = (endTime.time() - startTime.time())/1000000;
                           if(dtimeMS>50) {
                              LogEntry(_logger).level(Level::DEBUGGING) << "Time to reduce precision " << dtimeMS << "ms" << doLog;
                           }
                        }
#endif
                        if (!reg) {
                           continue;
                        }
                        regs->add(reg);
                        _regionCache.insert(RegionCache::value_type(R.unsafePointer(),reg.unsafePointer()));
                        _xRegionCache.insert(RegionCache::value_type(R.unsafePointer(),reg.unsafePointer()));
                     }

                     _projRegions.clear();
                  }
               }

            }

            void visit(const Paths& g)throws()
            {
               Reference< ::indigo::Group> n(new ::indigo::Group());

               CacheEntry& e = _cache[&g];
               assert(e.node==0);
               e.node = n.unsafePointer();

               _outputStack.back()->add(n);
               Reference< ::indigo::Paths> paths(new ::indigo::Paths());
               n->add(paths);

               // create a coordinates node
               // project each region
               for (size_t i=g.count();i-- > 0; ) {
                  Pointer<Path> P = g.shape(i);
                  if (P) {
                     // if the path was already projected and cached then use that
                     pair<PathCache::iterator,PathCache::iterator> pi = _pathCache.equal_range(P.unsafePointer());
                     if (pi.first!=pi.second) {
                        while(pi.first != pi.second) {
                           paths->add(pi.first->second);
                           _xPathCache.insert(*pi.first);
                           ++pi.first;
                        }
                     }
                     else {
                        const Region::Classification c = _projector.projection()->domain()->region()->classifyExtent(P->extent());
                        if (c==Region::OUTSIDE) {
                           continue;
                        }
                        P->project(_projector);
                        _projector.getPaths(_projPaths);
                        for (size_t j=0;j<_projPaths.size();++j) {
                           paths->add(_projPaths[j]);
                           _pathCache.insert(PathCache::value_type(P.unsafePointer(),_projPaths[j].unsafePointer()));
                           _xPathCache.insert(PathCache::value_type(P.unsafePointer(),_projPaths[j].unsafePointer()));
                        }
                        _projPaths.clear();
                     }
                  }
               }
            }

            void visit(const CompositeNode& g)throws()
            {
               // if there are no useful children then return immediately
               if (g.nonNullNodeCount()==0) {
                  return;
               }

               Reference< ::indigo::Group> n(new ::indigo::Group(g.nodeCount(),false));
               Reference< ::indigo::Node> root(n);
               if (g.isSeparator()) {
                  root = new ::indigo::Separator(root);
               }

               CacheEntry& e = _cache[&g];
               assert(e.node==0);
               e.node = root.unsafePointer();

               _outputStack.back()->add(root);
               _outputStack.push_back(n);

               // push a marker onto the stack to indicate that the output stack
               // should be popped
               _inputStack.push_back(Pointer<Node>());

               // do this in the reverse, so that when each node adds itself
               // to the group, it is done in the right order
               for (size_t i=g.nodeCount();i-->0;) {
                  Pointer<Node> node = g.getNode(i);
                  if (node) {
                     _inputStack.push_back(node);
                  }
               }
            }

            void visit(const Switch& g)throws()
            {
               if (g.visibleChild() == invalidIndex()) {
                  // nothing is visible
                  return;
               }
               Pointer<Node> node = g.node(g.visibleChild());
               if (!node) {
                  // there is no child in that slot
                  return;
               }

               Reference< ::indigo::Group> n(new ::indigo::Group(0,false));
               Reference< ::indigo::Node> root(n);
               if (g.isSeparator()) {
                  root = new ::indigo::Separator(root);
               }

               CacheEntry& e = _cache[&g];
               assert(e.node==0);
               e.node = root.unsafePointer();

               _outputStack.back()->add(root);
               _outputStack.push_back(n);

               // push a marker onto the stack to indicate that the output stack
               // should be popped
               _inputStack.push_back(Pointer<Node>());
               _inputStack.push_back(node);
            }

            void visit(const Marker& g)throws()
            {
               Pointer< ::indigo::Node> inode = g.node();
               if (!inode) {
                  return;
               }

               Reference<Projection> P = _projector.projection();
               ProjectionMapping m = P->project(g.location().latitude(),g.location().longitude());
               if (m.type()!=ProjectionMapping::POINT) {
                  return;
               }

               Reference< ::indigo::Group> n(new ::indigo::Group(4,false));
               Reference< ::indigo::Node> root(new ::indigo::Separator(n));

               CacheEntry& e = _cache[&g];
               assert(e.node==0);
               e.node = root.unsafePointer();

               _outputStack.back()->add(root);

               // translate to the specified location
               n->set(_resetTransformMode,0);
               n->set(::indigo::Transform2D::createTranslation(m.point().x(),m.point().y()),1);
               n->set(_concatTransformMode,2);
               n->set(inode,3);
            }

            /** The precision */
         public:
            double _precision;

            /** The projector */
         private:
            Projector2D& _projector;

            /** The reset transform mode */
         private:
            Reference< ::indigo::Node> _resetTransformMode;

            /** The concatenate transform mode */
         private:
            Reference< ::indigo::Node> _concatTransformMode;

            /** The input node stack */
         public:
            ::std::vector< Pointer< ::cartografo::geo::Node> > _inputStack;

            /** The output node stack */
         public:
            ::std::vector< Reference< ::indigo::Group> > _outputStack;

            /** Tikal regions */
         private:
            vector< Reference< Region2D> > _projRegions;

            /** Tikal regions */
         private:
            vector< Reference< Path2D> > _projPaths;

            /**
             * The node cache; this cache will be filled as the tree is rendered,
             * to avoid duplicate nodes
             */
         public:
            NodeCache _cache;

            /** The Region and path caches that persist */
         public:
            RegionCache _regionCache;
            PathCache _pathCache;

            /** The caches that will be populated during the rendering process and then
             * will be swapped with the region/path caches
             */
         public:
            RegionCache _xRegionCache;
            PathCache _xPathCache;

            /** The extent pyramid */
         public:
            Pointer< ExtentPyramid> _pyramid;

            size_t _nInside;
            size_t _nOutside;
            size_t _nOverlaps;

            /** The logger */
         public:
            Log _logger;
      };

      MapRenderer::MapRenderer()
      throws()
      {}

      MapRenderer::~MapRenderer()
      throws()
      {}

      void MapRenderer::setRootNode(const Pointer< Node>& node)
      throws()
      {
         _node= node;
         _renderer.reset(0); // remove the current renderer
      }

      void MapRenderer::setProjector(const Reference< Projector2D>& p, const Pointer< ExtentPyramid>& pyramid)
      throws()
      {
         _projector =p;
         _pyramid = pyramid;
         _renderer.reset(0); // remove the current renderer
      }

      void MapRenderer::setProjector(const Reference< Projector2D>& p)
      throws()
      {  setProjector(p,Pointer<ExtentPyramid>());}

      Pointer< ::indigo::Node> MapRenderer::renderMap()
      throws()
      {
         if (!_node || !_projector) {
            return Pointer< ::indigo::Node>();
         }
         try {
            // check if a renderer is needed; otherwise we may just need to reconstruct what's
            // new in the tree
            if (_renderer.get()==0) {
               _renderer.reset(new Renderer(*_projector,_pyramid));
            }
            _renderer->_nInside=0;
            _renderer->_nOutside=0;
            _renderer->_nOverlaps=0;

#if ENABLE_TIMING == 1
            ::canopy::time::Time startTime;
#endif
            Reference< ::indigo::Node> res(new ::indigo::Group(0,false));
            Reference< ::indigo::Node> root(new ::indigo::Separator(res));

            // input stack must be empty
            assert(_renderer->_inputStack.empty());
            assert(_renderer->_outputStack.empty());

            // push the current node onto the stack
            _renderer->_inputStack.push_back(_node);

            // now, push the node we want to return onto the output stack
            _renderer->_outputStack.push_back(res);
            while(!_renderer->_inputStack.empty()) {
               Pointer< ::cartografo::geo::Node> top = _renderer->_inputStack.back();
               _renderer->_inputStack.pop_back();
               if (top) {
                  // check if the node has already been worked on, then we can just reuse it
                  NodeCache::iterator cachePtr = _renderer->_cache.find(top.unsafePointer());
                  if (cachePtr==_renderer->_cache.end()) {
                     // not found, so process the node
                     top->accept(*_renderer);
                  }
                  else {
                     _renderer->_outputStack.back()->add(cachePtr->second.node);
                  }
               }
               else {
                  _renderer->_outputStack.pop_back();
               }
            }
            // at this point, the output stack must be have size 1
            //	assert(_renderer->_outputStack.size()==1);
            //	assert(_renderer->_outputStack.back()==res);

            // since the output stack may is non-empty, we need  to explictly clear it
            // to avoid a very subtle memory leak
            _renderer->_outputStack.clear();

            // finish up by swapping the region and path caches to
            // get rid of old nodes that are no longer used
            _renderer->_xRegionCache.swap(_renderer->_regionCache);
            _renderer->_xPathCache.swap(_renderer->_pathCache);
            // clear out the x-caches
            _renderer->_xRegionCache.clear();
            _renderer->_xPathCache.clear();

            // clear out the node traversal cache
            _renderer->_cache.clear();

            if (_renderer->_logger.isLoggable(Level::DEBUGGING)) {
#if ENABLE_TIMING == 1
               ::canopy::time::Time endTime;
               ULong dtimeMS = (endTime.time() - startTime.time())/1000000;
               if(dtimeMS>0) {
                  LogEntry(_renderer->_logger).level(Level::DEBUGGING) << "Time to render " << dtimeMS << "ms" << doLog;
               }
#endif
               size_t nTotal = _renderer->_nInside + _renderer->_nOutside+_renderer->_nOverlaps;
               if (nTotal>0) {
                  LogEntry(_renderer->_logger).debugging() << "Inside : " << (100*_renderer->_nInside)/nTotal << '%'
                  << ", Outside : " << (100*_renderer->_nOutside)/nTotal << '%'
                  << ", Overlaps : " << (100*_renderer->_nOverlaps)/nTotal << '%' << doLog;
               }
            }

            return root;
         }
         catch (const ::std::exception& e) {
            // don't use rendere's logger, as renderer may be null
            logger().caught("Unexpected exception caught",e);
            _renderer.reset(0);
         }
         catch (...) {
            // don't use rendere's logger, as renderer may be null
            logger().warn("Unknown exception caught");
            _renderer.reset(0);
            throw;
         }
         return Pointer< ::indigo::Node>();
      }

      void MapRenderer::setPrecision(double p)
      throws()
      {
         assert(p>=0);
         _renderer->_precision = p;
      }

      double MapRenderer::precision() const
      throws()
      {  return _renderer->_precision;}

      void MapRenderer::notifyNodeChanged(const Reference< Node>& node)
throws   ()
   {
      _renderer->_cache.erase(node.unsafePointer());
   }

}}
