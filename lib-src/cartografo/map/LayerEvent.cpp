#include <cartografo/map/LayerEvent.h>
#include <cartografo/geo/Node.h>

namespace cartografo {
  namespace map {

    LayerEvent::LayerEvent (const EventType& t, 
			    const ::timber::Reference<Layer>& g, 
			    const ::timber::Pointer< ::cartografo::geo::Node>& n) throws()
    : _type(t),_layer(g),_node(n) {}

    LayerEvent::~LayerEvent() throws() {}
  }
}
