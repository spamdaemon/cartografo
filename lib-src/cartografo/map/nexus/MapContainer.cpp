#include <cartografo/map/nexus/MapContainer.h>
#include <cartografo/map/Map.h>
#include <cartografo/map/MapEvent.h>
#include <cartografo/map/MapObserver.h>
#include <cartografo/map/Layer.h>
#include <cartografo/map/LayerEvent.h>
#include <cartografo/map/MapRenderer.h>

#include <cartografo/geo/Node.h>

#include <cartografo/Projector2D.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionDomain.h>
#include <cartografo/Region.h>
#include <cartografo/ExtentPyramid.h>
#include <cartografo/projector/SimpleProjector2D.h>
#include <cartografo/projection/CylindricalProjection.h>

#include <terra/GeodeticCoordinates.h>

#include <nexus/Viewer2D.h>
#include <indigo/Group.h>
#include <indigo/AffineTransform2D.h>

#include <timber/logging.h>

#include <iostream>

using namespace ::std;
using namespace ::canopy::time;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::nexus;
using namespace ::terra;

#define ENABLE_TIMING   0

namespace cartografo {
   namespace map {
      namespace nexus {
         namespace {

            static const size_t N_PYRAMID_LEVELS = 5;

            static Log logger()
            {
               return Log("cartografo.map.nexus.MapContainer");
            }

            static Reference< Projector2D> defaultProjector()
            {
               Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);
               Reference< Projection> p = ::cartografo::projection::CylindricalProjection::createMercatorProjection(0,
                     GeodeticDatum::create(S));
               Pointer< Projector2D> pp = ::cartografo::projector::SimpleProjector2D::create(p);
               if (!pp) {
                  logger().severe("Could not instantiate the default Mercator projection");
                  throw ::std::runtime_error("Could not instantiate the Mercator projection");
               }
               return pp;
            }

            struct ViewerImpl : public Viewer2D
            {
                  ViewerImpl()throws() {}

                  ~ViewerImpl()throws()
                  {}

                  void doLayout()throws()
                  {
                     Viewer2D::doLayout();
                  }

            };

         }

         struct MapContainer::MapViewPort : public ::indigo::view::ViewPort
         {
               MapViewPort(MapContainer& c)throws() : _container(c) {}
               ~MapViewPort()throws() {}

               void setViewPort(const ::indigo::view::ViewPort::Bounds& newBounds,
                     const ::indigo::view::ViewPort::Window& sz)throws ( ::std::invalid_argument) {

                  // first, update the "real" viewport of the viewer and make sure the scale is the same
                  // in both directions
                  ::nexus::ViewPort& vp = _container.viewer().viewPort();vp.setBounds(newBounds);
                  double s = vp.horizontalScale();

                  vp.resizeBoundsToScale(s,s,vp.getPoint(vp.centerPixel()));

                  // update this viewport with the new bounds and window
                  ::indigo::view::ViewPort::setViewPort(vp.bounds(),vp.viewWindow());

                  // notify the container that the bounds have changed
                  _container.viewPortUpdated();
            }

            MapContainer& _container;
         }
         ;

         struct MapContainer::MObserver : public MapObserver
         {
               MObserver(MapContainer& e)throws () : _map(e) {}

               ~MObserver()throws() {}

               void mapChanged(const MapEvent&)throws()
               {
                  _map.reproject(false);
               }

               void layerChanged(const LayerEvent& e)throws()
               {
                  if (e.node()) {
                     LayerCache::iterator i = _map._cache.find(e.source());
                     if (i!=_map._cache.end()) {
                        CacheEntry& entry = *(i->second);
                        entry._renderer->notifyNodeChanged(e.node());

                        logger().debugging("Layer changed");

                        if (entry._state < MapContainer::RECONSTRUCT) {
                           entry._state = MapContainer::RECONSTRUCT;
                           _map.reproject(false);
                        }
                     }
                  }
               }

            private:
               MapContainer& _map;
         };

         MapContainer::CacheEntry::CacheEntry()
         throws()
         : _renderer(new MapRenderer()),
         _state(MapContainer::REPROJECT)
         {}

         MapContainer::CacheEntry::~CacheEntry()
         throws()
         {}

         MapContainer::MapContainer()
         throws(::std::exception)
         : ProxyComponent(new ViewerImpl()),
         _prepareCount(0),_reprojectFromScratch(true),
         _map(new Map()),
         _projector(defaultProjector()),
         _scenegraph(new ::indigo::Group()),
         _primaryRoot(new ::indigo::Group()),
         _tmpRoot(new ::indigo::Group()),
         _mapObserver(new MObserver(*this)),
         _viewPort(new MapViewPort(*this))
         {
            viewer().setGraphic(_scenegraph);
            _scenegraph->add(_primaryRoot);
            _scenegraph->add(_tmpRoot);
            _map->addObserver(_mapObserver);
         }

         MapContainer::~MapContainer()
         throws()
         {
            logger().debugging("Destructor");
            _map->removeObserver(_mapObserver);
         }

         void MapContainer::doLayout()
         throws()
         {
            // layout the proxy component first, so we can get use the viewport
            ProxyComponent::doLayout();

            // get the viewport and copy it over

            ::nexus::ViewPort& vp = child<Viewer2D>().viewPort();
            _viewPort->setViewPort(vp.bounds(),vp.viewWindow());
         }

         void MapContainer::setSizeHints(const SizeHints& hints)
         throws()
         {
            viewer().setSizeHints(hints);
         }

         void MapContainer::setScaleFactor(double s)
         throws()
         {
            assert(s>0);
            _viewPort->resizeBoundsToScale(s,s,_viewPort->getPoint(_viewPort->centerPixel()));
            viewPortUpdated();
         }

         ::indigo::view::ViewPort& MapContainer::viewPort()
         throws()
         {
            return *_viewPort;
         }

         void MapContainer::viewPortUpdated()
         throws()
         {
            _map->setViewPort(*_viewPort);
         }

         void MapContainer::setProjection(const ::timber::Reference< Projection>& proj)
         throws()
         {
            Pointer<Projector2D> p = ::cartografo::projector::SimpleProjector2D::create(proj);
            if (p) {
               setProjector(p);
            }
            else {
               logger().warn("Could not create a projector; projection remains unchanged");
               throw ::std::runtime_error("Could not create a projector; projection remains unchanged");
            }
         }

         void MapContainer::setProjector(const ::timber::Reference< Projector2D>& projector)
         throws()
         {
            LogEntry(logger()).info() << "Setting new projector " << projector->projection()->epsgCode() << doLog;
            _projector = projector->copy();
            _pyramid = nullptr;
            _map->setProjection(projector->projection(),*_viewPort);
            reproject(true);
         }

         void MapContainer::setMap(const ::timber::Reference< Map>& xmap)
         throws()
         {
            if (_map!=xmap) {
               _map->removeObserver(_mapObserver);
               _map = xmap;
               _map->addObserver(_mapObserver);

               _map->setProjection(projection(),*_viewPort);

               reproject(true);
            }
         }

         ::timber::Reference< Map> MapContainer::map() const
         throws()
         {  return _map;}

         void MapContainer::reproject(bool clean)
         throws()
         {
            // need to remember if we reproject from scratch (especially if called recursively)
            if (clean) {
               _reprojectFromScratch = true;
            }

            // the prepare call may result in a recursive invocation of this
            // method; if the invocation is recursive, then we need to drop out immediately
            if (_prepareCount++ != 0) {
               return;
            }

            // prepare the map for rendering
            _map->prepare();

            // reset the count and remember if we should clean
            _prepareCount = 0;
            clean = _reprojectFromScratch;
            _reprojectFromScratch = false;

            // copy the old layer cache
            LayerCache newCache;

            if (clean) {
               logger().info("Reproject from scratch");
               _cache.clear();
               _primaryRoot->clear();
            }

            if (!_pyramid) {
               LogEntry(logger()).info() << "Projection domain extent : " << _projector->projection()->domain()->region()->extent() << doLog;
#if ENABLE_TIMING ==  1
               ::canopy::time::Time startTime;
#endif
               _pyramid = ExtentPyramid::createRegionPyramid(_projector->projection()->domain()->region(),N_PYRAMID_LEVELS);
#if ENABLE_TIMING == 1
               ::canopy::time::Time endTime;
               if (logger().isLoggable(Level::INFO)) {
                  ULong dtimeMS = (endTime.time() - startTime.time())/1000000;
                  if(dtimeMS>0) {
                     LogEntry(logger()).level(Level::INFO) << "Time to build pyramid with " << N_PYRAMID_LEVELS << " levels : " << dtimeMS << "ms" << doLog;
                  }
               }
#endif

            }

            // we need to build the scene graph for the map
            for (size_t i=0;i<_map->layerCount();++i) {
               Reference<Layer> layer = _map->layer(i);
               if (_map->isEnabled(layer)) {

                  // get the cached layer, if there is one
                  Pointer<CacheEntry> e = _cache[layer];
                  if (e) {
                     // if we've seen the layer before, consult the current
                     // state to see what to do with the cache
                     ChangeState changeState = e->_state;

                     switch(changeState) {
                        case NO_CHANGE:
                        logger().debugging("No layer change");
                        // we're done;
                        break;
                        case RECONSTRUCT:
                        logger().debugging("Reconstruct layer");
                        e->_node = e->_renderer->renderMap();
                        break;
                        case REPROJECT:
                        logger().debugging("Reproject layer");
                        default:// when in doubt reproject!
                        e = nullptr;// this will cause a reprojection!
                        break;
                     };
                  }

                  // either the layer needs to be reprojected, or it's the first
                  // time the layer is to be rendererd since becoming visible
                  if (!e) {
                     logger().debugging("Caching new layer");

                     e = new CacheEntry();
                     _cache[layer] = e;
                     e->_renderer->setProjector(_projector,_pyramid);
                     e->_renderer->setRootNode( layer->rootNode() );
                     e->_node = e->_renderer->renderMap();
                  }
                  newCache[layer] = e;
                  e->_state = NO_CHANGE;
                  if (e->_node) {
                     _tmpRoot->add(e->_node);
                  }
               }
            }

            // replace the old cache with the new one
            newCache.swap(_cache);

            // the tmpRoot now becomes the primary root
            _scenegraph->set(_tmpRoot,0);
            _scenegraph->set(_primaryRoot,1);
            // don't forget to swap the pointers
            Reference< ::indigo::Group>::swap(_tmpRoot,_primaryRoot);
            // and clear the new tmpRoot
            _tmpRoot->clear();
         }

         bool
         MapContainer::getGeodeticCoordinates(const ::nexus::Pixel& pixel, ::terra::GeodeticCoordinates* geoPt) const
         throws()
         {
            Reference<Projection> p = _projector->projection();

            // first check if the projection supports the inverse projection back into coordinates
            if (!p->isInverseProjectionEnabled()) {
               return false;
            }

            // get the view transform and compute the inverse to get a point in the coordinate frame
            // of the view
            const ::indigo::Point pt = viewer().transformPixel(pixel);

            // do an inverse projection of the point now,
            // put a try-catch around this, because a projection
            try {
               if (geoPt) {
                  *geoPt = p->projectInverse(pt.x(),pt.y());
               }
               else {
                  // don't care about the return value, but want to test if the pixel
                  // has a valid inverse projection
                  p->projectInverse(pt.x(),pt.y());
               }
               return true;
            }
            catch (...) {
               // expected exception, so ignore it
               return false;
            }
         }

         Pointer< ::indigo::PickInfo> MapContainer::pickNode(const Pixel& p) const
         throws()
         {
            ::std::unique_ptr<Viewer2D::PickInfo> info = viewer().pickNode(p);
            if (info.get()==0 || !info->node) {
               logger().info("Nothing picked");
               return Pointer< ::indigo::PickInfo>();
            }
            else {
               LogEntry(logger()).info() << "Picked a shape with index " << info->shape << doLog;
               return info->info;
            }
         }

         ::timber::Reference< Projection> MapContainer::projection() const
throws      ()
      {  return _projector->projection();}
   }}
}
