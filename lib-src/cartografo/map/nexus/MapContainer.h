#ifndef _CARTOGRAFO_MAP_MAPCONTAINER_H
#define _CARTOGRAFO_MAP_MAPCONTAINER_H

#ifndef _NEXUS_PROXYCOMPONENT_H
#include <nexus/ProxyComponent.h>
#endif

#ifndef _NEXUS_VIEWER2D_H
#include <nexus/Viewer2D.h>
#endif

#ifndef _INDIGO_VIEW_VIEWPORT_H
#include <indigo/view/ViewPort.h>
#endif

#include <map>

namespace nexus {
   class SizeHints;
}

namespace terra {
   class GeodeticCoordinates;
}

namespace indigo {
   class Node;
   class Group;
   class PickInfo;
}

namespace cartografo {
   class Projector2D;
   class Projection;
   class ExtentPyramid;

   namespace map {
      class Map;
      class Layer;
      class MapRenderer;
      class MapObserver;

      namespace nexus {
         /**
          * The MapContainer is the component that integrates a map into a user interface.
          */
         class MapContainer : public ::nexus::ProxyComponent
         {
               MapContainer&operator=(const MapContainer&);
               MapContainer(const MapContainer&);

               /** The map observer */
            private:
               class MObserver;

                /** The MapViewPort */
            private:
               class MapViewPort;

               /** The layer state */
            private:
               enum ChangeState
               {
                  NO_CHANGE = 0, ///< no change, nothing to do
                  RECONSTRUCT = 1, ///< reconstruct the scenegraph, but no need to reproject
                  REPROJECT = 2
               ///< reproject nodes and reconstruct the scenegraph
               };

               /** A layer entry containing information about a layer and its rendering state */
            private:
               class CacheEntry : public ::timber::Counted
               {
                     CacheEntry(const CacheEntry&);
                     CacheEntry&operator=(const CacheEntry&);

                     friend class MapContainer;

                     /** Create a new cache entry */
                  private:
                     CacheEntry()throws();

                     /** The destructor */
                  public:
                     ~CacheEntry()throws();

                     /** The layer node */
                     ::timber::Pointer< ::indigo::Node> _node;

                     /** The map renderer */
                     ::std::unique_ptr< MapRenderer> _renderer;

                     /** The layer state */
                     ChangeState _state;
               };

               /** The layer cache */
            private:
               typedef ::std::map< ::timber::Pointer< Layer>, ::timber::Pointer< CacheEntry> > LayerCache;

               /**
                * Create a new MapContainer. The container will have a default map
                * and use a Mercator projection projection of the WGS 84 spheroid.
                * @throws ::std::exception if the container could not be created
                */
            public:
               MapContainer()
               throws ( ::std::exception);

               /**
                * Destructor
                */
            public:
               ~MapContainer()throws();

               /** Do the layout */
            protected:
               void doLayout()throws();

               /**
                * Set the size hints for this Viewer2D.
                * @param hints the size hints
                */
            public:
               void setSizeHints(const ::nexus::SizeHints& hints)throws();

               /**
                * Set the scale factor. The units of the scale factor are in pixels per meter.
                * @param s the new scale factor
                */
            public:
               void setScaleFactor(double s)throws();

               /**
                * Get the current scale factor.  The units of the scale factor are in pixels per meter.
                * @return the current scale factor
                */
            public:
               ::indigo::view::ViewPort& viewPort()throws();

               /**
                * Set a specific projection. This will generate a default projector
                * and invoke setProjector.
                * @param proj a projection
                */
            public:
               void setProjection(const ::timber::Reference< Projection>& proj)throws();

               /**
                * Set the projector for this map. This places the map into 2D
                * rendering mode.
                * @note The projector is cloned, so that any changes
                * to the projector made will not be reflected in the view.
                * @param projector the projector
                */
            public:
               virtual void setProjector(const ::timber::Reference< Projector2D>& projector)throws();

               /**
                * Get the projection.
                * @return the projection
                */
            public:
               ::timber::Reference< Projection> projection() const throws();

               /**
                * Get the geodetic location that corresponds to the specified pixel. If
                * getPt==0, then this method can be used to determine if the pixel contains a valid point.
                * pointer
                * @param pt a pixel in component coordinates or 0
                * @param geoPt the output point.
                * @return true if the geodetic coordinates were computed, false if the pixel does not map into a point.
                */
            public:
               bool getGeodeticCoordinates(const ::nexus::Pixel& pt, ::terra::GeodeticCoordinates* geoPt) const throws();

               /**
                * Set a new map
                * @param map a new map
                */
            public:
               virtual void setMap(const ::timber::Reference< Map>& map)throws();

               /**
                * Get the current map.
                * @return the current map
                */
            public:
               ::timber::Reference< Map> map() const throws();

               /**
                * Pick an object at the specified pixel.
                * @param p a pixel
                */
            public:
               ::timber::Pointer< ::indigo::PickInfo> pickNode(const ::nexus::Pixel& p) const throws();

               /**
                * Get the viewer.
                * @return the viewer
                */
            private:
               inline ::nexus::Viewer2D& viewer() const throws()
               {  return ::nexus::ProxyComponent::child< ::nexus::Viewer2D>();}

               /**
                * Reproject the map.
                * @param clean a clean, new reprojection
                */
            private:
               void reproject(bool clean)throws();

               /**
                * The viewport has been modified.
                */
            private:
               void viewPortUpdated() throws();

               /** The current reprojection status */
            private:
               volatile int _prepareCount;

               /** True if the projection should be from scratch */
            private:
               volatile bool _reprojectFromScratch;

               /** The map */
            private:
               ::timber::Reference< Map> _map;

               /** The projector */
            private:
               ::timber::Reference< Projector2D> _projector;

               /** The scenegraph */
            private:
               ::timber::Reference< ::indigo::Group> _scenegraph;

               /** The primary root node (the one currently displayed) */
            private:
               ::timber::Reference< ::indigo::Group> _primaryRoot;

               /** The alternate root node, to which changes are applied */
            private:
               ::timber::Reference< ::indigo::Group> _tmpRoot;

               /** The layer cache */
            private:
               LayerCache _cache;

               /** A map observer */
            private:
               ::timber::Reference< MapObserver> _mapObserver;

               /** An extent pyramid */
            private:
               ::timber::Pointer< ExtentPyramid> _pyramid;

               /** The current viewport */
            private:
               ::std::unique_ptr< MapViewPort> _viewPort;
         };

      }
   }
}
#endif
