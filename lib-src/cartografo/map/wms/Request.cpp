#include <cartografo/map/wms/Request.h>
#include <timber/logging.h>
#include <sstream>
#include <iomanip>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::std;

namespace cartografo {
   namespace map {
      namespace wms {
         namespace {

            static Log logger()
            {
               return Log("cartografo.map.wms.Request");
            }

            static bool checkValue(size_t)
            {
               return true;
            }

            static bool checkValue(const Request::BBox&)
            {
               return true;
            }

            static bool checkValue(const Request::Color&)
            {
               return true;
            }

            static bool checkValue(bool)
            {
               return true;
            }

            template<class T> bool checkValue(const T& value)
            {
               return !value.empty();
            }

            template<class T> bool checkValue(const Pointer< T>& value)
            {
               return value;
            }

            template<class T> bool checkValue(const std::shared_ptr< T>& value)
            {
               return value!=nullptr;
            }

            template<class T> void emitValue(const T& value, ostream& out)
            {
               out << fixed << setprecision(10) << value;
            }

            void emitValue(const string& value, ostream& out)
            {
               out << ::timber::w3c::URI::escape(value);
            }

            template<class T> void emitValue(const Pointer< T>& value, ostream& out)
            {
               emitValue(*value, out);
            }

            template<class T> void emitValue(const vector< T>& value, ostream& out)
            {
               const char* sep = "";
               for (auto t = value.begin(); t != value.end(); ++t) {
                  out << sep;
                  sep = ",";
                  emitValue(*t, out);
               }
            }

            static void emitValue(const Request::Color& col, ostream& out)
            {
               auto fill = out.fill('0');
               auto w = out.width();

               out << "0x" << hex << setw(2) << (unsigned int) col.red << setw(2) << (unsigned int) col.green << setw(2)
                     << (unsigned int) col.blue << dec;
               out.fill(fill);
               out.width(w);
            }

            static void emitValue(const Request::BBox& bbox, ostream& out)
            {
               out << fixed << setprecision(10) << bbox.minX << ',' << bbox.minY << ',' << bbox.maxX << ','
                     << bbox.maxY;
            }

            static void emitValue(bool v, ostream& out)
            {
               out << (v ? "TRUE" : "FALSE");
            }

            template<class T>
            static void emitTag(const char* tag, const T& value, ostream& out, char sep = '&')
            {
               out << sep << tag << "=";
               emitValue(value, out);
            }

            template<class T>
            static void emitTag(const char* tag, const T& value, const char* defValue, ostream& out, char sep = '&')
            {
               out << sep << tag << "=";
               if (checkValue(value)) {
                  emitValue(value, out);
               }
               else {
                  out << defValue;
               }
            }

            template<class T>
            static bool emitOptionalTag(const char* tag, const T& value, ostream& out, char sep = '&')
            {
               if (!checkValue(value)) {
                  return false;
               }
               emitTag(tag, value, out, sep);
               return true;

            }

            template<class T>
            static void emitRequiredTag(const char* tag, const T& value, ostream& out, char sep = '&')
            {
               if (!checkValue(value)) {
                  throw runtime_error(string("Missing value for tag ") + tag);
               }
               emitTag(tag, value, out, sep);
            }

         }

         Request::Request()
         throws()
         : _service("WMS")
         {
            _bgColor.red = _bgColor.green = _bgColor.blue = 0xff;
            _transparent = false;
            _exceptions = ContentType::parse("application/vnd.ogc.se_xml");

         }

         Request::~Request()
         throws()
         {
         }

         ::std::shared_ptr< ::timber::w3c::URI> Request::getRequestURI() const
         throws()
         {
            try {
               ostringstream out;

               if (_url.empty()) {
                  logger().warn("Request URI not set");
                  return  ::std::shared_ptr< ::timber::w3c::URI >();
               }
               out << _url;

               emitRequiredTag("SERVICE",_service,out,'?');
               emitRequiredTag("REQUEST",_request,out);
               if (_request=="GetCapabilities") {
                  emitRequiredTag("SERVICE",_service,out);
                  emitOptionalTag("VERSION",_version,out);
                  emitOptionalTag("UPDATESEQUENCE",_updateSequence,out);
               }
               else if (_request=="GetMap") {
                  emitRequiredTag("VERSION",_version,out);
                  // according to spec, layers and styles are optional
                  // when the SLD tag is specified
                  emitOptionalTag("WFS",_wfs,out);
                  if (emitOptionalTag("SLD",_sld,out)) {
                     emitOptionalTag("LAYERS",_layers,out);
                     emitOptionalTag("STYLES",_styles,out);
                  }
                  else {
                     emitRequiredTag("LAYERS",_layers,out);
                     emitTag("STYLES",_styles,"",out);
                  }
                  emitRequiredTag("SRS",_crs,out);
                  emitRequiredTag("BBOX",_bbox,out);
                  emitRequiredTag("WIDTH",_width,out);
                  emitRequiredTag("HEIGHT",_height,out);
                  emitRequiredTag("FORMAT",_format,out);
                  emitTag("TRANSPARENT",_transparent,"FALSE",out);
                  emitOptionalTag("BGCOLOR",_bgColor,out);
                  emitOptionalTag("EXCEPTIONS",_exceptions,out);
                  emitOptionalTag("TIME",_time,out);
                  emitOptionalTag("ELEVATION",_elevation,out);
                  for (auto vsp=_vendor.begin();vsp!=_vendor.end();++vsp) {
                     emitTag(vsp->first.c_str(),vsp->second,out);
                  }
               }
               else if (_request=="GetFeatureInfo") {
                  emitRequiredTag("VERSION",_version,out);
                  emitRequiredTag("SERVICE",_service,out);
               }
               else {
                  logger().warn("Unexpected request " + _request);
                  return ::std::shared_ptr< ::timber::w3c::URI >();
               }

               const string urlString = out.str();
               logger().info("Request URL for WMS service " + urlString);

               return ::timber::w3c::URI::create(urlString);
            }
            catch (const exception& e) {
               // could not create the URL
               logger().warn("Could not create request",e);
            }
            return ::std::shared_ptr< ::timber::w3c::URI>();
         }

         void Request::setService(const string& s)
      throws (invalid_argument)
      {
         _service = s;
      }

      const string& Request::service() const
      throws()
      {
         return _service;
      }

      void Request::setVersion(const string& v)
   throws(invalid_argument)
   {
      _version = v;
   }

   const string& Request::version() const
   throws()
   {
      return _version;
   }

   void Request::setRequest(const string& req)
throws(invalid_argument)
{
   _request = req;
}

const string& Request::request() const
throws()
{
   return _request;
}

void Request::setUrl(const string& xurl)
throws(invalid_argument)
{
   _url = xurl;
}

const string& Request::url() const
throws()
{
   return _url;
}

void Request::setSLD(const string& xurl)
throws(invalid_argument)
{
   _sld = xurl;
}

const string& Request::sld() const
throws()
{
   return _sld;
}

void Request::setWFS(const string& xurl)
throws(invalid_argument)
{
   _wfs = xurl;
}

const string& Request::wfs() const
throws()
{
   return _wfs;
}

void Request::setFormat(const SharedRef< ContentType>& ct)
throws(invalid_argument)
{
   _format = ct;
}

::std::shared_ptr< ContentType> Request::format() const
throws()
{
   return _format;
}

void Request::setExceptions(const SharedRef< ContentType>& ct)
throws(invalid_argument)
{
   _exceptions = ct;
}

::std::shared_ptr< ContentType> Request::exceptions() const
throws()
{
   return _exceptions;
}

void Request::addLayer(const string& xlayer)
throws(invalid_argument)
{
   _layers.push_back(xlayer);
}

void Request::setLayers(const vector< string>& xlayers)
throws(invalid_argument)
{
   _layers = xlayers;
}

const vector< string>& Request::layers() const
throws()
{
   return _layers;
}

void Request::setWidth(size_t w)
throws(invalid_argument)
{
   _width = w;
}

size_t Request::width() const
throws()
{
   return _width;
}

void Request::setHeight(size_t h)
throws(invalid_argument)
{
   _height = h;
}

size_t Request::height() const
throws()
{
   return _height;
}

void Request::setBBox(const BBox& box)
throws(invalid_argument)
{
   _bbox = box;
}

const Request::BBox& Request::getBBox() const
throws()
{
   return _bbox;
}

void Request::setCRS(const string& xcrs)
throws(invalid_argument)
{
   _crs = xcrs;
}

const string& Request::crs() const
throws()
{
   return _crs;
}

void Request::setUpdateSequence(const string& seq)
throws(invalid_argument)
{
   _updateSequence = seq;
}

const string& Request::updateSequence() const
throws()
{
   return _updateSequence;
}

void Request::setStyles(const vector< string>& xstyles)
throws(invalid_argument)
{
   _styles = xstyles;
}

void Request::addStyle(const string& xstyle)
throws(invalid_argument)
{
   _styles.push_back(xstyle);
}

const vector< string>& Request::styles() const
throws()
{
   return _styles;
}

void Request::setTransparent(bool xtransparent)
throws(invalid_argument)
{
   _transparent = xtransparent;
}

bool Request::transparent() const
throws()
{
   return _transparent;
}

void Request::setBgColor(const Color& col)
throws(invalid_argument)
{
   _bgColor = col;
}

const Request::Color& Request::bgColor() const
throws()
{
   return _bgColor;
}

void Request::setTime(const string& tm)
throws(invalid_argument)
{
   _time = tm;
}

const string& Request::time() const
throws()
{
   return _time;
}

void Request::setElevation(const string& e)
throws(invalid_argument)
{
   _elevation = e;
}

const string& Request::elevation() const
throws()
{
   return _elevation;
}

void Request::addVendorParameter(const string& name, const string& param)
throws(invalid_argument)
{
   // TODO: check the parameter name
   _vendor[name].push_back(param);
}

const Request::VendorParameters& Request::vendorParameters() const
throws()
{
   return _vendor;
}

void Request::setQueryLayers(const vector< string>& xlayers)
throws(invalid_argument)
{
   _queryLayers = xlayers;
}

const vector< string>& Request::queryLayers() const
throws()
{
   return _queryLayers;
}

void Request::setInfoFormat(SharedRef< ContentType> ct)
throws(invalid_argument)
{
   _infoFormat = ct;
}

::std::shared_ptr< ContentType> Request::infoFormat() const
throws()
{
   return _infoFormat;
}

void Request::setFeatureCount(size_t count)
throws(invalid_argument)
{
   _featureCount = count;
}

size_t Request::featureCount() const
throws()
{
   return _featureCount;
}

void Request::setX(size_t xx)
throws(invalid_argument)
{
   _x = xx;
}

size_t Request::x() const
throws()
{
   return _x;
}

void Request::setY(size_t yy)
throws(invalid_argument)
{
   _y = yy;
}

size_t Request::y() const
throws ()
{
   return _y;
}

}
}
}
