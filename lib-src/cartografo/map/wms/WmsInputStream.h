#ifndef WMSINPUTSTREAM_H_
#define WMSINPUTSTREAM_H_

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace cartografo {
   namespace map {
      namespace wms {

         /*
          *
          */
         class WmsInputStream
         {
            public:
               WmsInputStream()throws();
               virtual ~WmsInputStream()throws         ();
      };
   }
}
}

#endif
