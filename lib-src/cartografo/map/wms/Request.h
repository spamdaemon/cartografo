#ifndef _CARTOGRAFO_MAP_WMS_REQUEST_H
#define _CARTOGRAFO_MAP_WMS_REQUEST_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _TIMBER_CONTENTTYPE_H
#include <timber/ContentType.h>
#endif

#ifndef _TIMBER_W3C_URI_H
#include <timber/w3c/URI.h>
#endif

#include <string>
#include <vector>

namespace cartografo {
   namespace map {
      namespace wms {
         /**
          * This class defines the various fields supported by the different types of
          * requests that can be made to WMS compliant server.
          * An example request may be of the following type:
          * <pre>http://localhost:8080/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=nurc:Img_Sample&styles=&bbox=-130.85168,20.7052,-62.0054,54.1141&width=680&height=330&srs=EPSG:4326&format=image/jpeg</pre>
          *
          * The request can be used to model each of these well-known WMS requests:
          * <ul>
          * <li>GetCapabilities
          * <li>GetMap
          * <li>GetFeatureInfo
          * </ul>
          *
          */
         class Request
         {
               /** The vendor paramters */
               typedef ::std::map< ::std::string, ::std::vector< ::std::string> > VendorParameters;

               /** A bounding box */
            public:
               struct BBox
               {
                  BBox(double minx, double miny, double maxx, double maxy) throws()
                        : minX(minx),maxX(maxx),maxY(maxy),minY(miny)
                              {}

                  BBox() throws()
                        : minX(0),maxX(0),maxY(0),minY(0)
                              {}

                     /** The left side of the bounding box */
                     double minX;
                     /** The right side of the bounding box */
                     double maxX;
                     /** The top side of the bounding box */
                     double maxY;
                     /** The bottom side of the bounding box */
                     double minY;
               };

               /** The color */
            public:
               struct Color
               {
                  /** Default constructor creates the color white */
                  Color(unsigned char r, unsigned char g,unsigned char b)throws() : red(r),green(g),blue(b) {}

                  /** Default constructor creates the color white */
                  Color()throws() : red(255),green(255),blue(255) {}

                     /** The red component (0-255) */
                     unsigned char red;
                     /** The green component (0-255) */
                     unsigned char green;
                     /** The blue component (0-255) */
                     unsigned char blue;
                  };

                  /**
                   * Create a new request object. This request
                   * cannot be used to retrieve an image until all required parameters
                   * have been set.
                   */
                  public:
                  Request()throws();

                  /**  Destructor. */
                  public:
                  ~Request()throws();

                  /**
                   * Get the HTTP request url for this request. If the request
                   * is not complete or erroneous, then this method return a null pointer.
                   * @return the URL that can be used to access the service
                   */
                  public:
                 ::std::shared_ptr< ::timber::w3c::URI> getRequestURI() const throws();



                  /**
                   * Set the service.
                   * Required for :
                   * <ul>
                   * <li>GetCapabilities
                   * <li>GetMap
                   * <li>GetFeatureInfo
                   * </ul>
                   * @param s the service
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setService(const ::std::string& s) throws( ::std::invalid_argument);

                  /**
                   * Get the WMS service.
                   * @return a WMS service name
                   */
                  public:
                  const ::std::string& service() const throws();

                  /**
                   * Set the WMS version identifier.
                   * Optional for :
                   * <ul>
                   * <li>GetCapabilities
                   * </ul>
                   * @param v a WMS version identifier
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setVersion(const ::std::string& v) throws( ::std::invalid_argument);

                  /**
                   * Get the WMS version identifier.
                   * @return a WMS version identifier
                   */
                  public:
                  const ::std::string& version() const throws();

                  /**
                   * Set  the request type.
                   *  Required for :
                   * <ul>
                   * <li>GetCapabilities
                   * <li>GetMap
                   * <li>GetFeatureInfo
                   * </ul>
                   * Both GetCapabilities and GetMap are required WMS requests.
                   * @param request a request type.
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setRequest(const ::std::string& req) throws( ::std::invalid_argument);

                  /**
                   * Get the type of WMS request to be made.
                   * @return a WMS request
                   */
                  public:
                  const ::std::string& request() const throws();

                  /**
                   * Set the base URL. The base url is the URL by which the server
                   * can be accessed. The individual request parameters will be added
                   * to the URL as a URL query part.
                   * @param url a base url
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setUrl(const ::std::string& url) throws( ::std::invalid_argument);

                  /**
                   * Get the base url.
                   * @return the current base url or an empty string if not set.
                   */
                  public:
                  const ::std::string& url() const throws();

                  /**
                   * Set the URL of a Styled Layer Descriptor as defined in the SLD specification.
                   * @param url a base url
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setSLD(const ::std::string& url) throws( ::std::invalid_argument);

                  /**
                   * Get the URL of a Styled Layer Descriptor as defined in the SLD specification.
                   * @return the  URL of a Styled Layer Descriptor or an empty string if not set.
                   */
                  public:
                  const ::std::string& sld() const throws();

                  /**
                   * Set the URL of a Web Feature Services providing features to be symbolized.
                   * @param url a WFS url
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setWFS(const ::std::string& url) throws( ::std::invalid_argument);

                  /**
                   * Get the WFS url that may provide features to be sympolized on the map.
                   * @return a WFS url or an empty string if not set.
                   */
                  public:
                  const ::std::string& wfs() const throws();

                  /**
                   * Set the request image format. The image format must a content type.
                   * @param ct a content type
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setFormat(const ::timber::SharedRef< ::timber::ContentType>& ct) throws( ::std::invalid_argument);

                  /**
                   * Get the content format.
                   * @return the content format
                   */
                  public:
                  ::std::shared_ptr< ::timber::ContentType> format() const throws();

                  /**
                   * Set the request response format if an error has occurred and no image is served.
                   * @param ct a content type
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setExceptions(const ::timber::SharedRef< ::timber::ContentType>& ct) throws( ::std::invalid_argument);

                  /**
                   * Get request response format to report errors.
                   * @return the content format
                   */
                  public:
                  ::std::shared_ptr< ::timber::ContentType> exceptions() const throws();

                  /**
                   * Set the layers to be be composed.
                   * @param layers a list of names
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setLayers(const ::std::vector< ::std::string>& layers) throws( ::std::invalid_argument);

                  /**
                   * Add a single layer.
                   * @param layer a layer
                                  * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void addLayer (const ::std::string& layer) throws (::std::invalid_argument);

                  /**
                   * Get the layers
                   * @return the layers to be retrieved.
                   */
                  public:
                  const ::std::vector< ::std::string>& layers() const throws();

                  /**
                   * Set the width.
                   * @param w the width of the image
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setWidth(size_t w) throws( ::std::invalid_argument);

                  /**
                   * Get the width of the requested image.
                   * return the request image width
                   */
                  public:
                  size_t width() const throws();

                  /**
                   * Set the height.
                   * @param h the height of the image
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setHeight(size_t h) throws( ::std::invalid_argument);

                  /**
                   * Get the height of the requested image.
                   * return the request image height
                   */
                  public:
                  size_t height() const throws();

                  /**
                   * Set the bounding box for the request.
                   * @param bbox the bounding box
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setBBox(const BBox& bbox) throws( ::std::invalid_argument);

                  /**
                   * Get the bounding box for the request.
                   * @return the bounding box
                   */
                  public:
                  const BBox& getBBox() const throws();

                  /**
                   * Set the spatial reference frame for the requested image.
                   * @param src the reference frame for the requested image.
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setCRS(const ::std::string& srs) throws( ::std::invalid_argument);

                  /**
                   * Get the spatial reference frame for the requested image.
                   * @return the reference frame for the requested image.
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  const ::std::string& crs() const throws();

                  /**
                   * Set the update sequence parameter.
                   * Optional for
                   * <ul>
                   * <li>GetCapabilities
                   * </ul>
                   * @param value the update sequence parameter.
                   */
                  public:
                  void setUpdateSequence(const ::std::string& srs) throws( ::std::invalid_argument);

                  /**
                   * Get the update sequence value.
                   * @return the update sequence value.
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  const ::std::string& updateSequence() const throws();

                  /**
                   * Set the styles to be be.
                   * @param styles a list of styles
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setStyles(const ::std::vector< ::std::string>& styles) throws( ::std::invalid_argument);

                  /**
                   * Add a single style.
                   * @param style a style
                                  * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void addStyle (const ::std::string& style) throws (::std::invalid_argument);

                  /**
                   * Get the styles
                   * @return the styles to be retrieved.
                   */
                  public:
                  const ::std::vector< ::std::string>& styles() const throws();

                  /**
                   * Set the transparency flag.
                   * @param transparent true to make image background transparent
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setTransparent(bool transparent) throws( ::std::invalid_argument);

                  /**
                   * Get the transparency flag.
                   * @return true to make image background transparent
                   */
                  public:
                  bool transparent() const throws();

                  /**
                   * Set the background color
                   * @param col the background color
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setBgColor(const Color& col) throws( ::std::invalid_argument);

                  /**
                   * Get the background color
                   * @return the background color
                   */
                  public:
                  const Color& bgColor() const throws();

                  /**
                   * Set the time parameter.
                   * @param tm the time parameter
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setTime(const ::std::string& tm) throws( ::std::invalid_argument);

                  /**
                   * Get the time paramter.
                   * @return the time paramter
                   */
                  public:
                  const ::std::string& time() const throws();

                  /**
                   * Set the elevation parameter.
                   * @param e the elevation parameter
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setElevation(const ::std::string& e) throws( ::std::invalid_argument);

                  /**
                   * Get the elevation paramter.
                   * @return the elevation paramter
                   */
                  public:
                  const ::std::string& elevation() const throws();

                  /**
                   * Add a vendor specific parameter. If such a vendor
                   * specific parameter already exists, then the value
                   * is appended.<p>
                   *
                   * @param name the name of a parameter
                   * @param value the parameter value.
                   * @throws ::std::invalid_argument if the name is a reserved name.
                   */
                  public:
                  void addVendorParameter(const ::std::string& name, const ::std::string& param) throws ( ::std::invalid_argument);

                  /**
                   * Get the vendor paramters.
                   * @return the vendor parameters
                   */
                  public:
                  const VendorParameters& vendorParameters() const throws();

                  /**
                   * Set the layers to be be composed.
                   * @param layers a list of names
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setQueryLayers(const ::std::vector< ::std::string>& layers) throws( ::std::invalid_argument);

                  /**
                   * Get the layers
                   * @return the layers to be retrieved.
                   */
                  public:
                  const ::std::vector< ::std::string>& queryLayers() const throws();

                  /**
                   * Set the request image format. The image format must a content type.
                   * @param ct a content type
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setInfoFormat(::timber::SharedRef< ::timber::ContentType> ct) throws( ::std::invalid_argument);

                  /**
                   * Get the content format.
                   * @return the content format
                   */
                  public:
                  ::std::shared_ptr< ::timber::ContentType> infoFormat() const throws();

                  /**
                   * Set the number of features about which to obtain information.
                   * @param count the feature count
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setFeatureCount(size_t count) throws( ::std::invalid_argument);

                  /**
                   * Get the number of features to query.
                   * @return the number of features to be queried.
                   */
                  public:
                  size_t featureCount() const throws();

                  /**
                   * Set the X coordinate in pixels of feature
                   * @param x X coordinate in pixels of feature
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setX(size_t xx) throws( ::std::invalid_argument);

                  /**
                   * Get the X coordinate in pixels of feature
                   * @return X coordinate in pixels of feature (measured from upper left corner=0)
                   */
                  public:
                  size_t x() const throws();

                  /**
                   * Set the Y coordinate in pixels of feature
                   * @param y Y coordinate in pixels of feature
                   * @throws ::std::invalid_argument if an invalid argument provided
                   */
                  public:
                  void setY(size_t y) throws( ::std::invalid_argument);

                  /**
                   * Get the height of the requested image.
                   * @return Y coordinate in pixels of feature (measured from
                   upper left corner=0)
                   */
                  public:
                  size_t y() const throws();

                  /** The base url */
                  private:
                  ::std::string _url;

                  /** The service */
                  private:
                  ::std::string _service;

                  /** The request */
                  private:
                  ::std::string _request;

                  /** The coordinate reference system */
                  private:
                  ::std::string _crs;

                  /** The requested image format */
                  private:
                  ::std::shared_ptr< ::timber::ContentType> _format;

                  /** The request version */
                  private:
                  ::std::string _version;

                  /** The update sequence */
                  private:
                  ::std::string _updateSequence;

                  /** The width */
                  private:
                  size_t _width;

                  /** The height */
                  private:
                  size_t _height;

                  /** The bounding box */
                  private:
                  BBox _bbox;

                  /** The layers */
                  private:
                  ::std::vector< ::std::string> _layers;

                  /** The styles */
                  private:
                  ::std::vector< ::std::string> _styles;

                  /** The exceptions format */
                  private:
                  ::std::shared_ptr< ::timber::ContentType> _exceptions;

                  /** True if transparent */
                  private:
                  bool _transparent;

                  /** The background color */
                  private:
                  Color _bgColor;

                  /** The time parameter */
                  private:
                  ::std::string _time;

                  /** The elevation */
                  private:
                  ::std::string _elevation;

                  /** A vendor parameter */
                  private:
                  VendorParameters _vendor;

                  /** The feature count */
                  private:
                  size_t _featureCount;

                  /** The X pixel */
                  private:
                  size_t _x;

                  /** The y pixel */
                  private:
                  size_t _y;

                  /** The info format */
                  private:
                  ::std::shared_ptr< ::timber::ContentType> _infoFormat;

                  /** The layers to be queried. */
                  private:
                  ::std::vector< ::std::string> _queryLayers;

                  /** The style url */
                  private:
                  ::std::string _sld;

                  /** The WFS */
                  private:
                  ::std::string _wfs;
               };

            }
         }
      }

#endif
