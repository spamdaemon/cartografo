#ifndef _CARTOGRAFO_PROJECTIONMAPPING_H
#define _CARTOGRAFO_PROJECTIONMAPPING_H

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#ifndef _TIKAL_SEGMENT2D_H
#include <tikal/Segment2D.h>
#endif

#ifndef _TIKAL_PATH2D_H
#include <tikal/Path2D.h>
#endif

namespace cartografo {
  /**
   * A projection mapping is the mapping from a point latitude and longitude
   * into either a 2D point or a 2D path.
   */
  class ProjectionMapping {
    /** The various types of mappings */
  public:
    enum Type {
      NONE, //< No mapping established
      POINT, //< a one-to-one mapping from point to point
      LINE,  //< a one-to-many mapping from point to a line
      PATH, //< a one-to-many mapping from a point to a path
    };
      
    /** 
     * Create an initial mapping with the NONE type
     */
  public:
    inline ProjectionMapping() throws()
      : _type(NONE)
    {}

    /** 
     * Create a mapping of type point
     * @param pt a point
     */
  public:
    inline ProjectionMapping(const ::tikal::Point2D& pt) throws()
      : _type(POINT),_point(pt)
    {}
      
    /** 
     * Create a mapping of type line
     * @param pt the closest point on the path
     * @param seg a line segment
     */
  public:
    inline ProjectionMapping(const ::tikal::Point2D& pt, const ::tikal::Segment2D& seg) throws()
      : _type(LINE),_point(pt),_line(seg)
    {}

    /** 
     * Create a mapping of type path
     * @param pt the closest point on the path
     * @param p the path to which a point was mapped
     */
  public:
    inline ProjectionMapping(const ::tikal::Point2D& pt, const ::timber::Reference< ::tikal::Path2D>& p) throws()
      : _type(PATH),_point(pt),_path(p)
    {}

    /** Destructor */
  public:
    inline ~ProjectionMapping() throws() {}
      
    /**
     * Get the current projection type.
     * @return mapping type
     */
  public:
    inline Type type() const throws() { return _type; }

    /**
     * Get the point or closest point. The point is always defined, unless
     * the type is NONE
     * @pre REQUIRE_TRUE(type()!=NONE)
     * @return the point or closest point
     */
  public:
    const ::tikal::Point2D& point() const throws() 
    {
      assert(type()!=NONE);
      return _point; 
    }

    /**
     * Get the line segment.
     * @pre REQUIRE_TRUE(type()==LINE)
     * @return the line into which a point was mapped
     */
  public:
    const ::tikal::Segment2D& line() const throws()
    {
      assert(type()==LINE);
      return _line;
    }


    /**
     * Get the path.
     * @pre REQUIRE_TRUE(type()==PATH)
     * @return the path into which a point was mapped
     */
  public:
    ::timber::Pointer< ::tikal::Path2D> path() const throws()
    {
      assert(type()==PATH);
      return _path;
    }
      
    /**
     * Set the none mapping
     */
  public:
    void setNoMapping() throws()
    { 
      _type = NONE; 
      _path = nullptr;
    }

    /**
     * Set the mapping of some latitude and longitude into a point.
     * @param pt a point 
     */
  public:
    inline void setPointMapping(const ::tikal::Point2D& pt) throws()
    {
      _type  = POINT;
      _point = pt; 
      _path = nullptr;
    }

    /**
     * Set the mapping of some latitude and longitude into a point.
     * @param x the x-coordinaet of a mapped point
     * @param y the y-coordinaet of a mapped point
     */
  public:
    inline void setPointMapping(double x, double y) throws()
    {
      _type  = POINT;
      _point.set(x,y);
      _path = nullptr;
    }

    /**
     * Map a latitude and longitude into a straight line 
     * @param pt the mapping of (lat,lon) into the closest point in the projection
     * @param seg the line segment
     */
  public:
    inline void setLineMapping (const ::tikal::Point2D& pt, const ::tikal::Segment2D& seg) throws()
    {
      _type = LINE;
      _point=pt;
      _line=seg;
      _path = nullptr;
    }
      
    /**
     * Map a latitude and longitude into a straight line 
     * @param pt the mapping of (lat,lon) into the closest point in the projection
     * @param p0 the start point of the line
     * @param p1 the end point of the line
     */
  public:
    inline void setLineMapping (const ::tikal::Point2D& pt, const ::tikal::Point2D& p0, const ::tikal::Point2D& p1) throws()
    {
      setLineMapping(pt,::tikal::Segment2D(p0,p1));
    }
      
    /**
     * Map a latitude and longitude into the specified path.
     * @param pt the mapping of (lat,lon) into the closest point in the projection
     * @param p the path into which the point was projected 
     */
  public:
    inline void setPathMapping (const ::tikal::Point2D& pt, const ::timber::Reference< ::tikal::Path2D>& p) throws()
    { 
      _type = PATH;
      _point=pt;
      _path=p;
    }


    /** The mapping type */
  private:
    Type _type;

    /** The point or closest point */
  private:
    ::tikal::Point2D _point;

    /** The line */
  private:
    ::tikal::Segment2D _line;

    /** A path */
  private:
    ::timber::Pointer< ::tikal::Path2D> _path;
  };
}

#endif
