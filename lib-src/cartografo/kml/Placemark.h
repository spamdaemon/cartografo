#ifndef _CARTOGRAFO_KML_PLACEMARK_H
#define _CARTOGRAFO_KML_PLACEMARK_H

#ifndef _CARTOGRAFO_KML_FEATURE_H
#include <cartografo/kml/Feature.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This class corresponds to a KML placemark.
       */
      class Placemark : public virtual Feature
      {
          /** The highlight state of a placemark */
         public:
            enum class StyleState {
               /** The normal rendering mode */
               NORMAL,

               /** The highlight mode */
               HIGHLIGHT
            };


            Placemark(const Placemark&);
            Placemark&operator=(const Placemark&);

            /** Default constructor */
         protected:
            Placemark()throws();

            /** Destructor */
         public:
            ~Placemark()throws();

            /**
             * Set the style state.
             * @param state the new style state
             */
         public:
            virtual void setStyleState (StyleState state) throws() = 0;
      };

   }

}

#endif
