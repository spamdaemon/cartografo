#ifndef _CARTOGRAFO_KML_FEATURE_H
#define _CARTOGRAFO_KML_FEATURE_H

#ifndef _TIMBER_WEAKLYCOUNT_H
#include <timber/WeaklyCountable.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * The feature is the baseclass for KML nodes.
       */
      class Feature : public virtual ::timber::WeaklyCountable
      {
           Feature(const Feature&);
            Feature&operator=(const Feature&);

            /** Default constructor */
         protected:
            Feature()throws();

            /** Destructor */
         public:
            ~Feature()throws();

            /**
             * Get the name of this feature.
             * @return the name or an empty string if not defined.
             */
         public:
            virtual const ::std::string& name() const throws() = 0;
      };

   }

}

#endif
