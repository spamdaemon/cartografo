#ifndef _CARTOGRAFO_KML_KMLFACTORY_H
#define _CARTOGRAFO_KML_KMLFACTORY_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_W3C_URI_H
#include <timber/w3c/URI.h>
#endif

#ifndef _TIMBER_IOS_IOEXCEPTION_H
#include <timber/ios/IOException.h>
#endif

namespace timber {
   class ContentType;
   namespace media {
      class ImageFactory;
   }
   namespace scheduler {
      class Scheduler;
   }
}

namespace terra {
   class GeodeticDatum;
}

namespace cartografo {
   namespace kml {
      class KmlNode;

      /**
       * This provides the means to create either KML features or KmlNode objects. Before creating
       * KML features, the factory can be configured in a variety of ways to control the renderering
       * and behavior of KML nodes.
       */
      class KmlFactory : public ::timber::Counted
      {
            /** Create a new KMLNode */
         protected:
            KmlFactory()throws();

            /** Destructor */
         public:
            ~KmlFactory()throws();

            /**
             * Create a KMZ content-type.
             * @return a KMZ- content type
             */
         public:
            static ::timber::SharedRef< ::timber::ContentType> kmzContentType() throws();


            /**
             * Create a KML content-type.
             * @return a KML content type
             */
         public:
            static ::timber::SharedRef< ::timber::ContentType> kmlContentType() throws();


            /**
             * Create a default KML factory. Normally, a provider should be use to create a KMLFactory instance.
             * @return a default factory
             * @throws ::std::exception if the factory is not available
             */
         public:
            static ::timber::Reference< KmlFactory> newFactory() throws( ::std::exception);

            /**
             * Set a scheduler that the factory should use when retrieving network links automatically. Changing
             * the scheduler after some links have already been scheduled may not necessarily affect those previously
             * scheduled links.
             * @param s a scheduler or 0 to use a default scheduler
             */
         public:
            virtual void setScheduler (::std::shared_ptr< ::timber::scheduler::Scheduler> s) throws() = 0;

            /**
             * Configure the image factory to use.
             * @param f an image factory or 0 to use a default factory
             */
         public:
            virtual void setImageFactory(const ::std::shared_ptr< ::timber::media::ImageFactory>& f)throws() = 0;

            /**
             * Create a KMLNode that retrieves its data from the specified URI.
             * @param uri a URI
             * @param datum a datum
             * @return a KMLNode
             * @throws ::timber::ios::IOException if the document could not be retrieved
             * @throws invalid_argument if the document is not a valid KML document
             */
         public:
            virtual ::timber::Reference<KmlNode> createNode (const ::timber::SharedRef< ::timber::w3c::URI>& uri,const ::timber::Reference< ::terra::GeodeticDatum>& datum) throws (::timber::ios::IOException,::std::invalid_argument);

            /**
             * Create a KMLNode that retrieves its data from the specified stream.
             * @param stream
             * @param uri the URI associated with the stream
             * @param datum a datum
             * @return a KMLNode
             * @throws ::timber::ios::IOException if the document could not be retrieved
             * @throws invalid_argument if the document is not a valid KML document
             */
         public:
            virtual ::timber::Reference<KmlNode> createNode (::std::istream& stream, const ::timber::SharedRef< ::timber::w3c::URI>& uri,const ::timber::Reference< ::terra::GeodeticDatum>& datum) throws (::timber::ios::IOException,::std::invalid_argument) = 0;
      };

   }

}

#endif
