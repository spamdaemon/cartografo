#ifndef _CARTOGRAFO_KML_KMLNODE_H
#define _CARTOGRAFO_KML_KMLNODE_H

#ifndef _CARTOGRAFO_GEO_COMPOSITENODE_H
#include <cartografo/geo/CompositeNode.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This is the baseclass for nodes that can represent entire KML documents. Unless specific
       * subclasses exist, this class does not support modifying the document, e.g. change the visibility
       * or styles of the geometries, folders, placemarks, etc.
       * <p>
       * Nodes are normally created by a KmlFactory.
       */
      class KmlNode : public ::cartografo::geo::CompositeNode
      {
            /** Create a new KMLNode */
         protected:
            KmlNode()throws();

            /** Destructor */
         public:
            virtual ~KmlNode()throws() = 0;
      };

   }

}

#endif
