#ifndef _CARTOGRAFO_KML_DOCUMENT_H
#define _CARTOGRAFO_KML_DOCUMENT_H

#ifndef _CARTOGRAFO_KML_CONTAINER_H
#include <cartografo/kml/Container.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This class corresponds to a KML placemark.
       */
      class Document : public virtual  Container
      {
            Document(const Document&);
            Document&operator=(const Document&);

            /** Default constructor */
         protected:
            Document()throws();

            /** Destructor */
         public:
            ~Document()throws();
      };

   }

}

#endif
