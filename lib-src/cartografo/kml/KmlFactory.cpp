#include <cartografo/kml/KmlFactory.h>
#include <cartografo/kml/KmlNode.h>
#include <cartografo/AreaBuilder.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/geo/nodes.h>
#include <cartografo/projector/IdentityProjector2D.h>

#include <cartografo/kml/GroundOverlay.h>
#include <cartografo/kml/Placemark.h>
#include <cartografo/kml/Document.h>
#include <cartografo/kml/Folder.h>
#include <cartografo/kml/NetworkLink.h>

#include <tikal/Area2D.h>
#include <indigo/nodes.h>
#include <terra/GeodeticCoordinates.h>
#include <timber/logging.h>
#include <timber/archive/ArchiveFactory.h>
#include <timber/media/ImageFactory.h>
#include <timber/scheduler/Scheduler.h>
#include <timber/scheduler/ThreadPoolScheduler.h>
#include <timber/ios/URIInputStream.h>
#include <timber/w3c/URI.h>
#include <timber/ios/util.h>
#include <timber/ContentType.h>

#include <canopy/mt/Mutex.h>
#include <canopy/mt/MutexGuard.h>
#include <canopy/fs/TempFile.h>

#include <cassert>
#include <memory>
#include <fstream>
#include <limits>

#if HAVE_LIBKML == 1
#include <kml/dom.h>
#endif

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;
using namespace ::tikal;
using namespace ::cartografo::projector;

namespace cartografo {
   namespace kml {
      namespace {
         static Log logger()
         {
            return "cartografo.kml.KmlNode";
         }
#if HAVE_LIBKML == 1

      static bool hasExtension(const string& s, const string& ext)
      {
         if (s.length() < ext.length()) {
            return false;
         }
         size_t pos = s.length() - ext.length();
         return s.find(ext.c_str(), pos, ext.length()) == pos;
      }

      using namespace ::kmldom;

      ::indigo::Color convertColor(const kmlbase::Color32& argb)
      {
         double a = argb.get_alpha();
         double r = argb.get_red();
         double g = argb.get_green();
         double b = argb.get_blue();

         return ::indigo::Color(r / 255.0, g / 255.0, b / 255.0, a / 255.0);
      }

      static ::std::shared_ptr< ContentType> getURIContentType(const ::timber::w3c::URI& uri)
      {
         ::std::shared_ptr< ContentType> ct;
         string p;

         if (uri.path()) {
            p = uri.path()->string();
         }

         if (!p.empty()) {
            if (hasExtension(p, ".kml")) {
               ct = KmlFactory::kmlContentType();
            }
            else if (hasExtension(p, ".kmz")) {
               ct = KmlFactory::kmzContentType();
            }
         }
         return ct;
      }

      struct Styles
      {
         Styles()throws()
         : iconStyle(new ::indigo::Group(true)),
         labelStyle(new ::indigo::Group()),
         lineStyle(new ::indigo::Group()),
         polygonStyle(new ::indigo::Group()),
         listStyle(new ::indigo::Group()),
         balloonStyle(new ::indigo::Group())
         {}

         ~Styles()throws() {}

         Reference< ::indigo::Group> iconStyle, labelStyle, lineStyle, polygonStyle, listStyle, balloonStyle;

      };

      struct KmlDocument;
      struct KmlFeature;
      struct KmlStyleBase;
      struct GoogleKmlNode;

      struct GoogleKmlFactory : public KmlFactory
      {
         GoogleKmlFactory()throws()
         : _imageFactory(::timber::media::ImageFactory::getBuiltinFactory()),
         _refreshScheduler(::timber::scheduler::ThreadPoolScheduler::create(1))
         {}
         ~GoogleKmlFactory()throws() {}

         Reference< KmlNode> createNode(const SharedRef< ::timber::w3c::URI>&,
               const Reference< GeodeticDatum>& datum)throws (::timber::ios::IOException,::std::invalid_argument);
         Reference< KmlNode> createNode(::std::istream& stream, const SharedRef< ::timber::w3c::URI>&,
               const Reference< GeodeticDatum>& datum)throws (::timber::ios::IOException,::std::invalid_argument);

         Reference< GoogleKmlNode> privateCreateNode(const SharedRef< ::timber::w3c::URI>&,
               const Reference< GeodeticDatum>& datum)throws (::timber::ios::IOException,::std::invalid_argument);
         Reference< GoogleKmlNode> privateCreateNode(::std::istream& stream,
               const SharedRef< ::timber::w3c::URI>&, const Reference< GeodeticDatum>& datum)throws (::timber::ios::IOException,::std::invalid_argument);

         Reference< GoogleKmlNode> privateCreateNode(::std::istream& stream,
               const SharedRef< ::timber::w3c::URI>&, ::std::shared_ptr< ContentType> ct,
               const Reference< GeodeticDatum>& datum)throws (::timber::ios::IOException,::std::invalid_argument);

         Pointer< KmlFeature> parseKml(const string& docstr, const ::std::shared_ptr< ::timber::w3c::URI>& bURI,
               const Reference< GeodeticDatum>& datum)
         throws ( invalid_argument);

         void setImageFactory(const ::std::shared_ptr< ::timber::media::ImageFactory>& f)throws()
         {
            if (f) {
               _imageFactory = f;
            }
            else {
               _imageFactory = ::timber::media::ImageFactory::getBuiltinFactory();
            }
         }
         void setScheduler(::std::shared_ptr< ::timber::scheduler::Scheduler> s)throws()
         {
            //TODO: what's the right thing to when switching the scheduler?
            if (s) {
               _refreshScheduler = ::std::move(s);
            }
            else {
               _refreshScheduler = ::timber::scheduler::ThreadPoolScheduler::create(1);
            }
         }

         private:
         SharedRef< ::timber::media::ImageFactory> _imageFactory;
         SharedRef< ::timber::scheduler::Scheduler> _refreshScheduler;
      };

      struct KmlStyleBase : public ::timber::Counted
      {
         KmlStyleBase()throws() {}
         ~KmlStyleBase()throws() {}

         /**
          * Apply this style to the specified feature
          */
         virtual Styles getStyles(StyleStateEnum styleState) const throws() = 0;

      };

      struct KmlStyle : public KmlStyleBase
      {
         KmlStyle()throws() {}
         ~KmlStyle()throws() {}

         Styles getStyles(StyleStateEnum styleState) const throws()
         {
            return _styles;
         }

         /**
          * Apply this style to the specified feature
          */
         Styles _styles;
      };

      struct KmlStyleMap : public KmlStyleBase
      {
         KmlStyleMap()throws() {}
         ~KmlStyleMap()throws() {}

         Styles getStyles(StyleStateEnum styleState) const throws()
         {
            auto i = _styles.find(styleState);
            if (i== _styles.end()) {
               logger().info("No styles found");
               return Styles();
            }
            else {
               return i->second->getStyles(styleState);
            }
         }

         /** The map of styles */
         public:
         ::std::map< StyleStateEnum, Reference< KmlStyleBase> > _styles;
      };

      struct KmlFeature : public virtual Feature
      {
         KmlFeature()throws()
         : _visible(true),_ancestorVisible(false),
         _featureRoot(new ::cartografo::geo::Group(true)),
         _root(new ::cartografo::geo::Group(true)),_styleState(STYLESTATE_NORMAL)
         {
         }

         ~KmlFeature()throws() {}

         inline const ::std::string& name() const throws() {return _name;}

         inline ::timber::Pointer< ::cartografo::geo::Node> node() const throws() {return _featureRoot;}

         inline bool isVisible() const throws() {return _visible;}
         inline bool isAncestorVisible() const throws() {return _ancestorVisible;}
         inline bool isShowing() const throws() {return isVisible() && isAncestorVisible();}

         void setAncestorVisible(bool ancestorVisible)throws()
         {
            updateVisibility(ancestorVisible,_visible);
         }

         void setVisible(bool featureVisible)throws()
         {
            updateVisibility(_ancestorVisible,featureVisible);
         }

         void updateVisibility(bool ancestorVisible, bool featureVisible)throws()
         {
            LogEntry(logger()).debugging() << "Visibility " << _name << " : " << ancestorVisible << " && " << featureVisible << doLog;
            // remember the effective visibility state
            bool wasShowing = isShowing();

            _ancestorVisible = ancestorVisible;
            _visible = featureVisible;

            // if the effective visibility has changed
            if (wasShowing != isShowing()) {
               _featureRoot->clear();
               if (!wasShowing) {
                  _featureRoot->add(_root);
               }
               visibilityChanged(!wasShowing);
            }
         }

         virtual void setShowAll(bool showAll)throws()
         {
            setVisible(showAll);
         }

         /** Notify this subclass that the effective visibility has changed
          * @param showing the value of <tt>this->isShowing()</tt>.
          **/
         virtual void visibilityChanged(bool showing)throws()
         {
            LogEntry(logger()).info() << "Visibility changed : " << showing << doLog;
         }

         void setStyleState(StyleStateEnum state)throws()
         {
            if (!_style || _styleState==state) {
               return;
            }
            _styleState = state;
            Styles s = _style->getStyles(state);
            _styles.balloonStyle->set(s.balloonStyle,0);
            _styles.iconStyle->set(s.iconStyle,0);
            _styles.labelStyle->set(s.labelStyle,0);
            _styles.lineStyle->set(s.lineStyle,0);
            _styles.polygonStyle->set(s.polygonStyle,0);
         }

         /** The visibility of this feature */
         public:
         bool _visible;

         /** True if the ancestor feature is visible as well. */
         private:
         bool _ancestorVisible;

         /** The factory that created this feature */
         public:
         ::timber::Pointer< GoogleKmlFactory> _factory;

         /** The feature name */
         public:
         ::std::string _name;

         /** The styles in effect for this feature */
         public:
         Pointer< KmlStyleBase> _style;

         /** The actual styles in effect */
         public:
         Styles _styles;

         /** The feature root itself */
         public:
         ::timber::Reference< ::cartografo::geo::Group> _featureRoot;

         /** The composite node for this feature */
         public:
         ::timber::Reference< ::cartografo::geo::Group> _root;

         /** The style state */
         public:
         StyleStateEnum _styleState;
      };

      struct GoogleKmlNode : public KmlNode
      {
         GoogleKmlNode(const Reference< GoogleKmlFactory>& factory, const ::std::string& docstr,
               const SharedRef< ::timber::w3c::URI>& uri, const Reference< GeodeticDatum>& datum)throws (invalid_argument)
         : _factory(factory),_datum(datum)
         {
            _feature = _factory->parseKml(docstr,uri, _datum);
            if (!_feature) {
               throw invalid_argument("Not a valid KML document");
            }

            // convert the feature to a node

            Reference< ::cartografo::geo::Group> node(new ::cartografo::geo::Group(true));
            node->add(new ::cartografo::geo::IndigoNode(new ::indigo::SolidFillNode(::indigo::Color(1,1,1,1))));
            node->add(new ::cartografo::geo::IndigoNode(new ::indigo::StrokeNode(::indigo::Color(0,0,0,1),2)));
            node->add(_feature->node());

            logger().debugging("Created a valid GoogleKmlNode");

            ::cartografo::geo::CompositeNode::setChildAt(node,0);
         }

         ~GoogleKmlNode()throws()
         {

         }

         /**
          * Get the KML feature that was loaded.
          * @return a kml feature or 0 if not loaded
          */
         Reference< KmlFeature> feature() const throws() {return _feature;}

         /** The factory to be used when creating or loading documents */
         private:
         const Reference< GoogleKmlFactory> _factory;

         private:
         const Reference< GeodeticDatum> _datum;

         private:
         Pointer< KmlFeature> _feature;

      };

      struct KmlContainer : public virtual Container, public KmlFeature
      {
         KmlContainer()throws() {}
         ~KmlContainer()throws() {}

         size_t featureCount() const throws() {return _features.size();}
         ::timber::Reference< Feature> feature(size_t i)
         {
            return _features.at(i);
         }

         void visibilityChanged(bool showing)throws()
         {
            for (Reference<KmlFeature>& f : _features) {
               f->setAncestorVisible(showing);
            }
         }

         virtual void setShowAll(bool showAll)throws()
         {
            for (Reference<KmlFeature>& f : _features) {
               f->setShowAll(showAll);
            }
            KmlFeature::setShowAll(showAll);
         }

         /**
          * Add a new feature.
          * @param f a feature
          */
         public:
         void addFeature(const Reference< KmlFeature>& f)throws()
         {
            f->setAncestorVisible(isShowing());
            _features.push_back(f);
            _root->add(f->node());
         }

         /** The features */
         private:
         vector< Reference< KmlFeature> > _features;
      };

      template<class T> struct RunnableFeature : public ::timber::scheduler::Runnable
      {

         /** The feature that is runnable */
         public:
         RunnableFeature(const Reference< T>& f)throws()
         : _feature(f->weakPointer()) {}

         ~RunnableFeature()throws() {}

         void run()throws()
         {
            try {
               Pointer< T> f = _feature.get();
               if (f->isShowing()) {
                  f->reload();
               }
            }
            catch (const ::std::exception& e) {
               logger().caught("Exception while attempting to reload KML link; link will not be refreshed again",e);
            }
         }

         private:
         ::timber::WeakPointer< T> _feature;
      };

      struct KmlNetworkLink : public virtual ::cartografo::kml::NetworkLink, public KmlFeature
      {

         KmlNetworkLink()throws()
         : _child(::cartografo::geo::ThreadSafeNode::create()),
           _maxDocumentSize(1024*1024),
           _refreshMode(REFRESHMODE_ONCHANGE),_refreshInterval(10)
         {
            _root->add(_child);
         }

         ~KmlNetworkLink()throws() {}

         void setURI(const ::std::shared_ptr< ::timber::w3c::URI>& uri)throws()
         {
            // force a reload
            {
               ::canopy::mt::MutexGuard<> G(_mutex);
               if (uri==_uri) {
                  return;
               }
               if (_uri && uri && _uri->equals(*uri)) {
                  return;
               }
               _uri = uri;
            }

            reload();
         }

         void visibilityChanged(bool showing)throws()
         {
            if (showing) {
               reload();
            }
         }

         bool reload()throws()
         {
            ::std::shared_ptr< ::timber::w3c::URI> uri;
            {
               ::canopy::mt::MutexGuard<> G(_mutex);
               if (_task) {
                  _task->cancel();
               }
               uri = _uri;
               if (_baseURI && uri) {
                  try {
                     uri = uri->resolve(*_baseURI);

                     if (uri->equals(*_baseURI)) {
                        logger().info("Base URL and network link url are the same; avoiding recursion");
                        return false;
                     }
                  }
                  catch (const ::std::exception& e) {
                     logger().caught("Could not reload link",e);
                  }
               }
            }

            if (!uri || !_datum) {
               return false;
            }

            // try to obtain the new child and set it
            try {
               logger().info("Loading link from : " +uri->string());
               Reference<GoogleKmlNode> kmlNode = _factory->privateCreateNode(uri,_datum);
               logger().info("Link loaded : " +uri->string());
               Pointer< ::cartografo::geo::Node> n = kmlNode->feature()->node();
               if (!n) {
                  logger().info("No node from link "+uri->string());
                  return true;
               }
               _child->setChild(n);
            }
            catch (const ::std::exception& e) {
               logger().caught("Failed to reload link " +uri->string(),e);
               return false;
            }

            // downloaded the node successfully, so now reschedule a task to refresh
            if (_refreshMode != REFRESHMODE_ONCHANGE) {
               // get the delay and convert from sec to msec
               ::std::uint64_t delayMS = 0;

               if (_refreshMode == REFRESHMODE_ONINTERVAL) {
                  delayMS = 1000.0 * _refreshInterval;
               }
               else if (_refreshMode==REFRESHMODE_ONEXPIRE) {
                  //FIXME: obtain the expiration
                  delayMS = 1000.0 * _refreshInterval;
               }
               else {
                  logger().info("Unexpected refresh mode");
               }

               if (delayMS>0) {
                  Reference<KmlNetworkLink> ref = toRef<KmlNetworkLink>();
                  auto runnable = ::std::make_shared<RunnableFeature<KmlNetworkLink>>(ref);
                  if (_task) {
                     _task->cancel();
                  }
                  _task = _scheduler->scheduleTask(runnable, ::timber::scheduler::TaskScheduler::Delay(delayMS));
                  if (_task) {
                     logger().debugging("Network link rescheduled");
                  }
                  else {
                     logger().info("Could not reschedule network link; link will not be refreshed again");
                     return false;
                  }
               }
            }
            return true;
         }

         ::canopy::mt::Mutex _mutex;

         Pointer< GeodeticDatum> _datum;

         /** The threadsafe node that we can update at any time */
         Reference< ::cartografo::geo::ThreadSafeNode> _child;

         /** The maximum size of document loaded remotely */
         size_t _maxDocumentSize;

         /** The base URL */
         ::std::shared_ptr< ::timber::w3c::URI> _baseURI;

         /** The URL */
        ::std::shared_ptr< ::timber::w3c::URI> _uri;

         /** The refresh mode */
         RefreshModeEnum _refreshMode;

         /** The refresh time in seconds */
         double _refreshInterval;

         ::std::shared_ptr< ::timber::scheduler::TaskScheduler> _scheduler;

         /** The scheduled task */
         private:
         ::std::shared_ptr< ::timber::scheduler::Task> _task;
      };

      struct KmlPlacemark : public virtual Placemark, public KmlFeature
      {
         KmlPlacemark()throws() {}
         ~KmlPlacemark()throws() {}

         void setStyleState(StyleState state)throws()
         {
            switch(state) {
               case StyleState::NORMAL:
               KmlFeature::setStyleState(STYLESTATE_NORMAL);
               break;
               case StyleState::HIGHLIGHT:
               KmlFeature::setStyleState(STYLESTATE_HIGHLIGHT);
               break;
               default:
               logger().warn("Unsupported style state");
               break;
            }
         }

         /** Set the geometry */
         public:
         void setGeometry(const Pointer< ::cartografo::geo::Node>& g)throws()
         {
            _root->add(g);
         }
      };

      struct KmlOverlay : public virtual KmlFeature
      {
         KmlOverlay()throws() {}
         ~KmlOverlay()throws() {}

         virtual void setRaster(const  ::std::shared_ptr< ::timber::media::ImageBuffer> &image, ::indigo::AffineTransform2D t) = 0;
      };

      struct KmlGroundOverlay : public virtual GroundOverlay, public virtual KmlOverlay
      {
         KmlGroundOverlay()throws()
         : _raster(new ::cartografo::geo::Raster())
         {
            _root->add(_raster);
         }

         ~KmlGroundOverlay()throws() {}

         void setRaster(const ::std::shared_ptr< ::timber::media::ImageBuffer> &image, ::indigo::AffineTransform2D t)
         {
            _image =image;
         }

         void setRotation(double rotDeg)throws()
         {
            if (rotDeg!=0) {
               //FIXME: what to do about this one
               //_raster->setRotation(::terra::toRadians(rotDeg));
            }
         }

         void setBounds(const ::terra::Bounds& b)throws()
         {
            _bounds = b;
            _raster->setRaster(RasterImage(b,_image));
         }

         /** The KML raster */
         private:
         ::timber::Reference< ::cartografo::geo::Raster> _raster;

         /** The image */
         private:
         ::indigo::Raster::ImagePtr _image;

         /** The extent of the overlay */
         private:
         ::terra::Bounds _bounds;
      };

      struct KmlFolder : public virtual Folder, public KmlContainer
      {
         KmlFolder()throws() {}
         ~KmlFolder()throws() {}
      };

      struct KmlDocument : public virtual Document, public KmlContainer
      {

         KmlDocument()throws() {}
         ~KmlDocument()throws() {}

         /**
          * Add a new style.
          * @param id a style id
          * @return the styles
          */
         public:
         Reference< KmlStyleBase> createStyle(const string& id, const Pointer< KmlStyleBase>& style)throws()
         {
            // add a hashmark
            const string sid = '#' + id;
            auto i = _styles.find(sid);
            if (i==_styles.end()) {
               LogEntry(logger()).debugging() << "Creating shared style " << sid << doLog;
               if (style) {
                  _styles.insert(make_pair(sid,style));
                  return style;
               }
               else {
                  Pointer< KmlStyleBase> s(new KmlStyle());
                  _styles.insert(make_pair(sid,s));
                  return s;
               }
            }
            else {
               LogEntry(logger()).warn() << "Already created shared style " << sid << doLog;
               // return a new style
               return i->second;
            }
         }

         /**
          * Find a style by its id
          * @param id an id that ideally starts with a hashmark
          * @param styles default
          */
         public:
         Pointer< KmlStyleBase> findStyle(const string& id) const throws()
         {
            const string sid = '#' + id;
            auto i = _styles.find(sid);
            if (i==_styles.end()) {
               i = _styles.find(id);
               if (i==_styles.end()) {
                  // style not found
                  LogEntry(logger()).warn() << "No such style " << sid << doLog;
                  return Pointer<KmlStyleBase>();
               }
            }
            return i->second;
         }

         /** The styles; these are mapping of URL to a group node; we use a pointer, but it will always be non-null */
         public:
         map< string, Reference< KmlStyleBase> > _styles;
      };

      // the visitor that builds a cartografo node
      struct GoogleNodeVisitor : public ::kmldom::Visitor
      {

         GoogleNodeVisitor(::std::shared_ptr< ::timber::w3c::URI> baseURI, const Reference< GeodeticDatum>& datum,
               const Reference< GoogleKmlFactory>& factory,
               const SharedRef< ::timber::media::ImageFactory>& ifactory,
               const SharedRef< ::timber::scheduler::Scheduler>& scheduler)
         : _baseURI(baseURI), _imageFactory(ifactory), _scheduler(scheduler), _datum(datum), _noOutline(
               new ::indigo::StrokeNode()), _noFill(new ::indigo::SolidFillNode()), _defaultIcon(
               _imageFactory->load("data/images/defaultPlacemark.png")), _factory(factory)
         {
            ::indigo::Point pts[4] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1}};

            ::indigo::Polygon p(4, pts);
            Pointer< ::indigo::Group> grp(new ::indigo::Group());
            grp->add(new ::indigo::Transform2D(::indigo::AffineTransform2D(0, 10000, 0, 0)));
            grp->add(new ::indigo::SolidFillNode(::indigo::Color(1, .2, .2, .5)));
            grp->add(new ::indigo::StrokeNode(::indigo::Color(1, 0, 0), 2));
            grp->add(new ::indigo::Polygons(p));

            _geometryType = Type_LineString;

            _defaultMarker = grp;
         }

         ~GoogleNodeVisitor()
         {
         }

         /**
          * Create an area from the current coordinates.
          *
          * @return an area
          */
         Pointer< Region> buildRegion()
         {
            Pointer< Area> a = AreaBuilder::createLoxodromicArea(_coordinates, false);
            if (!a) {
               logger().warn("Could not create area");
               return a;
            }

            // in order to determine if we're getting the smaller or the larger of two areas
            // we project it and see if we end up with regions that have holes.
            {
               // we compute the center of the region so that we can center the projection
               // around that point. This "should" help with cases where the region overlaps
               // the 180 degree mark and is speified in the wrong order, but the projection
               // only generates a single region.
               GeodeticCoordinates ctr = a->extent().getBounds(_datum).center();
               ctr = GeodeticCoordinates(0, ctr.longitude(), _datum);

               Reference< IdentityProjector2D> ip = new IdentityProjector2D(ctr);
               Pointer< Region2D> r = Projector2D::projectRegion(*ip, *a);

               // if the region has holes, then we know we must invert a
               if (!r) {
                  logger().warn("Could not project area");
                  return Pointer< Area>();
               }

               for (size_t i = 0, sz = r->areaCount(); i != sz; ++i) {
                  if (r->area(i)->contourCount() > 0) {
                     logger().debugging("Inverting area");
                     a = AreaBuilder::createLoxodromicArea(_coordinates, true);
                     break;
                  }
               }
               return a;
            }

         }

         /**
          * Visit an element.
          * @param e an element or null
          * @return true if e!=null, false otherwise
          */
         bool visit(const ElementPtr& e)
         {
            if (e) {
               LogEntry(logger()).tracing() << "Visiting " << typeid(*e).name() << doLog;
               e->Accept(this);
               return true;
            }
            else {
               return false;
            }
         }

         void VisitElement(const ElementPtr&)
         {
            // no op
         }

         void VisitAbstractLatLonBox(const AbstractLatLonBoxPtr& element)
         {
            double west = element->get_west();
            double east = element->get_east();
            double north = element->get_north();
            double south = element->get_south();

            _latlonBox = ::terra::Bounds::fromDegrees(south, west, north, east, _datum);
         }

         void VisitAbstractLink(const AbstractLinkPtr& element)
         {
            Pointer< KmlNetworkLink> f = feature().tryDynamicCast< KmlNetworkLink>();
            if (f) {
               try {
                  f->_uri = ::timber::w3c::URI::create(element->get_href());
               }
               catch (const ::std::exception& e) {
                  LogEntry(logger()).info() << "Invalid uri " << element->get_href() << doLog;
                  f->_uri = nullptr;
               }
               if (element->has_refreshmode()) {
                  switch (element->get_refreshmode()) {
                     case REFRESHMODE_ONCHANGE:
                     f->_refreshMode = REFRESHMODE_ONCHANGE;
                     break;
                     case REFRESHMODE_ONEXPIRE:
                     f->_refreshMode = REFRESHMODE_ONEXPIRE;
                     break;
                     case REFRESHMODE_ONINTERVAL:
                     f->_refreshMode = REFRESHMODE_ONINTERVAL;
                     break;
                     default:
                     logger().info("Unexpected refresh mode");
                     break;
                  }
               }
               if (element->has_refreshinterval()) {
                  f->_refreshInterval = element->get_refreshinterval();
               }
            }
         }

         void VisitAbstractView(const AbstractViewPtr& element)
         {
            // TODO: not supported
         }

         void VisitAlias(const AliasPtr& element)
         {
            // TODO: not supported
         }

         void VisitBalloonStyle(const BalloonStylePtr& element)
         {
            // TODO: not supported
         }

         void VisitBasicLink(const BasicLinkPtr& element)
         {
            // TODO: not supported
         }

         void VisitCamera(const CameraPtr& element)
         {
            // TODO: not supported
         }

         void VisitChange(const ChangePtr& element)
         {
            // TODO: not supported
         }

         void VisitColorStyle(const ColorStylePtr& element)
         {
            // TODO: not supported
         }

         void VisitContainer(const ContainerPtr& element)
         {
            VisitFeature(element);

            // visit all child features
            for (size_t i = 0, sz = element->get_feature_array_size(); i < sz; ++i) {
               visit(element->get_feature_array_at(i));
            }
         }

         void VisitCoordinates(const CoordinatesPtr& element)
         {
            _coordinates.clear();
            const size_t n = element->get_coordinates_array_size();
            if (n == 0) {
               return;
            }
            _coordinates.reserve(n);
            for (size_t i = 0; i < n; ++i) {
               const kmlbase::Vec3& vec = element->get_coordinates_array_at(i);
               _coordinates.push_back(
                     ::terra::GeodeticCoordinates::fromDegrees(vec.get_latitude(), vec.get_longitude(),
                           vec.get_altitude(), _datum));
               //                     LogEntry(logger()).debugging() << "Create coordinates " << _coordinates.back() << doLog;
            }
         }

         void VisitCreate(const CreatePtr& element)
         {
            // TODO: not supported
         }

         void VisitData(const DataPtr& element)
         {
            // TODO: not supported
         }

         void VisitDelete(const DeletePtr& element)
         {
            // TODO: not supported
         }

         void VisitDocument(const DocumentPtr& element)
         {
            Reference< KmlDocument> doc = pushNewFeature< KmlDocument>();

            // force creation of shared styles before we attempt to reference any
            // because we support an extension that enables references to shared style
            // using the id instead of "#"+id.
            for (size_t i = 0, sz = element->get_styleselector_array_size(); i < sz; ++i) {
               auto j = element->get_styleselector_array_at(i);
               if (j->Type() == Type_Style) {
                  visit(j);
                  _currentStyleMap = nullptr;
                  _currentStyles = nullptr;
               }
            }

            // force creation of shared styles before we attempt to reference any
            // because we support an extension that enables references to shared style
            // using the id instead of "#"+id.
            for (size_t i = 0, sz = element->get_styleselector_array_size(); i < sz; ++i) {
               auto j = element->get_styleselector_array_at(i);
               if (j->Type() != Type_Style) {
                  visit(j);
                  _currentStyleMap = nullptr;
                  _currentStyles = nullptr;
               }
            }

            VisitContainer(element);
            popFeature(doc);
         }

         void VisitExtendedData(const ExtendedDataPtr& element)
         {
            // TODO: not supported
         }

         void VisitFeature(const FeaturePtr& element)
         {
            if (element->has_name()) {
               feature()->_name = element->get_name();
            }

            KmlFeature& f = *feature();

            //FIXME: need to deal with visibility
            if (element->has_visibility()) {
               f._visible = element->get_visibility();
            }
            assert(!f.isShowing());

            // first, use the shared style
            // replace the default styles
            if (element->has_styleurl()) {
               f._style = getStyle(element->get_styleurl());
            }

            // inline style has precedence
            if (element->has_styleselector()) {
               f._style = nullptr;
               _currentStyleMap = nullptr;
               _currentStyles = nullptr;
               visit(element->get_styleselector());
               if (_currentStyleMap) {
                  f._style = _currentStyleMap;
               }
               else if (_currentStyles) {
                  f._style = _currentStyles;
               }
               _currentStyleMap = nullptr;
               _currentStyles = nullptr;
            }

            f._styleState = STYLESTATE_HIGHLIGHT;
            f.setStyleState(STYLESTATE_NORMAL);
         }

         void VisitField(const FieldPtr& element)
         {
            // TODO: not supported
         }

         void VisitFolder(const FolderPtr& element)
         {
            Reference< KmlFolder> folder = pushNewFeature< KmlFolder>();
            VisitContainer(element);
            popFeature(folder);
         }

         void VisitGeometry(const GeometryPtr& element)
         {
            // nothing to do
         }

         void VisitGroundOverlay(const GroundOverlayPtr& element)
         {
            Reference< KmlGroundOverlay> overlay = pushNewFeature< KmlGroundOverlay>();
            VisitOverlay(element);
            if (element->has_latlonbox()) {
               VisitLatLonBox(element->get_latlonbox());
               overlay->setBounds(_latlonBox);
               if (element->get_latlonbox()->has_rotation()) {
                  overlay->setRotation(element->get_latlonbox()->get_rotation());
               }
            }
            popFeature(overlay);
         }

         void VisitGxAnimatedUpdate(const GxAnimatedUpdatePtr& element)
         {
            // TODO: not supported
         }

         void VisitGxFlyTo(const GxFlyToPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxLatLonQuad(const GxLatLonQuadPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxPlaylist(const GxPlaylistPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxSoundCue(const GxSoundCuePtr& element)
         {
            // TODO: not supported
         }

         void VisitGxTimeSpan(const GxTimeSpanPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxTimeStamp(const GxTimeStampPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxTour(const GxTourPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxTourControl(const GxTourControlPtr& element)
         {
            // TODO: not supported
         }

         void VisitGxTourPrimitive(const GxTourPrimitivePtr& element)
         {
            // TODO: not supported
         }

         void VisitGxWait(const GxWaitPtr& element)
         {
            // TODO: not supported
         }

         void VisitHotSpot(const HotSpotPtr& element)
         {

            if (!_rasterImage) {
               return;
            }
            ::std::shared_ptr< ::timber::media::Image> image = _rasterImage;

            size_t w = image->width();
            size_t h = image->height();
            double x = w * (element->has_x() ? element->get_x() : 0.5);
            double y = h * (element->has_y() ? element->get_y() : 0.5);

            // note: indigo icons have a coordinate frame where (0,0) is the top-left
            // corner and (w-1,h-1) is the bottom-right corner.
            // This is different from kml origin
            if (element->has_xunits()) {
               switch (element->get_xunits()) {
                  case UNITS_PIXELS:
                  x = element->get_x();
                  break;
                  case UNITS_FRACTION:
                  x = element->get_x() * w;
                  break;
                  case UNITS_INSETPIXELS:
                  x = w - 1 - element->get_x();
                  break;
                  default:
                  logger().warn("Unexpected x-pixel unit");
                  break;
               }
            }

            if (element->has_yunits()) {
               switch (element->get_yunits()) {
                  case UNITS_PIXELS:
                  y = h - 1 - element->get_y();
                  break;
                  case UNITS_FRACTION:
                  y = element->get_y() * h;
                  break;
                  case UNITS_INSETPIXELS:
                  y = element->get_y();
                  break;
                  default:
                  logger().warn("Unexpected y-pixel unit");
                  break;
               }
            }
            //FIXME: do we apply the scale first or last
            ::indigo::AffineTransform2D current = _rasterTransform;

            if (x != 0 || y != 0) {
               ::indigo::AffineTransform2D translate = ::indigo::AffineTransform2D::createTranslation(-x, -y);
               _rasterTransform = ::indigo::AffineTransform2D(current, translate);
            }
         }

         ::std::shared_ptr< ::timber::media::ImageBuffer> loadImage(const string& url)throws()
         {
            auto uri = ::timber::w3c::URI::create(url);
            if (_baseURI) {
               uri = uri->resolve(*_baseURI);
            }

            // load the image
            ::std::shared_ptr< ::timber::media::Image>& image = _icons[uri];
            if (!image) {
               // load the image
               try {

                  ::timber::ios::URIInputStream stream(uri);
                  image = _imageFactory->read(stream);
               }
               catch (const exception& e) {
                  LogEntry(logger()).caught(e) << "Could not load image from url " << *uri << doLog;
               }
               if (!image) {
                  logger().info("Using default icon");
                  image = _defaultIcon;
               }
            }
            return ::std::dynamic_pointer_cast<::timber::media::ImageBuffer>(image);
         }

         void VisitIcon(const IconPtr& element)
         {
            _rasterImage = nullptr;
            _rasterTransform = ::indigo::AffineTransform2D();
            if (element->has_href()) {
               _rasterImage = loadImage(element->get_href());
            }
         }

         void VisitIconStyle(const IconStylePtr& element)
         {
            // first, visit the icon
            visit(element->get_icon());
            if (_rasterImage) {
               if (element->has_scale()) {
                  _rasterTransform = ::indigo::AffineTransform2D::createScale(element->get_scale());
               }
               if (!visit(element->get_hotspot())) {
                  // since a hotspot isn't defined we use .5*w x 0.5*h
                  double w = _rasterImage->width() / 2;
                  double h = _rasterImage->height() / 2;
                  ::indigo::AffineTransform2D translate = ::indigo::AffineTransform2D::createTranslation(-w, -h);
                  ::indigo::AffineTransform2D current = _rasterTransform;
                  _rasterTransform = ::indigo::AffineTransform2D(current, translate);
               }
               // needs to be an icon, not a raster
               Reference< ::indigo::Icon> icon(new ::indigo::Icon(_rasterImage,_rasterTransform));
               _currentStyles->_styles.iconStyle->add(icon);
               _rasterImage = nullptr;
               _rasterTransform = ::indigo::AffineTransform2D();
            }
         }

         void VisitIconStyleIcon(const IconStyleIconPtr& element)
         {
            _rasterImage = nullptr;
            _rasterTransform = ::indigo::AffineTransform2D();
            if (element->has_href()) {
               _rasterImage = loadImage(element->get_href());
            }
         }

         void VisitImagePyramid(const ImagePyramidPtr& element)
         {
            // TODO: not supported
         }

         void VisitInnerBoundaryIs(const InnerBoundaryIsPtr& element)
         {
            _geometry = nullptr;
            _region = nullptr;
            if (visit(element->get_linearring())) {
               _region = buildRegion();
               _geometry = nullptr;
            }
         }

         void VisitItemIcon(const ItemIconPtr& element)
         {
            // TODO: not supported
         }

         void VisitKml(const KmlPtr& element)
         {
            _lastFeature = nullptr;
            visit(element->get_feature());
         }

         void VisitLabelStyle(const LabelStylePtr& element)
         {
            if (element->has_color()) {
               ::indigo::Color color = convertColor(element->get_color());
               _currentStyles->_styles.labelStyle->set(new ::indigo::StrokeNode(color, 1), 0);
            }
         }

         void VisitLatLonAltBox(const LatLonAltBoxPtr& element)
         {
         }

         void VisitLatLonBox(const LatLonBoxPtr& element)
         {
            VisitAbstractLatLonBox(element);
         }

         void VisitLineString(const LineStringPtr& element)
         {
            _geometry = nullptr;
            if (visit(element->get_coordinates())) {
               if (_geometryType != Type_Polygon) {
                  Pointer< Path> p = PathBuilder::createGeodesicPath(_coordinates, false);
                  if (p) {
                     Reference< ::cartografo::geo::Group> g = new ::cartografo::geo::Group(true);
                     const Styles s = feature()->_styles;
                     g->add(new ::cartografo::geo::IndigoNode(s.lineStyle));
                     g->add(new ::cartografo::geo::Paths(p));
                     _geometry = g;
                  }
               }
            }
         }

         void VisitLineStyle(const LineStylePtr& element)
         {
            if (element->has_color() || element->has_width()) {
               ::indigo::Color color = convertColor(
                     element->has_color() ? element->get_color() : kmlbase::Color32(0xffffffff));
               double w = element->has_width() ? element->get_width() : 1.0;
               _currentStyles->_styles.lineStyle->set(new ::indigo::StrokeNode(color, w), 0);
            }
         }

         void VisitLinearRing(const LinearRingPtr& element)
         {
            _geometry = nullptr;
            if (visit(element->get_coordinates())) {
               if (_geometryType != Type_Polygon) {
                  Pointer< Path> p = PathBuilder::createGeodesicPath(_coordinates, true);
                  if (p) {
                     Reference< ::cartografo::geo::Group> g = new ::cartografo::geo::Group(true);
                     const Styles& s = feature()->_styles;
                     g->add(new ::cartografo::geo::IndigoNode(s.lineStyle));
                     g->add(new ::cartografo::geo::Paths(p));
                     _geometry = g;
                  }
               }
            }
         }

         void VisitLink(const LinkPtr& element)
         {
            VisitAbstractLink(element);
         }

         void VisitLinkSnippet(const LinkSnippetPtr& element)
         {
            // TODO: not supported
         }

         void VisitListStyle(const ListStylePtr& element)
         {
            // TODO: not supported
         }

         void VisitLocation(const LocationPtr& element)
         {
            // TODO: not supported
         }

         void VisitLod(const LodPtr& element)
         {
            // TODO: not supported
         }

         void VisitLookAt(const LookAtPtr& element)
         {
            // TODO: not supported
         }

         void VisitMetadata(const MetadataPtr& element)
         {
            // TODO: not supported
         }

         void VisitModel(const ModelPtr& element)
         {
            // TODO: not supported
         }

         void VisitMultiGeometry(const MultiGeometryPtr& element)
         {
            _geometry = nullptr;
            _geometryType = Type_MultiGeometry;
            Reference< ::cartografo::geo::Group> g = new ::cartografo::geo::Group();
            for (size_t i = 0, sz = element->get_geometry_array_size(); i < sz; ++i) {
               visit(element->get_geometry_array_at(i));
               if (_geometry) {
                  g->add(_geometry);
                  _geometry = nullptr;
               }
            }
            assert(_geometry == nullptr);
            if (g->nodeCount() != 0) {
               _geometry = g;
            }
            _geometryType = Type_LineString;
         }

         void VisitNetworkLink(const NetworkLinkPtr& element)
         {
            Reference< KmlNetworkLink> link = pushNewFeature< KmlNetworkLink>();
            link->_baseURI = _baseURI;
            link->_datum = _datum;
            link->_scheduler = _scheduler;
            VisitFeature(element);
            visit(element->get_link());
            popFeature(link);
            if (link->isShowing()) {
               link->reload();
            }
         }

         void VisitNetworkLinkControl(const NetworkLinkControlPtr& element)
         {
            // TODO: not supported
         }

         void VisitObject(const ObjectPtr& element)
         {
            // TODO: not supported
         }

         void VisitOrientation(const OrientationPtr& element)
         {
            // TODO: not supported
         }

         void VisitOuterBoundaryIs(const OuterBoundaryIsPtr& element)
         {
            _geometry = nullptr;
            _region = nullptr;
            if (visit(element->get_linearring())) {
               _region = buildRegion();
               _geometry = nullptr;
            }
         }

         void VisitOverlay(const OverlayPtr& element)
         {
            VisitFeature(element);
            Reference< KmlOverlay> overlay = feature();

            if (element->has_icon()) {
               VisitIcon(element->get_icon());
               overlay->setRaster(_rasterImage,_rasterTransform);
               _rasterImage = nullptr;
               _rasterTransform = ::indigo::AffineTransform2D();
            }
         }

         void VisitOverlayXY(const OverlayXYPtr& element)
         {
            // TODO: not supported
         }

         void VisitPair(const PairPtr& element)
         {
            Reference< KmlStyleMap> smap = _currentStyleMap;
            if (!element->has_key()) {
               return;
            }
            Pointer< KmlStyleBase> s;

            if (element->has_styleurl()) {
               s = getStyle(element->get_styleurl());
            }
            else if (element->has_styleselector()) {
               _currentStyles = nullptr;
               // temporarily reset the style map and then restore it
               _currentStyleMap = nullptr;
               visit(element->get_styleselector());
               s = _currentStyles;
               if (!s) {
                  s = _currentStyleMap;
               }
               _currentStyleMap = smap;
            }
            if (s) {
               smap->_styles.insert(make_pair((StyleStateEnum) element->get_key(), s));
            }
         }

         void VisitPhotoOverlay(const PhotoOverlayPtr& element)
         {
            // TODO: not supported
         }

         void VisitPlacemark(const PlacemarkPtr& element)
         {
            Reference< KmlPlacemark> placemark = pushNewFeature< KmlPlacemark>();
            VisitFeature(element);
            if (visit(element->get_geometry())) {
               if (_geometry) {
                  placemark->setGeometry(_geometry);
                  _geometry = nullptr;
               }
            }
            popFeature(placemark);
         }

         void VisitPoint(const PointPtr& element)
         {
            _geometry = nullptr;
            _geometryType = Type_Point;
            if (visit(element->get_coordinates())) {
               if (_coordinates.size() == 1) {
                  Pointer< KmlPlacemark> p = feature().tryDynamicCast< KmlPlacemark>();
                  if (p) {
                     LogEntry(logger()).debugging() << "Visit point of placemark at " << _coordinates.front() << doLog;
                     _geometry = new ::cartografo::geo::Marker(_coordinates.front(), p->_styles.iconStyle);
                  }
                  else {
                     logger().info("Not a placemark point; using line-style");
                     Reference< ::cartografo::geo::Group> g = new ::cartografo::geo::Group(true);
                     const Styles& s = feature()->_styles;
                     g->add(new ::cartografo::geo::IndigoNode(s.lineStyle));
                     g->add(new ::cartografo::geo::Locations(_coordinates.front()));
                     _geometry = g;
                  }
               }
            }
            _geometryType = Type_LineString;
         }

         void VisitPolyStyle(const PolyStylePtr& element)
         {
            if (element->has_outline()) {
               _currentStyles->_styles.polygonStyle->add(_currentStyles->_styles.lineStyle);
            }
            else {
               _currentStyles->_styles.polygonStyle->add(_noOutline);
            }
            if (element->has_fill()) {
               ::indigo::Color color = convertColor(
                     element->has_color() ? element->get_color() : kmlbase::Color32(0xffffffff));
               _currentStyles->_styles.polygonStyle->add(new ::indigo::SolidFillNode(color));
            }
            else {
               _currentStyles->_styles.polygonStyle->add(_noFill);
            }
         }

         void VisitPolygon(const PolygonPtr& element)
         {
            _geometry = nullptr;
            _region = nullptr;
            _geometryType = Type_Polygon;
            if (visit(element->get_outerboundaryis())) {
               Pointer< Region> r = _region;
               for (size_t i = 0, sz = element->get_innerboundaryis_array_size(); r && i < sz; ++i) {
                  _region = nullptr;
                  visit(element->get_innerboundaryis_array_at(i));
                  if (_region) {
                     logger().info("Subtract regions");
                     r = Region::subtract(r, _region);
                     _region = nullptr;
                  }
               }
               if (r) {
                  Reference< ::cartografo::geo::Group> g = new ::cartografo::geo::Group(true);
                  const Styles& s = feature()->_styles;
                  g->add(new ::cartografo::geo::IndigoNode(s.polygonStyle));
                  g->add(new ::cartografo::geo::Regions(r));
                  _geometry = g;
               }
               _region = nullptr;
            }
            _geometryType = Type_LineString;
         }

         void VisitRegion(const RegionPtr& element)
         {
            // TODO: not supported
         }

         void VisitResourceMap(const ResourceMapPtr& element)
         {
            // TODO: not supported
         }

         void VisitRotationXY(const RotationXYPtr& element)
         {
            // TODO: not supported
         }

         void VisitScale(const ScalePtr& element)
         {
            // TODO: not supported
         }

         void VisitSchema(const SchemaPtr& element)
         {
            // TODO: not supported
         }

         void VisitSchemaData(const SchemaDataPtr& element)
         {
            // TODO: not supported
         }

         void VisitScreenOverlay(const ScreenOverlayPtr& element)
         {
            // TODO: not supported
         }

         void VisitScreenXY(const ScreenXYPtr& element)
         {
            // TODO: not supported
         }

         void VisitSimpleData(const SimpleDataPtr& element)
         {
            // TODO: not supported
         }

         void VisitSimpleField(const SimpleFieldPtr& element)
         {
            // TODO: not supported
         }

         void VisitSize(const SizePtr& element)
         {
            // TODO: not supported
         }

         void VisitSnippet(const SnippetPtr& element)
         {
            // TODO: not supported
         }

         void VisitStyle(const StylePtr& element)
         {
            assert(!_currentStyles);
            assert(!_currentStyleMap);

            _currentStyles = new KmlStyle();

            // first, setup the styles
            if (element->has_id()) {
               Pointer< KmlDocument> doc = feature().tryDynamicCast< KmlDocument>();
               if (doc) {
                  logger().info("Got a style " + element->get_id());
                  _currentStyles = doc->createStyle(element->get_id(), _currentStyles);
               }
               else {
                  LogEntry(logger()).info() << "Ignoring local style with id " << element->get_id() << doLog;
               }
            }

            visit(element->get_balloonstyle());
            visit(element->get_iconstyle());
            visit(element->get_labelstyle());
            visit(element->get_linestyle());
            visit(element->get_liststyle());
            visit(element->get_polystyle());
         }

         void VisitStyleMap(const StyleMapPtr& element)
         {
            assert(!_currentStyles);
            assert(!_currentStyleMap);

            _currentStyleMap = new KmlStyleMap();

            // first, setup the styles
            if (element->has_id()) {
               Pointer< KmlDocument> doc = feature().tryDynamicCast< KmlDocument>();
               if (doc) {
                  logger().info("Got a style map " + element->get_id());
                  _currentStyleMap = doc->createStyle(element->get_id(), _currentStyleMap);
               }
               else {
                  LogEntry(logger()).info() << "Ignoring local style with id " << element->get_id() << doLog;
               }
            }

            for (size_t i = 0, sz = element->get_pair_array_size(); i < sz; ++i) {
               visit(element->get_pair_array_at(i));
            }

         }

         void VisitStyleSelector(const StyleSelectorPtr& element)
         {
            // nothing to do
         }

         void VisitSubStyle(const SubStylePtr& element)
         {
            // nothing to do
         }

         void VisitTimePrimitive(const TimePrimitivePtr& element)
         {
            // TODO: not supported
         }

         void VisitTimeSpan(const TimeSpanPtr& element)
         {
            // TODO: not supported
         }

         void VisitTimeStamp(const TimeStampPtr& element)
         {
            // TODO: not supported
         }

         void VisitUpdate(const UpdatePtr& element)
         {
            // TODO: not supported
         }

         void VisitUpdateOperation(const UpdateOperationPtr& element)
         {
            // TODO: not supported
         }

         void VisitUrl(const UrlPtr& element)
         {
            // TODO: not supported
         }

         void VisitVec2(const Vec2Ptr& element)
         {
            // for some reason, the hotspot doesn't implement the accept call, so we need to
            // check if this is a hot spot
            const HotSpotPtr hotspot = AsHotSpot(element);
            if (hotspot) {
               VisitHotSpot(hotspot);
               return;
            }
         }

         void VisitViewVolume(const ViewVolumePtr& element)
         {
            // TODO: not supported
         }

         /**
          * Get a styles root node. If the node does not yet exist, then an empty indigo node is created.
          * @return an indigo node
          */
         private:
         Pointer< KmlStyleBase> getStyle(const string& styleURL)
         {
            Pointer< KmlStyleBase> res;
            if (styleURL.empty()) {
               return res;
            }
            Pointer< KmlDocument> doc = featureDocument();
            if (doc) {
               res = doc->findStyle(styleURL);
               if (!res) {
                  if (styleURL[0] == '#') {
                     // create a style on the fly
                     res = doc->createStyle(styleURL.substr(1), res);
                  }
               }
            }

            if (!res) {
               // download a style
               try {
                  auto uri = ::timber::w3c::URI::create(styleURL);
                  if (_baseURI) {
                     uri = uri->resolve(*_baseURI);
                  }
                  if (uri->fragment()) {
                     string fragment = uri->fragment()->string();
                     Reference< GoogleKmlNode> node = _factory->privateCreateNode(uri, _datum);
                     Pointer< KmlDocument> remoteDoc = node->feature().tryDynamicCast< KmlDocument>();
                     if (remoteDoc) {
                        res = remoteDoc->findStyle('#' + fragment);
                     }
                     if (res) {
                        LogEntry(logger()).info() << "Loaded style from " << styleURL << doLog;
                     }
                     else {
                        LogEntry(logger()).info() << "Failed to load style from " << styleURL << doLog;
                     }
                  }
                  else {
                     LogEntry(logger()).info() << "Style url " << styleURL << " has no fragment part " << doLog;
                  }
               }
               catch (::timber::ios::IOException& e) {
                  LogEntry(logger()).caught(e) << "Failed to fetch style from " << styleURL << doLog;
               }
               catch (::std::exception& e) {
                  LogEntry(logger()).caught(e) << "Failed to fetch style from " << styleURL << doLog;
               }
            }
            return res;
         }

         /**
          * Check if the specified element is a descendent of a given type node
          * @param element an element
          * @return true if the element is a descendent of another node.
          */
         private:
         static bool isDescendentOf(const ElementPtr& elemt, const KmlDomType type)throws()
         {
            ElementPtr p = elemt->GetParent();
            while (p) {
               if (p->IsA(type)) {
                  return true;
               }
               p = p->GetParent();
            }
            return false;
         }

         /**
          * Get the current feature
          * @return the current feature
          */
         private:
         Reference< KmlFeature> feature() const throws() {return _features.back();}

         /**
          * Get the ancestor document.
          * @return an ancestor document.
          */
         private:
         Pointer< KmlDocument> featureDocument()throws()
         {
            Pointer<KmlDocument> doc;
            for (auto i = _features.rbegin();!doc && i!=_features.rend();++i) {
               doc = i->tryDynamicCast<KmlDocument>();
            }
            return doc;
         }

         /**
          * Push a new feature.
          * @param f a feature
          */
         private:
         template <class T>
         Reference< T> pushNewFeature()throws()
         {
            Reference<T> f(new T());
            pushFeature(f);
            return f;
         }

         /**
          * Push a new feature. This also copies the styles of the parent
          * into the new feature
          * @param f a feature
          */
         private:
         void pushFeature(const Reference< KmlFeature>& f)throws()
         {
            if (!_features.empty()) {
               Reference<KmlContainer> c = _features.back();
               f->_style = c->_style;
               c->addFeature(f);
            }
            else {
               Reference<KmlStyle> s = new KmlStyle();
               s->_styles.iconStyle->add(new ::indigo::Icon(_defaultIcon));
               f->_style = s;
            }
            f->_factory = _factory;
            _features.push_back(f);
         }

         /**
          * Pop a feature.
          * @param expected feature to pop
          */
         private:
         void popFeature(const Reference< KmlFeature>& expected)throws()
         {
            Reference<KmlFeature> res = popFeature();
            assert(res==expected && "Expected to pop a specific feature");
         }

         /**
          * Pop a feature.
          * @param expected feature to pop
          * @return the popped feature
          */
         private:
         Reference< KmlFeature> popFeature()throws()
         {
            if (_features.empty()) {
               ::std::abort();
            }
            auto f = _features.back();
            _features.pop_back();
            _lastFeature = f;
            return f;
         }

         private:
         ::std::shared_ptr< ::timber::w3c::URI> _baseURI;

         private:
         SharedRef< ::timber::media::ImageFactory> _imageFactory;

         /** The link scheduler */
         private:
         SharedRef< ::timber::scheduler::Scheduler> _scheduler;

         /** The features that are being built top-down */
         private:
         vector< Reference< KmlFeature> > _features;

         /** The style being created; this structure is populated top-down */
         private:
         Pointer< KmlStyle> _currentStyles;

         /** The current style map */
         private:
         Pointer< KmlStyleMap> _currentStyleMap;

         /** The default marker
          */
         private:
         Pointer< ::indigo::Node> _defaultMarker;

         /** The geodetic datum used by KML */
         private:
         const Reference< ::terra::GeodeticDatum> _datum;

         /** The styles; these are mapping of URL to a group node; we use a pointer, but it will always be non-null */
         private:
         map< string, Styles> _styles;

         /** The current raster */
         private:
         ::std::shared_ptr< ::timber::media::ImageBuffer> _rasterImage;

         /** The current raster */
         private:
         ::indigo::AffineTransform2D _rasterTransform;

         /** The styles; these are mapping of URL to a group node; we use a pointer, but it will always be non-null */
         private:
         map< SharedRef< ::timber::w3c::URI>, ::std::shared_ptr< ::timber::media::Image> > _icons;

         /** The default styles */
         private:
         Styles _defaultStyles;

         /** The bounds of a latlon box */
         private:
         ::terra::Bounds _latlonBox;

         /**
          * A region built from inner and outer boundaries
          */
         private:
         Pointer< ::cartografo::Region> _region;

         /** The last coordinates that were parsed */
         private:
         vector< ::terra::GeodeticCoordinates> _coordinates;

         /** The geometry type to be built */
         private:
         KmlDomType _geometryType;

         /** The geometry nodes built bottom-up */
         private:
         Pointer< ::cartografo::geo::Node> _geometry;

         /** Nodes to turn off outline and fill, respectively */
         private:
         Reference< ::indigo::Node> _noOutline, _noFill;

         /** The default placemark image */
         private:
         SharedRef< ::timber::media::Image> _defaultIcon;

         /** The most recently created feature */
         public:
         Pointer< Feature> _lastFeature;

         /** The factory to be used when creating or loading documents */
         private:
         const Reference< GoogleKmlFactory> _factory;
      };

      Reference< KmlNode> GoogleKmlFactory::createNode(::std::istream& stream,
            const SharedRef< ::timber::w3c::URI>& uri, const Reference< ::terra::GeodeticDatum>& datum)
      throws (::timber::ios::IOException,::std::invalid_argument)
      {
         Reference< GoogleKmlNode> node = privateCreateNode( stream, uri,datum);
         node->feature()->setAncestorVisible(true);
         return node;
      }

      // first, open the stream; if that fails, due to a bad URI, then we throw an IOException
      Reference< KmlNode> GoogleKmlFactory::createNode(const SharedRef< ::timber::w3c::URI>& uri,
            const Reference< ::terra::GeodeticDatum>& datum)
      throws (::timber::ios::IOException,::std::invalid_argument)
      {
         Reference< GoogleKmlNode> node = privateCreateNode( uri,datum);
         node->feature()->setAncestorVisible(true);
         return node;
      }

      Reference< GoogleKmlNode> GoogleKmlFactory::privateCreateNode(::std::istream& stream,
            const SharedRef< ::timber::w3c::URI>& uri, const Reference< ::terra::GeodeticDatum>& datum)
      throws (::timber::ios::IOException,::std::invalid_argument)
      {
         return privateCreateNode( stream, uri,getURIContentType(*uri),datum);
      }

      // first, open the stream; if that fails, due to a bad URI, then we throw an IOException
      Reference< GoogleKmlNode> GoogleKmlFactory::privateCreateNode(const SharedRef< ::timber::w3c::URI>& uri,
            const Reference< ::terra::GeodeticDatum>& datum)
      throws (::timber::ios::IOException,::std::invalid_argument)
      {
         unique_ptr< ::timber::ios::URIInputStream> stream;
         try {
            stream.reset(new ::timber::ios::URIInputStream(uri));
         }
         catch (const ::timber::ios::IOException& e) {
            throw;
         }
         catch (const ::std::exception& e) {
            throw ::timber::ios::IOException(string("Could not retrieve document :")+e.what());
         }
         return privateCreateNode(*stream,uri,stream->contentType(),datum);
      }

      Reference< GoogleKmlNode> GoogleKmlFactory::privateCreateNode(::std::istream& stream,
            const SharedRef< ::timber::w3c::URI>& uri,::std::shared_ptr< ContentType> ct,
            const Reference< ::terra::GeodeticDatum>& datum)
      throws (::timber::ios::IOException,::std::invalid_argument)
      {

         // need to determine the content type if not provided.
         if (!ct) {
            ct = ContentType::guess(stream,512);
            if (!ct) {
               throw ::std::invalid_argument("No content-type");
            }
         }

         if (ct->type()!="application") {
            throw ::std::invalid_argument("Unexpected content type " + ct->toString());
         }

         // if it's a KMZ file, then we need to download the entire archive to a temporary file,
         // find the first entry with a .kml extension, and then recursive call this method again
         // with a URI that points to the archive entry
         if (ct->subType()=="vnd.google-earth.kmz" || ct->subType()=="zip") {
            LogEntry(logger()).info() << "Loading KMZ from url " << *uri << doLog;
            // create a temp file for the archive
            ::canopy::fs::TempFile tmpFile;
            {
               // download the archive to the temp folder
               ofstream os(tmpFile.path().c_str());
               ::timber::ios::fetchStream(stream,numeric_limits<size_t>::max(),os);
            }

            ::std::shared_ptr< ::timber::archive::Archive> archive = ::timber::archive::ArchiveFactory::getFactory()->openArchive(tmpFile.path());
            if(!archive) {
               LogEntry(logger()).warn() << "Could not open KMZ archive : " << *uri << doLog;
               throw new invalid_argument("KMZ archives are not supported");
            }

            // find the first entry in the archive that ends with ".kml"
            for (::std::unique_ptr< ::timber::archive::Cursor> c = archive->openCursor();*c;++(*c)) {
               if (hasExtension(c->entry().name(),".kml")) {
                  // need a new URI before we can make the recursive call
                  auto archiveURI = ::timber::w3c::URI::create("archive:"+tmpFile.path()+"!/"+c->entry().name());
                  return privateCreateNode(c->entry().stream(), archiveURI,kmlContentType(),datum);
               }
            }

            throw invalid_argument("No KML file found in the archive");
         }

         // if it's a KML file, then we just load it and process it.
         if (ct->subType()=="vnd.google-earth.kml+xml" || ct->subType()=="xml") {
            LogEntry(logger()).info() << "Loading KML from url " << *uri << doLog;
            // it's an XML document just fetch it into a string
            std::ostringstream os;
            ::timber::ios::fetchStream(stream,numeric_limits<size_t>::max(),os);
            const string str(os.str());
            os.str("");
            return new GoogleKmlNode(toRef<GoogleKmlFactory>(),str,uri,datum);
         }

         // getting here means that we were not able to read the document
         throw ::std::invalid_argument("Unexpected content type " + ct->toString());
      }

      Pointer< KmlFeature> GoogleKmlFactory::parseKml(const string& docstr, const ::std::shared_ptr< ::timber::w3c::URI>& bURI,
            const Reference< GeodeticDatum>& datum)
      throws (invalid_argument)
      {
         string errstr;
         // strange, but if we specify an incorrect namespace, then ParseAtom creates a SIGSEV
         kmldom::ElementPtr elmt = kmldom::ParseNS(docstr, &errstr);

         if (elmt) {
            GoogleNodeVisitor visitor(bURI,datum, toRef<GoogleKmlFactory>(),_imageFactory,_refreshScheduler);
            visitor.visit(elmt);
            return visitor._lastFeature;
         }

         string msg("Not a valid XML document");

         if (!errstr.empty()) {
            msg += " : ";
            msg += errstr;
         }

         throw invalid_argument(msg);
      }

#endif
   }

   ::timber::SharedRef< ::timber::ContentType> KmlFactory::kmzContentType()
   throws()
   {
      return ContentType::parse("application/vnd.google-earth.kmz");
   }
   ::timber::SharedRef< ::timber::ContentType> KmlFactory::kmlContentType()
   throws()
   {
      return ContentType::parse("application/vnd.google-earth.kml+xml");
   }

   KmlFactory::KmlFactory()
   throws()
   {

   }

   KmlFactory::~KmlFactory()
   throws ()
   {
   }

   Reference< KmlFactory> KmlFactory::newFactory()
   throws( ::std::exception)
   {
#if    HAVE_LIBKML == 1
      return new GoogleKmlFactory();
#else
      throw ::std::runtime_error("Default KmlFactory not configured");
#endif
   }

   Reference< KmlNode> KmlFactory::createNode(const SharedRef< ::timber::w3c::URI>& uri,
         const Reference< ::terra::GeodeticDatum>& datum)
         throws (::timber::ios::IOException,::std::invalid_argument)
   {
      // first, open the stream; if that fails, due to a bad URI, then we throw an IOException
      unique_ptr< ::timber::ios::URIInputStream> stream;
      try {
         stream.reset(new ::timber::ios::URIInputStream(uri));
      }
      catch (const ::timber::ios::IOException& e) {
         throw;
      }
      catch (const ::std::exception& e) {
         throw ::timber::ios::IOException(string("Could not retrieve document :") + e.what());
      }
      return createNode(*stream, uri, datum);
   }

}

}
