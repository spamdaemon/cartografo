#ifndef _CARTOGRAFO_KML_CONTAINER_H
#define _CARTOGRAFO_KML_CONTAINER_H

#ifndef _TIMBER_REFERENCE_H
#include <timber/Reference.h>
#endif


#ifndef _CARTOGRAFO_KML_FEATURE_H
#include <cartografo/kml/Feature.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This class corresponds to a KML container.
       */
      class Container : public virtual Feature
      {
            Container(const Container&);
            Container&operator=(const Container&);

            /** Default constructor */
         protected:
            Container()throws();

            /** Destructor */
         public:
            ~Container()throws();

            /**
             * Get the number of features in this container.
             * @return the number features
             */
         public:
           virtual size_t featureCount() const throws() = 0;

           /**
            * Get a feature at the specified index.
            * @param i the index of a feature
            * @return a feature or null if there is no such feature.
            */
         public:
           virtual ::timber::Reference<Feature> feature(size_t i) = 0;
      };

   }

}

#endif
