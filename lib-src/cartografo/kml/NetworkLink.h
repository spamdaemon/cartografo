#ifndef _CARTOGRAFO_KML_NETWORKLINK_H
#define _CARTOGRAFO_KML_NETWORKLINK_H

#ifndef _CARTOGRAFO_KML_FEATURE_H
#include <cartografo/kml/Feature.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This class corresponds to a KML NetworkLink.
       */
      class NetworkLink : public virtual Feature
      {

            NetworkLink(const NetworkLink&);
            NetworkLink&operator=(const NetworkLink&);

            /** Default constructor */
         protected:
            NetworkLink()throws();

            /** Destructor */
         public:
            ~NetworkLink()throws();

      };

   }

}

#endif
