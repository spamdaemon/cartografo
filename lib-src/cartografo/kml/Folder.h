#ifndef _CARTOGRAFO_KML_FOLDER_H
#define _CARTOGRAFO_KML_FOLDER_H

#ifndef _CARTOGRAFO_KML_CONTAINER_H
#include <cartografo/kml/Container.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This class corresponds to a KML placemark.
       */
      class Folder : public virtual Container
      {
            Folder(const Folder&);
            Folder&operator=(const Folder&);

            /** Default constructor */
         protected:
            Folder()throws();

            /** Destructor */
         public:
            ~Folder()throws();
      };

   }

}

#endif
