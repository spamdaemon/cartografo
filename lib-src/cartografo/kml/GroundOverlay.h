#ifndef _CARTOGRAFO_KML_GROUNDOVERLAY_H
#define _CARTOGRAFO_KML_GROUNDOVERLAY_H

#ifndef _CARTOGRAFO_KML_FEATURE_H
#include <cartografo/kml/Feature.h>
#endif

namespace cartografo {
   namespace kml {

      /**
       * This class corresponds to a KML placemark.
       */
      class GroundOverlay : public virtual Feature
      {
            GroundOverlay(const GroundOverlay&);
            GroundOverlay&operator=(const GroundOverlay&);

            /** Default constructor */
         protected:
            GroundOverlay()throws();

            /** Destructor */
         public:
            ~GroundOverlay()throws();
      };

   }

}

#endif
