#include <cartografo/Projection.h>
//FIXME#include <cartografo/projection/IdentityProjection.h>
#include <cartografo/ProjectionRequest.h>
#include <cartografo/ProjectionDomain.h>

#include <tikal/Point2D.h>

#include <terra/Bounds.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Location.h>

using namespace ::terra;

namespace cartografo {
   Projection::Projection()
   throws()
   {}
   Projection::~Projection()
   throws()
   {}

   ::timber::Reference< Projection> Projection::create(const ::timber::Reference< ProjectedCRS>& crs,
         const ::timber::Reference< ProjectionDomain>& boundary)
   throws(::std::invalid_argument)
   {
      struct Impl : public Projection {
         Impl(const ::timber::Reference< ProjectedCRS>& xcrs,const ::timber::Reference< ProjectionDomain>& xboundary) throws()
         : _crs(xcrs),_domain(xboundary) {
         }

         ~Impl() throws() {}

         ::timber::Reference< ProjectionDomain> domain() const throws()
         {
            return _domain;
         }

         ::terra::EpsgCode epsgCode() const throws() {return _crs->epsgCode();}

         bool equals (const CoordinateReferenceSystem& e) const throws()
         {
            const Impl* obj = dynamic_cast<const Impl*>(&e);
            if (obj!=0) {
               return _crs->equals(*obj->_crs);
            }
            else {
               return _crs->equals(e);
            }
         }

         size_t projectUnsafe(size_t n, tikal::Point2D* lonlat) const throws()
         {
            tikal::Point2D* cur=lonlat;
            tikal::Point2D* end = cur+n;
            while(cur!=end) {
               const EPSG4979Coordinates epsg4979 {cur->y(),cur->x(),0};
               double res[2];
               if (!_crs->fromEPSG4979(epsg4979,res)) {
                  break;
               }
               cur->set(res[0],res[1]);
               ++cur;
            }
            return cur-lonlat;
         }

         ::terra::GeodeticCoordinates projectInverse(double x, double y) const
         throws( ::std::exception)
         {
            EPSG4979Coordinates epsg4979;
            double pts[] = {x,y};
            if (!_crs->toEPSG4979(pts,epsg4979)) {
               throw ::std::runtime_error("Could not inverse project");
            }
            return epsg4979;
         }

         bool isInverseProjectionEnabled() const throws() {return true;}
         size_t project(size_t n, ProjectionRequest* req) const throws()
         {
            ProjectionRequest* cur=req;
            ProjectionRequest* end=cur+n;
            while(cur!=end) {
               const EPSG4979Coordinates epsg4979 {cur->lat,cur->lon,cur->alt};
               double res[2];
               if (!_crs->fromEPSG4979(epsg4979,res)) {
                  break;
               }
               cur->map.setPointMapping(::tikal::Point2D(res[0],res[1]));
               ++cur;
            }
            return cur-req;
         }

         private:
         ::timber::Reference< ProjectedCRS> _crs;
         const ::timber::Reference< ProjectionDomain> _domain;
      };
      if (!crs->datum()->equals(*boundary->center().datum())) {
         throw ::std::invalid_argument("Incompatible datums specified");
      }

      Projection* res = new Impl(crs, boundary);
return   res;
}

::timber::Reference< Projection> Projection::createIdentityProjection(const GeodeticCoordinates& center)
throws()
{
   //FIXME return Projection(::gps::geo::projection::IdentityProjection::create(center));
   return ::timber::Pointer<Projection>();
}

ProjectionMapping& Projection::project(double lat, double lon, double alt, ProjectionMapping& map) const
throws()
{
   ProjectionRequest req;
   req.lat = lat;
   req.lon = lon;
   req.alt = alt;
   project(1,&req);
   map = req.map;
   return map;
}

ProjectionMapping& Projection::project(const GeodeticCoordinates& pt, ProjectionMapping& map) const
throws()
{
   ProjectionRequest req;
   req.lat = pt.latitude();
   req.lon = pt.longitude();
   req.alt = pt.height();
   project(1,&req);
   map = req.map;
   return map;
}

ProjectionMapping Projection::project(double lat, double lon, double alt) const
throws()
{
   ProjectionRequest req;
   req.lat = lat;
   req.lon = lon;
   req.alt = alt;
   project(1,&req);
   return req.map;
}

ProjectionMapping Projection::project(const GeodeticCoordinates& pt) const
throws()
{
   ProjectionRequest req;
   req.lat = pt.latitude();
   req.lon = pt.longitude();
   req.alt = pt.height();
   project(1,&req);
   return req.map;
}
::timber::Reference< ::terra::Datum> Projection::datum() const
throws()
{  return domain()->center().datum();}

size_t Projection::coordinateCount() const
throws()
{
   return 2;
}

double Projection::distance(const double* p1, const double* p2) const
throws()
{
   const double p[] = {p1[0] - p2[0], p1[1]-p2[1]};
   return ::std::sqrt(p[0]*p[0] + p[1]*p[1]);
}

bool
Projection::toEPSG4979(const double* coords, EPSG4979Coordinates& epsg4979) const
throws()
{
   try {
      ::terra::GeodeticCoordinates inv = projectInverse(coords[0],coords[1]);
      const ::terra::Location geo( inv );
      geo.epsg4979Coordinates(epsg4979);
      return true;
   }
   catch (const ::std::exception& e) {
      return false;
   }
}

bool Projection::fromEPSG4979(const EPSG4979Coordinates& epsg4979, double* coords) const
throws()
{
   ProjectionMapping map = project(epsg4979.lat, epsg4979.lon, epsg4979.alt);
   if (map.type() != ProjectionMapping::POINT) {
      return false;
   }
   else {
      coords[0] = map.point().x();
      coords[1] = map.point().y();
      return true;
   }
}

bool Projection::equals(const ::terra::CoordinateReferenceSystem& frame) const
throws()
{
   return this == dynamic_cast<const Projection*>(&frame);
}
::terra::EpsgCode Projection::epsgCode() const
throws() {return EpsgCode();}

}
