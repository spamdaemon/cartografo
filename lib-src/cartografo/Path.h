#ifndef _CARTOGRAFO_PATH_H
#define _CARTOGRAFO_PATH_H

#ifndef _CARTOGRAFO_PROJECTABLE_H
#include <cartografo/Projectable.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace terra {
  class Extent;
}

namespace cartografo {
  class PathSegment;

  /**
   * A Path represents a 1-dimensional structure on an unspecified spheroid. A path consists
   * typically of path segments, which may be unconnected. A path segment is either a loxodrome,
   * i.e. a line of constant bearing, or a geodesic, which is a great circle arc on the spheroid.
   * Instances of paths are typically constructed using a PathBuilder.<p>
   * Paths do not retain information about any specific spheroid.
   */
  class Path : public Projectable {
    Path(const Path&);
    Path&operator=(const Path&);
      
    /** 
     * Create an empty path.
     */
  protected:
    Path () throws();
      
    /** The destructor */
  public:
    ~Path() throws();
      
    /**
     * Map this path into a loxodromic path where the length of each segment
     * is limited to the specified length.
     * @param length the length of each straight line segment.
     * @return a loxodromic path with a bounded segment length
     */
  public:
    virtual ::timber::Reference<Path> toBoundedLoxodromicPath (double length) const throws() = 0;

    /**
     * Create a loxodromic path.
     * @param err error in meters
     * @return a path that is a loxodromic path
     */
  public:
    virtual ::timber::Reference<Path> toLoxodromicPath (double err) const throws() = 0;

    /**
     * Get the number of path segments.
     * @return the number of path segments
     */
  public:
    virtual size_t segmentCount() const throws() = 0;

    /**
     * Get a segment iterator. The path segments will be enumerated
     * such that a segment's end point is the same as the next segment's
     * start point.
     * @return an iterator of path segments.
     * @pre i < segmentCount()
     */
  public:
    virtual const PathSegment& segment(size_t i) const throws() = 0;
      
    /**
     * Get the extent of this path. The extent defines only the area
     * taken up by the boundary points and not the area enclosed by this
     * path. This is a very subtle distinction and affects only paths that
     * enclose one of the poles. For example, a circle centered at the north pole
     * will have extent that encloses a parallel, but not the north pole itself.
     * @return the extent of this path
     */
  public:
    virtual ::terra::Extent extent() const throws() = 0;
  };
      
}


#endif
