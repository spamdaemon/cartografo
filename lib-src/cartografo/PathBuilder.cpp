#include <cartografo/PathBuilder.h>
#include <cartografo/AbstractPath.h>

#include <terra/Bounds.h>
#include <terra/Extent.h>
#include <terra/Horizon.h>
#include <terra/Geodesic.h>
#include <terra/terra.h>

#include <timber/logging.h>
#include <canopy/arraycopy.h>

#include <cmath>
#include <cstdlib>
#include <cassert>
#include <vector>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
  namespace {
    static Log logger() { return Log("cartografo.PathBuilder"); }

    static void createCoordinates (double lat, double lon, LatLon& result)
    {
      if (lat >  ::terra::PI/2) {
	lon +=  ::terra::PI;
	lat =  ::terra::PI - lat;
      }
      else if (lat < - ::terra::PI/2) {
	lon +=  ::terra::PI;
	lat = - ::terra::PI - lat;
      }
      result[0] = lat;
      result[1] = clampLongitude(lon);
    }

    static Pointer<Path> createPath (size_t n, const GeodeticCoordinates* coords, bool close, bool loxodromic)
    {
      if (n==0) {
	return Pointer<Path>();
      }
      PathBuilder b;
      b.moveTo(coords[0]);
      for(size_t i=1;i<n;++i) {
	if (coords[i]!=coords[i-1]) {
	  if (loxodromic) {
	    b.loxodromeTo(coords[i]);
	  }
	  else {
	    b.geodesicTo(coords[i]);
	  }
	}
      }
      if (close) {
	if (loxodromic) {
	  b.loxodromeTo(coords[0]);
	}
	else {
	  b.geodesicTo(coords[0]);
	}
      }
      return b.path();
    }
      
    static Pointer<Path> createPath (const ::std::vector<GeodeticCoordinates>& coords, bool close, bool loxodromic)
    { return createPath(coords.size(),&coords[0],close,loxodromic); }
  }
    
  struct PathBuilder::WayPoint {
    inline WayPoint () {}
      
    inline WayPoint (const GeodeticCoordinates& pt, PathSegment::Type type, Direction dir=SHORTEST)
      : _havePoint(true),_point(pt),
	_deltaLat(0),_deltaLon(0),_deltaHeight(0),
	_type(type),_direction(dir)
    {}

#if 0
    inline WayPoint (double dLat, double dLon, double dHeight, PathSegment::Type type)
      : _havePoint(false),_deltaLat(dLat),_deltaLon(dLon),_deltaHeight(dHeight),_type(type),_direction(SHORTEST)
    {}
#endif

    bool _havePoint;
    GeodeticCoordinates _point;
    double _deltaLat,_deltaLon,_deltaHeight;
    PathSegment::Type _type;
    Direction _direction;
  };

  PathBuilder::PathBuilder() throws()
  : _capacity(100),_waypoints(0)
  {
    _waypoints = new WayPoint[_capacity];
    reset();
  }
    
  PathBuilder::~PathBuilder() throws()
  {
    delete [] _waypoints;
  }
    
  void PathBuilder::reset() throws()
  {
    _needMoveTo = true;
    _size = 0;
    _path = Pointer<Path>();
  }
    
  const PathBuilder::WayPoint& PathBuilder::lastWayPoint() const throws()
  {
    assert(!_needMoveTo);
    return _waypoints[_size-1];
  }
  

  void PathBuilder::addWayPoint (const WayPoint& p) throws()
  {
    if (_size>0) {
      const WayPoint& last = lastWayPoint();
      switch(p._type) {
      case PathSegment::MOVE:
      case PathSegment::GEODESIC:
      case PathSegment::MERIDIAN:
	// creating a geodesic to the same location isn't that well defined
	if (last._point.isSameLocation(p._point)) {
	  return;
	}
      case PathSegment::LOXODROME:
	// for loxodromes, we allow them to move to the same
	// location, but possibly taking a different path; this
	// allows parallels to be described simply from -180,0 to 180,0
	if (last._point == p._point) {
	  return;
	}
	break;
      }
    }
    
    assert (_capacity > 0 && "Capacity not greater than 0");
    if (_size>=_capacity-3) {
      _capacity = 2*_capacity+3;
      WayPoint* cp = new WayPoint[_capacity];
      ::canopy::arraycopy(_waypoints,cp,_size);
      delete[] _waypoints;
      _waypoints = cp;
    }
      
    if (_size>0) {
      const GeodeticCoordinates& q = _waypoints[_size-1]._point;
      if(::std::abs(q.latitude())==terra::PI/2) {
	if (p._point.longitude()!=q.longitude() && p._point.latitude()!=q.latitude()) {
	  const GeodeticCoordinates cs(q.latitude(),p._point.longitude(),q.height(),q.datum());
	  _waypoints[_size++] = WayPoint(cs,p._type);
	}
      }
    }
      
    if(_size>0 && ::std::abs(p._point.latitude())== ::terra::PI/2) {
      const GeodeticCoordinates& q = _waypoints[_size-1]._point;
      if (p._point.longitude()!=q.longitude() && p._point.latitude()!=q.latitude()) {
	const GeodeticCoordinates cs(p._point.latitude(),q.longitude(),p._point.height(),p._point.datum());
	_waypoints[_size++] = WayPoint(cs,p._type);
      }
    }
    _waypoints[_size++] = p;
    _path = Pointer<Path>();
  }
    
  void PathBuilder::moveTo (const GeodeticCoordinates& p) throws()
  {
    _needMoveTo = false;
    addWayPoint(WayPoint(p,PathSegment::MOVE));
  }
    
  void PathBuilder::loxodromeAcrossPole (double pole, const GeodeticCoordinates& to) throws()
  {
    addWayPoint(WayPoint(GeodeticCoordinates(pole,lastWayPoint()._point.longitude(),to.datum()),PathSegment::LOXODROME));
    addWayPoint(WayPoint(GeodeticCoordinates(pole,to.longitude(),to.datum()),PathSegment::LOXODROME));
    addWayPoint(WayPoint(to,PathSegment::LOXODROME));
  }
  

  void PathBuilder::geodesicTo (const GeodeticCoordinates& p) throws (::std::exception)
  {
    if (_needMoveTo) {
      throw ::std::runtime_error("geodesicTo requires a prior call to moveTo");
    }
    const double delta = toRadians(.00025);
    const double eps = .0001;
    
    
    GeodeticCoordinates to(p);
    if (p.latitude() > ::terra::PI/2 - delta) {
      to = GeodeticCoordinates(::terra::PI/2,p.longitude(),p.datum());
    }
    else if (p.latitude() < delta -::terra::PI/2) {
      to = GeodeticCoordinates(-::terra::PI/2,p.longitude(),p.datum());
    }

    const GeodeticCoordinates from(lastWayPoint()._point); // need to copy

    // be able to deal with antipodal points in a graceful way
    if (from.antipodalCoordinates().isSameLocation(to)) {
      loxodromeAcrossPole(to.latitude() > 0 ? ::terra::PI/2 :  -::terra::PI/2, to);
      return;
    }
    

    // get the last way point and compute the geodesic that needs to be used
    const ::terra::Geodesic G(from,to);

    // the sin of 0 indicates going either north or south, in which case only loxodromes are actually
    // needed
    const double dir = ::std::sin(G.bearing());
    if (::std::abs(dir)< eps) {
      // if the longitude are not same going straight north or south, then that means
      // we're crossing on of the poles; in this case we insert moves to the respective pole
      // with the same longtitude as the from and then move on the pole along to's longitude
      
      if (::std::abs(from.longitude()-to.longitude()) > delta) {
	const double pole = ::std::cos(G.bearing()) > 0 ? ::terra::PI/2 : -::terra::PI/2;
	loxodromeAcrossPole(pole,to);
      }
      else {
	addWayPoint(WayPoint(to,PathSegment::LOXODROME));
      }
    }
    else {
      addWayPoint(WayPoint(to,PathSegment::GEODESIC));
    }
  }
    
  void PathBuilder::loxodromeTo (const GeodeticCoordinates& p, Direction dir) throws (::std::exception)
  {
    if (_needMoveTo) {
      throw ::std::runtime_error("loxodromeTo requires a prior call to moveTo");
    }
    const double delta = toRadians(.00025);
    if (p.latitude() > ::terra::PI/2 - delta) {
      addWayPoint(WayPoint(GeodeticCoordinates(::terra::PI/2,p.longitude(),p.datum()),PathSegment::LOXODROME,dir));
    }
    else if (p.latitude() < delta -::terra::PI/2) {
      addWayPoint(WayPoint(GeodeticCoordinates(-::terra::PI/2,p.longitude(),p.datum()),PathSegment::LOXODROME,dir));
    }
    else {
      addWayPoint(WayPoint(p,PathSegment::LOXODROME,dir));
    }
  }
  
    
  Pointer<Path> PathBuilder::path() const throws()
  {
    struct xPath : public AbstractPath {
      xPath (size_t n, const WayPoint* pts) throws()
      : _segments(new PathSegment[n]),_count(n),_isLoxodromic(false)
      {
	assert (pts[0]._havePoint);
	assert (pts[0]._type==PathSegment::MOVE);

	_segments[0] = PathSegment(pts[0]._point);
	double lat = _segments[0].endPoint().latitude();
	double lon = _segments[0].endPoint().longitude();
	_extent = Extent(lat,lon);
	
	
	bool allLoxodromes = true;
	for (size_t i=1;i<n;++i) {
	  const WayPoint& b = pts[i];
	  if (b._havePoint) {
	    if (b._type== PathSegment::MOVE) {
	      _segments[i] = PathSegment(b._point);
	    }
	    else {
	      const GeodeticCoordinates& from = _segments[i-1].endPoint();
	      const GeodeticCoordinates& to = b._point;
	      double dLon = to.longitude() - from.longitude();
	      double dLat = to.latitude() - from.latitude();
	      double dHgt = to.height() - from.height();

	      switch(b._direction) {
	      case EAST:
		if (dLon < 0) {
		  dLon += 2* ::terra::PI;
		}
		break;
	      case WEST:
		if (dLon > 0) {
		  dLon -= 2* ::terra::PI;
		}
		break;
	      case SHORTEST:
		if (dLon > ::terra::PI) {
		  dLon -= 2* ::terra::PI;
		}
		else if (dLon < - ::terra::PI) {
		  dLon += 2* ::terra::PI;
		}
		break;
	      default:
		assert(false && "Invalid case");
		::std::abort();
	      }
	      _segments[i] = PathSegment(from,dLat,dLon,dHgt,b._type);
	    }
	  }
	  else {
	    _segments[i] = PathSegment(_segments[i-1].endPoint(),b._deltaLat,b._deltaLon,b._deltaHeight,b._type);
	  }
	  if (b._type==PathSegment::MOVE) {
	    lat = _segments[i].endPoint().latitude();
	    lon = _segments[i].endPoint().longitude();
	  }
	  else {
	    lat = lat+_segments[i].deltaLatitude();
	    lon = lon+_segments[i].deltaLongitude();
	  }
	  if (b._type!=PathSegment::LOXODROME && b._type!=PathSegment::MOVE) {
	    allLoxodromes = false;
	  }
	  _extent = _extent.merge(Extent(lat,lon));
	}
	_isLoxodromic = allLoxodromes;
      }
	
      ~xPath() throws() { 
	delete[] _segments;
      }
	
      Extent extent() const throws() { 
	return _extent; 
      }

      size_t segmentCount() const throws() 
      { return _count; }
	
      const PathSegment& segment (size_t i) const throws()
      { 
	assert(i<_count);
	return _segments[i];
      }
	
      Reference<Path> toLoxodromicPath (double err) const throws()
      {
	if (_isLoxodromic) {
	  return toRef<Path>();
	}
	else {
	  return AbstractPath::toLoxodromicPath(err);
	}
      }

    private:
      PathSegment* _segments;
      size_t _count;
      bool _isLoxodromic;
      Extent _extent;
    };

    if (_path!=nullptr || _size==0) {
      return _path;
    }
      
    Path* tmp = new xPath(_size,_waypoints); 
    _path  = Pointer<Path>(tmp);
    return _path;
  }

  Reference<Path> PathBuilder::createLoxodromicPath (const GeodeticCoordinates& p, const GeodeticCoordinates& q)
  {
    const GeodeticCoordinates coords[] = { p,q};
    return createPath(2,coords,false,true); 
  }

  Pointer<Path> PathBuilder::createLoxodromicPath (size_t n, const GeodeticCoordinates* coords, bool close)
  { return createPath(n,coords,close,true); }
      
  Pointer<Path> PathBuilder::createLoxodromicPath (const ::std::vector<GeodeticCoordinates>& coords, bool close)
  { return createPath(coords,close,true); }

  Pointer<Path> PathBuilder::createGeodesicPath (size_t n, const GeodeticCoordinates* coords, bool close)
  { return createPath(n,coords,close,false); }
      
  Pointer<Path> PathBuilder::createGeodesicPath (const ::std::vector<GeodeticCoordinates>& coords, bool close)
  { return createPath(coords,close,false); }
    
  Reference<Path> PathBuilder::createGeodesicPath (const GeodeticCoordinates& p, const GeodeticCoordinates& q)
  {
    const GeodeticCoordinates coords[] = { p,q};
    return createPath(2,coords,false,false); 
  }

  Pointer<Path> PathBuilder::createBoundsPath (const Bounds& b)
  { return createBoundsPath(b,::terra::PI/2,::terra::PI/2); }

  Pointer<Path> PathBuilder::createBoundsPath (const Bounds& b, double deltaLat, double deltaLon)
  {
    struct xPath : public AbstractPath {
      xPath (const Bounds& bnds, double dLat, double dLon) throws()
      : _extent(bnds)
      {
	const GeodeticCoordinates tl = bnds.northWest();
	double width = _extent.east()-_extent.west();
	while (width < 0) {
	  width += 2*::terra::PI;
	}
	double height = _extent.north()-_extent.south();
	_segments.push_back(PathSegment(tl));
	  
	double w = 0;
	while (width-w > dLon) {
	  _segments.push_back(PathSegment(_segments[_segments.size()-1].endPoint(),0,dLon,PathSegment::LOXODROME));
	  w += dLon;
	}
	_segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),0,width-w,PathSegment::LOXODROME));
	double h = 0;
	while (height-h > dLat) {
	  _segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),-dLat,0,PathSegment::LOXODROME));
	  h += dLat;
	}
	_segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),-(height-h),0,PathSegment::LOXODROME));
	w = 0;
	while (width-w > dLon) {
	  _segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),0,-dLon,PathSegment::LOXODROME));
	  w += dLon;
	}
	_segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),0,-(width-w),PathSegment::LOXODROME));
	h = 0;
	while (height-h > dLat) {
	  _segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),dLat,0,PathSegment::LOXODROME));
	  h += dLat;
	}
	_segments.push_back( PathSegment(_segments[_segments.size()-1].endPoint(),tl,PathSegment::LOXODROME));

	double minx = tl.longitude();
	double maxx = minx;
	double miny = tl.latitude();
	double maxy = miny;
	double lat  = miny;
	double lon  = minx;
	for (size_t seg=0;seg<_segments.size();++seg) {
	  lat += _segments[seg].deltaLatitude();
	  lon += _segments[seg].deltaLongitude();

	  minx = ::std::min(lon,minx);
	  miny = ::std::min(lat,miny);
	  maxx = ::std::max(lon,maxx);
	  maxy = ::std::max(lat,maxy);
	}
	miny = clampLatitude(miny);
	maxy = clampLatitude(maxy);

	_extent = Extent(miny,minx,maxy,maxx);
      }
	
      ~xPath() throws() {}
      
      Extent extent() const throws() { return _extent; }

      size_t segmentCount() const throws() 
      { return _segments.size(); }
	
      const PathSegment& segment (size_t i) const throws()
      { 
	assert(i<_segments.size());
	return _segments[i];
      }
      
    private:
      ::std::vector<PathSegment> _segments;
      Extent _extent;
    };

    Path* tmp = new xPath(b,deltaLat,deltaLon);
    return Pointer<Path>(tmp);
  }

  Reference<Path> PathBuilder::createLoxodromicCircle(const GeodeticCoordinates& ctr, double radius, size_t nPoints)
  {
      if (radius<0.0) {
	throw invalid_argument("Invalid radius");
      }
      if (nPoints<3) {
	throw invalid_argument("Not enough points for loxodromic circle");
      }
	
      PathBuilder B;
	
      Geodesic arc(radius,0);
      const GeodeticCoordinates top(arc.move(ctr));
      const double deltaAz = toRadians(360.0/nPoints);
      
      // when we're at one of the poles, then we need to create 
      // a parallel in a clockwise orientation
      if (ctr.latitude()== ::terra::PI/2) {
	// create a parallel at the specified latitude
	double lat  = top.latitude();
	double dLon = deltaAz;
	double startLon =  ::terra::PI;
	double endLon = - ::terra::PI;
	B.moveTo(GeodeticCoordinates(lat,startLon,ctr.datum()));
	for (double lon = startLon-dLon;lon > endLon; lon -= dLon) {
	  B.loxodromeTo(GeodeticCoordinates(lat,lon,ctr.datum()));
	}
	B.loxodromeTo(GeodeticCoordinates(lat,endLon,ctr.datum()));
      }
      else if (ctr.latitude()==- ::terra::PI/2) {
	// create a parallel at the specified latitude
	double lat  = top.latitude();
	double dLon = deltaAz;
	double startLon = - ::terra::PI;
	double endLon =  ::terra::PI;
	B.moveTo(GeodeticCoordinates(lat,startLon,ctr.datum()));
	for (double lon = startLon+dLon;lon<endLon; lon += dLon) {
	  B.loxodromeTo(GeodeticCoordinates(lat,lon,ctr.datum()));
	}
	B.loxodromeTo(GeodeticCoordinates(lat,endLon,ctr.datum()));
      }
      else {
	// enumerate the vertices of the circle in clockwise order, in increaseing
	// azimuth around the center
	B.moveTo(top);
	for (double az=deltaAz;az<2.0* ::terra::PI;az+=deltaAz) {
	  arc = Geodesic(radius,az);
	  GeodeticCoordinates x = arc.move(ctr);
	  B.loxodromeTo(x);
	}
	B.loxodromeTo(top);
      }
      return B.path();
  }

  Reference<Path> PathBuilder::createLoxodromicCircle(const GeodeticCoordinates& ctr, double radius)
  { return createLoxodromicCircle(ctr,radius,32); }

  Reference<Path> PathBuilder::createRadiansCircle(const GeodeticCoordinates& ctr, double radRadius, size_t nVertices)
  {
    if (radRadius<0.0) {
      throw invalid_argument("Invalid radius");
    }
      
    LatLon lat,lon;

    // if the circle center is a pole, then we need to generate a parallel
    if (ctr.latitude()== ::terra::PI/2) {
      Geodesic G(ctr,GeodeticCoordinates(ctr.latitude()-radRadius,ctr.longitude(),ctr.datum()));
      return createLoxodromicCircle(ctr,G.range(),nVertices);
    }
    else if (ctr.latitude()==- ::terra::PI/2) {
      Geodesic G(ctr,GeodeticCoordinates(ctr.latitude()+radRadius,ctr.longitude(),ctr.datum()));
      return createLoxodromicCircle(ctr,G.range(),nVertices);
    }
    else {
      createCoordinates(ctr.latitude()+radRadius,ctr.longitude(),lat);
      createCoordinates(ctr.latitude(),ctr.longitude()+radRadius,lon);
	
      Geodesic geoLat(ctr,GeodeticCoordinates(lat[0],lat[1],ctr.height(),ctr.datum()));
      Geodesic geoLon(ctr,GeodeticCoordinates(lon[0],lon[1],ctr.height(),ctr.datum()));
      //FIXME: the geoLon calculation is totally bogus; it's really computing the size of a loxodrome
      //       but that's not what we want; we need to find the loxodrome that's orthogonal to the 
      //       north-south geodesic circle at the center point; so, for now, just use geoLon=getLat
      geoLon = geoLat;

      return createLoxodromicCircle(ctr,::std::max(geoLat.range(),geoLon.range()),nVertices);
    }
  }


  Reference<Path> PathBuilder::createRadiansCircle(const GeodeticCoordinates& ctr, double radRadius)
  {
    return createRadiansCircle(ctr,radRadius,32);
  }

  Pointer<Path> PathBuilder::createHorizonPath (const GeodeticCoordinates& obs) throws()
  {
    const Horizon H(obs); // the horizon
    const size_t nPoints = 72;
    
    PathBuilder B;
    
    const double deltaAz = toRadians(360.0/nPoints);
    
    
    GeodeticCoordinates start(H.point(0));
    B.moveTo(start);

    
    if (obs.latitude()== ::terra::PI/2) {
      for (double az=2.0* ::terra::PI-deltaAz;az>0;az-=deltaAz) {
	B.loxodromeTo(H.point(az));
      }
    }
    else {
      for (double az=deltaAz;az<(2.0* ::terra::PI - deltaAz/2);az+=deltaAz) {
	B.loxodromeTo(H.point(az));
      }
    }
    B.loxodromeTo(start);
    return B.path();
  }


  Reference<Path> PathBuilder::createGeodesicCircle(const GeodeticCoordinates& a, const GeodeticCoordinates& b)
  {
    if (a.equals(b) || a.equals(b.antipodalCoordinates())) {
      throw ::std::invalid_argument("Points are either equal or antipodal; cannot create geodesic circle");
    }

    const GeodeticCoordinates anti(a.antipodalCoordinates());

    PathBuilder B;

    const double r = a.datum()->spheroid().meridianArcLength(::terra::PI/2);
    const double az = ::terra::Geodesic(a,b).bearing();

    // check the azimuth, if it's going (almost) straight north or south, then we build a special circle
    if (::std::abs(::std::sin(az)) < .00001  || ::std::abs(::std::abs(a.latitude())- ::terra::PI/2) < .00001) {
      // this is a special geodesic, which goes through the poles and we can 
      // use loxodromes (actually meridian arcs)
      B.moveTo(GeodeticCoordinates(::terra::PI/2,b.longitude(),a.datum()));
      B.loxodromeTo(GeodeticCoordinates(-::terra::PI/2,b.longitude(),a.datum()));
      B.loxodromeTo(GeodeticCoordinates(::terra::PI/2,b.longitude(),a.datum()));
    }
    else {
      B.moveTo(a);
      B.geodesicTo(Geodesic(r,az).move(a));
      B.geodesicTo(anti);
      B.geodesicTo(Geodesic(r,-az).move(a));
      B.geodesicTo(a);
    }
    
    return B.path();
  }

}
