#ifndef _CARTOGRAFO_GEO_PATHS_H
#define _CARTOGRAFO_GEO_PATHS_H

#ifndef _CARTOGRAFO_GEO_SHAPES_H
#include <cartografo/geo/Shapes.h>
#endif

#ifndef _CARTOGRAFO_PATH_H
#include <cartografo/Path.h>
#endif

#include <vector>

namespace cartografo {
  namespace geo {

    /**
     * This graphic is a path container. All paths
     * in this container a rendered with the same attributes.
     */
    class Paths : public Shapes< ::timber::Pointer<Path> > {
      Paths&operator=(const Paths&);
    
      /** Default constructor */
    public:
      inline Paths () throws()
      {}
	
      /**
       * Create a paths with a single path. 
       * @param p  paths
       */
    public:
      Paths (const ::std::vector< ::timber::Reference<Path> >& p) throws();
	
      /**
       * Create a paths with a single path. 
       * @param p a path
       */
    public:
      Paths (const ::timber::Reference<Path>& p) throws();
      
      /** 
       * Copy constructor that copies the handle,
       * but keeps the same implementation.
       * @param g paths
       */
    private:
      Paths (const Paths& g) throws();
      
      /**  Destroy this graphic handle */
    public:
      ~Paths () throws();

      
    protected:
      void acceptNodeVisitor (NodeVisitor& v) const throws();
    };
  }
}
#endif
