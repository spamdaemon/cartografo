#ifndef _CARTOGRAFO_GEO_REGIONS_H
#define _CARTOGRAFO_GEO_REGIONS_H

#ifndef _CARTOGRAFO_GEO_SHAPES_H
#include <cartografo/geo/Shapes.h>
#endif

#ifndef _CARTOGRAFO_REGION_H
#include <cartografo/Region.h>
#endif

#include <vector>

namespace cartografo {
  namespace geo {
  
    /**
     * This graphic is a region container. All regions
     * in this container a rendered with the same attributes.
     */
    class Regions : public Shapes< ::timber::Pointer<Region> > {
      Regions&operator=(const Regions&);

      /** Default constructor */
    public:
      inline Regions () throws()
      {}
	
      /**
       * Create a regions with a single region. 
       */
    public:
      Regions (const ::timber::Reference<Region>& p) throws();
      
      /**
       * Create a regions.
       * @param p a list of regions
       */
    public:
      Regions (const ::std::vector< ::timber::Reference<Region> >& p) throws();
      
      /** 
       * Copy constructor that copies the handle,
       * but keeps the same implementation.
       */
    private:
      Regions (const Regions& g) throws();
      
      /**  Destroy this graphic handle */
    public:
      ~Regions () throws();

      
    protected:
      void acceptNodeVisitor (NodeVisitor& v) const throws();
	
    };
  }
}
#endif
