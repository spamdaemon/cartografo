#include <cartografo/geo/ThreadSafeNode.h>
#include <timber/ThreadsafePointer.h>

namespace cartografo {
   namespace geo {
      ThreadSafeNode::ThreadSafeNode()
      throws()
      {
      }

      ThreadSafeNode::~ThreadSafeNode()
      throws()
      {
      }
      ::timber::Reference< Node> ThreadSafeNode::create()
      throws()
      {
         return create(::timber::Pointer< Node>());
      }

      ::timber::Reference< Node> ThreadSafeNode::create(const ::timber::Pointer< Node>& child)
      throws ()
      {
         struct Impl : public ThreadSafeNode
         {
               Impl(const ::timber::Pointer< Node>& c) throws()
                     : _child(c)
               {
               }
               ~Impl() throws()
               {
               }

               void acceptNodeVisitor(NodeVisitor& v) const throws()
               {
                  ::timber::Pointer< Node> c = child();
                  if (c) {
                     c->accept(v);
                  }
               }

               void setChild(const ::timber::Pointer< Node>& c)throws()
               {
                  ::timber::Pointer< Node> old = _child.getAndSet(c);
                  if (old != c) {
                     notifyChange();
                  }
               }

               ::timber::Pointer< Node> child() const throws()
               {
                  return _child.get();
               }

            private:
               ::timber::ThreadsafePointer< Node> _child;
         };
         Impl* res = new Impl(child);
         return res;
      }
   }

}
