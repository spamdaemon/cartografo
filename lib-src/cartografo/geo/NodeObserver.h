#ifndef _CARTOGRAFO_GEO_NODEOBSERVER_H
#define _CARTOGRAFO_GEO_NODEOBSERVER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  namespace geo {
    /** A graphic event */
    class NodeEvent;
  
    /**
     * This class provides the interface by which 
     * changes to a node can be propagated.
     */
    class NodeObserver {
    
      /** Destroy this observer */
    public:
      virtual ~NodeObserver() throws() = 0;
    
      /**
       * A node has changed.
       * @param e a node event
       */
    public:
      virtual void nodeChanged (const NodeEvent& e) throws() = 0;	
    };
  }
}
#endif
