#include <cartografo/geo/Switch.h>
#include <cartografo/geo/NodeVisitor.h>

namespace cartografo {
  namespace geo {
  
    Switch::~Switch () throws()
    {}
  
    void Switch::acceptNodeVisitor (NodeVisitor& v) const throws()
    { v.visit(*this); }
  
    void Switch::setVisibleChild (size_t child) throws()
    {
      if (child!=_visibleChild) {
	_visibleChild = child;
	notifyChange();
      }
    }
  }
}
