#include <cartografo/geo/CompositeNode.h>
#include <cartografo/geo/NodeVisitor.h>

using namespace ::timber;

namespace cartografo {
   namespace geo {
      namespace {

         static size_t countNonNull(const Pointer< Node>& l = Pointer< Node> (),
               const Pointer< Node>& m = Pointer< Node> (), const Pointer< Node>& r = Pointer< Node> ())
throws      ()
      {
         size_t n=0;
         if (l) {
            ++n;
         }
         if (m) {
            ++n;
         }
         if (r) {
            ++n;
         }
         return n;
      }

   }
   CompositeNode::CompositeNode()throws()
   : _nonNullChildren(0), _separator(false)
   {}

   CompositeNode::CompositeNode(size_t n)
   throws() :_nonNullChildren(0), _separator(false)
   {  _children.resize(n);}

   CompositeNode::CompositeNode(size_t n, bool sep)
   throws() : _nonNullChildren(0), _separator(sep)
   {  _children.resize(n);}

   CompositeNode::CompositeNode(bool separator)
   throws()
   : _nonNullChildren(0), _separator(separator) {}

   CompositeNode::CompositeNode(const Pointer< Node>& l, const Pointer< Node>& m, const Pointer< Node>& r)
   throws() : _nonNullChildren(countNonNull(l,m,r)), _separator(false)
   {
      _children.resize(3);
      _children.push_back(l);
      _children.push_back(m);
      _children.push_back(r);
   }

   CompositeNode::CompositeNode(const Pointer< Node>& l, const Pointer< Node>& r)
   throws() :_nonNullChildren(countNonNull(l,r)), _separator(false)
   {
      _children.resize(2);
      _children.push_back(l);
      _children.push_back(r);
   }

   CompositeNode::CompositeNode(const Pointer< Node>& l, const Pointer< Node>& m, const Pointer< Node>& r, bool sep)
   throws() : _nonNullChildren(countNonNull(l,m,r)), _separator(sep)
   {
      _children.resize(3);
      _children.push_back(l);
      _children.push_back(m);
      _children.push_back(r);
   }

   CompositeNode::CompositeNode(const Pointer< Node>& l, const Pointer< Node>& r, bool sep)
   throws() : _nonNullChildren(countNonNull(l,r)), _separator(sep)
   {
      _children.resize(2);
      _children.push_back(l);
      _children.push_back(r);
   }

   CompositeNode::CompositeNode(const CompositeNode& g)
   throws()
   : Node(),_children(g._children) , _nonNullChildren(g._nonNullChildren),_separator(g._separator)
   {}

   CompositeNode::~CompositeNode()
   throws()
   {}

   CompositeNode::CompositeEvent::~CompositeEvent()
   throws()
   {}

   bool CompositeNode::removeChild(const Pointer< Node>& g)
   throws()
   {
      if (!g) {
         // no such node
         return false;
      }

      for (size_t i=0;i<nodeCount();++i) {
         if (_children[i]==g) {
            removeChildAt(i);
            return true;
         }
      }
      return false;
   }

   size_t CompositeNode::indexOf(const Pointer< Node>& g) const
   throws()
   {
      for (size_t i=0;i<nodeCount();++i) {
         if (_children[i]==g) {
            return i;
         }
      }
      return invalidIndex();
   }

   void CompositeNode::setChildAt(const Pointer< Node>& g, size_t pos)
   throws()
   {
      assert(pos <= nodeCount());
      if (pos==nodeCount()) {
         _children.push_back(g);
         if (observerCount()>0) {
            fireEvent(CompositeEvent(CompositeEvent::ADDED, this,g,pos));
         }
         if (g) {
            ++_nonNullChildren;
            attachNode(g);
         }
      }
      else if (_children[pos]!=g) {
         Pointer<Node> c = _children[pos];
         if(c) {
            --_nonNullChildren;
            detachNode(c);
         }
         _children[pos] = g;
         if (g) {
            ++_nonNullChildren;
            attachNode(g);
         }
         if (observerCount()>0) {
            fireEvent(CompositeEvent(CompositeEvent::REMOVED,this,c,pos));
            fireEvent(CompositeEvent(CompositeEvent::ADDED, this,g,pos));
         }
      }
   }

   void CompositeNode::insertChildAt(const Pointer< Node>& g, size_t pos)
   throws()
   {
      assert(pos <= nodeCount());
      if (pos==nodeCount()) {
         _children.push_back(g);
      }
      else {
         _children.insert(_children.begin()+pos,g);
      }
      if (g) {
         ++_nonNullChildren;
         attachNode(g);
      }
      if (observerCount()>0) {
         fireEvent(CompositeEvent(CompositeEvent::ADDED, this,g,pos));
      }
   }

   Pointer< Node> CompositeNode::removeChildAt(size_t pos)
   throws()
   {
      Pointer<Node> g (_children[pos]);
      if(g) {
         --_nonNullChildren;
         detachNode(g);
      }
      _children.erase(_children.begin()+pos);
      if (observerCount()>0) {
         fireEvent(CompositeEvent(CompositeEvent::REMOVED,this,g,pos));
      }
      return g;
   }

   void CompositeNode::removeChildren()
   throws()
   {
      if (nodeCount()>0) {
         for (size_t i=0;i<nodeCount();++i) {
            if(_children[i]) {
               --_nonNullChildren;
               detachNode(_children[i]);
            }
         }
         _children.clear();
         assert (_nonNullChildren==0);
         if (observerCount()>0) {
            fireEvent(CompositeEvent(CompositeEvent::CLEARED,this));
         }
      }
   }

   void CompositeNode::packChildren()
   throws()
   {
      size_t sz = nodeCount();

      // if all children are non-null then there's nothing to pack
      if (sz==_nonNullChildren) {
         return;
      }
      for (size_t i=0;i<sz;++i) {
         for (size_t j=i+1;_children[i] && j<sz;++j) {
            Pointer<Node>::swap(_children[i],_children[j]);
         }
      }

      // remove all 0 at the end
      bool changed = false;
      while (_children[--sz]==nullptr) {
         changed = true;
         _children.pop_back();
      }

      if (changed && observerCount()>0) {
         fireEvent(CompositeEvent(CompositeEvent::PACKED,this));
      }
   }

   void CompositeNode::acceptNodeVisitor(NodeVisitor& v) const
   throws()
   {  v.visit(*this);}

   void CompositeNode::attachToNodeGraph(NodeGraph& g)
   throws()
   {
      Node::attachToNodeGraph(g);
      for (size_t i=0;i<_children.size();++i) {
         if(_children[i]) {
            attachNode(_children[i],g);
         }
      }
   }

   void CompositeNode::detachFromNodeGraph(NodeGraph& g)
   throws ()
   {
      for (size_t i=0;i<_children.size();++i) {
         if (_children[i]) {
            detachNode(_children[i],g);
         }
      }
      Node::detachFromNodeGraph(g);
   }

}
}
