#ifndef _CARTOGRAFO_GEO_INDIGONODE_H
#define _CARTOGRAFO_GEO_INDIGONODE_H

#ifndef _CARTOGRAFO_GEO_H
#include <cartografo/geo/geo.h>
#endif

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif

namespace indigo {
  class Node;
}

namespace cartografo {
  namespace geo {
    /**
     * This class allows indigo nodes to be inserted into the
     * the NodeGraph structure to provide additional support 
     * for pixel graphics for overlays, and other things.
     */
    class IndigoNode : public Node {
      IndigoNode&operator=(const IndigoNode&);

      /** A node observer for the indigo node */
    private:
      class Observer;

      /**
       * The observer is a friend 
       * @bug Is this necessary?
       */
    private:
      friend class Observer;

      /** Default constructor */
    public:
      IndigoNode () throws();

      /** 
       * Create a composite node with the specified children
       * @param n an indigo node
       */
    public:
      IndigoNode (const  ::timber::Pointer< ::indigo::Node>& n) throws();

      /** 
       * Copy constructor that copies the handle,
       * but keeps the same implementation.
       */
    public:
      IndigoNode (const IndigoNode& g) throws();
	
      /**  Destroy this node handle */
    public:
      ~IndigoNode () throws();

      /**
       * Get the current indigo node.
       * @return a node or null if not set
       */
    public:
      ::timber::Pointer< ::indigo::Node> node() const throws();

      /**
       * Get the current indigo node.
       * @param n an indigo node
       */
    public:
      void setNode(const ::timber::Pointer< ::indigo::Node>& n) throws();


      /**
       * Accept the specified visitor. Calls
       * the Node::acceptNodeVisitor() method, followed
       * by dispatchNodeVisitor();
       * @param v a visitor.
       */
    protected:
      void acceptNodeVisitor (NodeVisitor& v) const throws();

      /** The children */
    private:
      ::std::unique_ptr<Observer> _observer;
    };
  }
}
#endif
