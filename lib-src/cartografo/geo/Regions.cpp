#include <cartografo/geo/Regions.h>
#include <cartografo/geo/NodeVisitor.h>

namespace cartografo {
  namespace geo {
    
    Regions::Regions (const ::timber::Reference<Region>& p) throws()
    : Shapes< ::timber::Pointer<Region> >(p) {}
    
    Regions::Regions (const ::std::vector< ::timber::Reference<Region> >& p) throws()
    {
      for (size_t i=0,sz=p.size();i<sz;++i) {
	add(p[i]);
      }
    }

    Regions::Regions (const Regions& g) throws()
    : Shapes< ::timber::Pointer<Region> >(g) {}
    
    Regions::~Regions () throws()
    {}
    
    void Regions::acceptNodeVisitor (NodeVisitor& v) const throws()
    { v.visit(*this); }
  }
}

