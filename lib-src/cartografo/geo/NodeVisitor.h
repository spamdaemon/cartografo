#ifndef _CARTOGRAFO_GEO_NODEVISITOR_H
#define _CARTOGRAFO_GEO_NODEVISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
   namespace geo {

      /**
       * @name Forward declarations of core gfx classes.
       * @{
       */
      class CompositeNode;
      class IndigoNode;
      class Locations;
      class Marker;
      class Paths;
      class Raster;
      class Regions;
      class Switch;
      /*@}*/

      /**
       * The abstract visitor class.
       */
      class NodeVisitor
      {
            /** Destroy this visitor */
         public:
            virtual ~NodeVisitor()throws() = 0;

            /**
             * @name Visitation functions for all core gfx classes.
             * @{
             */

            /**
             * Visit a CompositeNode
             * @param g a CompositeNode
             */
         public:
            virtual void visit(const CompositeNode& g)throws() = 0;

            /**
             * Visit an indigo node.
             * @param g an indigo node
             */
         public:
            virtual void visit(const IndigoNode& g)throws() = 0;

            /**
             * Visit a Locations shape
             * @param g a locations shape
             */
         public:
            virtual void visit(const Locations& g)throws() = 0;

            /**
             * Visit a marker
             * @param g a marker
             */
         public:
            virtual void visit(const Marker& g)throws() = 0;

            /**
             * Visit a Paths shape
             * @param g a Paths shape
             */
         public:
            virtual void visit(const Paths& g)throws() = 0;

            /**
             * Visit a raster node
             * @param g a raster node
             */
         public:
            virtual void visit(const Raster& g)throws() = 0;

            /**
             * Visit a Regions shape
             * @param g a Regions shape
             */
         public:
            virtual void visit(const Regions& g)throws() = 0;

            /**
             * Visit a Switch node
             * @param g a Switch
             */
         public:
            virtual void visit(const Switch& g)throws() = 0;

            /*@}*/

      };
   }
}
#endif
