#include <cartografo/geo/IndigoNode.h>
#include <cartografo/geo/NodeVisitor.h>

#include <indigo/Node.h>
#include <indigo/SceneGraph.h>
#include <indigo/GfxObserver.h>

using namespace ::timber;

namespace cartografo {
  namespace geo {

    class IndigoNode::Observer : public ::indigo::SceneGraph, public ::indigo::GfxObserver {
    public:
      Observer(IndigoNode& owner) throws()
      : _owner(owner)
      {}

    public:
      ~Observer() throws()
      {}
      
    public:
      /** The SceneGraph interface */
      void setRootNode (::timber::Pointer< ::indigo::Node> h) throws()
      {
	if (rootNode()!=h) {
	  ::indigo::SceneGraph::setRootNode(h);
	  _owner.notifyChange();
	}
      }

    protected:
      ::indigo::GfxObserver* createObserver( ::indigo::Node& n) throws()
      { return this; }

      /** The GfxObserver interface */
    protected:
      void gfxChanged (const ::indigo::GfxEvent&) throws()
      { _owner.notifyChange(); }

      /** The owner */
    private:
      IndigoNode& _owner;
    };

    IndigoNode::IndigoNode () throws()
    : _observer(new Observer(*this))
    {}
    
    IndigoNode::IndigoNode (const IndigoNode& g) throws()
    : Node(),_observer(new Observer(*this))
    { _observer->setRootNode(g.node()); }

    IndigoNode::IndigoNode (const Pointer< ::indigo::Node>& n) throws()
    : _observer(new Observer(*this))
    { _observer->setRootNode(n); }
    
    
    IndigoNode::~IndigoNode () throws()
    { _observer->setRootNode( Pointer< ::indigo::Node>()); }
    
    void IndigoNode::setNode (const Pointer< ::indigo::Node>& n) throws()
    { _observer->setRootNode(n); }

    Pointer< ::indigo::Node> IndigoNode::node() const throws() 
    { return _observer->rootNode(); }
   
    void IndigoNode::acceptNodeVisitor (NodeVisitor& v) const throws()
    { v.visit(*this); }

  }
}
