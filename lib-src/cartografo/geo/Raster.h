#ifndef _CARTOGRAFO_GEO_RASTER_H
#define _CARTOGRAFO_GEO_RASTER_H

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif

#ifndef _CARTOGRAFO_RASTERIMAGE_H
#include <cartografo/RasterImage.h>
#endif


#include <cstdint>

namespace cartografo {
   namespace geo {
      /**
       * A raster is an image rendered at specific location. The image itself will
       * have its own associated projected coordinate system.
       */
      class Raster : public Node
      {
            Raster&operator=(const Raster&);

            /**
             * Default constructor.
             */
         public:
            Raster()throws();

            /**
             * Create a raster without an image at the specified location.
             * @param raster the raster
             */
         public:
            Raster(const RasterImage& raster)throws();

             /**
             * Copy constructor that copies the handle,
             * but keeps the same implementation.
             */
         public:
            Raster(const Raster& g)throws();

            /**  Destroy this node handle */
         public:
            ~Raster()throws();

            /**
             * Get the raster image.
             * @return the raster image
             */
         public:
            const RasterImage& raster() const throws() { return _raster; }

            /**
             * Set a new raster.
             * @param raster a new raster
             */
         public:
            void setRaster (const RasterImage& raster) throws();

            /**
             * Accept the specified visitor. Calls
             * the Node::acceptNodeVisitor() method, followed
             * by dispatchNodeVisitor();
             * @param v a visitor.
             */
         protected:
            void acceptNodeVisitor(NodeVisitor& v) const throws();

         private:
            RasterImage _raster;
      };
   }
}
#endif
