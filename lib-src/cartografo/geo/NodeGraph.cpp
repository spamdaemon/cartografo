#include <cartografo/geo/NodeGraph.h>

namespace cartografo {
  namespace geo {

    NodeGraph::NodeGraph () throws()
    {}
  
    NodeGraph::~NodeGraph() throws()
    {
      assert(!_root && "NodeGraph still has a handle to the root");
    }
  
    void NodeGraph::setRootNode (::timber::Pointer<Node> h) throws()
    {
      if (_root!=h) {
	if (_root) {
	  Node::detachNode(_root,*this);
	}
	_root = h; 
	if (_root) {
	  Node::attachNode(_root,*this);
	}
      }
    }
  
    NodeObserver* NodeGraph::createObserver (Node&) throws()
    { return 0; }
  
    void NodeGraph::destroyObserver (NodeObserver*) throws()
    {  }
  
  }
}
