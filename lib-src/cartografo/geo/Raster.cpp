#include <cartografo/geo/Raster.h>
#include <cartografo/geo/NodeVisitor.h>

using namespace ::timber;

namespace cartografo {
   namespace geo {

      Raster::Raster()
      throws()
      {}

      Raster::Raster(const Raster& g)
      throws()
      : Node(),_raster(g._raster)
      {}

      Raster::Raster(const RasterImage& r)
      throws()
      : _raster(r)
      {
      }

      Raster::~Raster()
      throws()
      {}

      void Raster::setRaster(const RasterImage& r)
      throws()
      {
         _raster = r;
         notifyChange();
      }

      void Raster::acceptNodeVisitor(NodeVisitor& v) const
throws   ()
   {  v.visit(*this);}

}}
