#include <cartografo/geo/Locations.h>
#include <cartografo/geo/NodeVisitor.h>

namespace cartografo {
   namespace geo {

      using namespace ::terra;

      Locations::Locations(const GeodeticCoordinates& p)
      throws()
      : Shapes<GeodeticCoordinates >(p),_pointSize(1)
      {
      }

      Locations::Locations(size_t n, const GeodeticCoordinates* pts)
      throws()
      : Shapes< GeodeticCoordinates >(n,pts),_pointSize(1)
      {
      }

      Locations::Locations(const ::std::vector< GeodeticCoordinates>& p)
      throws()
      : Shapes<GeodeticCoordinates>(p),_pointSize(1)
      {
      }

      Locations::Locations(const Locations& g)
      throws()
      : Shapes< GeodeticCoordinates >(g),_pointSize(g._pointSize)
      {
      }

      Locations::~Locations()
      throws()
      {}

      void Locations::setPointSize(size_t sz)
      throws()
      {
         _pointSize = sz;
      }

      void Locations::acceptNodeVisitor(NodeVisitor& v) const
throws   ()
   {  v.visit(*this);}
}
}
