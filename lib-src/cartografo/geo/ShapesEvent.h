#ifndef _CARTOGRAFO_GEO_SHAPESEVENT_H
#define _CARTOGRAFO_GEO_SHAPESEVENT_H

#ifndef _CARTOGRAFO_GEO_NODEEVENT_H
#include <cartografo/geo/NodeEvent.h>
#endif

namespace cartografo {
  namespace geo {
    
    /**
     * This class is created by Scene graph objects.
     * and is passed to NodeObserver.
     */
    class ShapesEvent : public NodeEvent {
      /** The event type */
    public:
      enum Type { ADDED, MODIFIED, REMOVED };
      
      /** 
       * Create a new ShapesEvent 
       * @param g the source graphic object
       * @param t the type of shape event
       * @param i the index of a shape involved
       */
    public:
      ShapesEvent (const ::timber::Pointer<Node>& g,Type t, size_t i) throws();
      
      /** Destroy this event */
    public:
      ~ShapesEvent () throws();
      
      /** 
       * Get the type of shape event.
       * @return the type of change that occurred to the shape node
       */
    public:
      inline Type type() const throws() { return _type; }
      
      /**
       * Get the index of the shape that was added, modified, or removed. If
       * the index is an invalid index, then the change involved all children.
       * @return the index of the involved shape
       */
    public:
      inline size_t index() const throws() { return _index; }
      
      /** The event type */
    private:
      Type _type;
      
      /** The index of the involved item */
    private:
      size_t _index;
    };
  }
}
#endif
