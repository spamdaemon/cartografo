#ifndef _CARTOGRAFO_GEO_MARKER_H
#define _CARTOGRAFO_GEO_MARKER_H

#ifndef _CARTOGRAFO_GEO_H
#include <cartografo/geo/geo.h>
#endif

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

namespace indigo {
  class Node;
}

namespace cartografo {
  namespace geo {
    /**
     * This class allows indigo nodes to be inserted into the
     * the NodeGraph structure below a transform which moves
     * the local coordinate system to the specified location.
     * If the location cannot be projected then the indigo node
     * will not be rendered.
     */
    class Marker : public Node {
      Marker&operator=(const Marker&);
      
      /** A location */
    public:
      typedef ::terra::GeodeticCoordinates GeodeticCoordinates;

      /** A node observer for the indigo node */
    private:
      class Observer;

      /**
       * The observer is a friend 
       * @bug Is this necessary?
       */
    private:
      friend class Observer;

      /** 
       * Create a node at the specified location
       * @param loc the location
       */
    public:
      Marker (const GeodeticCoordinates& loc) throws();

      /** 
       * Create a composite node with the specified children
       * @param loc the location
       * @param n an indigo node
       */
    public:
      Marker (const GeodeticCoordinates& loc, const  ::timber::Pointer< ::indigo::Node>& n) throws();

      /** 
       * Copy constructor that copies the handle,
       * but keeps the same implementation.
       */
    public:
      Marker (const Marker& g) throws();
	
      /**  Destroy this node handle */
    public:
      ~Marker () throws();

      /**
       * Set the location. 
       * @param loc the new location
       */
    public:
      void setLocation(const GeodeticCoordinates& loc) throws();

      /**
       * Set the location 
       * @param loc the new location
       */
    public:
      const GeodeticCoordinates& location() const throws();

      /**
       * Get the current indigo node.
       * @return a node or null if not set
       */
    public:
      ::timber::Pointer< ::indigo::Node> node() const throws();

      /**
       * Get the current indigo node.
       * @param n an indigo node
       */
    public:
      void setNode(const ::timber::Pointer< ::indigo::Node>& n) throws();

      /**
       * Accept the specified visitor. Calls
       * the Node::acceptNodeVisitor() method, followed
       * by dispatchNodeVisitor();
       * @param v a visitor.
       */
    protected:
      void acceptNodeVisitor (NodeVisitor& v) const throws();

      /** The children */
    private:
      ::std::unique_ptr<Observer> _observer;
    };
  }
}
#endif
