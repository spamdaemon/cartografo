#include <cartografo/geo/Paths.h>
#include <cartografo/geo/NodeVisitor.h>

namespace cartografo {
  namespace geo {
  
    Paths::Paths (const ::timber::Reference<Path>& p) throws()
    : Shapes< ::timber::Pointer<Path> >(p) {}

    Paths::Paths (const ::std::vector< ::timber::Reference<Path> >& p) throws()
    {
      for (size_t i=0,sz=p.size();i<sz;++i) {
	add(p[i]);
      }
    }

    Paths::Paths (const Paths& g) throws()
    : Shapes< ::timber::Pointer<Path> >(g) {}
  
    Paths::~Paths () throws()
    {}
  
    void Paths::acceptNodeVisitor (NodeVisitor& v) const throws()
    { v.visit(*this); }
  }
}
