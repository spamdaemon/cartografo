#ifndef _CARTOGRAFO_GEO_GROUP_H
#define _CARTOGRAFO_GEO_GROUP_H

#ifndef _CARTOGRAFO_GEO_COMPOSITENODE_H
#include <cartografo/geo/CompositeNode.h>
#endif

namespace cartografo {
  namespace geo {
    /**
     * This class provides the methods to compose a tree
     * of nodes. A group can either be a separator group (default),
     * in which case graphics state is restored after the group
     * is rendered, or just a group, where graphic state is
     * not restored.
     */
    class Group : public CompositeNode {
      Group&operator=(const Group&);

      /** 
       * Create group that that acts as a separator.
       */
    public:
      Group () throws();

      /** 
       * Default constructor 
       * @param separator true if this group is a separator group
       */
    public:
      Group (bool separator) throws();

      /** 
       * Constructor that pre-allocates space.
       * @param n number of children to pre-allocate
       */
    public:
      Group (size_t n) throws();

      /** 
       * Constructor that pre-allocates space.
       * @param n number of children to pre-allocate
       * @param separator true if this group is a separator group
       */
    public:
      Group (size_t n, bool separator) throws();
      
      /**
       * Create a group with two children.
       * @param l the left child
       * @param r the right child
       */
    public:
      Group (const ::timber::Pointer<Node>& l, const ::timber::Pointer<Node>& r) throws();
 
      /**
       * Create a group with two children.
       * @param l the left child
       * @param r the right child
       * @param sep true if this node should be a separator
       */
    public:
      Group (const ::timber::Pointer<Node>& l, const ::timber::Pointer<Node>& r, bool sep) throws();
 
 
      /**
       * Create a group with three children.
       * @param l the left child
       * @param m the middle child
       * @param r the right child
       */
    public:
      Group (const ::timber::Pointer<Node>& l, const ::timber::Pointer<Node>& m, const ::timber::Pointer<Node>& r) throws();
 
      /**
       * Create a group with three children.
       * @param l the left child
       * @param m the middle child
       * @param r the right child
       * @param sep true if this node should be a separator
       */
    public:
      Group (const ::timber::Pointer<Node>& l, const ::timber::Pointer<Node>& m, const ::timber::Pointer<Node>& r, bool sep) throws();
 
      /** 
       * Copy constructor that copies the handle,
       * but keeps the same implementation.
       * @param g the original group
       */
    private:
      Group (const Group& g) throws();
      
      /**  Destroy this graphic */
    public:
      ~Group () throws();

      /**
       * Get the specified node.
       * @param pos the position of a graphic
       * @return the node at the specified position or 0
       * if there isn't one
       * @see getNode()
       */
    public:
      inline ::timber::Pointer<Node> node (size_t pos) const throws()
      { return childAt(pos); }
      
      /**
       * Set the graphic at the specified position. If the new graphic
       * is 0, then the current graphic is only erased, and the
       * graphics next to it are not shifted. Calls fireEvent()
       * if a change was made.
       * @param g the new graphic or 0
       * @param pos a position
       * @pre REQUIRE_RANGE(pos,0,nodeCount())
       */
    public:
      inline void set (const ::timber::Pointer<Node>& g, size_t pos) throws()
      { setChildAt(g,pos); }
	
      /**
       * Insert a graphic at the specified position. Calls fireEvent()
       * if a change was made.
       * @param g the new graphic or 0
       * @param pos a position
       * @pre REQUIRE_RANGE(pos,0,nodeCount())
       */
    public:
      inline void insert (const ::timber::Pointer<Node>& g, size_t pos) throws()
      { insertChildAt(g,pos); }

      /**
       * Add a graphic. Calls fireEvent().
       * @param g a new graphic
       */
    public:
      inline void add (const ::timber::Pointer<Node>& g) throws()
      { addChild(g); }

      /**
       * Remove the graphic at the specified position. Calls fireEvent().
       * @param pos a position
       * @pre REQUIRE_RANGE(pos,0,nodeCount()-1)
       * @return the graphic at the position
       */
    public:
      inline ::timber::Pointer<Node> remove (size_t pos) throws()
      { return removeChildAt(pos); }

      /**
       * Remove the graphic. Calls fireEvent().
       * @param g a graphic 
       * @return true if the graphic was removed, false if not found
       */
    public:
      inline bool remove (const ::timber::Pointer<Node>& g) throws()
      { return removeChild(g); }

      /**
       * Clear this group.
       */
    public:
      inline void clear () throws()
      { removeChildren(); }

      /**
       * Pack this group. Removes all positions
       * where there are no children.
       */
    public:
      inline void pack() throws()
      { packChildren(); }
    };
  }
}
#endif
