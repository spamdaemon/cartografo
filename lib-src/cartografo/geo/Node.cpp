#include <cartografo/geo/Node.h>
#include <cartografo/geo/NodeGraph.h>
#include <cartografo/geo/NodeObserver.h>
#include <cartografo/geo/NodeEvent.h>

using namespace ::timber;

namespace cartografo {
  namespace geo {

    struct Node::DispatchFunction {
      inline DispatchFunction (const NodeEvent& ev) : _event(ev) {}
      inline void operator() (Dispatchable& d) const throws()
      { 
	if (d._observer!=0) {
	  d._observer->nodeChanged(_event); 
	}
      }
      const NodeEvent& _event;
    };

    struct Node::AttachToNodeGraph
    {
      inline AttachToNodeGraph (const Reference<Node>& n, NodeGraph& sg) 
	: _node(n),_sg(sg),_attached(false) {}
      inline void operator() (Dispatchable& d) throws()
      { 
	if (d._sceneGraph==&_sg) {
	  // already attached
	  ++d._refCount;
	  _attached=true;
	}
      }

      void attach()
      {
	if (!_attached)  {

	  NodeObserver* obs = Node::createObserver(_sg,*_node);
	  try {
	    Dispatchable d(obs,&_sg);
	    _node->_dispatcher.add(d);
	    _node->attachToNodeGraph(_sg);
	    assert(_node->_dispatcher.contains(d));
	    _attached = true; // just in case
	  }
	  catch (...) {
	    // just in case, even through we're not throwing here
	    Node::destroyObserver(_sg,obs);
	    throw;
	  }
	}
      }

      Reference<Node> _node;
      NodeGraph& _sg;
      bool _attached;
    };

    struct Node::DetachFromNodeGraph
    {
      inline DetachFromNodeGraph (const Reference<Node>& n, NodeGraph& sg) 
	: _node(n),_sg(sg) {}
      inline void operator() (Dispatchable& d) const throws()
      { 
	if (d._sceneGraph==&_sg) {
	  if (-- d._refCount == 0) {
	    _node->detachFromNodeGraph(_sg);
	    NodeObserver* o = d._observer;
	    d._sceneGraph=0;
	    _node->_dispatcher.remove(d);
	    if (o!=0) {
	      Node::destroyObserver(_sg,o);
	    }
	  }
	}
      }
      Reference<Node> _node;
      NodeGraph& _sg;
    };

    struct Node::AttachFunction {
      inline AttachFunction (const Reference<Node>& n) : _node(n) {}
      inline void operator() (Dispatchable& d) const throws()
      { attachNode(_node,*d._sceneGraph); }
      Reference<Node> _node;
    };

    struct Node::DetachFunction {
      inline DetachFunction (const Reference<Node>& n) : _node(n) {}
      inline void operator() (Dispatchable& d) const throws()
      { detachNode(_node,*d._sceneGraph); }
      Reference<Node> _node;
    };

      
    Node::~Node() throws()
    {}

    void Node::attachToNodeGraph (NodeGraph&) throws()
    {}

    void Node::detachFromNodeGraph (NodeGraph&) throws()
    {}

    void Node::attachNode (const Reference<Node>& node, NodeGraph& g) throws()
    {
      AttachToNodeGraph xattach(node,g);
      node->_dispatcher.dispatch(xattach);
      // need to the actual attaching if the node was not yet registered
      xattach.attach();
    }

    void Node::detachNode (const Reference<Node>& node, NodeGraph& g) throws()
    {
      DetachFromNodeGraph xdetach(node,g);
      node->_dispatcher.dispatch(xdetach);
    }
      

    void Node::fireEvent (const NodeEvent& e) throws()
    {
      DispatchFunction t(e);
      _dispatcher.dispatch(t);
    }

    void Node::detachNode (const Reference<Node>& node) throws()
    {
      assert(&(*node)!=this);
      DetachFunction t(node);
      _dispatcher.dispatch(t);
    }

    void Node::attachNode (const Reference<Node>& node) throws()
    {
      assert(&(*node)!=this);
      AttachFunction t(node);
      _dispatcher.dispatch(t);
    }

    void Node::acceptNodeVisitor (NodeVisitor&) const throws()
    {}

    void Node::notifyChange () throws()
    {
      if (observerCount()>0) {
	fireEvent(NodeEvent(this)); 
      }
    }

    NodeObserver* Node::createObserver (NodeGraph& sg, Node& node) throws()
    {
      return sg.createObserver(node); 
    }

    void Node::destroyObserver (NodeGraph& sg, NodeObserver* obs) throws()
    {
      sg.destroyObserver(obs); 
    }
  }
}
