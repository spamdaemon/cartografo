#ifndef _CARTOGRAFO_GEO_NODEEVENT_H
#define _CARTOGRAFO_GEO_NODEEVENT_H

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {
  namespace geo {
    /**
     * This class is created by Scene graph objects.
     * and is passed to NodeObserver.
     */
    class NodeEvent {
      /** Destroy this event */
    public:
      virtual ~NodeEvent () throws();

      /** 
       * Create a new NodeEvent 
       * @param g the source graphic object
       */
    public:
      inline NodeEvent (const ::timber::Pointer<Node>& g) throws()
	: _node(g) {}
      
      /** 
       * Get the source object.
       * @return the source object
       */
    public:
      inline ::timber::Pointer<Node> source() const throws()
      { return _node; }

      /** The source object */
    private:
      ::timber::Pointer<Node> _node;
    };
  }
}
#endif
