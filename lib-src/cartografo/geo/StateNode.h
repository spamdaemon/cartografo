#ifndef _CARTOGRAFO_GEO_STATENODE_H
#define _CARTOGRAFO_GEO_STATENODE_H

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif


namespace cartografo {
  namespace geo {

    /**
     * This graphic is a material container. All materials
     * in this container a rendered with the same attributes.
     */
    class StateNode : public Node {
      /** No copy operator */
      StateNode&operator=(const StateNode&);
      StateNode(const StateNode&);

      /** Default constructor */
    protected:
      inline StateNode () throws()
      {}
    
      /**  Destroy this graphic handle */
    public:
      ~StateNode () throws();
    };
  }
}
#endif
