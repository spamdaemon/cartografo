#ifndef _CARTOGRAFO_GEO_NODE_H
#define _CARTOGRAFO_GEO_NODE_H

#ifndef _CARTOGRAFO_GEO_H
#include <cartografo/geo/geo.h>
#endif

#ifndef _TIMBER_EVENT_DISPATCHER_H
#include <timber/event/Dispatcher.h>
#endif

namespace cartografo {
  namespace geo {
    /** A graphic observer */
    class NodeObserver;
  
    /** A graphic observer */
    class NodeEvent;
  
    /** A visitor */
    class NodeVisitor;
  
    /** A scene graph */
    class NodeGraph;
  
    /** This class represents a graphic node. */
    class Node : public ::timber::Counted {
      Node&operator=(const Node&);
      Node(const Node&);
    
      /** The scene graph will be a friend of the node */
      friend class NodeGraph;
    
      /** A dispatchable */
    private:
      struct Dispatchable {
	inline Dispatchable (NodeObserver* o, NodeGraph* g) throws()
	  : _observer(o),_sceneGraph(g),_refCount(1) {}
      
	NodeObserver* _observer;
	NodeGraph* _sceneGraph;
	UShort _refCount;
	bool operator==(const Dispatchable& d) const throws()
	{ return _sceneGraph == d._sceneGraph; }
      };
    
      /** A dispatch function */
    private:
      struct DispatchFunction;

      /** The function that attaches a node to a scenegraph */
    private:
      struct AttachToNodeGraph;

      /** The function that detaches a node to a scenegraph */
    private:
      struct DetachFromNodeGraph;

      /** An attach function */
    private:
      struct AttachFunction;
    
      /** A detach function */
    private:
      struct DetachFunction;
    
      /** Default constructor */
    protected:
      inline Node () throws() {}
    
      /** Destroy this node */
    public:
      ~Node () throws();
    
      /**
       * Return a reference to self.
       * @return this
       */
    private:
      inline Node& self() const throws()
      { return const_cast<Node&>(*this); }
    
      /**
       * Get the number of observers
       * @return the number of observers.
       */
    protected:
      inline size_t observerCount() const throws()
      { return _dispatcher.size(); }
    
      /**
       * Notify the observers of the specified event.
       * @param e an event
       */
    protected:
      void fireEvent (const NodeEvent& e) throws();
    
      /**
       * Send a gfx event to all registered listeners.
       * This just call <code>fireEvent(NodeEvent(this))</code>
       */
    protected:
      void notifyChange () throws();
    
      /**
       * Accept the specified visitor
       * @param v a visitor
       */
    public:
      inline void accept (NodeVisitor& v) const throws()
      { acceptNodeVisitor(v); }
    
      /**
       * Accept the specified visitor. By default this method does nothing.
       * @param v a visitor
       */
    protected:
      virtual void acceptNodeVisitor (NodeVisitor& v) const throws();
    
      /**
       * Attach this node to the specified scene graph.
       * This method does nothing by default.
       * @param sg a scene graph
       */
    protected:
      virtual void attachToNodeGraph (NodeGraph& sg) throws();
      
      /**
       * Detach this node from the specified scenegraph. It is
       * an error if sg was not attached. This method does nothing
       * by default.
       * @param sg a scene graph. 
       */
    protected:
      virtual void detachFromNodeGraph (NodeGraph& sg) throws();
      
      /**
       * Detach this node from the specified scenegraph. It is
       * an error if sg was not attached. 
       * @param node a node
       * @param sg a scene graph. 
       */
    protected:
      static void detachNode (const ::timber::Reference<Node>& node, NodeGraph& sg) throws();
      
      /**
       * Attach the given node to the specified scene graph. If
       * this node has already been attached, then the reference
       * count for this scene graph is simply bumped up by 1.
       * @param node a node
       * @param sg a scene graph
       */
    protected:
      static void attachNode (const ::timber::Reference<Node>& node, NodeGraph& sg) throws();
    
      /**
       * A node requires detaching; subclasses must call this if
       * a new node is being removed while it it attached.
       * @param node a node or 0
       */
    protected:
      void detachNode (const ::timber::Reference<Node>& node) throws();

      /**
       * Attach the specifeid node. Subclasses must call this if
       * a new node is being added while it is attached.
       * @param node a node or 0
       */
    protected:
      void attachNode (const ::timber::Reference<Node>& node) throws();

      /**
       * Create an observer for the specified graphic. 
       * @param sg a scene graph
       * @param node a node
       * @return an observer for the graphic
       */
    private:
      static NodeObserver* createObserver (NodeGraph& sg, Node& node) throws();

      /**
       * Destroy the specified observer of a scene graph 
       */
    private:
      static void destroyObserver (NodeGraph& sg, NodeObserver* obs) throws();
    
      /** The dispatcher */
    private:
      mutable ::timber::event::Dispatcher<Dispatchable> _dispatcher;
    };
  }
}
#endif
