#ifndef _CARTOGRAFO_GEO_H
#define _CARTOGRAFO_GEO_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

/**
 * This namespace defines the basic graphics
 * infrastructure for drawing shapes.
 */
namespace cartografo {    
  namespace geo {

    /** The types from timber that are used */
    using ::timber::UInt;
    using ::timber::Int;
  
    using ::timber::ULong;
    using ::timber::Long;

    using ::timber::UShort;
    using ::timber::Short;

    using ::timber::UByte;
    using ::timber::Byte;

    /** The index type */
    typedef size_t Index;

    /** The invalid index is just a negative number */
    inline size_t invalidIndex() { return ~size_t(0); }

    /**
     * Round a double to a 16-bit integer. It is assumed
     * that the integer part of the floating point
     * value will fit into a signed short.
     * @param x a double
     * @return <code>(short)(x>=0 ? x+0.5 : x-0.5)</code>
     */
    inline Short roundToShort(double x)
    { return static_cast<Short>(x+ (x > 0 ? .5 : -0.5)); } 


    /**
     * Round a double to an integer. It is assumed
     * that the integer part of the floating point
     * value will fit into a signed integer.
     * @param x a double
     * @return <code>(int)(x>=0 ? x+0.5 : x-0.5)</code>
     */
    inline Int roundToInt(double x)
    { return static_cast<Int>(x+ (x > 0 ? .5 : -0.5)); } 

    /**
     * Round a double to a long. It is assumed
     * that the integer part of the floating point
     * value will fit into a signed long.
     * @param x a double
     * @return <code>(long long)(x>=0 ? x+0.5 : x-0.5)</code>
     */
    inline Long roundToLong(double x)
    { return static_cast<Long>(x+ (x > 0 ? .5 : -0.5)); } 
  

    inline double sqr(double x) { return x*x; }


    /**
     * Compute the hashvalue for a character
     * @param i a character
     * @return a hashvalue for i
     */
    inline Int	hash (char i) throws()	
    { return i; } 
  
    /**
     * Compute the hashvalue for a wide character
     * @param i a character
     * @return a hashvalue for i
     */
    inline Int	hash (wchar_t i) throws()	
    { return i; } 
  
    /**
     * Compute the hashvalue for a byte.
     * @param i a byte
     * @return a hashvalue for i
     */
    inline Int	hash (Byte i) throws()	
    { return i; } 
  
    /**
     * Compute the hashvalue for an unsigned byte
     * @param i an unsigned byte
     * @return a hashvalue for i
     */
    inline Int	hash (UByte i) throws()	
    { return i; } 
  
    /**
     * Compute the hashvalue for an unsigned short.
     * @param i an unsigned short
     * @return a hashvalue for i
     */
    inline Int	hash (UShort i) throws()
    { return i; } 
  
    /**
     * Compute the hashvalue for a short.
     * @param i a short
     * @return a hashvalue for i
     */
    inline Int	hash (Short i) throws()	
    { return i; } 

    /**
     * Compute the hashvalue for an unsigned integer.
     * @param i an unsigned integer
     * @return a hashvalue for i
     */
    inline Int	hash (UInt i) throws()	
    { return (Int)i; } 

    /**
     * Compute the hashvalue for an integer.
     * @param i an integer
     * @return a hashvalue for i
     */
    inline Int	hash (Int i) throws()	
    { return i; } 

    /**
     * Compute the hashvalue for a 64-bit long.
     * @param i a 64-bit long
     * @return a hashvalue for i
     */
    inline Int	hash (Long i) throws()	
    { return (Int)(i>>32)^(i&0xffffffff); } 

    /**
     * Compute the hashvalue for an unsigned 64-bit long .
     * @param i an unsigned 64-bit long
     * @return a hashvalue for i
     */
    inline Int	hash (ULong i) throws()	
    { return (Int)(i>>32)^(Int)(i&0xffffffff); }

    /**
     * Compute the hashvalue for a float.
     * @param flt a float
     * @return a hashvalue for flt
     */
    inline Int	hash (float flt) throws()	
    {
      union { 
	float f;
	Int   i;
      } x;
      x.f = flt;
      
      return x.i; 
    } 

    /**
     * Compute the hashvalue for a double.
     * @param flt a double
     * @return a hashvalue for flt
     */
    inline Int	hash (double flt) throws()	
    { 
      union { 
	double f;
	Int i[2];
      } x;
      x.f = flt;
      return x.i[0] ^ x.i[1];
    }

    /**
     * Compute the hashvalue for a bounded string.
     * @param str a string
     * @param length the length of the string
     * @return a hashvalue for the bounded string
     */
    Int hashString (const char* str, Int length) throws();

    /**
     * Compute the hashvalue for the 0-terminated string.
     * @param str a null-terminated string
     * @return a hashvalue for the string
     */
    Int hashString (const char* str) throws();

    /**
     * Compute the hashvalue for a bounded string, but ignore the case
     * @param str a string
     * @param length the length of the string
     * @return a hashvalue for the bounded string
     */
    Int hashStringIgnoreCase (const char* str, Int length) throws();

    /**
     * Compute the hashvalue for the 0-terminated string, but ignore the case.
     * @param str a null-terminated string
     * @return a hashvalue for the string
     */
    Int hashStringIgnoreCase (const char* str) throws();

    /**
     * Compute the hashvalue for a pointer
     * @param addr a pointer
     * @return the hashvalue for the address
     */
    inline	Int	hash (const void* addr) throws()	
    { return *reinterpret_cast<const Int*>(addr); }
  
    /**
     * Inclusively clamp a value to within a range.
     * @param v the value to be clamped
     * @param lo the low value
     * @param hi the high value
     * @pre !(hi<lo)
     * @return a value v such that lo <= v <= hi
     */
    template <class T>
      inline T clamp (const T& v, const T& lo, const T& hi) throws()
      { 
	assert(!(hi<lo));
	if (v<lo) {
	  return lo;
	}
	else if (v>hi) {
	  return hi;
	}
	return v;
      }

  }
}
#endif
