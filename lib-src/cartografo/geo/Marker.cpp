#include <cartografo/geo/Marker.h>
#include <cartografo/geo/NodeVisitor.h>

#include <indigo/Node.h>
#include <indigo/SceneGraph.h>
#include <indigo/GfxObserver.h>

using namespace ::timber;

namespace cartografo {
  namespace geo {

    class Marker::Observer : public ::indigo::SceneGraph, public ::indigo::GfxObserver {
    public:
      Observer(const Marker::GeodeticCoordinates& loc, Marker& owner) throws()
      : _owner(owner),_location(loc)
      {}
      
    public:
      ~Observer() throws()
      {}
      
    public:
      /** The SceneGraph interface */
      void setRootNode (::timber::Pointer< ::indigo::Node> h) throws()
      {
	if (rootNode()!=h) {
	  ::indigo::SceneGraph::setRootNode(h);
	  _owner.notifyChange();
	}
      }

    public:
      void setLocation (const GeodeticCoordinates& loc) throws()
      {
	if (_location!=loc) {
	  _location = loc;
	  _owner.notifyChange();
	}
      }
    
    public:
      const Marker::GeodeticCoordinates& location () const throws()
      { return _location; }
      
   protected:
      ::indigo::GfxObserver* createObserver( ::indigo::Node& n) throws()
      { return this; }

      /** The GfxObserver interface */
    protected:
      void gfxChanged (const ::indigo::GfxEvent&) throws()
      { _owner.notifyChange(); }

      

      /** The owner */
    private:
      Marker& _owner;

      /** The location */
    private:
      Marker::GeodeticCoordinates _location;
    };

    Marker::Marker (const GeodeticCoordinates& loc) throws()
    : _observer(new Observer(loc,*this))
    {}
    
    Marker::Marker (const Marker& g) throws()
    : Node(),_observer(new Observer(g.location(),*this))
    { _observer->setRootNode(g.node()); }

    Marker::Marker (const GeodeticCoordinates& loc, const Pointer< ::indigo::Node>& n) throws()
    : _observer(new Observer(loc,*this))
    { _observer->setRootNode(n); }
    
    
    Marker::~Marker () throws()
    { _observer->setRootNode( Pointer< ::indigo::Node>()); }
    
    void Marker::setLocation (const GeodeticCoordinates& loc) throws()
    { _observer->setLocation(loc); }
    
    const Marker::GeodeticCoordinates& Marker::location () const throws()
    { return _observer->location(); }

    void Marker::setNode (const Pointer< ::indigo::Node>& n) throws()
    { _observer->setRootNode(n); }

    Pointer< ::indigo::Node> Marker::node() const throws() 
    { return _observer->rootNode(); }
   
    void Marker::acceptNodeVisitor (NodeVisitor& v) const throws()
    { v.visit(*this); }

  }
}
