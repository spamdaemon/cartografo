#ifndef _CARTOGRAFO_GEO_SWITCH_H
#define _CARTOGRAFO_GEO_SWITCH_H

#ifndef _CARTOGRAFO_GEO_GROUP_H
#include <cartografo/geo/Group.h>
#endif

namespace cartografo {
  namespace geo {

    /**
     * This class is a special kind of group, where only one
     * child is visible at any time.
     * 
     * @date 31 Jul 2003
     */
    class Switch : public Group {
      Switch(const Switch&);
      Switch&operator=(const Switch&);
    
      /** Default constructor */
    public:
      inline Switch () throws()
	: _visibleChild(invalidIndex())
	{}

      /** 
       * Default constructor 
       * @param separator true if this switch is a separator switch
       */
    public:
      inline Switch (bool separator) throws()
	: Group(separator),_visibleChild(invalidIndex())
	{}

      /** 
       * Constructor that pre-allocates space.
       * @param n number of children to pre-allocate
       */
    public:
      inline Switch (size_t n) throws()
	: Group(n),_visibleChild(invalidIndex()) {}

      /** 
       * Constructor that pre-allocates space.
       * @param n number of children to pre-allocate
       * @param separator true if this switch is a separator switch
       */
    public:
      inline Switch (size_t n, bool separator) throws()
	: Group(n,separator),_visibleChild(invalidIndex()) {}

      /**  Destroy this graphic */
    public:
      ~Switch () throws();

      /**
       * Set the index of the child that is to be visible.
       * It is not required that the child index be valid
       * @param child an index of a child or an invalidIndex
       */
    public:
      virtual void setVisibleChild (size_t child) throws();

      /**
       * Get the currently visible child. 
       * @return the index of the currently visible child
       */
    public:
      inline size_t visibleChild () const throws()
      { return _visibleChild; }

      /**
       * Accept the specified visitor. Calls
       * the Node::acceptNodeVisitor() method, followed
       * by dispatchNodeVisitor();
       * @param v a visitor.
       */
    protected:
      void acceptNodeVisitor (NodeVisitor& v) const throws();

      /** True if this is a separator switch */
    private:
      size_t _visibleChild;
	
    };
  }
}
#endif
