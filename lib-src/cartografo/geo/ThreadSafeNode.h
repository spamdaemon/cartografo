#ifndef _CARTOGRAFO_GEO_THREADSAFENODE_H
#define _CARTOGRAFO_GEO_THREADSAFENODE_H

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif

namespace cartografo {
   namespace geo {
      /**
       * This node has a single child which can be set by arbitrary threads.
       */
      class ThreadSafeNode : public Node
      {
            ThreadSafeNode(const ThreadSafeNode&);
            ThreadSafeNode&operator=(const ThreadSafeNode&);

            /**
             * Default constructor.
             */
         private:
            ThreadSafeNode()throws();

            /**  Destroy this graphic */
         public:
            ~ThreadSafeNode()throws();

            /**
             * Create a new ThreadSafeNode.
             * @return a threadsafe node
             */
         public:
            static ::timber::Reference< Node> create()throws();

            /**
             * Create a new ThreadSafeNode.
             * @param child the initial child.
             * @return a threadsafe node
             */
         public:
            static ::timber::Reference< Node> create(const ::timber::Pointer< Node>& child)throws();

            /**
             * Set the child.
             * @param child the new child.
             */
         public:
            virtual void setChild(const ::timber::Pointer< Node>& child)throws() = 0;

            /**
             * Get the current child. Note that the child maybe removed at any time!
             * @return the current child.
             */
         public:
            virtual ::timber::Pointer< Node> child() const throws() = 0;

      };
   }
}
#endif
