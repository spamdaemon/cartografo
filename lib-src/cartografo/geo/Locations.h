#ifndef _CARTOGRAFO_GEO_LOCATION_H
#define _CARTOGRAFO_GEO_LOCATION_H

#ifndef _CARTOGRAFO_GEO_SHAPES_H
#include <cartografo/geo/Shapes.h>
#endif

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

namespace cartografo {
  namespace geo {
    
    /**
     * A list of geodetic coordinates.
     */
    class Locations : public Shapes< ::terra::GeodeticCoordinates> {
      /** A location */
    public:
      typedef ::terra::GeodeticCoordinates GeodeticCoordinates;
      
      Locations&operator=(const GeodeticCoordinates&);
      
      /** Default constructor */
    public:
      Locations () throws();
    
      /**
       * Create a points with a single point. 
       */
    public:
      Locations (const GeodeticCoordinates& p) throws();
    
      /**
       * Create a set of points.
       * @param n the number of points
       * @param pts a set of points
       */
    public:
      Locations (size_t n, const GeodeticCoordinates* pts) throws();
    
      /**
       * Create a set of points.
       * @param pts a set of points
       */
    public:
      Locations (const ::std::vector<GeodeticCoordinates>& pts) throws();

      /** 
       * Copy constructor that copies the handle,
       * but keeps the same implementation.
       */
    private:
      Locations (const Locations& g) throws();
  
      /**
       * Set the width in pixels with which the points should be rendered. If
       * the specified size is set to a value less than 1, then the point size
       * will be set to 0.
       * @param sz the size of a point in pixels
       */
    public:
      void setPointSize (size_t sz) throws();

      /**
       * Get the point size. 
       * @return the point size in pixels.
       */
    public:
      inline size_t pointSize () const throws()
      { return _pointSize; }

      /**  Destroy this graphic handle */
    public:
      ~Locations () throws();
      
    protected:
      void acceptNodeVisitor (NodeVisitor& v) const throws();
      
      /** The point size */
    private:
      size_t _pointSize;
    };
  }
}
#endif

