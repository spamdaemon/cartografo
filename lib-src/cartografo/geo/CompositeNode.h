#ifndef _CARTOGRAFO_GEO_COMPOSITENODE_H
#define _CARTOGRAFO_GEO_COMPOSITENODE_H

#ifndef _CARTOGRAFO_GEO_H
#include <cartografo/geo/geo.h>
#endif

#ifndef _CARTOGRAFO_GEO_NODE_H
#include <cartografo/geo/Node.h>
#endif

#ifndef _CARTOGRAFO_GEO_NODEEVENT_H
#include <cartografo/geo/NodeEvent.h>
#endif

#include <vector>

namespace cartografo {
   namespace geo {
      /**
       * This class is the base class for composite graphics.
       * It provides methods to insert child graphics and set
       * child graphics;
       */
      class CompositeNode : public Node
      {
            CompositeNode&operator=(const CompositeNode&);

            /**
             * A composite event.
             */
         public:
            class CompositeEvent : public NodeEvent
            {
                  /** The event id */
               public:
                  enum ID
                  {
                     CLEARED, //< all children have been removed
                     REMOVED, //< a single child has been removed
                     ADDED, //< a single child has been added
                     PACKED,
                  //< the node has been packed
                  };

                  /** Destroy this event */
               public:
                  ~CompositeEvent()throws();

                  /**
                   * Create a new event for hierarchy changes.
                   * @param i the type of event
                   * @param p the composite parent
                   * @param c the child that was involved
                   * @param pos the position associated with the child
                   */
               public:
                  inline CompositeEvent(ID i, const ::timber::Pointer< Node>& p, const ::timber::Pointer< Node>& c,
                        size_t pos)throws()
                  : NodeEvent(p), _id(i), _child(c), _pos(pos) {}

                  /**
                   * Create a new event for hierarchy changes.
                   * @param i the type of event
                   * @param p the composite parent
                   */
               public:
                  inline CompositeEvent(ID i, const ::timber::Pointer< Node>& p)throws()
                  : NodeEvent(p), _id(i), _pos(invalidIndex()) {}

                  /**
                   * Get the event id.
                   * @return the event id
                   */
               public:
                  inline ID id() const throws() {return _id;}

                  /**
                   * Get the object was added or removed. It can return 0 for
                   * some types of events.
                   * @return the object that was added or removed
                   */
               public:
                  inline ::timber::Pointer< Node> child() const throws() {return _child;}

                  /**
                   * Get the position of the child
                   * @return the position of the child
                   */
               public:
                  inline size_t position() const throws() {return _pos;}

                  /** The event id */
               private:
                  ID _id;

                  /** The child involved */
               private:
                  ::timber::Pointer< Node> _child;

                  /** The position of the child */
               private:
                  size_t _pos;
            };

            /** Create a <em>non-separator</em> node. */
         protected:
            CompositeNode()throws();

            /**
             * Default constructor
             * @param separator true if this group is a separator group
             */
         public:
            CompositeNode(bool separator)throws();

            /**
             * Create a composite node with the specified children
             * @param l the left child
             * @param m the middle child
             * @param r the right child
             */
         protected:
            CompositeNode(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& m,
                  const ::timber::Pointer< Node>& r)throws();

            /**
             * Create a composite node with the specified children
             * @param l the left child
             * @param r the right child
             */
         protected:
            CompositeNode(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& r)throws();

            /**
             * Constructor that pre-allocates space.
             * @param n number of children to pre-allocate
             */
         protected:
            CompositeNode(size_t n)throws();

            /**
             * Create a composite node with the specified children
             * @param l the left child
             * @param m the middle child
             * @param r the right child
             * @param sep true to make this a separator node
             */
         protected:
            CompositeNode(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& m,
                  const ::timber::Pointer< Node>& r, bool sep)throws();

            /**
             * Create a composite node with the specified children
             * @param l the left child
             * @param r the right child
             * @param sep true to make this a separator node
             */
         protected:
            CompositeNode(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& r, bool sep)throws();

            /**
             * Constructor that pre-allocates space.
             * @param n number of children to pre-allocate
             * @param sep true to make this a separator node
             */
         protected:
            CompositeNode(size_t n, bool sep)throws();

            /**
             * Copy constructor that copies the handle,
             * but keeps the same implementation.
             */
         protected:
            CompositeNode(const CompositeNode& g)throws();

            /**  Destroy this node handle */
         public:
            ~CompositeNode()throws();

            /**
             * Test if this is a separator group. If this
             * is a separator group, then rendering the group
             * must preserve the current graphics state.
             * @return true if this is a separator group
             */
         public:
            inline bool isSeparator() const throws()
            {  return _separator;}

            /**
             * Get the number of child nodes
             * @return the number of child nodes including null children
             */
         public:
            inline size_t nodeCount() const throws()
            {  return _children.size();}

            /**
             * Get the number of non-null child nodes.
             * @return the true number of children.
             */
         public:
            inline size_t nonNullNodeCount() const throw()
            {
               return _nonNullChildren;
            }

            /**
             * Get the indicated child node. This method is typically used from a visitor and
             * callers <em>must</em> must not assume anything about the returned node as the type
             * of node, it's position, is entirely under control over the subclass.
             * @param pos a graphic position
             * @return the graphic at the specified position or 0 if there isn't one
             */
         public:
            inline ::timber::Pointer< Node> getNode(size_t pos) const throws()
            {
               return childAt(pos);
            }

            /**
             * Test if the there is good child at the specified position.
             * @param pos a node position
             * @return true if there <code>node(pos)</code> would return non-zero.
             */
         protected:
            inline bool hasNode(size_t pos) const throws()
            {
               return
               pos<_children.size()
               && _children[pos]!=nullptr;
            }

            /**
             * Get the index of the specified graphic.
             * @param g a graphic
             * @return the position of the node or invalidIndex()
             */
         protected:
            size_t indexOf(const ::timber::Pointer< Node>& g) const throws();

            /**
             * Get the specified child.
             * @param pos a child
             * @return the child at the specified position or 0
             * if there isn't one
             */
         protected:
            inline ::timber::Pointer< Node> childAt(size_t pos) const throws()
            {  return _children.at(pos);}

            /**
             * Set the child at the specified position. If the new child
             * is 0, then the current child is only erased, and the
             * children next to it are not shifted. Calls fireEvent()
             * if a change was made.
             * @param g the new node or 0
             * @param pos a position
             * @pre REQUIRE_RANGE(pos,0,nodeCount())
             */
         protected:
            void setChildAt(const ::timber::Pointer< Node>& g, size_t pos)throws();

            /**
             * Insert a child at the specified position. Calls fireEvent()
             * if a change was made.
             * @param g the new graphic or 0
             * @param pos a position
             * @pre REQUIRE_RANGE(pos,0,nodeCount())
             */
         protected:
            void insertChildAt(const ::timber::Pointer< Node>& g, size_t pos)throws();

            /**
             * Add a child. Calls fireEvent().
             * @param g a new graphic
             */
         protected:
            inline void addChild(const ::timber::Pointer< Node>& g)throws()
            {  setChildAt(g,nodeCount());}

            /**
             * Add a child. Calls fireEvent().
             * @param g a new graphic
             */
         protected:
            bool removeChild(const ::timber::Pointer< Node>& g)throws();

            /**
             * Remove the child at the specified position. Calls fireEvent().
             * @param pos a position
             * @pre REQUIRE_RANGE(pos,0,nodeCount()-1)
             * @return the child at the position
             */
         protected:
            ::timber::Pointer< Node> removeChildAt(size_t pos)throws();

            /**
             * Remove all children.
             */
         protected:
            void removeChildren()throws();

            /**
             * Accept the specified visitor. Calls
             * the Node::acceptNodeVisitor() method, followed
             * by dispatchNodeVisitor();
             * @param v a visitor.
             */
         protected:
            void acceptNodeVisitor(NodeVisitor& v) const throws();

            /**
             * Pack this composite. Removes all positions
             * where there are no children. If <tt>nodeCount()==nonNullNodeCount()</tt> then
             * this method has no effect.
             */
         protected:
            void packChildren()throws();

         protected:
            void attachToNodeGraph(NodeGraph& sg)throws();
            void detachFromNodeGraph(NodeGraph& sg)throws();

            /** The children */
         private:
            ::std::vector< ::timber::Pointer< Node> > _children;

            /**
             * The number of non-null children.
             */
         private:
            size_t _nonNullChildren;

            /** True if this is a separator node */
         private:
            const bool _separator;

      };
   }
}
#endif
