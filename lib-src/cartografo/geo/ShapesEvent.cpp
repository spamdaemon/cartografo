#include <cartografo/geo/ShapesEvent.h>


namespace cartografo {
  namespace geo {
    
    ShapesEvent::ShapesEvent (const ::timber::Pointer<Node>& g,Type t, size_t i) throws()
    : NodeEvent(g),_type(t),_index(i)
    {}
    
    ShapesEvent::~ShapesEvent() throws() {}
    
  }
}
