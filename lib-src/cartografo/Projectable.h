#ifndef _CARTOGRAFO_PROJECTABLE_H
#define _CARTOGRAFO_PROJECTABLE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace cartografo {

  class Projector;

  /**
   * A projectable is basically an aggregation of possibly overapping areas.
   */
  class Projectable : public ::timber::Counted {
    /** No copy construction */ 
    Projectable(const Projectable&);
    Projectable&operator=(const Projectable&);

    /** Default constructor. */
  protected:
    Projectable () throws();

    /** Destructor */
  public:
    virtual ~Projectable() throws() = 0;
    
    /**
     * Project this object using the specified projector.
     * @param p a projector
     */
  public:
    virtual void project (Projector& p) const = 0;
  };
}


#endif
