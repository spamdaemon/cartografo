#ifndef _CARTOGRAFO_REGION_H
#define _CARTOGRAFO_REGION_H

#ifndef _CARTOGRAFO_PROJECTABLE_H
#include <cartografo/Projectable.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <vector>

namespace terra {
  class GeodeticDatum;
  class Extent;
  class Bounds;
}

namespace cartografo {

  class Projector;
  class Area;

  /**
   * A region represents an area on an unspecified spheroid. The region
   * may consist of disconnect subregions. The primary functionality provided
   * by region is to check if a location is inside the region and to obtain
   * the bounds of the region.<p>
   * Typically, clients will use Area to instantiate a region which is bounded
   * by an exterior and multiple interior paths. A AreaBuilder can be used to construct
   * individual areas.<p>
   * Region do not retain information about spheroids.
   */
  class Region : public Projectable {
    /** No copy construction */ 
    Region(const Region&);
    Region&operator=(const Region&);

    /** 
     * The classification is used to determine quickly if a region is contained within some
     * other region. 
     */
  public:
    enum Classification {
      /** This represents no overlap between an extent and a region or a region and a region */
      OUTSIDE = -1,

      /** 
       * This represents a possible overlap between an extent and a region, or a region and a region.
       * It is possible that region or extent to be tested are entirely outside or entirely inside, or 
       * only partially overlap.
       */
      POSSIBLE_OVERLAP = 0,
      
      /** This represents a containment of an extent or region within another region */
      CONTAINED = 1
    };

    /** 
     * Create an empty region.
     */
  protected:
    Region () throws();

    /** Destructor */
  public:
    ~Region() throws();

    /**
     * @name Construction functions. These are simple constructors that
     * build regions from other primitive types.
     * @{
     */

    /**
     * Create a region that encompasses a given bounds.
     * @param b the region bounds
     * @return a region that encompasses the specified bounds
     */
  public:
    static ::timber::Reference<Region> createBoundsRegion (const ::terra::Bounds& b) throws();

    /**
     * Create a region that encompasses the spheroid.
     * @param s a spheroid
     * @return a region that encompasses the spheroid
     */
  public:
    static ::timber::Reference<Region> createSpheroidRegion (const ::timber::Reference< ::terra::GeodeticDatum>& s) throws();

    /**
     * Create a region consisting of multiple non-intersecting regions.
     * @param v a vector of regions
     * @return a region that is the composite of th specified regions.
     */
  public:
    static ::timber::Pointer<Region> createRegion (const ::std::vector< ::timber::Reference<Region> >& s) throws();

    /**
     * Create a region consisting of multiple non-intersecting regions.
     * @param v a vector of regions
     * @return a region that is the composite of th specified regions.
     */
  public:
    static ::timber::Pointer<Region> createRegion (const ::std::vector< ::timber::Reference<Area> >& s) throws();
      

    /**
     * Create a region for an extent.
     * @param e an extent
     * @return the extent as a region object
     */
  public:
    static ::timber::Reference<Region> createExtentRegion (const ::terra::Extent& e) throws();
    
    /*@}*/

    /**
     * @name Geometric construction functions. These constructors build regions from 
     * other regions using geometric operations.
     * @{
     */
    /**
     * Intersect two regions.
     * @param a a region
     * @param b a region
     * @return the intersection region or 0 if the region is empty
     */
  public:
    static ::timber::Pointer<Region> intersect (const ::timber::Pointer<Region>& a, const ::timber::Pointer<Region>& b) throws();

    /**
     * Compute the union of two regions
     * @param a a region
     * @param b a region
     * @return a region representing the union
     */
  public:
    static ::timber::Pointer<Region> merge (const ::timber::Pointer<Region>& a, const ::timber::Pointer<Region>& b) throws();

    /**
     * Compute the difference of two regions
     * @param a a region
     * @param b a region
     * @return the components of region <em>a</em> that do not also lie in region <em>b</em>
     */
  public:
    static ::timber::Pointer<Region> subtract (const ::timber::Reference<Region>& a, const ::timber::Pointer<Region>& b) throws();
      
    /**
     * Compute the exclusive-or between two regions
     * @param a a region
     * @param b a region
     * @return a region that contains parts of region <em>a</em> and <em>b</em>, but not their intersection.
     */
  public:
    static ::timber::Pointer<Region> exclusiveOR (const ::timber::Pointer<Region>& a, const ::timber::Pointer<Region>& b) throws();
       
    /*@}*/
      
    /**
     * Normalize this region. This will among other things remove self-intersections.
     * If the region is (close to) degenerate then a null pointer may be returned.
     * @return a new region or this region if already normalized
     */
  public:
    virtual ::timber::Pointer<Region> normalize() const throws();

    /**
     * Test if this region contains the specified coordinates inside
     * @param lat latitude in radians
     * @param lon longitude in radians
     * @return true if the point is inside
     */
  public:
    virtual bool contains (double lat, double lon) const throws();

    /**
     * Get the extent of this region. The extent returns the
     * bounding box around the values that will be passed
     * to a projector. 
     * @return the extent of this region
     */
  public:
    virtual ::terra::Extent extent() const throws() = 0;

    /**
     * Classify an extent with respect to this region. 
     * The default implementation uses the normalized extent of this region to
     * compute a result.
     * @param r an extent
     * @return OUTSIDE if the extent is completely outside this region
     *         POSSIBLE_OVERLAP if the extent may partially intersect this region
     *         CONTAINED if the extent is completely contained within this region
     */
  public:
    virtual Classification classifyExtent (const ::terra::Extent& r) const throws();

    /**
     * Classify a region with respect to this region. The default for this method
     * is to return @code classifyExtent(r.extent()) @endcode.
     * @param r the region to be classified
     * @return OUTSIDE if the extent is completely outside this region
     *         POSSIBLE_OVERLAP if the extent may partially intersect this region
     *         CONTAINED if the extent is completely contained within this region
     */
  public:
    virtual Classification classifyRegion (const Region& r) const throws();
  };
}


#endif
