#ifndef _CARTOGRAFO_PROJECTIONDOMAIN_H
#define _CARTOGRAFO_PROJECTIONDOMAIN_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif 

namespace terra {
  class Extent;
  class GeodeticCoordinates;
}
namespace cartografo {
  class Region;
  /**
   * The projection class allows Location objects to be
   * projected into a  cartesian coordinate system.
   */
  class ProjectionDomain : public ::timber::Counted {
    ProjectionDomain(const ProjectionDomain&);
    ProjectionDomain&operator=(const ProjectionDomain&);

    /** Default constructor. */
  protected:
    ProjectionDomain () throws();
      

    /** Destructor. */
  protected:
    ~ProjectionDomain () throws();
      

    /**
     * Get the path that outlines the projection domain. 
     * @return the path that outlines the projection domain
     */
  public:
    virtual ::timber::Reference<Region> region() const throws() = 0;

    /**
     * Get the center of this projection.
     * @return the point which is mapped to a single point (0,0) in the plane
     */
  public:
    virtual ::terra::GeodeticCoordinates center() const throws() = 0;
  };
    
}

#endif
