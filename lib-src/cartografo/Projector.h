#ifndef _CARTOGRAFO_PROJECTOR_H
#define _CARTOGRAFO_PROJECTOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIKAL_BOUNDINGBOX2D_H
#include <tikal/BoundingBox2D.h>
#endif

#ifndef _TERRA_EXTENT_H
#include <terra/Extent.h>
#endif

namespace cartografo {

    /** 
     * Instances of a projector are able to project geographic areas and paths
     * into mostly 2D regions and paths.
     * <p>
     * Projectors are not threadsafe. 
     */
    class Projector : public ::timber::Counted {
      /** No copy methods */
      Projector(const Projector&);
      Projector&operator=(const Projector&);

      /** The arc types */
    public:
      enum ArcType {
	GEODESIC,
	LOXODROME
      };

      /** 
       * An arc defined by its end point and type. The end-point of an arc is 
       * not constrained to the interval (-M_PI;M_PI), but may be an arbitrary value.
       * For example, to represent the arc from 160W to 160E along some parallel, we
       * would generate the following projection:
       * <pre>
       *   moveTo(toRadians(160),0);
       *   Arc arc(toRadians(-160+360),0);
       *   moveALongArc(1,&arc);
       * </pre>
       */
    public:
      struct Arc {
	double endLat; //< The end-latitude
	double endLon; //< The end-longitude
	double endAlt; //< The end-altitude
	ArcType arcType; //< The arc type from the previous position
      };
      
      /** 
       * Create a projector with an ementation
       */
    protected:
      inline Projector () throws()
	{}

      /**
       * Destroy this projector.
       */
    public:
      ~Projector() throws();

      /**
       * Clone or copy this projector. The effect of this function is as if 
       * the copy constructor was invoked on this projector.
       * <p>
       * Transient state must not be copied by this projector. Transient state 
       * are such things as cached objects, started paths, etc. It is 
       * best to call this function only from a newly create projector.
       *
       * @return a copy of this projector
       */
    public:
      virtual Projector* copy() const throws() = 0;

      /**
       * @name The projection functions for an area and a path
       * @{
       */
      


      /**
       * Begin an outside path of an area. 
       * @param windCW true if the path winds in CW direction
       * @param bounds optional known bounds for the path
       * @return false if the path will not be projected by this projector
       */
    public:
      virtual bool beginOutsidePath(bool windCW, const ::terra::Extent* bounds) = 0;
      
      /**
       * Begin the inside path of an area. An outside path must 
       * have been begun before creating an inside path. The path
       * contour will be inside the region enclosed by the most 
       * recently started outside path.
       * @param bounds optional known bounds for the path
       * @return false if the path will not be projected by this projector
       */
    public:
      virtual bool beginInsidePath(bool windCW, const ::terra::Extent* bounds) = 0;

      /**
       * Begin projection of a new path. If path was already begun
       * then finish that path, keep it, and add a new path. 
       * @param bounds optional known bounds for the path
       * @return false if the path will not be projected by this projector
       */
    public:
      virtual bool beginPath(const ::terra::Extent* bounds) = 0;

      /**
       * Move to the specified position and start a new path in the
       * way that the previous path was started.
       * @param lat the start latitude
       * @param lon the start longitude
       * @param alt the start altitude
       * @param bounds optional known bounds for the path being started
       * @return false if the path will not be projected by this projector
       */
    public:
      virtual bool moveTo(double lat, double lon, double alt, const ::terra::Extent* bounds) = 0;
	
      /**
       * Move along a series of arcs from the most recent position to a new position.
       * by the specified delta values.
       * @param n the number of position deltas
       * @param delta delta positions
       * @param type the type of each arc
       * @param bounds the extent of the arcs being sent in this method call or 0
       */
    public:
      virtual void moveAlongArcs (size_t n, const Arc* delta, ArcType type, const ::terra::Extent* bounds) = 0;

      /*@}*/
    };
  }

#endif
