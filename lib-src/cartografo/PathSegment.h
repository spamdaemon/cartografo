#ifndef _CARTOGRAFO_PATHSEGMENT_H
#define _CARTOGRAFO_PATHSEGMENT_H

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

namespace cartografo {
  /**
   * A path segment 
   */
  class PathSegment {
    /** The segment type */
  public:
    enum Type {
      MOVE,      //< PathSegment starts a new path at a new location
      LOXODROME, //< PathSegment is a loxodrome between two points
      MERIDIAN,  //< PathSegment is along a meridian
      GEODESIC,  //< PathSegment is a geodesic between two points
    };
    
    /**
     * Create a default path segment of type MOVE of 0,0
     */
  public:
    PathSegment () throws();
    
    /**
     * Create a default path segment of type MOVE 
     * @param pos the positio of the move
     */
  public:
    PathSegment (const terra::GeodeticCoordinates& pos) throws();
    
    /**
     * Create path segment. The distance travelled will be the smallest possible.
     * @param s the start vertex
     * @param e end vertex
     * @param t the type of this segment
     */
  public:
    PathSegment (const terra::GeodeticCoordinates& s, const terra::GeodeticCoordinates& e, Type t) throws();
    
    /**
     * Create path segment. The value of @code s.latitude()+dLat @endcode will
     * be clamped into the range (-M_PI/2,M_PI/2).
     * @param s the segment's start point
     * @param dLat the latitude difference
     * @param dLon the longitude difference
     * @param t the type of this segment
     */
  public:
    PathSegment (const terra::GeodeticCoordinates& s, double dLat, double dLon, Type t) throws();

    /**
     * Create path segment.  The value of @code s.latitude()+dLat @endcode will
     * be clamped into the range (-M_PI/2,M_PI/2).
     * @param s the segment's start point
     * @param dLat the latitude difference
     * @param dLon the longitude difference
     * @param dHeight the height difference
     * @param t the type of this segment
     * @pre REQUIRE_RANGE(s.latitude()+dLat,-M_PI/2,M_PI/2)
     */
  public:
    PathSegment (const terra::GeodeticCoordinates& s, double dLat, double dLon, double dHeight, Type t) throws();

    /**
     * Get the segment type 
     * @return the type of segment
     */
  public:
    inline Type type() const throws()
    { return _type; }

    /**
     * Get the point at which this segment terminates.
     * @return the point at which this segment terminates
     */
  public:
    inline const terra::GeodeticCoordinates& endPoint() const throws()
    { return _point; }

    /**
     * Get the number of degrees latitude that were travelled from
     * the start point to point()
     * @return the latitude difference between the start point and point()
     */
  public:
    inline double deltaLatitude() const throws() { return _deltaLat; }
      
    /**
     * Get the number of degrees longitude that were travelled from
     * the start point to point()
     * @return the longitude difference between the start point and point()
     */
  public:
    inline double deltaLongitude() const throws() { return _deltaLon; }

    /**
     * Get the height difference between the start point and point()
     * @return the height difference between the start point and point()
     */
  public:
    inline double deltaHeight() const throws() { return _deltaHeight; }

    /**
     * Compare two path segments.
     * @param s a segment
     * @return true if this segment and s contain exactly the same points
     */
  public:
    bool equals (const PathSegment& s) const throws();

    /** The start vertex */
  private:
    terra::GeodeticCoordinates _point;

    /** The delta latitude */
  private:
    double _deltaLat;

    /** The delta longitude */
  private:
    double _deltaLon;

    /** The height delta */
  private:
    double _deltaHeight;

    /** The type */
  private:
    Type _type;

  };

  /**
   * Equality operator. Using PathSegment::equals()
   */
  inline bool operator==(const PathSegment& a, const PathSegment& b) throws()
  { return a.equals(b); }

    
  /**
   * Inequality operator. Using PathSegment::equals()
   */
  inline bool operator!=(const PathSegment& a, const PathSegment& b) throws()
  { return !a.equals(b); }

}

#endif
