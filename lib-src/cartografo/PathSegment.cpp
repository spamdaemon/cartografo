#include <cartografo/PathSegment.h>
#include <cmath>

using namespace ::timber;
using namespace ::terra;

namespace cartografo {
  PathSegment::PathSegment () throws()
  : _deltaLat(0),_deltaLon(0),_type(MOVE)
  {}
    
  PathSegment::PathSegment (const GeodeticCoordinates& pos) throws()
  : _point(pos),_deltaLat(0),_deltaLon(0),_deltaHeight(0),_type(MOVE)
  {}

  PathSegment::PathSegment (const GeodeticCoordinates& s, double dLat, double dLon, double dHeight, Type t) throws()
  : _deltaLat((s.latitude()+dLat)-s.latitude()),_deltaLon(dLon),_deltaHeight(dHeight),_type(t)
  {
    _point = GeodeticCoordinates(clampLatitude(s.latitude()+dLat),clampLongitude(s.longitude()+dLon),s.height()+dHeight,s.datum());
  }
    
  PathSegment::PathSegment (const GeodeticCoordinates& s, double dLat, double dLon, Type t) throws()
  : _deltaLat((s.latitude()+dLat)-s.latitude()),_deltaLon(dLon),_deltaHeight(0),_type(t)
  {
    _point = GeodeticCoordinates(clampLatitude(s.latitude()+dLat),clampLongitude(s.longitude()+dLon),s.height(),s.datum());
  }

      
  PathSegment::PathSegment (const GeodeticCoordinates& s, const GeodeticCoordinates& e, Type t) throws()
  : _point(e),_deltaLat(e.latitude()-s.latitude()),_deltaHeight(e.height()-s.height()),_type(t)
  {
    assert(s.datum()->equals(*e.datum()));

    double dLon = e.longitude()-s.longitude();
    if (::std::abs(dLon)> ::terra::PI) {
      if (dLon < 0) {
	dLon += 2.0* ::terra::PI;
      }
      else {
	dLon -= 2.0* ::terra::PI;
      }
    }
    _deltaLon = dLon;
  }
    
  bool PathSegment::equals (const PathSegment& s) const throws()
  { 
    if (_type!=s._type) {
      return false;
    }
    switch (_type) {
    case MOVE:
      return _point.equals(s._point);
    default:
      return _deltaLat==s._deltaLat && _deltaLon==s._deltaLon;
    }
  }
    
}

