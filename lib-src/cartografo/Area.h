#ifndef _CARTOGRAFO_AREA_H
#define _CARTOGRAFO_AREA_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _CARTOGRAFO_REGION_H
#include <cartografo/Region.h>
#endif

namespace cartografo {
  class Path;

  /**
   * An area is a connected region using a single exterior path that bounds the
   * inside of the area. Multiple inside boundaries are used to defined holes inside
   * the area. The order in which vertices of the bounding paths is defined is important.
   * Inside paths must enumerate their vertices oppopsite to that of the external boundary path.
   */
  class Area : public Region {
    Area(const Area&);
    Area&operator=(const Area&);
      
    /**  Default constructor. */
  protected:
    Area () throws();
      
    /** Destructor */
  public:
    ~Area() throws();

    /**
     * Test if the area's outer boundary is specified in 
     * clockwise or counter clockwise order. The order of the
     * vertices of the inside paths is opposite that of the outside
     * path.
     * 
     * @return true if this area is specified in clockwise order
     */
  public:
    virtual bool isCWArea() const throws() = 0;
      
    /**
     * Get the outside path.
     * @return the single outside path
     */
  public:
    virtual ::timber::Reference<Path> outsidePath() const throws() = 0;

    /**
     * Get the number of interior paths.
     * @return the number of paths used to define holes
     */
  public:
    virtual size_t pathCount() const throws() = 0;
      
    /**
     * Get an inside path.
     * @param i the index of an internal path
     * @return get an inside path.
     * @pre i<pathCount()
     */
  public:
    virtual ::timber::Reference<Path> insidePath(size_t i) const throws() = 0;
  };
}

#endif
