#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTIONDOMAIN_H
#define _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTIONDOMAIN_H

#ifndef _CARTOGRAFO_PROJECTIONDOMAIN_H
#include <cartografo/ProjectionDomain.h>
#endif

namespace cartografo {
  namespace projection {
    
    /**
     * The base class for all projections.
     */
    class AbstractProjectionDomain : public ProjectionDomain {
      /** No copy methods allowed */
      AbstractProjectionDomain(const AbstractProjectionDomain&);
      AbstractProjectionDomain&operator=(const AbstractProjectionDomain&);
      
      /**
       * Default Constructor 
       */
    protected:
      AbstractProjectionDomain() throws();
      
      /** Destroy this projection */
    public:
      ~AbstractProjectionDomain() throws();
    };
  }
}
#endif
