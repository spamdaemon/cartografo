#include <cartografo/projection/AzimuthalProjection.h>
#include <cartografo/projection/SimpleProjectionDomain.h>
#include <cartografo/AreaBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/Area.h>
#include <cartografo/Region.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionRequest.h>

#include <terra/terra.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Bounds.h>

#include <tikal/Point2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Region2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/Path2DBuilder.h>
#include <timber/logging.h>

#include <cmath>
#include <vector>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;
using namespace ::terra;

namespace cartografo {
   namespace projection {

      namespace {
         static Log logger()
         {
            return "cartografo.projection.AzimuthalProjection";
         }

         static void expectSpheroid(const GeodeticCoordinates& x) throws(UnsupportedDatum)
         {
            if (x.datum()->spheroid().flattening() != 0.0) {
               logger().info("Expected a spheroid, but got an ellipsoid instead");
               throw ::terra::UnsupportedDatum(x.datum());
            }
         }

         // the shared projection parameters
         struct ProjectionParameters
         {
               ProjectionParameters(const GeodeticCoordinates& x, double angleRadius) :
                  _center(x), _radius(x.meanRadius()), _lat(x.latitude()), _lon(x.longitude()),
                        _cosLat(::std::cos(_lat)), _sinLat(::std::sin(_lat)), _cosLon(::std::cos(_lon)),
                        _sinLon(::std::sin(_lon)),
                        _domain(new SimpleProjectionDomain(x, AreaBuilder::createRadiansCircle(x, angleRadius, 180)))
               {
                  LogEntry(logger()).debugging() << "New Azimuthal projection : " << x << "  r=" << toDegrees(
                        angleRadius) << doLog;
               }

               ProjectionParameters(const GeodeticCoordinates& x) :
                  _center(x), _radius(x.meanRadius()), _lat(x.latitude()), _lon(x.longitude()),
                        _cosLat(::std::cos(_lat)), _sinLat(::std::sin(_lat)), _cosLon(::std::cos(_lon)),
                        _sinLon(::std::sin(_lon)),
                        _domain(new SimpleProjectionDomain(x, AreaBuilder::createHorizonArea(x)))
               {
                  LogEntry(logger()).debugging() << "New Azimuthal projection : horizon centered on " << x << doLog;
               }

               const GeodeticCoordinates _center;
               const double _radius;
               const double _lat, _lon;
               const double _cosLat, _sinLat;
               const double _cosLon, _sinLon;
               const Reference< ProjectionDomain> _domain;
         };

         struct TiltedPerspectiveProjector
         {
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  return false;
               }
         };

         struct VerticalPerspectiveProjector
         {
               VerticalPerspectiveProjector(const GeodeticCoordinates& x) :
                  _alt(x.height() / x.meanRadius() + 1)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  double cosLat = ::std::cos(r.lat);
                  double sinLat = ::std::sin(r.lat);
                  double cosLon = ::std::cos(r.lon - p._lon);
                  double sinLon = ::std::sin(r.lon - p._lon);
                  double cos_c = p._sinLat * sinLat + p._cosLat * (cosLat * cosLon);
                  if (cos_c <= 0) {
                     r.map.setNoMapping();
                     return false;
                  }
                  if (cos_c > 1) {
                     cos_c = 1;
                  }
                  double k = 0;
                  if ((_alt - cos_c) == 0.0) {
                     r.map.setNoMapping();
                     return false;
                  }
                  else {
                     k = (_alt - 1) / (_alt - cos_c);
                  }
                  double rk = p._radius * k;
                  double x = rk * cosLat * sinLon;
                  double y = rk * (p._cosLat * sinLat - p._sinLat * cosLat * cosLon);

                  r.map.setPointMapping(Point2D(x, y));
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  const double phi = ::std::sqrt(pt.x() * pt.x() + pt.y() * pt.y());
                  double c_tmp1 = p._radius * (_alt - 1);
                  double c_tmp2 = 1 - phi * phi * (_alt + 1) / (p._radius * c_tmp1);
                  c_tmp2 = ::std::max(0., c_tmp2);
                  double c_tmp3 = (_alt - ::std::sqrt(c_tmp2)) / (c_tmp1 / phi + phi / c_tmp1);
                  c_tmp3 = ::std::min(c_tmp3, ::std::max(c_tmp3, -1.));
                  double c = ::std::asin(c_tmp3);
                  if (_alt < 0 && phi > c_tmp1 / _alt) {
                     c = ::terra::PI - c;
                  }

                  const double cos_c = ::std::cos(c);
                  const double sin_c = ::std::sin(c);
                  if (cos_c < 0) {
                     return false;
                  }
                  double tmp = cos_c * p._sinLat + (pt.y() * sin_c * p._cosLat / phi);
                  if (tmp < -1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  tmp = phi * p._cosLat * cos_c - pt.y() * p._sinLat * sin_c;
                  if (::std::abs(tmp) < 1E-10) {
                     return false;
                  }
                  //	  double lon = ::std::atan(pt.x()*sin_c/tmp);
                  double lon = ::std::atan2(pt.x() * sin_c, tmp);
                  coords[0] = lat;
                  coords[1] = clampLongitude(lon + p._lon);
                  return !::std::isnan(coords[0]) && !::std::isnan(coords[1]);
               }

            private:
               double _alt;
         };

         struct GnomonicProjector
         {
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  double cosLat = ::std::cos(r.lat);
                  double sinLat = ::std::sin(r.lat);
                  double cosLon = ::std::cos(r.lon - p._lon);
                  double sinLon = ::std::sin(r.lon - p._lon);
                  double cos_c = p._sinLat * sinLat + p._cosLat * (cosLat * cosLon);
                  if (cos_c <= 0 || cos_c > 1) {
                     r.map.setNoMapping();
                     return false;
                  }
                  if (cos_c > 1) {
                     cos_c = 1;
                  }
                  double k = 1.0 / cos_c;

                  double rk = p._radius * k;
                  double x = rk * cosLat * sinLon;
                  double y = rk * (p._cosLat * sinLat - p._sinLat * cosLat * cosLon);

                  r.map.setPointMapping(Point2D(x, y));
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  const double phi = ::std::sqrt(pt.x() * pt.x() + pt.y() * pt.y());
                  const double c = ::std::atan(phi / p._radius);
                  const double cos_c = ::std::cos(c);
                  const double sin_c = ::std::sin(c);
                  if (cos_c < 0) {
                     return false;
                  }
                  double tmp = cos_c * p._sinLat + (pt.y() * sin_c * p._cosLat / phi);
                  if (tmp < -1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  tmp = phi * p._cosLat * cos_c - pt.y() * p._sinLat * sin_c;
                  if (::std::abs(tmp) < 1E-10) {
                     return false;
                  }
                  double lon = ::std::atan2(pt.x() * sin_c, tmp);
                  coords[0] = lat;
                  coords[1] = clampLongitude(lon + p._lon);
                  return !::std::isnan(coords[0]) && !::std::isnan(coords[1]);
               }
         };

         struct StereographicProjector
         {
               StereographicProjector(double k0) :
                  _k0(k0)
               {
               }

               StereographicProjector() :
                  _k0(1)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  double cosLat = ::std::cos(r.lat);
                  double sinLat = ::std::sin(r.lat);
                  double cosLon = ::std::cos(r.lon - p._lon);
                  double sinLon = ::std::sin(r.lon - p._lon);

                  double cos_c = p._sinLat * sinLat + p._cosLat * (cosLat * cosLon);
                  if (cos_c < 0) {
                     r.map.setNoMapping();
                     return false;
                  }

                  double k = 2 * _k0 / (1.0 + p._sinLat * sinLat + p._cosLat * cosLat * cosLon);
                  double rk = p._radius * k;
                  double x = rk * cosLat * sinLon;
                  double y = rk * (p._cosLat * sinLat - p._sinLat * cosLat * cosLon);

                  r.map.setPointMapping(Point2D(x, y));
                  return true;
               }
               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  const double phi = ::std::sqrt(pt.x() * pt.x() + pt.y() * pt.y());
                  const double c = 2 * ::std::atan(phi / (2 * p._radius * _k0));
                  const double cos_c = ::std::cos(c);
                  const double sin_c = ::std::sin(c);
                  if (cos_c < 0) {
                     return false;
                  }
                  double tmp = cos_c * p._sinLat + (pt.y() * sin_c * p._cosLat / phi);
                  if (tmp < -1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  tmp = phi * p._cosLat * cos_c - pt.y() * p._sinLat * sin_c;
                  if (::std::abs(tmp) < 1E-10) {
                     return false;
                  }
                  double lon = ::std::atan2(pt.x() * sin_c, tmp);
                  coords[0] = lat;
                  coords[1] = clampLongitude(lon + p._lon);
                  return true;
               }

            private:
               double _k0;
         };

         struct LambertEqualAreaProjector
         {
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  double cosLat = ::std::cos(r.lat);
                  double sinLat = ::std::sin(r.lat);
                  double cosLon = ::std::cos(r.lon - p._lon);
                  double sinLon = ::std::sin(r.lon - p._lon);

                  double cos_c = p._sinLat * sinLat + p._cosLat * (cosLat * cosLon);
                  if (cos_c < 0) {
                     r.map.setNoMapping();
                     return false;
                  }

                  double k = ::std::sqrt(2 / (1.0 + p._sinLat * sinLat + p._cosLat * cosLat * cosLon));
                  double rk = p._radius * k;
                  double x = rk * cosLat * sinLon;
                  double y = rk * (p._cosLat * sinLat - p._sinLat * cosLat * cosLon);

                  r.map.setPointMapping(Point2D(x, y));
                  return true;
               }
               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  const double phi = ::std::sqrt(pt.x() * pt.x() + pt.y() * pt.y());
                  double tmp = phi / (2 * p._radius);
                  if (tmp > 1) {
                     return false;
                  }
                  const double c = 2 * ::std::asin(tmp);
                  const double cos_c = ::std::cos(c);
                  const double sin_c = ::std::sin(c);
                  if (cos_c < 0) {
                     return false;
                  }
                  tmp = cos_c * p._sinLat + (pt.y() * sin_c * p._cosLat / phi);
                  if (tmp < -1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  tmp = phi * p._cosLat * cos_c - pt.y() * p._sinLat * sin_c;
                  if (::std::abs(tmp) < 1E-10) {
                     return false;
                  }
                  double lon = ::std::atan2(pt.x() * sin_c, tmp);
                  coords[0] = lat;
                  coords[1] = clampLongitude(lon + p._lon);
                  return !::std::isnan(coords[0]) && !::std::isnan(coords[1]);
               }
         };

         struct EquidistantProjector
         {
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  double cosLat = ::std::cos(r.lat);
                  double sinLat = ::std::sin(r.lat);
                  double cosLon = ::std::cos(r.lon - p._lon);
                  double sinLon = ::std::sin(r.lon - p._lon);

                  double cos_c = p._sinLat * sinLat + p._cosLat * (cosLat * cosLon);
                  if (cos_c <= -.999) {
                     r.map.setNoMapping();
                     return false;
                  }
                  if (cos_c > 1) {
                     cos_c = 1;
                  }
                  const double c = ::std::acos(cos_c);
                  const double sin_c = ::std::sin(c);
                  double k = (sin_c == 0) ? 1 : (c / sin_c);
                  double rk = p._radius * k;
                  double x = rk * cosLat * sinLon;
                  double y = rk * (p._cosLat * sinLat - p._sinLat * cosLat * cosLon);

                  r.map.setPointMapping(Point2D(x, y));
                  return true;
               }
               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  const double phi = ::std::sqrt(pt.x() * pt.x() + pt.y() * pt.y());
                  double tmp = phi / (2 * p._radius);
                  if (tmp > 1) {
                     return false;
                  }
                  const double c = 2 * ::std::asin(tmp);
                  const double cos_c = ::std::cos(c);
                  const double sin_c = ::std::sin(c);
                  if (cos_c <= -1) {
                     return false;
                  }
                  tmp = cos_c * p._sinLat + (pt.y() * sin_c * p._cosLat / phi);
                  if (tmp < -1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  tmp = phi * p._cosLat * cos_c - pt.y() * p._sinLat * sin_c;
                  if (::std::abs(tmp) < 1E-10) {
                     return false;
                  }
                  double lon = ::std::atan2(pt.x() * sin_c, tmp);
                  coords[0] = lat;
                  coords[1] = clampLongitude(lon + p._lon);
                  return !::std::isnan(coords[0]) && !::std::isnan(coords[1]);
               }
         };

         struct OrthographicProjector
         {
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& p) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,p);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }
               bool forward(ProjectionRequest& r, const ProjectionParameters& p) const
               {
                  double rcosLat = p._radius * ::std::cos(r.lat);
                  double rsinLat = p._radius * ::std::sin(r.lat);
                  double cosLon = ::std::cos(r.lon - p._lon);
                  double sinLon = ::std::sin(r.lon - p._lon);

                  double rcos_c = p._sinLat * rsinLat + p._cosLat * (rcosLat * cosLon);
                  if (rcos_c < 0) {
                     r.map.setNoMapping();
                     return false;
                  }
                  double x = rcosLat * sinLon;
                  double y = rsinLat * p._cosLat - p._sinLat * (rcosLat * cosLon);

                  r.map.setPointMapping(Point2D(x, y));
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& p, LatLon& coords) const
               {
                  const double phi = ::std::sqrt(pt.x() * pt.x() + pt.y() * pt.y());
                  const double sin_c = phi / p._radius;
                  if (sin_c > 1.0) {
                     return false;
                  }
                  const double c = ::std::asin(sin_c);
                  double cos_c = ::std::cos(c);

                  double lat = ::std::asin(cos_c * p._sinLat + pt.y() * sin_c * p._cosLat / phi);
                  double lon = ::std::atan2(pt.x() * sin_c, (phi * p._cosLat * cos_c - pt.y() * p._sinLat * sin_c));
                  lon = clampLongitude(lon + p._lon);
                  coords[0] = lat;
                  coords[1] = lon;
                  return !::std::isnan(coords[0]) && !::std::isnan(coords[1]);
               }
         };

         template<class PF>
         struct BaseAzimuthalProjection : public AzimuthalProjection
         {

               // base azimuthal using the horizon
               BaseAzimuthalProjection(const PF& pf, const GeodeticCoordinates& x)throws()
               : _projector(pf),_parameters(x)
               {
               }

               BaseAzimuthalProjection(const PF& pf, const GeodeticCoordinates& x, double angleRadius)throws()
               : _projector(pf),_parameters(x,angleRadius)
               {
               }

               BaseAzimuthalProjection(const GeodeticCoordinates& x, double angleRadius)throws()
               : _projector(PF()),_parameters(x,angleRadius)
               {
               }

               ~BaseAzimuthalProjection()throws()
               {
               }

               Reference< ProjectionDomain> domain() const throws()
               {  return _parameters._domain;}

               inline bool projectForward(ProjectionRequest& req) const throws()
               {  return _projector.forward(req,_parameters);}

               size_t projectUnsafe(size_t n, Point2D* lonlat) const throws()
               {
                  const Point2D* start = lonlat;
                  const Point2D* end = lonlat + n;
                  Point2D* out = lonlat;
                  while(lonlat<end) {
                     _projector.forwardUnsafe(*lonlat,out,_parameters);
                     ++lonlat;
                  }
                  return out - start;
               }

               size_t project(size_t n, ProjectionRequest* req) const throws()
               {
                  size_t i=0;
                  while (i<n) {
                     projectForward(req[i]);
                     if (req[i].map.type()==ProjectionMapping::NONE) {
                        break;
                     }
                     ++i;
                  }
                  return i;
               }

               GeodeticCoordinates projectInverse(double x, double y) const throws(std::exception)
               {
                  LatLon inv;
                  const Point2D pt(x, y);
                  if (_projector.inverse(pt, _parameters, inv)) {
                     return GeodeticCoordinates(inv[0], inv[1], center().datum());
                  }
                  throw ::std::runtime_error("Could not create inverse projection");
               }

               bool isInverseProjectionEnabled() const throw()
               {
                  return true;
               }

               inline const ProjectionParameters& parameters() const throws()
               {  return _parameters;}

               inline GeodeticCoordinates center() const throws()
               {  return _parameters._center;}

            protected:
               const PF _projector;
               const ProjectionParameters _parameters;
         };
      }

      AzimuthalProjection::AzimuthalProjection()
      throws()
      {}

      AzimuthalProjection::~AzimuthalProjection()
      throws()
      {}

      Reference< Projection> AzimuthalProjection::createGnomonicProjection(const GeodeticCoordinates& x)
throws   (::terra::UnsupportedDatum)
   {
      expectSpheroid(x);
      return new BaseAzimuthalProjection<GnomonicProjector>(x,::terra::PI/2-::terra::PI/1024);
   }

   Reference< Projection> AzimuthalProjection::createStereographicProjection(const GeodeticCoordinates& x)
   throws(::terra::UnsupportedDatum)
   {
      expectSpheroid(x);
      return new BaseAzimuthalProjection<StereographicProjector>(x,::terra::PI/2-::terra::PI/1024);
   }

Reference< Projection> AzimuthalProjection::createLambertEqualAreaProjection(const GeodeticCoordinates& x)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(x);
   return new BaseAzimuthalProjection<LambertEqualAreaProjector>(x,::terra::PI/2-::terra::PI/1024);
}

Reference< Projection> AzimuthalProjection::createEquidistantProjection(const GeodeticCoordinates& x)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(x);
   return new BaseAzimuthalProjection<EquidistantProjector>(x,::terra::PI/2-::terra::PI/1024);
}

Reference< Projection> AzimuthalProjection::createOrthographicProjection(const GeodeticCoordinates& x)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(x);
   return new BaseAzimuthalProjection<OrthographicProjector>(x,::terra::PI/2-::terra::PI/1024);
}

Reference< Projection> AzimuthalProjection::createVerticalPerspectiveProjection(const GeodeticCoordinates& x,
      double dist)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(x);

   GeodeticCoordinates pos = GeodeticCoordinates(x.latitude(), x.longitude(), x.height() + dist, x.datum());
   VerticalPerspectiveProjector pf(pos);
   return new BaseAzimuthalProjection<VerticalPerspectiveProjector>(pf,pos);
}

Reference< Projection> AzimuthalProjection::createTiltedPerspectiveProjection(const GeodeticCoordinates& x)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(x);
   return new BaseAzimuthalProjection<TiltedPerspectiveProjector>(x,::terra::PI/2-::terra::PI/1024);
}

}
}
