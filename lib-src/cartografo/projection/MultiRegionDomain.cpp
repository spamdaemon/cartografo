#include <cartografo/projection/MultiRegionDomain.h>
#include <cartografo/Area.h>
#include <cartografo/Region.h>
#include <cartografo/AreaBuilder.h>

#include <terra/Bounds.h>

using namespace ::timber;
using namespace ::terra;

namespace cartografo {
    namespace projection {

      MultiRegionDomain::MultiRegionDomain(const GeodeticCoordinates& ctr, const ::std::vector<Reference<Region> >& regs) throws()
      : _center(ctr),_region(Region::createRegion(regs))
      {	
      }
      
      MultiRegionDomain::~MultiRegionDomain() throws()
      {}

      Reference<Region> MultiRegionDomain::region() const throws()
      {
	return _region;
      }

      GeodeticCoordinates MultiRegionDomain::center() const throws()
      { return _center; }

    }
  }
