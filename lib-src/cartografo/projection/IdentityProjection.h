#ifndef _CARTOGRAFO_PROJECTION_IDENTITYPROJECTION_H
#define _CARTOGRAFO_PROJECTION_IDENTITYPROJECTION_H

#ifndef _CARTOGRAFO_PROJECTION_H
#include <cartografo/Projection.h>
#endif

namespace terra {
  class Bounds;
  class GeodeticCoordinates;
}

namespace cartografo {
  namespace projection {

    /**
     * The identity projection maps a latitude and longitude in radians into
     * their equivalents in degrees centered around a given point.
     */
    class IdentityProjection {
      IdentityProjection(const IdentityProjection&);
      IdentityProjection&operator=(const IdentityProjection&);

      /**
       * Create an identity projection 
       * @param center the projection center
       */
    public:
      static ::timber::Reference<Projection> create(const ::terra::GeodeticCoordinates& center) throws();

      /**
       * Create an identity projection 
       * @param center the projection center
       * @param bounds the bounds of the projection
       */
    public:
      static ::timber::Reference<Projection> create(const ::terra::GeodeticCoordinates& center, const ::terra::Bounds& b) throws();

    };
  }
}

#endif
