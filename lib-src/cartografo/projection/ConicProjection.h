#ifndef _CARTOGRAFO_PROJECTION_CONICPROJECTION_H
#define _CARTOGRAFO_PROJECTION_CONICPROJECTION_H

#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTION_H
#include <cartografo/projection/AbstractProjection.h>
#endif

#ifndef _TERRA_UNSUPPORTEDDATUM_H
#include <terra/UnsupportedDatum.h>
#endif

namespace terra {
   class Bounds;
   class GeodeticCoordinates;
}

namespace cartografo {
   namespace projection {

      /**
       * The base class for all conic projetions.
       */
      class ConicProjection : public AbstractProjection
      {
            ConicProjection(const ConicProjection&);
            ConicProjection&operator=(const ConicProjection&);

            /**
             * Create a new conic projection.
             */
         protected:
            ConicProjection()throws();

            /** Destroy this projection. */
         public:
            ~ConicProjection()throws();

            /**
             * @name Required implementations
             * @{
             */

            /*@}*/

            /**
             * @name Basic conic projections.
             * @{
             */

            /**
             * The Polyconic Projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @param bounds the region to be projected
             * @return a polyconic projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createPolyConicProjection(const ::terra::GeodeticCoordinates& pot,
                  const ::terra::Bounds& b)
            throws( ::terra::UnsupportedDatum);

            /**
             * The Bipolar Oblique Projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @param bounds the region to be projected
             * @param lat1 the first standard latitude
             * @param lat2 the second standard latitude
             * @return  a bipolar projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createBipolarObliqueProjection(
                  const ::terra::GeodeticCoordinates& pot, const ::terra::Bounds& b, double lat1, double lat2)
            throws( ::terra::UnsupportedDatum);

            /**
             * The Albers Equal Area Projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored. The two standard
             * parallell lat1 and lat2 must not be equidistant from the equator.
             * @param pot the point of tangency
             * @param bounds the region to be projected
             * @param lat1 the first standard latitude
             * @param lat2 the second standard latitude
             * @pre REQUIRE_FALSE(lat1==-lat2)
             * @return  an equal area projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createAlbersEqualAreaProjection(
                  const ::terra::GeodeticCoordinates& pot, const ::terra::Bounds& b, double lat1, double lat2)
            throws( ::terra::UnsupportedDatum);

            /**
             * The Lambert Conformal Projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @param bounds the region to be projected
             * @param lat1 the first standard latitude
             * @param lat2 the second standard latitude
             * @return  an lambert conformal projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createLambertConformalProjection(
                  const ::terra::GeodeticCoordinates& pot, const ::terra::Bounds& b, double lat1, double lat2)
            throws( ::terra::UnsupportedDatum);

            /**
             * The equidistant projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @param bounds the region to be projected
             * @param lat1 the first standard latitude
             * @param lat2 the second standard latitude
             * REQUIRE_TRUE((lat1==lat2 && ::std::sin(lat1)!=0) || (::std::cos(lat1)-::std::cos(lat2))!=0.0);
             * @return  an equidistant equivalent projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createEquidistantProjection(
                  const ::terra::GeodeticCoordinates& pot, const ::terra::Bounds& b, double lat1, double lat2)
            throws( ::terra::UnsupportedDatum);

            /**
             * The Bonne projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @param bounds the region to be projected
             * @return  an bonne projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createBonneProjection(const ::terra::GeodeticCoordinates& pot,
                  const ::terra::Bounds& b)
            throws( ::terra::UnsupportedDatum);

            /*@}*/

      };
   }
}
#endif
