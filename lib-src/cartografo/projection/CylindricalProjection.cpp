#include <cartografo/projection/CylindricalProjection.h>
#include <cartografo/projection/SimpleProjectionDomain.h>
#include <cartografo/projection/MultiRegionDomain.h>
#include <cartografo/AreaBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/Area.h>
#include <cartografo/Region.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionRequest.h>

#include <timber/logging.h>

#include <terra/GeodeticDatum.h>
#include <terra/terra.h>
#include <terra/Bounds.h>
#include <terra/Geodesic.h>

#include <tikal/Point2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Region2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/Path2DBuilder.h>

#include <cmath>
#include <cerrno>
#include <vector>
#include <iostream>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;
using namespace ::terra;

namespace cartografo {
   namespace projection {

      namespace {
         static Log logger()
         {
            return Log("cartografo.projection.CylindricalProjection");
         }

         static void expectSpheroid(const Reference< GeodeticDatum>& x) throws(UnsupportedDatum)
         {
            if (x->spheroid().flattening() != 0.0) {
               logger().info("Expected a spheroid, but got an ellipsoid instead");
               throw ::terra::UnsupportedDatum(x);
            }
         }

         inline double clampAsin(double x)
         throws()
         {
            return x < -1 ? -1 : (x > 1 ? 1 : x);
         }

         static Reference< ProjectionDomain> createMultiDomain(const ::terra::GeodeticCoordinates& pot,
               const ::std::vector< ::terra::Bounds>& bnds, double dLat, double dLon)
         {
            ::std::vector< Reference< Region> > domainAreas;
            for (size_t i = 0; i < bnds.size(); ++i) {
               Reference< Area> r = AreaBuilder::createBoundsArea(bnds[i], dLat, dLon);
               domainAreas.push_back(r);
            }

            return new MultiRegionDomain(pot, domainAreas);
         }

         // the shared projection parameters
         struct ProjectionParameters
         {
               ProjectionParameters(const GeodeticCoordinates& x, const Bounds& b)
                     : _center(x), _radius(x.meanRadius()), _lat(x.latitude()), _lon(x.longitude()), _cosLat(
                           ::std::cos(_lat)), _sinLat(::std::sin(_lat)), _cosLon(::std::cos(_lon)), _sinLon(
                           ::std::sin(_lon)), _domain(new SimpleProjectionDomain(x, b)), _spheroid(
                           x.datum()->spheroid())
               {
                  LogEntry("cartografo.projection.CylindricalProjection").info() << "Projection parameters extent : "
                        << Extent(b) << doLog;

               }

               ProjectionParameters(const GeodeticCoordinates& x, const Reference< Region>& b)
                     : _center(x), _radius(x.meanRadius()), _lat(x.latitude()), _lon(x.longitude()), _cosLat(
                           ::std::cos(_lat)), _sinLat(::std::sin(_lat)), _cosLon(::std::cos(_lon)), _sinLon(
                           ::std::sin(_lon)), _domain(new SimpleProjectionDomain(x, b)), _spheroid(
                           x.datum()->spheroid())
               {
               }

               ProjectionParameters(const GeodeticCoordinates& x, Reference< ProjectionDomain> b)
                     : _center(x), _radius(x.meanRadius()), _lat(x.latitude()), _lon(x.longitude()), _cosLat(
                           ::std::cos(_lat)), _sinLat(::std::sin(_lat)), _cosLon(::std::cos(_lon)), _sinLon(
                           ::std::sin(_lon)), _domain(b), _spheroid(x.datum()->spheroid())
               {
               }

               const GeodeticCoordinates _center;
               const double _radius;
               const double _lat, _lon;
               const double _cosLat, _sinLat;
               const double _cosLon, _sinLon;
               Reference< ProjectionDomain> _domain;
               const Spheroid& _spheroid;
         };

         struct MercatorEllipsoidProjection
         {
               MercatorEllipsoidProjection(const ProjectionParameters&)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double x, y;
                  double dLon = r.lon - pp._lon;
                  if (::std::abs(dLon > ::terra::PI)) {
                     r.map.setNoMapping();
                     return false;
                  }
                  pp._spheroid.computeMercatorProjection(pp._lon, r.lat, r.lon, x, y);
                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double lat, lon;
                  pp._spheroid.computeInverseMercatorProjection(pp._lon, pt.x(), pt.y(), lat, lon);
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }
         };

         struct MercatorSpheroidProjection
         {
               MercatorSpheroidProjection(const ProjectionParameters&)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double dLon = r.lon - pp._lon;
                  if (::std::abs(dLon > ::terra::PI)) {
                     r.map.setNoMapping();
                     return false;
                  }
                  double x = pp._radius * dLon;
                  double y = pp._radius * ::std::log(::std::tan((::terra::PI + 2 * r.lat) / 4.0));
                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double lat = ::terra::PI / 2 - 2 * ::std::atan(::std::exp(-pt.y() / pp._radius));
                  double lon = pt.x() / pp._radius + pp._lon;

                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }
         };

         struct EquidistantEllipsoidProjection
         {
               EquidistantEllipsoidProjection(const ProjectionParameters& pp, double latS)
                     : _cosLatS(::std::cos(latS))
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
            private:
               const double _cosLatS;
         };

         struct EquidistantSpheroidProjection
         {
               EquidistantSpheroidProjection(const ProjectionParameters& pp, double latS)
                     : _cosLatS(::std::cos(latS))
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double dLon = r.lon - pp._lon;
                  if (::std::abs(dLon > ::terra::PI)) {
                     r.map.setNoMapping();
                     return false;
                  }
                  double x = pp._radius * dLon * _cosLatS;
                  double y = pp._radius * r.lat;
                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double lat = pt.y() / pp._radius;
                  if (lat < -::terra::PI / 2 || lat > ::terra::PI / 2) {
                     return false;
                  }
                  double lon = pp._lon + pt.x() / (pp._radius * _cosLatS);
                  if (lon < -::terra::PI || lon > ::terra::PI) {
                     return false;
                  }
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            private:
               const double _cosLatS;
         };

         struct TransverseMercatorEllipsoidProjection
         {
               TransverseMercatorEllipsoidProjection(const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct ObliqueMercatorEllipsoidProjection
         {
               ObliqueMercatorEllipsoidProjection(const GeodeticCoordinates& ctr, double az)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct ObliqueMercatorSpheroidProjection
         {
               ObliqueMercatorSpheroidProjection(const GeodeticCoordinates& ctr, double az)
                     : _k0(1), _azimuth(clampLatitude(az))
               {
                  double sinAz = ::std::sin(_azimuth);
                  double cosAz = ::std::cos(_azimuth);

                  if (::std::abs(cosAz) == 1) {
                     sinAz = 0;
                  }
                  else if (::std::abs(sinAz) == 1) {
                     cosAz = 0;
                  }

                  double sinLatPole = clampAsin(::std::cos(ctr.latitude()) * sinAz);
                  _latPole = ::std::asin(sinLatPole);
                  const double tmp = -::std::sin(ctr.latitude()) * sinAz;

                  // if we have atan2(0,0), then we can choose any convenient pole
                  bool anyPole = cosAz == 0 && tmp == 0;

                  _lonPole = ::std::atan2(-cosAz, tmp);
                  _lonPole = clampLongitude(_lonPole + ctr.longitude());

                  // make sure to use the northern pole
                  // @todo I wonder if _latPole should have the same sign at ctr.latitude()?
                  if (_latPole < 0) {
                     _latPole = -_latPole;
                     _lonPole = clampLongitude(_lonPole + ::terra::PI);
                  }
                  if (sinAz < 0) {
                     _lonPole = clampLongitude(_lonPole + ::terra::PI);
                  }
                  if (cosAz < 0) {
                     _lonPole = clampLongitude(_lonPole + ::terra::PI);
                  }

                  _cosLatPole = ::std::cos(_latPole);
                  _sinLatPole = ::std::sin(_latPole);

                  if (::std::abs(_cosLatPole) == 1) {
                     _sinLatPole = 0;
                  }
                  else if (::std::abs(_sinLatPole) == 1) {
                     _cosLatPole = 0;
                  }
                  if (anyPole) {
                     ::std::cerr << "Using any pole" << ::std::endl;
                     _lonPole = clampLongitude(ctr.longitude() - ::terra::PI / 2);
                  }
                  _lon0 = clampLongitude(_lonPole + ::terra::PI / 2);
                  _center = GeodeticCoordinates(0.0, _lon0, 0, ctr.datum());
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  else if (r.map.type() == ProjectionMapping::NONE) {
                     *(ptOut++) = Point2D(100000000000000.0, 1000000000.0);
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  const double R = pp._radius * _k0;
                  const double dLon = r.lon - _lon0;
                  double sinDLon = ::std::sin(dLon);
                  double cosDLon = ::std::cos(dLon);
                  double cosLat = ::std::cos(r.lat);
                  double sinLat = ::std::sin(r.lat);

                  if (::std::abs(cosDLon) == 1) {
                     sinDLon = 0;
                  }
                  else if (::std::abs(sinDLon) == 1) {
                     cosDLon = 0;
                  }

                  if (::std::abs(cosLat) == 1) {
                     sinLat = 0;
                  }
                  else if (::std::abs(sinLat) == 1) {
                     cosLat = 0;
                  }

                  const double A = _sinLatPole * sinLat - _cosLatPole * cosLat * sinDLon;

                  if (::std::abs(A) == 1) {
                     r.map.setNoMapping();
                     return false;
                  }

                  // instead of using tan(lat) in the numerator, we use sin/cos instead.
                  double top = sinLat * _cosLatPole + cosLat * _sinLatPole * sinDLon;
                  double bot = cosDLon * cosLat;

                  double x = R * ::std::atan2(top, bot);
                  double y = 0.5 * R * ::std::log((1 + A) / (1 - A));
                  // we're reversing the order from Snyder's order
                  r.map.setPointMapping(-y, x);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double R = _k0 * pp._radius;
                  double x = pt.y() / R; // Snyder reverses X and Y
                  double y = -pt.x() / R;

                  double cosh_y = ::std::cosh(y);
                  if (cosh_y == 0) {
                     return false;
                  }
                  double sinLat = _sinLatPole * ::std::tanh(y) + _cosLatPole * ::std::sin(x) / cosh_y;
                  double lat = ::std::asin(clampAsin(sinLat));
                  double lon = _lon0
                        + ::std::atan2(_sinLatPole * ::std::sin(x) - _cosLatPole * ::std::sinh(y), ::std::cos(x));

                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            public:
               const GeodeticCoordinates center() const throws()
               {
                  return _center;
               }

            public:
               void poles(LatLon& p1, LatLon& p2) const throws()
               {
                  p1[0] = _latPole;
                  p1[1] = _lonPole;
                  p2[0] = -_latPole;
                  p2[1] = clampLongitude(_lonPole - ::terra::PI);
               }

            public:
               double azimuth() const throws()
               {
                  return _azimuth;
               }

            private:
               double _k0;
               double _azimuth;
               double _cosLatPole;
               double _sinLatPole;
               double _latPole;
               double _lonPole;
               double _lon0;
               GeodeticCoordinates _center;
         };

         struct TransverseMercatorSpheroidProjection
         {
               TransverseMercatorSpheroidProjection(const ProjectionParameters& pp)
                     : _k0(1)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  const double dLon = r.lon - pp._lon;
                  const double sinDLon = ::std::sin(dLon);
                  const double cosDLon = ::std::cos(dLon);
                  const double cosLat = ::std::cos(r.lat);
                  const double B = clampAsin(cosLat * sinDLon);
                  if (B == 1) {
                     r.map.setNoMapping();
                     return false;
                  }

                  double x = 0.5 * (pp._radius * _k0) * ::std::log((1 + B) / (1 - B));
                  double y = (pp._radius * _k0) * (::std::atan2(::std::sin(r.lat), cosLat * cosDLon) - pp._lat);
                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double R = _k0 * pp._radius;
                  double D = pt.y() / R + pp._lat;

                  double lat = ::std::asin(clampAsin(::std::sin(D) * ::std::cosh(pt.x() / R)));
                  double lon = pp._lon + ::std::atan2(::std::sinh(pt.x() / R), ::std::cos(D));

                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }
            private:
               double _k0;
         };

         struct CassiniEllipsoidProjection
         {
               CassiniEllipsoidProjection(const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct CassiniSpheroidProjection
         {
               CassiniSpheroidProjection(const ProjectionParameters& pp)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  const double dLon = r.lon - pp._lon;
                  const double sinDLon = ::std::sin(dLon);
                  const double cosDLon = ::std::cos(dLon);
                  const double cosLat = ::std::cos(r.lat);

                  const double B = clampAsin(cosLat * sinDLon);
                  double x = pp._radius * ::std::asin(B);
                  double y = pp._radius * (::std::atan2(::std::sin(r.lat), cosLat * cosDLon) - pp._lat);
                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double D = pt.y() / pp._radius + pp._lat;

                  double lat = ::std::asin(clampAsin(::std::sin(D) * ::std::cos(pt.x() / pp._radius)));
                  double lon = pp._lon + ::std::atan2(::std::tan(pt.x() / pp._radius), ::std::cos(D));

                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }
            private:
         };

         struct MillerEllipsoidProjection
         {
               MillerEllipsoidProjection(const ProjectionParameters& pp)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct MillerSpheroidProjection
         {
               MillerSpheroidProjection(const ProjectionParameters& pp)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double dLon = r.lon - pp._lon;
                  if (::std::abs(dLon > ::terra::PI)) {
                     r.map.setNoMapping();
                     return false;
                  }
                  double x = pp._radius * dLon;
                  double y = pp._radius * ::std::log(::std::tan(::terra::PI / 4 + 0.4 * r.lat)) / 0.8;
                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double lat = 2.5 * ::std::atan(::std::exp(0.8 * pt.y() / pp._radius)) - (5.0 / 8.0) * ::terra::PI;
                  double lon = pt.x() / pp._radius + pp._lon;

                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            private:
         };

         struct EqualAreaEllipsoidProjection
         {
               EqualAreaEllipsoidProjection(const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct EqualAreaSpheroidProjection
         {
               EqualAreaSpheroidProjection(const ProjectionParameters& pp)
                     : _radius_div_cosLatS(pp._radius / pp._cosLat), _radius_mul_cosLatS(pp._radius * pp._cosLat)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r, pp);
                  if (r.map.type() != ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double dLon = r.lon - pp._lon;
                  if (::std::abs(dLon > ::terra::PI)) {
                     r.map.setNoMapping();
                     return false;
                  }
                  double x = _radius_mul_cosLatS * dLon;
                  double y = _radius_div_cosLatS * ::std::sin(r.lat);

                  r.map.setPointMapping(x, y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double tmp = pt.y() / _radius_div_cosLatS;
                  if (tmp < -1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  double lon = pt.x() / _radius_mul_cosLatS + pp._lon;
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }

                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            private:
               const double _radius_div_cosLatS;
               const double _radius_mul_cosLatS;
         };

         template<class PF>
         struct BaseCylindricalProjection : public CylindricalProjection
         {

               BaseCylindricalProjection(const GeodeticCoordinates& x)throws()
                     : _parameters(x, Bounds(x.datum())), _projector(_parameters)
               {
               }

               BaseCylindricalProjection(const GeodeticCoordinates& x, Reference< ProjectionDomain> b)throws()
                     : _parameters(x, b), _projector(_parameters)
               {
               }

               BaseCylindricalProjection(const GeodeticCoordinates& x, double latS)throws()
                     : _parameters(x, Bounds(x.datum())), _projector(_parameters, latS)
               {
               }

               BaseCylindricalProjection(const GeodeticCoordinates& x, const Bounds& b, double latS)throws()
                     : _parameters(x, b), _projector(_parameters, latS)
               {
               }

               BaseCylindricalProjection(const GeodeticCoordinates& x, const Bounds& b)throws()
                     : _parameters(x, b), _projector(_parameters)
               {
               }

               BaseCylindricalProjection(const PF& pf, const GeodeticCoordinates& x, const Bounds& b)throws()
                     : _parameters(x, b), _projector(pf)
               {
               }

               BaseCylindricalProjection(const PF& pf, const GeodeticCoordinates& x,
                     Reference< ProjectionDomain> b)throws()
                     : _parameters(x, b), _projector(pf)
               {
               }

               ~BaseCylindricalProjection()throws()
               {
               }

               Reference< ProjectionDomain> domain() const throws()
               {
                  return _parameters._domain;
               }

               inline bool projectForward(ProjectionRequest& req) const throws()
               {
                  return _projector.forward(req, _parameters);
               }

               size_t projectUnsafe(size_t n, Point2D* lonlat) const throws()
               {
                  const Point2D* start = lonlat;
                  const Point2D* end = lonlat + n;
                  Point2D* out = lonlat;
                  while (lonlat < end) {
                     _projector.forwardUnsafe(*lonlat, out, _parameters);
                     ++lonlat;
                  }
                  return out - start;
               }

               size_t project(size_t n, ProjectionRequest* req) const throws()
               {
                  size_t i = 0;
                  while (i < n) {
                     projectForward(req[i]);
                     if (req[i].map.type() == ProjectionMapping::NONE) {
                        break;
                     }
                     ++i;
                  }
                  return i;
               }

               GeodeticCoordinates projectInverse(double x, double y) const throws(::std::exception)
               {
                  LatLon inv;
                  const Point2D pt(x, y);
                  if (_projector.inverse(pt, _parameters, inv)) {
                     return GeodeticCoordinates(inv[0], inv[1], center().datum());
                  }
                  throw ::std::runtime_error("Could not create inverse projection");
               }

               bool isInverseProjectionEnabled() const throw()
               {
                  return true;
               }

               inline const ProjectionParameters& parameters() const throws()
               {
                  return _parameters;
               }

               inline GeodeticCoordinates center() const throws()
               {
                  return _parameters._center;
               }

            protected:
               const ProjectionParameters _parameters;
               const PF _projector;
         };
      }

      CylindricalProjection::CylindricalProjection()
      throws()
      {
      }

      CylindricalProjection::~CylindricalProjection()
      throws()
      {
      }

      Reference< Projection> CylindricalProjection::createMercatorProjection(double lon,
            const Reference< GeodeticDatum>& datum)
            throws(::terra::UnsupportedDatum)
      {
         GeodeticCoordinates pot = GeodeticCoordinates(0.0, clampLongitude(lon), datum);
         double left = clampLongitude(pot.longitude() - toRadians(179.5));
         double right = clampLongitude(pot.longitude() + toRadians(179.5));
         double lat = toRadians(88.);

         Bounds b(GeodeticCoordinates(lat, left, pot.datum()), GeodeticCoordinates(-lat, right, pot.datum()));
         LogEntry("cartografo.projection.CylindricalProjection").info() << "Bounds for MercatorProjection : " << b
               << doLog;

         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< MercatorSpheroidProjection>(pot, b);
         }
         else {
            return new BaseCylindricalProjection< MercatorEllipsoidProjection>(pot, b);
         }
      }

      Reference< Projection> CylindricalProjection::createEqualAreaProjection(double lon,
            const Reference< GeodeticDatum>& datum)
            throws(::terra::UnsupportedDatum)
      {
         expectSpheroid(datum);
         GeodeticCoordinates pot = GeodeticCoordinates(0.0, clampLongitude(lon), datum);
         assert(::std::cos(pot.latitude()) != 0.0);
         double left = clampLongitude(pot.longitude() - toRadians(89.));
         double right = clampLongitude(pot.longitude() + toRadians(89.));
         double lat = toRadians(88.);

         Bounds b(GeodeticCoordinates(lat, left, pot.datum()), GeodeticCoordinates(-lat, right, pot.datum()));
         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< EqualAreaSpheroidProjection>(pot, b);
         }
         else {
            return new BaseCylindricalProjection< EqualAreaEllipsoidProjection>(pot, b);
         }
      }

      Reference< Projection> CylindricalProjection::createMillerProjection(double lon,
            const Reference< GeodeticDatum>& datum)
            throws(::terra::UnsupportedDatum)
      {
         expectSpheroid(datum);
         GeodeticCoordinates pot = GeodeticCoordinates(0.0, clampLongitude(lon), datum);
         double left = clampLongitude(pot.longitude() - toRadians(179.875));
         double right = clampLongitude(pot.longitude() + toRadians(179.875));
         double lat = toRadians(88.);

         Bounds b(GeodeticCoordinates(lat, left, pot.datum()), GeodeticCoordinates(-lat, right, pot.datum()));
         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< MillerSpheroidProjection>(pot, b);
         }
         else {
            return new BaseCylindricalProjection< MillerEllipsoidProjection>(pot, b);
         }
      }

      Reference< Projection> CylindricalProjection::createEquidistantProjection(double lon,
            const Reference< GeodeticDatum>& datum)
            throws(::terra::UnsupportedDatum)
      {
         expectSpheroid(datum);

         double latS = 0;
         GeodeticCoordinates pot = GeodeticCoordinates(0.0, clampLongitude(lon), datum);

         double left = clampLongitude(pot.longitude() - toRadians(89.));
         double right = clampLongitude(pot.longitude() + toRadians(89.));
         double lat = toRadians(88.);

         Bounds b(GeodeticCoordinates(lat, left, pot.datum()), GeodeticCoordinates(-lat, right, pot.datum()));
         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< EquidistantSpheroidProjection>(pot, b, latS);
         }
         else {

            return new BaseCylindricalProjection< EquidistantEllipsoidProjection>(pot, b, latS);
         }
      }

      Reference< Projection> CylindricalProjection::createCassiniProjection(const GeodeticCoordinates& ctr)
      throws(::terra::UnsupportedDatum)
      {
         expectSpheroid(ctr.datum());
         const GeodeticCoordinates pot(0, ctr.longitude(), ctr.datum());

         ::std::vector< Bounds> domains;
         double left = clampLongitude(pot.longitude() - toRadians(89.));
         double right = clampLongitude(pot.longitude() + toRadians(89.));
         double lat = toRadians(88.);
         domains.push_back(
               Bounds(GeodeticCoordinates(lat, left, pot.datum()), GeodeticCoordinates(-lat, right, pot.datum())));

         left = clampLongitude(::terra::PI + left);
         right = clampLongitude(::terra::PI + right);

         domains.push_back(
               Bounds(GeodeticCoordinates(lat, left, pot.datum()),
                     GeodeticCoordinates(1.0 / 1024, right, pot.datum())));
         domains.push_back(
               Bounds(GeodeticCoordinates(-1.0 / 1024, left, pot.datum()),
                     GeodeticCoordinates(-lat, right, pot.datum())));
         Reference< ProjectionDomain> domain = createMultiDomain(pot, domains, toRadians(1.), toRadians(1.));

         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< CassiniSpheroidProjection>(pot, domain);
         }
         else {
            return new BaseCylindricalProjection< CassiniEllipsoidProjection>(pot, domain);
         }
      }

      Reference< Projection> CylindricalProjection::createTransverseMercatorProjection(const GeodeticCoordinates& ctr)
      throws(::terra::UnsupportedDatum)
      {
         expectSpheroid(ctr.datum());
         const GeodeticCoordinates pot(0, ctr.longitude(), ctr.datum());

         ::std::vector< Bounds> domains;
         double left = clampLongitude(pot.longitude() - toRadians(89.));
         double right = clampLongitude(pot.longitude() + toRadians(89.));
         double lat = toRadians(88.);
         domains.push_back(
               Bounds(GeodeticCoordinates(lat, left, pot.datum()), GeodeticCoordinates(-lat, right, pot.datum())));

         left = clampLongitude(::terra::PI + left);
         right = clampLongitude(::terra::PI + right);

         domains.push_back(
               Bounds(GeodeticCoordinates(lat, left, pot.datum()),
                     GeodeticCoordinates(1.0 / 1024, right, pot.datum())));
         domains.push_back(
               Bounds(GeodeticCoordinates(-1.0 / 1024, left, pot.datum()),
                     GeodeticCoordinates(-lat, right, pot.datum())));

         Reference< ProjectionDomain> domain = createMultiDomain(pot, domains, toRadians(.2), toRadians(.2));

         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< TransverseMercatorSpheroidProjection>(pot, domain);
         }
         else {
            return new BaseCylindricalProjection< TransverseMercatorEllipsoidProjection>(pot, domain);
         }

      }

      Reference< Projection> CylindricalProjection::createObliqueMercatorProjection(const GeodeticCoordinates& pot,
            double az)
            throws (::terra::UnsupportedDatum)
      {
         expectSpheroid(pot.datum());

         const ObliqueMercatorSpheroidProjection pp(pot, az);
         az = pp.azimuth();
         // compute the geodesic to the antipodal point, and use the range,
         // which is half the Spheroid's circumference, to compute a point
         // to the left and the right of the pot.
         const Geodesic G(pot, pot.antipodalCoordinates());
         const GeodeticCoordinates left = Geodesic(G.range() - 500000, az - ::terra::PI).move(pp.center());
         const GeodeticCoordinates right = Geodesic(G.range() - 500000, az).move(pp.center());

         LatLon p1, p2;
         pp.poles(p1, p2);
         const GeodeticCoordinates pole1(p1[0], p1[1], pot.datum());
         const GeodeticCoordinates pole2(p2[0], p2[1], pot.datum());

         // remove the pole regions from the domain
         const Reference< Area> poleRegion1 = AreaBuilder::createLoxodromicCircle(pole1, 1000000, 64);
         const Reference< Area> poleRegion2 = AreaBuilder::createLoxodromicCircle(pole2, 1000000, 64);

         LogEntry(logger()).info() << "AZIMUTH : " << toDegrees(pp.azimuth()) << ::std::endl << "CENTER  : "
               << pp.center() << ::std::endl << "POLE 1  : " << pole1 << ::std::endl << "POLE 2  : " << pole2
               << ::std::endl << "LEFT    : " << left << ::std::endl << "RIGHT   : " << right << doLog;

         ::std::vector< Reference< Region> > regs;

         AreaBuilder B;
         B.beginOutsidePath(pole1, true); //FIXME: is this truly always Clockwise?
         B.geodesicTo(::std::sin(az) < 0 ? left : right);
         B.geodesicTo(pole2);
         B.geodesicTo(::std::sin(az) < 0 ? right : left);
         B.geodesicTo(pole1);

         Pointer< Region> D = AreaBuilder::toBoundedLoxodromicArea(B.area(), 500000);

         D = D->normalize();

         if (!D->contains(pp.center().latitude(), pp.center().longitude())) {
            //D = Area(B.path()->toBoundedLoxodromicPath(500000),false).toRegion();
            D = Region::subtract(Region::createSpheroidRegion(pot.datum()), D);
            assert(D->contains(pp.center().latitude(), pp.center().longitude()));
         }
         D = Region::subtract(D, poleRegion1);
         D = Region::subtract(D, poleRegion2);

         {
            double epsilon = toRadians(1.0);
            const Bounds hLeft(::terra::PI / 2 - epsilon, -::terra::PI, -::terra::PI / 2 + epsilon, 0, pot.datum());
            const Bounds hRight(::terra::PI / 2 - epsilon, 0, -::terra::PI / 2 + epsilon, ::terra::PI, pot.datum());
            Pointer< Region> rLeft = Region::intersect(D, Region::createBoundsRegion(hLeft));
            Pointer< Region> rRight = Region::intersect(D, Region::createBoundsRegion(hRight));
            regs.push_back(rLeft);
            regs.push_back(rRight);
         }

         Reference< ProjectionDomain> domain(new MultiRegionDomain(pp.center(), regs));

         if (pot.datum()->spheroid().flattening() == 0.0) {
            return new BaseCylindricalProjection< ObliqueMercatorSpheroidProjection>(pp, pp.center(), domain);
         }
         else {
            return new BaseCylindricalProjection< ObliqueMercatorEllipsoidProjection>(
                  ObliqueMercatorEllipsoidProjection(pot, az), pot, domain);
         }
      }

   }
}
