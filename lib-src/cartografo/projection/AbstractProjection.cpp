#include <cartografo/projection/AbstractProjection.h>
#include <cartografo/ProjectionRequest.h>
#include <terra/GeodeticCoordinates.h>
#include <tikal/Point2D.h>

namespace cartografo {
  namespace projection {
    
    AbstractProjection::AbstractProjection() throws() {}
    AbstractProjection::~AbstractProjection() throws() {}
    
    ::terra::GeodeticCoordinates AbstractProjection::projectInverse (double,double) const throws (::std::exception)
    { throw ::std::runtime_error("Inverse projection not available"); }
    
    bool AbstractProjection::isInverseProjectionEnabled () const throws()
    { return false; }
    
    size_t  AbstractProjection::projectUnsafe(size_t n, ::tikal::Point2D* lonlat) const throws()
    {
      // could be implemented more efficiently, but for now projects each point
      ProjectionRequest req;
      ::tikal::Point2D* start = lonlat;
      const ::tikal::Point2D* end = start+n;
      ::tikal::Point2D* out = start;
      while(lonlat!=end) {
	req.lat= lonlat->y();
	req.lon= lonlat->x();
	req.alt= 0;
	++lonlat;

	project(1,&req);
	if (req.map.type()!=ProjectionMapping::NONE) {
	  *out = req.map.point();
	  ++out;
	}
      }
      return out-start;
    }
  }
}
 
