#include <cartografo/projection/SimpleProjectionDomain.h>
#include <cartografo/Area.h>
#include <cartografo/AreaBuilder.h>

#include <terra/Bounds.h>

#include <timber/logging.h>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

namespace cartografo {
  namespace projection {
    namespace {
      static Log logger() { return Log("cartografo.projection.SimpleProjectionDomain"); }
    }

    SimpleProjectionDomain::SimpleProjectionDomain(const GeodeticCoordinates& ctr, const Extent& e) throws()
    : _center(ctr),
    //FIXME: hardcoded constants
      _region(AreaBuilder::createBoundsArea(e.getBounds(ctr.datum()),toRadians(10.),toRadians(10.)))
    {}

    SimpleProjectionDomain::SimpleProjectionDomain(const GeodeticCoordinates& ctr, const Reference<Region>& p) throws()
    : _center(ctr),
      _region(p)
    {}

    SimpleProjectionDomain::~SimpleProjectionDomain() throws()
    {}

    Reference<Region> SimpleProjectionDomain::region() const throws()
    {
      return _region;
    }

    GeodeticCoordinates SimpleProjectionDomain::center() const throws()
    { return _center; }
  }
}
