#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTION_H
#define _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTION_H

#ifndef _CARTOGRAFO_PROJECTION_H
#include <cartografo/Projection.h>
#endif

namespace cartografo {
  namespace projection {
    
    /**
     * The AbstractProjection provides some default implementations
     * for pure virtual methods of the Projection class.
     */
    class AbstractProjection : public Projection {
      AbstractProjection(const AbstractProjection&);
      AbstractProjection&operator=(const AbstractProjection&);
      
      /** The default constructor */
    protected:
      AbstractProjection() throws();
      
      /** Destroy this projection */
    public:
      ~AbstractProjection() throws();
      
      
      /**
       * @name default implementations
       * @{
       */
    public:
      ::terra::GeodeticCoordinates projectInverse (double x, double y) const throws (::std::exception);
      
      bool isInverseProjectionEnabled () const throws();
      
      /**
       * Project unsafe will be implemented int terms of a regular projection
       */
      size_t projectUnsafe(size_t n, ::tikal::Point2D* lonlat) const throws();
      /*@}*/
      
    };
  }
}

#endif
