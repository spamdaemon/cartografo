#include <cartografo/projection/ConicProjection.h>
#include <cartografo/projection/SimpleProjectionDomain.h>
#include <cartografo/PathBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/Area.h>
#include <cartografo/Region.h>
#include <cartografo/Projection.h>
#include <cartografo/ProjectionRequest.h>

#include <terra/terra.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Bounds.h>

#include <tikal/Point2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/Region2D.h>
#include <tikal/VertexOrder.h>
#include <tikal/Path2DBuilder.h>

#include <timber/logging.h>

#include <cmath>
#include <vector>
#include <iostream>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;
using namespace ::terra;

namespace cartografo {
   namespace projection {

      namespace {

         static Log logger()
         {
            return Log("cartografo.projection.ConicProjection");
         }
         static void expectSpheroid(const GeodeticCoordinates& x) throws(UnsupportedDatum)
         {
            if (x.datum()->spheroid().flattening() != 0.0) {
               logger().info("Expected a spheroid, but got an ellipsoid instead");
               throw ::terra::UnsupportedDatum(x.datum());
            }
         }

         static double sign(double x)
         {
            return x < 0 ? -1 : (x > 0 ? 1 : 0);
         }
         static double sqr(double x)
         {
            return x * x;
         }

         // the shared projection parameters
         struct ProjectionParameters
         {
               ProjectionParameters(const GeodeticCoordinates& x, const Bounds& b) :
                  _center(x), _radius(x.meanRadius()), _lat(x.latitude()), _lon(x.longitude()),
                        _cosLat(::std::cos(_lat)), _sinLat(::std::sin(_lat)), _cosLon(::std::cos(_lon)),
                        _sinLon(::std::sin(_lon)), _domain(new SimpleProjectionDomain(x, b))
               {
               }

               const GeodeticCoordinates _center;
               const double _radius;
               const double _lat, _lon;
               const double _cosLat, _sinLat;
               const double _cosLon, _sinLon;
               const Reference< ProjectionDomain> _domain;
         };

         struct BonneEllipsoidProjection
         {
               BonneEllipsoidProjection(const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct BonneSpheroidProjection
         {
               BonneSpheroidProjection(const ProjectionParameters& pp) :
                  _cotLat(1.0 / ::std::tan(pp._lat)), _signLat(sign(pp._lat))
               {

               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double p = pp._radius*(_cotLat + pp._lat - r.lat);
                  if (p==0) {
                     r.map.setPointMapping(0,0);
                     return true;
                  }

                  double E = pp._radius*(r.lon - pp._lon)*::std::cos(r.lat)/p;
                  double x = p*::std::sin(E);
                  double y = pp._radius * _cotLat - p*::std::cos(E);
                  r.map.setPointMapping(x,y);
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double p = ::sqrt(sqr(pt.x())+sqr(pp._radius*_cotLat - pt.y())) * _signLat;
                  double lat = _cotLat + pp._lat - p/pp._radius;
                  double lon = pp._lon;
                  double cosLat = ::std::cos(lat);
                  if (cosLat!=0.0) {
                     lon += p * ::std::atan2(_signLat*pt.x(),_signLat*(pp._radius*_cotLat - pt.y()))/(pp._radius*cosLat);
                  }
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }
                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            private:
               double _cotLat;
               double _signLat;
         };

         struct EquidistantEllipsoidProjection
         {
               EquidistantEllipsoidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct EquidistantSpheroidProjection
         {
               EquidistantSpheroidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
                  assert((lat1 == lat2 && ::std::sin(lat1) != 0) || (::std::cos(lat1) - ::std::cos(lat2)) != 0.0);
                  if (lat1 == lat2) {
                     _n = ::std::sin(lat1);
                  }
                  else {
                     _n = (::std::cos(lat1) - ::std::cos(lat2)) / (lat2 - lat1);
                  }

                  _signN = sign(_n);
                  _G = ::std::cos(lat1) / _n + lat1;
                  _RG = pp._radius * _G;
                  _p0 = _RG - pp._radius * pp._lat;
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  if (::std::abs(r.lon-pp._lon)> ::terra::PI) {
                     r.map.setNoMapping();
                     return false;
                  }
                  const double theta = _n*(r.lon - pp._lon);
                  const double p = _RG-pp._radius*r.lat;

                  double x = p*::std::sin(theta);
                  double y = _p0 - p*::std::cos(theta);
                  r.map.setPointMapping(x,y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double p = _signN * ::std::sqrt(sqr(pt.x()) + sqr(_p0 - pt.y()));
                  double theta = ::std::atan2(_signN*pt.x(),_signN*(_p0 - pt.y()));

                  double lat = _G - p/pp._radius;
                  double lon = pp._lon + theta/_n;
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }
                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            private:
               double _signN;
               double _G;
               double _RG;
               double _n;
               double _p0;
         };

         struct LambertConformalEllipsoidProjection
         {
               LambertConformalEllipsoidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct LambertConformalSpheroidProjection
         {
               LambertConformalSpheroidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
                  _n = ::std::log(::std::cos(lat1) / ::std::cos(lat2)) / ::std::log(
                        ::std::tan(::terra::PI / 4 + lat2 / 2) / ::std::tan(::terra::PI / 4 + lat1 / 2));
                  _RF = pp._radius * ::std::cos(lat1) * ::std::pow(::std::tan(::terra::PI / 4 + lat1 / 2), _n) / _n;
                  _p0 = _RF / ::std::pow(::std::tan(::terra::PI / 4 + pp._lat / 2), _n);
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  double p=_RF/::std::pow(::std::tan( ::terra::PI/4 + r.lat/2),_n);

                  if (::std::abs(r.lon-pp._lon)> ::terra::PI) {
                     r.map.setNoMapping();
                     return false;
                  }
                  const double theta = _n*(r.lon - pp._lon);

                  double x = p*::std::sin(theta);
                  double y = _p0 - p*::std::cos(theta);
                  r.map.setPointMapping(x,y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  double theta;
                  if (_n<0) {
                     theta = ::std::atan2(-pt.x(),pt.y()-_p0);
                  }
                  else {
                     theta = ::std::atan2(pt.x(),_p0 - pt.y());
                  }
                  double lon = theta/_n + pp._lon;

                  const double p = sign(_n)*::std::sqrt(sqr(pt.x()) + sqr(_p0 - pt.y()));
                  double lat;
                  if (p==0) {
                     lat = sign(_n)* ::terra::PI/2;
                  }
                  else {
                     lat = 2* ::std::atan(::std::pow(_RF/p,1.0/_n)) - ::terra::PI/2;
                  }
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }
                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }
            private:
               double _n;
               double _RF;
               double _p0;
         };

         struct AlbersEqualAreaEllipsoidProjection
         {
               AlbersEqualAreaEllipsoidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct AlbersEqualAreaSpheroidProjection
         {
               AlbersEqualAreaSpheroidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
                  double sinLat1 = ::std::sin(lat1);
                  _n = 0.5 * (sinLat1 + ::std::sin(lat2));
                  _C = sqr(::std::cos(lat1)) + 2 * _n * sinLat1;
                  _g = sqr(pp._radius / _n) * _C;
                  _h = 2.0 * sqr(pp._radius) / _n;
                  _signN = sign(_n);
                  _p0 = _signN * ::std::sqrt(::std::max(0.0, (_g - _h * pp._sinLat)));
               }

               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  if (::std::abs(r.lon-pp._lon)> ::terra::PI) {
                     r.map.setNoMapping();
                     return false;
                  }
                  const double p = _signN * ::std::sqrt(::std::max(0.0,(_g - _h * ::std::sin(r.lat))));
                  const double theta = _n*(r.lon - pp._lon);

                  double x = p*::std::sin(theta);
                  double y = _p0 - p*::std::cos(theta);
                  r.map.setPointMapping(x,y);
                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {

                  double theta = ::std::atan2(_signN*pt.x(),_signN*(_p0-pt.y()));
                  double lon = theta/_n + pp._lon;
                  const double p = ::std::sqrt(sqr(pt.x()) + sqr(_p0 - pt.y()));
                  double tmp = (_C - sqr(p*_n/pp._radius))/(2*_n);
                  if (tmp<-1 || tmp > 1) {
                     return false;
                  }
                  double lat = ::std::asin(tmp);
                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }

            private:
               double _g;
               double _h;
               double _n;
               double _signN;
               double _C;
               double _p0;
         };

         struct BipolarObliqueEllipsoidProjection
         {
               BipolarObliqueEllipsoidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct BipolarObliqueSpheroidProjection
         {
               BipolarObliqueSpheroidProjection(double lat1, double lat2, const ProjectionParameters& pp)
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct PolyConicEllipsoidProjection
         {
               PolyConicEllipsoidProjection()
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  r.map.setNoMapping();
                  return false;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  return false;
               }
         };

         struct PolyConicSpheroidProjection
         {
               PolyConicSpheroidProjection()
               {
               }
               bool forwardUnsafe(const Point2D& pt, Point2D*& ptOut, const ProjectionParameters& pp) const throws()
               {
                  ProjectionRequest r;
                  r.lat = pt.y();
                  r.lon = pt.x();
                  r.alt = 0;
                  forward(r,pp);
                  if (r.map.type()!=ProjectionMapping::NONE) {
                     *(ptOut++) = r.map.point();
                     return true;
                  }
                  return false;
               }

               bool forward(ProjectionRequest& r, const ProjectionParameters& pp) const throws()
               {
                  if (::std::abs(r.lon-pp._lon)>= ::terra::PI/2.0) {
                     r.map.setNoMapping();
                     return false;
                  }
                  const double tanLat = ::std::tan(r.lat);
                  double x,y;
                  if (tanLat==0.0) {
                     x = r.lon - pp._lon;
                     y = -pp._lat;
                  }
                  else {
                     double E = ::std::sin(r.lat)*(r.lon - pp._lon);
                     x = ::std::sin(E)/tanLat;
                     y = r.lat - pp._lat + (1.0 - ::std::cos(E))/tanLat;
                  }
                  r.map.setPointMapping(pp._radius*x,pp._radius*y);

                  return true;
               }

               bool inverse(const Point2D& pt, const ProjectionParameters& pp, LatLon& coords) const throws()
               {
                  if (pt.y() == -pp._radius*pp._lat) {
                     coords[0] = 0;
                     coords[1] = pt.x()/pp._radius + pp._lon;
                     return true;
                  }

                  double A = pp._lat + pt.y()/pp._radius;
                  double B = sqr(pt.x()/pp._radius) + sqr(A);

                  double thetaNew = A;
                  double thetaOld = 0;
                  size_t iteration=0;
                  do {
                     if (++iteration==10) {
                        // too many iterations
                        return false;
                     }
                     thetaOld = thetaNew;
                     double tanTheta = ::std::tan(thetaOld);
                     thetaNew = thetaOld - ((A*(thetaOld* tanTheta+1) - thetaOld - 0.5*(sqr(thetaOld) + B) * tanTheta)/((thetaOld - A)/tanTheta-1));
                  }while(::std::abs(thetaNew-thetaOld) > 1E-8);

                  double lat = thetaNew;
                  double lon = pp._lon;
                  double sinLat = ::std::sin(lat);
                  if (sinLat!=0) {
                     lon += ::std::asin(pt.x()* ::std::tan(lat)/pp._radius)/sinLat;
                  }
                  if (::std::isnan(lat) || ::std::isnan(lon)) {
                     return false;
                  }
                  coords[0] = clampLatitude(lat);
                  coords[1] = clampLongitude(lon);
                  return true;
               }
         };

         template<class PF>
         struct BaseConicProjection : public ConicProjection
         {

               BaseConicProjection(const GeodeticCoordinates& x, const Bounds& b, double lat1, double lat2)throws()
               : _parameters(x,b),_projector(lat1,lat2,_parameters)
               {
               }

               BaseConicProjection(const GeodeticCoordinates& x, const Bounds& b, double lat1)throws()
               : _parameters(x,b),_projector(lat1,_parameters)
               {
               }

               BaseConicProjection(const GeodeticCoordinates& x, const Bounds& b)throws()
               : _parameters(x,b),_projector(_parameters)
               {
               }

               BaseConicProjection(const PF& pf, const GeodeticCoordinates& x, const Bounds& b)throws()
               : _parameters(x,b),_projector(pf)
               {
               }

               ~BaseConicProjection()throws()
               {
               }

               Reference< ProjectionDomain> domain() const throws()
               {  return _parameters._domain;}

               inline bool projectForward(ProjectionRequest& req) const throws()
               {
                  return _projector.forward(req,_parameters);
               }

               size_t projectUnsafe(size_t n, Point2D* lonlat) const throws()
               {
                  const Point2D* start = lonlat;
                  const Point2D* end = lonlat + n;
                  Point2D* out = lonlat;
                  while(lonlat<end) {
                     _projector.forwardUnsafe(*lonlat,out,_parameters);
                     ++lonlat;
                  }
                  return out - start;
               }

               size_t project(size_t n, ProjectionRequest* req) const throws()
               {
                  size_t i=0;
                  while (i<n) {
                     projectForward(req[i]);
                     if (req[i].map.type()==ProjectionMapping::NONE) {
                        break;
                     }
                     ++i;
                  }
                  return i;
               }

               GeodeticCoordinates projectInverse(double x, double y) const throws(::std::exception)
               {
                  LatLon inv;
                  const Point2D pt(x, y);
                  if (_projector.inverse(pt, _parameters, inv)) {
                     return GeodeticCoordinates(inv[0], inv[1], center().datum());
                  }
                  throw ::std::runtime_error("Could not create inverse projection");
               }

               bool isInverseProjectionEnabled() const throw()
               {
                  return true;
               }

               inline const ProjectionParameters& parameters() const throws()
               {  return _parameters;}

               inline GeodeticCoordinates center() const throws()
               {  return _parameters._center;}

            protected:
               const ProjectionParameters _parameters;
               const PF _projector;
         };
      }

      ConicProjection::ConicProjection()
      throws()
      {}

      ConicProjection::~ConicProjection()
      throws()
      {}

      Reference< Projection>
      ConicProjection::createPolyConicProjection(const GeodeticCoordinates& pot, const Bounds& b)
throws   (::terra::UnsupportedDatum)
   {
      expectSpheroid(pot);
      if (pot.datum()->spheroid().flattening()==0) {
         return new BaseConicProjection<PolyConicSpheroidProjection>(PolyConicSpheroidProjection(),pot,b);
      }
      else {
         return new BaseConicProjection<PolyConicEllipsoidProjection>(PolyConicEllipsoidProjection(),pot,b);
      }
   }

   Reference<Projection> ConicProjection::createBipolarObliqueProjection (const GeodeticCoordinates& pot, const Bounds& b, double lat1, double lat2) throws(::terra::UnsupportedDatum)
   {
      expectSpheroid(pot);
      if (pot.datum()->spheroid().flattening()==0) {
         return new BaseConicProjection<BipolarObliqueSpheroidProjection>(pot,b,lat1,lat2);
      }
      else {
         return new BaseConicProjection<BipolarObliqueEllipsoidProjection>(pot,b,lat1,lat2);
      }
   }

Reference< Projection> ConicProjection::createAlbersEqualAreaProjection(const GeodeticCoordinates& pot,
      const Bounds& b, double lat1, double lat2)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(pot);
   assert(!(lat1==-lat2));
   if (pot.datum()->spheroid().flattening()==0) {
      return new BaseConicProjection<AlbersEqualAreaSpheroidProjection>(pot,b,lat1,lat2);
   }
   else {
      return new BaseConicProjection<AlbersEqualAreaEllipsoidProjection>(pot,b,lat1,lat2);
   }
}

Reference< Projection> ConicProjection::createLambertConformalProjection(const GeodeticCoordinates& pot,
      const Bounds& b, double lat1, double lat2)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(pot);
   if (pot.datum()->spheroid().flattening()==0) {
      return new BaseConicProjection<LambertConformalSpheroidProjection>(pot,b,lat1,lat2);
   }
   else {
      return new BaseConicProjection<LambertConformalEllipsoidProjection>(pot,b,lat1,lat2);
   }
}

Reference< Projection> ConicProjection::createEquidistantProjection(const GeodeticCoordinates& pot, const Bounds& b,
      double lat1, double lat2)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(pot);
   assert((lat1==lat2 && ::std::sin(lat1)!=0) || (::std::cos(lat1)-::std::cos(lat2))!=0.0);
   if (pot.datum()->spheroid().flattening()==0) {
      return new BaseConicProjection<EquidistantSpheroidProjection>(pot,b,lat1,lat2);
   }
   else {
      return new BaseConicProjection<EquidistantEllipsoidProjection>(pot,b,lat1,lat2);
   }
}

Reference< Projection> ConicProjection::createBonneProjection(const GeodeticCoordinates& pot, const Bounds& b)
throws(::terra::UnsupportedDatum)
{
   expectSpheroid(pot);
   if (!b.contains(pot)) {
      logger().warn("Center for Bonne projection is not in inside the projection's bounds");
   }

   if (pot.datum()->spheroid().flattening()==0) {
      return new BaseConicProjection<BonneSpheroidProjection>(pot,b);
   }
   else {
      return new BaseConicProjection<BonneEllipsoidProjection>(pot,b);
   }
}

}
}
