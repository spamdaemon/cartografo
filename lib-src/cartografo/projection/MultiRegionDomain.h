#ifndef _CARTOGRAFO_PROJECTION_MULTIREGIONDOMAIN_H
#define _CARTOGRAFO_PROJECTION_MULTIREGIONDOMAIN_H

#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTIONDOMAIN_H
#include <cartografo/projection/AbstractProjectionDomain.h>
#endif

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

#ifndef _TERRA_EXTENT_H
#include <terra/Extent.h>
#endif

#ifndef _CARTOGRAFO_REGION_H
#include <cartografo/Region.h>
#endif

#include <vector>

namespace terra {
}

namespace cartografo {
  namespace projection {
    /**
     * The base class for all projections.
     */
    class MultiRegionDomain : public AbstractProjectionDomain {
      MultiRegionDomain(const MultiRegionDomain&);
      MultiRegionDomain&operator=(const MultiRegionDomain&);
      
      /** 
       * A constructor.
       * @param ctr the projection center
       * @param regs the regions that make up the domain
       */
    public:
      MultiRegionDomain(const ::terra::GeodeticCoordinates& ctr, const ::std::vector< ::timber::Reference<Region> >& regs) throws();
      
      /** Destroy this projection */
    public:
      ~MultiRegionDomain() throws();

      /**
       * Get the point of tangency for this projection.
       * @return the center point for this projection
       */
    public:
      ::terra::GeodeticCoordinates center() const throws();

      /** The center of the projection */
    private:
      const ::terra::GeodeticCoordinates _center;

    public:
      ::timber::Reference<Region> region() const throws();

      /** The composite region */
    private:
      ::timber::Pointer<Region> _region;
    };
  }
}

#endif
