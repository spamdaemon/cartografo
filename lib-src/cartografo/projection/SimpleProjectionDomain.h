#ifndef _CARTOGRAFO_PROJECTION_SIMPLEPROJECTIONDOMAIN_H
#define _CARTOGRAFO_PROJECTION_SIMPLEPROJECTIONDOMAIN_H

#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTIONDOMAIN_H
#include <cartografo/projection/AbstractProjectionDomain.h>
#endif

#ifndef _CARTOGRAFO_REGION_H
#include <cartografo/Region.h>
#endif

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

#ifndef _TERRA_EXTENT_H
#include <terra/Extent.h>
#endif

namespace cartografo {
  namespace projection {
    /**
     * The base class for all projections.
     */
    class SimpleProjectionDomain : public AbstractProjectionDomain {
      SimpleProjectionDomain(const SimpleProjectionDomain&);
      SimpleProjectionDomain&operator=(const SimpleProjectionDomain&);
	
      /**
       * Create a domain specified by an extent. The outline
       * of the extent defines the outside path of the domain.
       * @param e an extent
       */
    public:
      SimpleProjectionDomain(const ::terra::GeodeticCoordinates& ctr, const ::terra::Extent& e) throws();

      /** 
       * A constructor.
       * @param ctr the projection center
       * @param a region with a single area.
       * @pre REQIUIRE(p.areaCount()==1)
       */
    public:
      SimpleProjectionDomain(const ::terra::GeodeticCoordinates& ctr, const ::timber::Reference<Region>& p) throws();
	
      /** Destroy this projection */
    public:
      ~SimpleProjectionDomain() throws();

      /**
       * Get the point of tangency for this projection.
       * @return the center point for this projection
       */
    public:
      ::terra::GeodeticCoordinates center() const throws();

      /** The center of the projection */
    private:
      const ::terra::GeodeticCoordinates _center;

    public:
      ::timber::Reference<Region> region() const throws();
	
      /** The region (it needs to be a pointer due to constructor reasons) */
    private:
      ::timber::Reference<Region> _region;
    };
  }
}
#endif
