#include <cartografo/projection/IdentityProjection.h>
#include <cartografo/projection/AbstractProjection.h>
#include <cartografo/projection/SimpleProjectionDomain.h>
#include <cartografo/ProjectionDomain.h>
#include <cartografo/ProjectionRequest.h>

#include <terra/Bounds.h>
#include <terra/terra.h>
#include <terra/GeodeticCoordinates.h>

#include <tikal/Point2D.h>
#include <cmath>

using namespace ::timber;
using namespace ::terra;
using namespace ::tikal;

namespace cartografo {
  namespace projection {
    namespace {
      struct IdentityProjectionImpl : public AbstractProjection {
  
	/**
	 * Create a new identity projection using the specified spheroid.
	 * @param ctr the point on the spheroid that will be mapped to (0,0)
	 */
      public:
	IdentityProjectionImpl (const GeodeticCoordinates& ctr, const Bounds& e) throws()
	: _extent(e),
	  _lat(ctr.latitude()),_lon(ctr.longitude()),
	  _domain(new SimpleProjectionDomain(ctr,_extent))
	{}
	
	/**
	 * Destroy this projection
	 */
	~IdentityProjectionImpl() throws()
	{}
	  
      public:
	  
	Reference<ProjectionDomain> domain() const throws()
	{ return _domain; }
	
	size_t project (size_t n, ProjectionRequest* reqs) const throws()
	{
	  for (size_t i=0;i<n;++i) {
	    ProjectionRequest& req = reqs[i];
	    double lat = req.lat - _lat;
	    double lon = clampLongitude(req.lon-_lon);
	    if (_extent.contains(lat,lon)) {
	      Point2D pt(lon,lat);
	      if (::std::abs(lat)< ::terra::PI/2) {
		req.map.setPointMapping(pt);
	      }
	      else {
		double y = pt.y();
		double x = -::terra::PI;
		req.map.setLineMapping(pt,Point2D(x,y),Point2D(-x,y));
	      }
	    }
	    else {
	      req.map.setNoMapping();
	    }
	  }
	  return n;
	}
	  
	GeodeticCoordinates projectInverse (double lon, double lat) const throws (::std::exception)
	{
	  if (GeodeticCoordinates::verifyCoordinatesRadians(lat,lon)) {
	    lat += _lat;
	    lon += _lon;
	    lon = clampLongitude(lon);
	    if (GeodeticCoordinates::verifyCoordinatesRadians(lat,lon)) {
	      if (_extent.contains(lat,lon)) {
		return GeodeticCoordinates(lat,lon,0,_domain->center().datum());
	      }
	      throw ::std::invalid_argument("Point outside the projection bounds");
	    }
	  }
	  throw ::std::invalid_argument("Invalid point for inverse projection");
	}
	  
	bool isInverseProjectionEnabled () const throws() 
	{ return true; }
	  
	size_t projectUnsafe(size_t n, Point2D* lonlat) const throws()
	{
	  if (_lat!=0 || _lon!=0) {
	    for (size_t i=0;i<n;++i) {
	      lonlat[i] = Point2D(lonlat[i].x()-_lon,lonlat[i].y()-_lat);
	    }
	  }
	  return n;
	}

      private:
	const Bounds _extent;
	  
	/** The center */
      private:
	const double _lat,_lon;

      private:
	Reference<ProjectionDomain> _domain;
      };
    }
     
    Reference<Projection> IdentityProjection::create(const GeodeticCoordinates& c, const Bounds& e) throws()
    {	 return new IdentityProjectionImpl(c,e); }
    
    Reference<Projection> IdentityProjection::create(const GeodeticCoordinates& c) throws()
    { return create(c,Bounds()); }
    
  }
}
