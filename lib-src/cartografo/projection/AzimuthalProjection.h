#ifndef _CARTOGRAFO_PROJECTION_AZIMUTHALPROJECTION_H
#define _CARTOGRAFO_PROJECTION_AZIMUTHALPROJECTION_H

#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTION_H
#include <cartografo/projection/AbstractProjection.h>
#endif

#ifndef _TERRA_UNSUPPORTEDDATUM_H
#include <terra/UnsupportedDatum.h>
#endif

namespace terra {
   class GeodeticCoordinates;
}

namespace cartografo {
   namespace projection {

      /**
       * The base class for all azimuthal projetions.
       */
      class AzimuthalProjection : public AbstractProjection
      {
            AzimuthalProjection(const AzimuthalProjection&);
            AzimuthalProjection&operator=(const AzimuthalProjection&);

            /**
             * Create a new azimuthal projection.
             */
         protected:
            AzimuthalProjection()throws();

            /** Destroy this projection. */
         public:
            ~AzimuthalProjection()throws();

            /**
             * @name Required implementations
             * @{
             */

            /*@}*/

            /**
             * @name Basic azimuthal projections.
             * @{
             */

            /**
             * Create a gnomonic projection. The center of
             * the projection is the specified point of tangency.
             * The underlying spheroid must not be an ellipsoid.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @pre REQUIRE_TRUE(pot.datum().flattening()==0)
             * @return a gnomonic projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createGnomonicProjection(const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create a stereographic projection. The center of
             * the projection is the specified point of tangency.
             * The underlying spheroid must not be an ellipsoid.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @pre REQUIRE_TRUE(pot.datum().flattening()==0)
             * @return a stereographic projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createStereographicProjection(
                  const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * The Lambert Equal Area Projection.The center of
             * the projection is the specified point of tangency.
             * The underlying spheroid must not be an ellipsoid.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @pre REQUIRE_TRUE(pot.datum().flattening()==0)
             * @return  an azimuthal equivalent projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createLambertEqualAreaProjection(
                  const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create an azimuthal equidistant projection.The center of
             * the projection is the specified point of tangency.
             * The underlying spheroid must not be an ellipsoid.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @pre REQUIRE_TRUE(pot.datum().flattening()==0)
             * @return  an azimuthal equidistant projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection>
                  createEquidistantProjection(const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create an orthographic projection.The center of
             * the projection is the specified point of tangency.
             * The underlying spheroid must not be an ellipsoid.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @pre REQUIRE_TRUE(pot.datum().flattening()==0)
             * @return  an orthographic projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createOrthographicProjection(
                  const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create a vertical perspective projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @param dist the distance of the observer from the point of tangency along the radial
             * @return  a conformal projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createVerticalPerspectiveProjection(
                  const ::terra::GeodeticCoordinates& pot, double dist)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create a tilted perspective projection.The center of
             * the projection is the specified point of tangency.
             * The altiude at the point of tangency is ignored.
             * @param pot the point of tangency
             * @return  a conformal projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createTiltedPerspectiveProjection(
                  const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /*@}*/

      };
   }
}

#endif
