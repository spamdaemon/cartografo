#ifndef _CARTOGRAFO_PROJECTION_CYLINDRICALPROJECTION_H
#define _CARTOGRAFO_PROJECTION_CYLINDRICALPROJECTION_H

#ifndef _CARTOGRAFO_PROJECTION_ABSTRACTPROJECTION_H
#include <cartografo/projection/AbstractProjection.h>
#endif

#ifndef _TERRA_UNSUPPORTEDDATUM_H
#include <terra/UnsupportedDatum.h>
#endif

namespace terra {
   class GeodeticDatum;
   class GeodeticCoordinates;
}

namespace cartografo {
   namespace projection {

      /**
       * The base class for all cylindrical projections.
       */
      class CylindricalProjection : public AbstractProjection
      {
            CylindricalProjection(const CylindricalProjection&);
            CylindricalProjection&operator=(const CylindricalProjection&);

            /**
             * Create a new cylindrical projection
             */
         protected:
            CylindricalProjection()throws();

            /** Destroy this projection. */
         public:
            ~CylindricalProjection()throws();

            /**
             * @name Required implementations
             * @{
             */

            /*@}*/
            /**
             * @name Basic cylindrical projections.
             * @{
             */

            /**
             * Create a Mercator projector with the projection center at (0,lon);
             * @param lon the longitude (in radians)
             * @param datum the datum for this projection
             * @return a Mercator projection
             * @throws ::terra::UnsupportedDatum if the CRS is not supported
             */
         public:
            static ::timber::Reference< Projection> createMercatorProjection(double lon,
                  const ::timber::Reference< ::terra::GeodeticDatum>& datum)
            throws( ::terra::UnsupportedDatum);

            /**
             * The equal area projection with the projection center at (0,lon);
             * @param lon the longitude (in radians)
             * @param datum the datum for this projection
             * @return an equal area projection
             * @throws ::terra::UnsupportedDatum if the CRS is not supported
             */
         public:
            static ::timber::Reference< Projection> createEqualAreaProjection(double lon,
                  const ::timber::Reference< ::terra::GeodeticDatum>& datum)
            throws( ::terra::UnsupportedDatum);

            /**
             * The Miller cylindrical projection with the projection center at (0,lon);
             * @param lon the longitude (in radians)
             * @param datum the datum for this projection
             * @return the Miller cylindrical projection
             * @throws ::terra::UnsupportedDatum if the CRS is not supported
             */
         public:
            static ::timber::Reference< Projection> createMillerProjection(double lon,
                  const ::timber::Reference< ::terra::GeodeticDatum>& datum)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create a cylindrical equidistant projection with the projection center at (0,lon);
             * @param lon the longitude (in radians)
             * @param datum the datum for this projection
             * @return  a cylindrical equidistant projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createEquidistantProjection(double lon,
                  const ::timber::Reference< ::terra::GeodeticDatum>& datum)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create the Cassini projection.
             * @param pot the center of the projection
             * @return  a cylindrical equivalent projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createCassiniProjection(const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create the transverse Mercator projection.
             * @param pot the center of the projection
             * @return the transverse Mercator projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createTransverseMercatorProjection(
                  const ::terra::GeodeticCoordinates& pot)
            throws( ::terra::UnsupportedDatum);

            /**
             * Create the oblique Mercator projection.
             * @param pot the center of the projection
             * @param az the obliqueness angle in radians
             * @return the oblique Mercator projection
             * @throws ::terra::UnsupportedDatum if the CRS of pot is not supported
             */
         public:
            static ::timber::Reference< Projection> createObliqueMercatorProjection(
                  const ::terra::GeodeticCoordinates& pot, double az)
            throws( ::terra::UnsupportedDatum);

            /*@}*/

      };
   }
}
#endif
