#ifndef _CARTOGRAFO_AREABUILDER_H
#define _CARTOGRAFO_AREABUILDER_H

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

#ifndef _CARTOGRAFO_AREA_H
#include <cartografo/Area.h>
#endif

#ifndef _CARTOGRAFO_PATHBUILDER_H
#include <cartografo/PathBuilder.h>
#endif

#include <vector>

namespace terra {
  class Bounds;
}

namespace cartografo {

  /**
   * A region builder enables the creation ofsurface regions.
   */
  class AreaBuilder {
    /** No copy methods */
    AreaBuilder(const AreaBuilder&);
    AreaBuilder&operator=(const AreaBuilder&);
    
    /** The direction of travel for loxodromes */
  public:
    enum Direction {
      EAST = PathBuilder::EAST,
      WEST = PathBuilder::WEST,
      SHORTEST = PathBuilder::SHORTEST
    };
    
    /**
     * Create a new region builder.
     */
  public:
    AreaBuilder() throws();

    /** Destructor */
  public:
    ~AreaBuilder() throws();

    /**
     * Reset this builder.
     */
  public:
    void reset() throws();

    /**
     * Begin a new outside path of an area
     * @param x a point
     * @param windCW the winding order of the outside path
     */
  public:
    void beginOutsidePath (const ::terra::GeodeticCoordinates& x, bool windCW) throws();
      
    /**
     * Create a geodesic from the last point to the specified point.
     * The general direction will be such that the length of this segment is minimal.
     * The insertion of a point at either pole may introduce extra points at that pole.
     * @param x a point
     * @throws ::std::exception if there was no prior moveTo() call
     */
  public:
    void geodesicTo (const ::terra::GeodeticCoordinates& x) throws (::std::exception);

    /**
     * Create a loxodrome from the last point to the specified point. The general
     * direction will be such that the length of this segment is minimal. The insertion
     * of a point at either pole may introduce extra points at that pole.
     * @param x a point
     * @param dir the direction of travel
     * @throws Exception if there was no prior moveTo() call
     */
  public:
    void loxodromeTo (const ::terra::GeodeticCoordinates& x, Direction dir = SHORTEST) throws (::std::exception);

    /**
     * Create a polygon region defined by vertices. The regions joining the vertices are 
     * loxodromic arcs.
     * @param coords the waypoints along the outside of the region.
     * @param windCW true if the vertices are enumerated in clockwise order as seen from an observer inside the area
     * @return a region consisting of loxodromic segments
     */
  public:
    static ::timber::Pointer<Area> createLoxodromicArea (const ::std::vector< ::terra::GeodeticCoordinates>& coords, bool windCW);

    /**
     * Create a polygon region defined by vertices. The regions joining the vertices are 
     * loxodromic arcs.
     * @param n the number of waypoints
     * @param coords the waypoints along the region
     * @param windCW true if the vertices are enumerated in clockwise order as seen from an observer inside the area
      * @return a region consisting of loxodromic segments
     */
  public:
    static ::timber::Pointer<Area> createLoxodromicArea (size_t n, const ::terra::GeodeticCoordinates* coords, bool windCW);

    /**
     * Create a polygon region defined by vertices. The regions joining the vertices are 
     * geodesic arcs.
     * @param n the number of waypoints
     * @param coords the waypoints along the region
     * @param windCW true if the vertices are enumerated in clockwise order as seen from an observer inside the area
     * @return a region consisting of geodesic segments
     */
  public:
    static ::timber::Pointer<Area> createGeodesicArea (size_t n, const ::terra::GeodeticCoordinates* coords, bool windCW);

    /**
     * Create a polygon region defined by vertices. The regions joining the vertices are 
     * geodesic arcs.
     * @param coords the waypoints along the outside of the region.
     * @param windCW true if the vertices are enumerated in clockwise order as seen from an observer inside the area
     * @return a region consisting of geodesic segments
     */
  public:
    static ::timber::Pointer<Area> createGeodesicArea (const ::std::vector< ::terra::GeodeticCoordinates>& coords, bool windCW);

    /**
     * Create the region defined by a bounds object.
     * @param bounds a bounds object
     * @return a region representing the bounds region
     */
  public:
    static ::timber::Pointer<Area> createBoundsArea (const ::terra::Bounds& bounds);

    /**
     * Create the region defined by a bounds object.
     * @param bounds a bounds object
     * @param dLat the difference in latitude between points on the region
     * @param dLon the difference in longitude between points on the region
     * @return a region representing the bounds region
     */
  public:
    static ::timber::Pointer<Area> createBoundsArea (const ::terra::Bounds& bounds, double dLat, double dLon);
      
    /**
     * Create a region defined by the geodesic circle through two specified points. This function effectively cuts
     * the spheroid in half and returns on of the two halves.
     * @todo probably, we'll need to specified which of the amiguous region we want.
     * @param a a point
     * @param b a point 
     * @pre REQUIRE_FALSE(a.equals(b))
     * @pre REQUIRE_FALSE(a.equals(b.anitpodalCoordinates()))
     * @return a region.
     */
  public:
    static ::timber::Reference<Area> createGeodesicCircle(const ::terra::GeodeticCoordinates& a, const ::terra::GeodeticCoordinates& b);
      
    /**
     * Create a circular region around the specified coordinates. The radius of the circle is specified
     * in meters.
     * @param ctr the center of the circle
     * @param radius the radius of the circle in meters
     * @param nVertices the number of vertices for the circle
     * @pre REQUIRE_GREATER_OR_EQUAL(radius,0.0)
     * @return a circular region
     */
  public:
    static ::timber::Reference<Area> createLoxodromicCircle(const ::terra::GeodeticCoordinates& ctr, double radius, size_t nVertices);
      
    /**
     * Create a circular region around the specified coordinates. The radius of the circle is specified
     * in meters.
     * @param ctr the center of the circle
     * @param radius the radius of the circle in meters
     * @pre REQUIRE_GREATER_OR_EQUAL(radius,0.0)
     * @return a circular region
     */
  public:
    static ::timber::Reference<Area> createLoxodromicCircle(const ::terra::GeodeticCoordinates& ctr, double radiusM);
      
    /**
     * Create a circular region around the specified coordinates. The radius of the circle is specified
     * in degrees radians.
     * @param ctr the circle center
     * @param radRadius the radius in radians of the circle
     * @param nVertices the number of vertices for the circle
     * @pre REQUIRE_GREATER(radius,0.0);
     * @return a circular region
     */
  public:
    static ::timber::Reference<Area> createRadiansCircle(const ::terra::GeodeticCoordinates& ctr, double radRadius, size_t nVertices);

    /**
     * Create a circular region around the specified coordinates. The radius of the circle is specified
     * in degrees radians.
     * @param ctr the circle center
     * @param radRadius the radius in radians of the circle
     * @pre REQUIRE_GREATER(radius,0.0);
     * @return a circular region
     */
  public:
    static ::timber::Reference<Area> createRadiansCircle(const ::terra::GeodeticCoordinates& ctr, double radRadius);

    /**
     * Create a region around the specified observer all the way to the horizon. The observer
     * must be <em>above</em> the ellipsoid f or this function to return a meaningful result.
     * @param obs an observer location
     * @return a region or 0 if none could be created for the observer
     */
  public:
    static ::timber::Pointer<Area> createHorizonArea (const ::terra::GeodeticCoordinates& obs) throws();

       
    /**
     * Map this area into a loxodromic area where the outside path and the inside paths
     * are loxodromic paths
     * @param length the length of each straight line segment.
     * @return a loxodromic area with a bounded segment length
     */
  public:
    static ::timber::Reference<Area> toBoundedLoxodromicArea (const ::timber::Reference<Area>& area, double length) throws();

    /**
     * Create a loxodromic area.
     * @param err error in meters
     * @return a area that is a loxodromic area
     */
  public:
    static ::timber::Reference<Area> toLoxodromicArea (const ::timber::Reference<Area>& area,double err) throws();

    /**
     * Get the region buit up to now. If an area was begin then it is closed by this call.
     * @return the region built so far
     */
  public:
    ::timber::Pointer<Area> area() throws();

    /** Close the current area */
  private:
    void closeArea() throws();

    /** True if we need a move to */
  private:
    bool _needMoveTo;

    /** The path ordering */
  private:
    bool _windCW;

    /** The current start point */
  private:
    ::terra::GeodeticCoordinates _startPoint;

    /** The current end point */
  private:
    ::terra::GeodeticCoordinates _endPoint;

    /** A path builder */
  private:
    PathBuilder _builder;

    /** The current area */
  private:
    mutable ::timber::Pointer<Area> _area;
  };
}
#endif
