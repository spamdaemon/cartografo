#include <cartografo/ExtentPyramid.h>
#include <terra/Extent.h>
#include <timber/logging.h>
#include <canopy/time/Time.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::terra;

#define ENABLE_TIMING 0

namespace cartografo {
  namespace {

    static Log logger() { return Log("cartografo.ExtentPyramid"); }
    
    static Region::Classification classifyExtentAccurate (const Extent& e, const Reference<Region>& r, Pointer<Region>* intersection) throws()
    {
      Extent ext(e);
      ext.shiftTowards(r->extent());
      
      if (ext.encloses(r->extent())) {
	return Region::POSSIBLE_OVERLAP;
      }

      if (r->classifyExtent(ext) == Region::OUTSIDE) {
	return Region::OUTSIDE;
      }
      try {
	Reference<Region> eRegion = Region::createExtentRegion(ext);
	Pointer<Region>  iRegion = Region::intersect(eRegion,r);
	if (intersection!=0) {
	  *intersection = iRegion;
	}
	if (!iRegion) {
	  return Region::OUTSIDE;
	}
	if (!Region::subtract(eRegion,r)) {
	  return Region::CONTAINED;
	}
      }
      catch (...) {
	logger().warn("Unexpected exception");
	// in case of exception, report a possible overlap
      }

      return Region::POSSIBLE_OVERLAP;
    }
    
    struct PyramidCell : public ExtentPyramid {
      /**
       * Create new pyramid level. The extent of the region defined by this cell
       * is classified as being either entirely inside or overlapping a region
       */
      PyramidCell(const Reference<Region>& r, size_t nMoreLevels)
	: _nCells(0),_cellClassification(Region::POSSIBLE_OVERLAP)
      {
	Extent w(-PI/2,-PI,PI/2,0.0);
	Extent e(-PI/2,0,PI/2,PI);
	
	Pointer<Region> intersection;
	Region::Classification c =  classifyExtentAccurate(w,r,&intersection);
	if (c!=Region::OUTSIDE) {
	  // this shouldn't really happen
	  if (!intersection) {
	    intersection = r;
	  }
	  _cells[_nCells] = new PyramidCell(w,c);
	  _cells[_nCells]->insert(intersection,nMoreLevels);
	  ++_nCells;
	}
	c =  classifyExtentAccurate(e,r,&intersection);
	if (c!=Region::OUTSIDE) {
	  // this shouldn't really happen
	  if (!intersection) {
	    intersection = r;
	  }
	  _cells[_nCells] = new PyramidCell(e,c);
	  _cells[_nCells]->insert(intersection,nMoreLevels);
	  ++_nCells;
	}
      }
      
      /**
       * Create new pyramid level. The extent of the region defined by this cell
       * is classified as being either entirely inside or overlapping a region
       */
      PyramidCell(const Extent& e,Region::Classification cls )
	: _extent(e),_nCells(0),_cellClassification(cls)
      {}
      
      ~PyramidCell () throws() {}

    private:
      void insert(const Reference<Region>& r, size_t nMoreLevels)
      {
	if (nMoreLevels==0) {
	  return;
	}
	
	// reduce the number of levels when inserting recursively
	--nMoreLevels;

	// create the subextents and check if the region intersects one of them
	double midLon = (_extent.west()+_extent.east())/2;
	double midLat = (_extent.south()+_extent.north())/2;
	
	Extent exts[4];
	exts[0] = Extent(midLat,_extent.west(),_extent.north(),midLon); // NW
	exts[1] = Extent(midLat,midLon,_extent.north(),_extent.east()); // NE
	exts[2] = Extent(_extent.south(),midLon,midLat,_extent.east()); // SE
	exts[3] = Extent(_extent.south(),_extent.west(),midLat,midLon); // SW
	
	for (size_t i=0;i<4;++i) {
	  Pointer<Region> intersection;
	  Region::Classification c = classifyExtentAccurate(exts[i],r,&intersection);
	  if (c!=Region::OUTSIDE) {
	    // this shouldn't really happen
	    if (!intersection) {
	      intersection = r;
	    }
	    _cells[_nCells] = new PyramidCell(exts[i],c);
	    // insert the region recrusively into the new cell; but not 
	    // if the cell is entirely contained in the region
	    if (c==Region::POSSIBLE_OVERLAP) {
	      _cells[_nCells]->insert(intersection,nMoreLevels);
	    }
	    ++_nCells;
	  }
	}
      }

    public:
      void gatherLeafExtents(vector<Extent>& v) const throws()
      {
	if (_nCells==0) {
	  v.push_back(_extent);
	}
	else {
	  for (UInt8 i=0;i<_nCells;++i) {
	    _cells[i]->gatherLeafExtents(v);
	  }
	}
      }

      
      Region::Classification classifyExtent (const ::terra::Extent& e) const throws()
      {
	// at leaf, we can just return the cell's classification; 
	if (_nCells==0) {
	  return _cellClassification;
	}

	// determine which types of cells are touched by the extent; if the extent
	// touches the same kind of cell, then that is the result that will be returned;
	// if the cells that are touched are different, then there's a possible overlap
	// e.g. if an extent only classifies as OUTSIDE, then the whole extent is outside
	//      likewise, if the extent only classifies as INSIDE, then the whole extent is
	//      guaranteed to be inside
	
	Region::Classification res = Region::OUTSIDE;
	UInt8 i = 0;
	// find a cell withi which the extent overlaps; that cell's classification will
	// be the initial classification for this cell
	for (i=0;i<_nCells;++i) {
	  Extent ext(e);
	  if (ext.shiftTowards(_cells[i]->_extent)) {
	    res = _cells[i]->classifyExtent(ext);
	    break;
	  }
	}
	
	// if the overlap is not a possible overlap, then the extent might be 
	// entirely outside or entirely inside
	for (/**/;res!=Region::POSSIBLE_OVERLAP && i<_nCells;++i) {
	  // recursively classify
	  Extent ext(e);
	  if (ext.shiftTowards(_cells[i]->_extent)) {
	    const Region::Classification cls = _cells[i]->classifyExtent(ext);
	    if (cls!=res) {
	      res = Region::POSSIBLE_OVERLAP;
	    }
	  }
	}
	return res;
      }

      /** The four pyramids; first index is latitude, second one is longitude */
    private:
      Pointer<PyramidCell> _cells[4];

      /** The extent for this pyramid */
    private:
      const Extent _extent;

      /** The number of cells */
    private:
      UInt8 _nCells;

      /** The classification of this cell with respect to the region */
    private:
      const Region::Classification _cellClassification;
    };
    
    /** The top-level cell */
    struct TopLevelCell : public PyramidCell {
      TopLevelCell(const Reference<Region>& r, size_t nMoreLevels) throws() 
      :PyramidCell(r,nMoreLevels),
	_regionExtent(r->extent())
      {}
      ~TopLevelCell() throws() {}

      Region::Classification classifyExtent (const ::terra::Extent& e) const throws()
      {
	Extent ext(e);
	if (!ext.shiftTowards(_regionExtent)) {
	  return Region::OUTSIDE;
	}
	
#if ENABLE_TIMING == 1
	::canopy::time::Time startTime;
	
	Region::Classification c = PyramidCell::classifyExtent(e);
	::canopy::time::Time endTime;
	if (logger().isLoggable(Level::DEBUGGING)) {
	  ULong dtimeUS = (endTime.time() - startTime.time())/1000;
	  if(dtimeUS>0) {
	    LogEntry(logger()).level(Level::DEBUGGING) << "Classify extent in " << dtimeUS << "us" << doLog;
	  }
	}	    
	
	return c;
#else
	return PyramidCell::classifyExtent(ext);
#endif
      }

      /** The extent of the original region */
    private:
      const Extent _regionExtent;
    };
  }

  ExtentPyramid::ExtentPyramid() throws()
  {}

  ExtentPyramid::~ExtentPyramid() throws()
  {}

  Reference<ExtentPyramid> ExtentPyramid::createRegionPyramid(const Reference<Region>& r, size_t nLevels) throws()
  {
    Reference<TopLevelCell> P(new TopLevelCell(r,nLevels));
    return P;
  }

}
