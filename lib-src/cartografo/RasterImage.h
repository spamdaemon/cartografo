#ifndef _CARTOGRAFO_RASTERIMAGE_H
#define _CARTOGRAFO_RASTERIMAGE_H

#ifndef _CARTOGRAFO_GEO_H
#include <cartografo/geo/geo.h>
#endif

#ifndef _TERRA_LOCATION_H
#include <terra/Location.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#include <cstdint>

namespace terra {
   class Bounds;
}

namespace cartografo {
   /**
    * A raster is an image rendered at specific location. The image itself will
    * have its own associated projected coordinate system.
    */
   class RasterImage
   {
         /** The image pointer */
      public:
         typedef ::std::shared_ptr< ::timber::media::ImageBuffer> ImagePtr;

         /** A location */
      public:
         typedef ::terra::Location Location;

         /** The raster bounds */
      public:
         class Bounds
         {
            public:
               Bounds()throws();

               /**
                * Create a bounds from a different bounds object
                *
                */
            public:
               Bounds(const ::terra::Bounds& b)throws();

               /** Create a new raster bounds */
            public:
               Bounds(const Location& tl, const Location& tr, const Location& br, const Location& bl)throws();

               /** Destructor */
            public:
               ~Bounds()throws();

               /**
                * Compare two bounds. Two bounds are equal if all corresponding points are equal.
                * @param b a bounds
                * @return if each corner equals its corresponding corner in the other object
                */
            public:
               bool operator==(const Bounds& b) const throws();

               /**
                * Set a new CRS this bounds.
                * @param crs a new crs
                * @return true if the CRS was updated for each point, false otherwise
                */
            public:
               bool setCRS(const ::timber::Reference< ::terra::CoordinateReferenceSystem>& newCrs)throws();

               /**
                * Get the coordinate reference system for the bounds.
                * @return the spheroid
                */
            public:
               inline const ::timber::Reference< ::terra::CoordinateReferenceSystem>& crs() const throws() {return _topLeft.crs();}

               /**
                * Get the top-left corner of this raster.
                * @return the top-left corner.
                */
            public:
               inline const Location& topLeft() const throws() {return _topLeft;}

               /**
                * Get the top-right corner of this raster.
                * @return the top-right corner.
                */
            public:
               inline const Location& topRight() const throws() {return _topRight;}

               /**
                * Get the bottom-right corner of this raster.
                * @return the bottom-right corner.
                */
            public:
               inline const Location& bottomRight() const throws() {return _bottomRight;}

               /**
                * Get the bottom-right corner of this raster.
                * @return the bottom-right corner.
                */
            public:
               inline const Location& bottomLeft() const throws() {return _bottomLeft;}

               /**
                * Get the distance between the topLeft and topRight corners.
                * @return the width between topLeft() and topRight()
                */
            public:
               double topWidth() const throws();

               /**
                * Get the distance between the bottomLeft and bottomRight corners.
                * @return the width between bottomLeft() and bottomRight()
                */
            public:
               double bottomWidth() const throws();

               /**
                * Get the distance between the topLeft and bottomLeft corners.
                * @return the width between topLeft() and bottomLeft()
                */
            public:
               double leftHeight() const throws();

               /**
                * Get the distance between the topRight and bottomRight corners.
                * @return the width between topRight() and bottomRight()
                */
            public:
               double rightHeight() const throws();

               /**
                * Get the height of the raster. The height is defined as the distance
                * between the topLeft and bottomLeft corners.
                * @return the height or -1 if the image height is to be used
                */
            public:
               double height() const throws();

            private:
               Location _topLeft, _topRight, _bottomRight, _bottomLeft;
         };

         /**
          * Default constructor without an image.
          */
      public:
         RasterImage()throws();

         /**
          * Create a raster without an image at the specified location.
          * @param bounds the raster's bounding box
          */
      public:
         RasterImage(const Bounds& bounds)throws();

         /**
          * Create a raster with an image. The image is assumed to be in the coordinate reference frame
          * of the location.
          * @param bounds the raster's bounding box
          * @param image an image pointer (can be null)
          */
      public:
         RasterImage(const Bounds& bounds, const ImagePtr& image)throws();

         /**
          * Copy constructor that copies the handle,
          * but keeps the same implementation.
          */
      public:
         RasterImage(const RasterImage& g)throws();

         /**  Destroy this node handle */
      public:
         ~RasterImage()throws();

         /**
          * Get the top-left corner of this raster.
          * @return the top-left corner.
          */
      public:
         inline const Bounds& bounds() const throws() {return _bounds;}

         /**
          * Get the image.
          * @return an image or a null pointer
          */
      public:
         inline const ImagePtr& image() const throws() {return _image;}

         /** The geographic locations corresponding to the corners of the image. */
      private:
         Bounds _bounds;

         /** The image itself */
      private:
         ImagePtr _image;
   };
}
#endif
