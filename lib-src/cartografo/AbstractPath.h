#ifndef _CARTOGRAFO_ABSTRACTPATH_H
#define _CARTOGRAFO_ABSTRACTPATH_H

#ifndef _CARTOGRAFO_PATH_H
#include <cartografo/Path.h>
#endif

namespace cartografo {
  /**
   * A contour is defined by a connected set of vertices and may possibly
   * be closed.
   */
  class AbstractPath : public Path {
    /** No copying allowed */
    AbstractPath(const AbstractPath&);
    AbstractPath&operator=(const AbstractPath&);
    
    /** The default geodesic  error in meters */
  public:
    static constexpr double DEFAULT_GEODESIC_ERROR = 1000;
    
    /** Create a new contour impl */
  protected:
    inline AbstractPath() throws() {}
    
    /** Destroy this contour  impl */
  protected:
    ~AbstractPath() throws();
    
    /**
     * Map this path into a loxodromic path where the length of each segment
     * is limited to the specified length.
     * @param length the length of each straight line segment.
     * @return a loxodromic path with a bounded segment length
     */
  public:
    ::timber::Reference<Path> toBoundedLoxodromicPath (double length) const throws();
    
    /**
     * Map this path into a loxodromic path. Path's that are known to be loxodromic
     * should override this method and return themselves.
     * @param err the error when mapping this path to a loxodromic path
     * @return a path that is a loxodromic path
     */
  public:
    ::timber::Reference<Path> toLoxodromicPath (double err) const throws();
    
    /**
     * Project this path into an equivalent path in a plane.
     * @param p a projection
     * @return a path on a plane
     */
  public:
    void project (Projector& p) const;
  };
}

#endif
