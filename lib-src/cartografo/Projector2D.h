#ifndef _CARTOGRAFO_PROJECTOR2D_H
#define _CARTOGRAFO_PROJECTOR2D_H

#ifndef _CARTOGRAFO_PROJECTOR_H
#include <cartografo/Projector.h>
#endif

#include <vector>

namespace tikal {
   class Path2D;
   class Region2D;
}

namespace cartografo {
   /** A projection */
   class Projection;
   class Region;

   /**
    * Instances of a projector are able to project geographic areas and paths
    * into mostly 2D regions and paths.
    */
   class Projector2D : public Projector
   {
         Projector2D(const Projector2D&);
         Projector2D&operator=(const Projector2D&);

         /**
          * Default constructor.
          */
      protected:
         Projector2D()throws();

         /**
          * Destructor.
          */
      public:
         ~Projector2D()throws();

         /**
          * Get a projector for the specified projection.
          * @param p a projection
          * @return a projector or 0 if an error occurs
          */
      public:
         static ::timber::Pointer< Projector2D> create(const ::timber::Reference< Projection>& p)throws();

         /**
          * Clone or copy this projector. The effect of this function is as if
          * the copy constructor was invoked on this projector.
          * <p>
          * Transient state must not be copied by this projector. Transient state
          * are such things as cached objects, started paths, etc. It is
          * best to call this function only from a newly create projector.
          *
          * @return a copy of this projector
          */
      public:
         virtual Projector2D* copy() const throws() = 0;

         /**
          * Get the projection.
          * @return the projection
          */
      public:
         virtual ::timber::Reference< Projection> projection() const throws() = 0;

         /**
          * Get the 2d path that was projected. It is undefined
          * whether or not this method will return the same result
          * for consecutive invocations.
          * @param result a vector to which the projected paths are added
          * @return the number of paths added to the result
          */
      public:
         virtual size_t getPaths(std::vector< ::timber::Reference< ::tikal::Path2D> >& result)throws() = 0;

         /**
          * Get the regions that were projected since the last call.
          * @param result a vector to which the projected regions are added
          * @return a list of regions
          */
      public:
         virtual size_t getRegions(std::vector< ::timber::Reference< ::tikal::Region2D> >& result)throws() = 0;

         /**
          * Project a region using a projector.
          * @param p a projector
          * @param r a region
          * @return a region2D
          */
      public:
         static ::timber::Pointer< ::tikal::Region2D> projectRegion(Projector2D& p, const Region& r)throws();
   };
}

#endif
