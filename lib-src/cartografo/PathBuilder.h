#ifndef _CARTOGRAFO_PATHBUILDER_H
#define _CARTOGRAFO_PATHBUILDER_H

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

#ifndef _CARTOGRAFO_PATH_H
#include <cartografo/Path.h>
#endif

#ifndef _CARTOGRAFO_PATHSEGMENT_H
#include <cartografo/PathSegment.h>
#endif

#include <vector>

namespace terra {
  class Bounds;
}

namespace cartografo {

  /**
   * A path builder enables the creation ofsurface paths.
   */
  class PathBuilder {
    /** No copy methods */
    PathBuilder(const PathBuilder&);
    PathBuilder&operator=(const PathBuilder&);

  public:
    enum Direction {
      EAST,
      WEST,
      SHORTEST
    };
    
    /** A path point */
  private:
    struct WayPoint;
      
    /**
     * Create a new path builder.
     */
  public:
    PathBuilder() throws();

    /** Destructor */
  public:
    ~PathBuilder() throws();

    /**
     * Reset this builder.
     */
  public:
    void reset() throws();

    /**
     * Move to a new position.
     * @param x a point
     */
  public:
    void moveTo (const ::terra::GeodeticCoordinates& x) throws();
      
    /**
     * Create a geodesic from the last point to the specified point.
     * The general direction will be such that the length of this segment is minimal.
     * The insertion of a point at either pole may introduce extra points at that pole.
     * @param x a point
     * @throws ::std::exception if there was no prior moveTo() call
     */
  public:
    void geodesicTo (const ::terra::GeodeticCoordinates& x) throws (::std::exception);

    /**
     * Create a loxodrome from the last point to the specified point.  The insertion
     * of a point at either pole may introduce extra points at that pole.
     * @param x a point
     * @param dir the direction of travel
     * @throws Exception if there was no prior moveTo() call
     */
  public:
    void loxodromeTo (const ::terra::GeodeticCoordinates& x, Direction dir = SHORTEST) throws (::std::exception);

    /**
     * Create a loxodromic path between two points.
     * @param p the start point
     * @param q the end point
     * @return a loxodromic path
     */
  public:
    static ::timber::Reference<Path> createLoxodromicPath (const ::terra::GeodeticCoordinates& p, const ::terra::GeodeticCoordinates& q);

    /**
     * Create a loxodromic path. The path between cooordinates is
     * defined as the loxodrome between two points.
     * @param n the number of waypoints
     * @param coords the waypoints along the path
     * @param close close the path if true
     * @return a path consisting of loxodomic segments
     */
  public:
    static ::timber::Pointer<Path> createLoxodromicPath (size_t n, const ::terra::GeodeticCoordinates* coords, bool close);
      
    /**
     * Create a loxodromic path. The path between cooordinates is
     * defined as the loxodrome between two points.
     * @param coords the waypoints along the path
     * @param close close the path if true
     * @return a path consisting of loxodomic segments
     */
  public:
    static ::timber::Pointer<Path> createLoxodromicPath (const ::std::vector< ::terra::GeodeticCoordinates>& coords, bool close);
    
    /**
     * Create a geodesic path between two points.
     * @param p the start point
     * @param q the end point
     * @return a geodesic path
     */
  public:
    static ::timber::Reference<Path> createGeodesicPath (const ::terra::GeodeticCoordinates& p, const ::terra::GeodeticCoordinates& q);

    /**
     * Create a geodesic path. The path between cooordinates is
     * defined as the geodesic between two points.
     * @param n the number of waypoints
     * @param coords the waypoints along the path
     * @param close close the path if true
     * @return a path consisting of geodesic segments
     */
  public:
    static ::timber::Pointer<Path> createGeodesicPath (size_t n, const ::terra::GeodeticCoordinates* coords, bool close);
      
    /**
     * Create a geodesic path. The path between cooordinates is
     * defined as the geodesic between two points.
     * @param coords the waypoints along the path
     * @param close close the path if true
     * @return a path consisting of geodesic segments
     */
  public:
    static ::timber::Pointer<Path> createGeodesicPath (const ::std::vector< ::terra::GeodeticCoordinates>& coords, bool close);

    /**
     * Create a path for the specified bounds object. The resulting path is created in clockwise order!
     * @param bounds a bounds object
     * @return the closed path that corresponds to the outline of the bounds
     */
  public:
    static ::timber::Pointer<Path> createBoundsPath (const ::terra::Bounds& bounds);

    /**
     * Create a path for the specified bounds object. The resulting path is created in clockwise order!
     * @param bounds a bounds object
     * @param dLat the difference in latitude between points on the path
     * @param dLon the difference in longitude between points on the path
     * @return the closed path that corresponds to the outline of the bounds
     */
  public:
    static ::timber::Pointer<Path> createBoundsPath (const ::terra::Bounds& bounds, double dLat, double dLon);
      
    /**
     * Create a closed path for a geodesic circle through two specified points.
     * @param a a point
     * @param b a point 
     * @pre REQUIRE_FALSE(a.equals(b))
     * @pre REQUIRE_FALSE(a.equals(b.anitpodalCoordinates()))
     * @return a path that represents a circle
     */
  public:
    static ::timber::Reference<Path> createGeodesicCircle(const ::terra::GeodeticCoordinates& a, const ::terra::GeodeticCoordinates& b);

    /**
     * Create a closed path for a circle with radius given in meters.
     * @param ctr the center of the circle
     * @param radius the radius of the circle in meters
     * @param nVertices the number of vertices for the circle
     * @pre REQUIRE_GREATER_OR_EQUAL(radius,0.0)
     * @return a path that represents a circle
     */
  public:
    static ::timber::Reference<Path> createLoxodromicCircle(const ::terra::GeodeticCoordinates& ctr, double radius, size_t nVertices);
      
    /**
     * Create a closed path for a circle with radius given in meters.
     * @param ctr the center of the circle
     * @param radius the radius of the circle in meters
     * @pre REQUIRE_GREATER_OR_EQUAL(radius,0.0)
     * @return a path that represents a circle
     */
  public:
    static ::timber::Reference<Path> createLoxodromicCircle(const ::terra::GeodeticCoordinates& ctr, double radius);
      
    /**
     * Create a closed path for a circle with a radius given in radians.
     * @param ctr the circle center
     * @param radRadius the radius in radians of the circle
     * @param nVertices the number of vertices for the circle
     * @pre REQUIRE_GREATER(radius,0.0);
     * @return a path in the shape of a circle
     */
  public:
    static ::timber::Reference<Path> createRadiansCircle(const ::terra::GeodeticCoordinates& ctr, double radRadius, size_t nVertices);

    /**
     * Create a closed path for a circle with a radius given in radians.
     * @param ctr the circle center
     * @param radRadius the radius in radians of the circle
     * @pre REQUIRE_GREATER(radius,0.0);
     * @return a path in the shape of a circle
     */
  public:
    static ::timber::Reference<Path> createRadiansCircle(const ::terra::GeodeticCoordinates& ctr, double radRadius);

    /**
     * Create a path for the that follows the outline of the horizon.  The observer
     * must be <em>above</em> the ellipsoid f or this function to return a meaningful result.
     * @param obs an observer location
     * @return a path or 0 if none could be created for the observer
     */
  public:
    static ::timber::Pointer<Path> createHorizonPath (const ::terra::GeodeticCoordinates& obs) throws();

    /**
     * Get the path as built up to now.
     * @return the path built so far
     */
  public:
    ::timber::Pointer<Path> path() const throws();

    /**
     * Add new way point
     * @param p a way point
     */
  private:
    void addWayPoint (const WayPoint& p) throws();

    /**
     * Add a new waypoint that crosses the specified pole.
     * @param pole either PI/2 or -PI/2
     * @param to the point to which to move
     */
  private:
    void loxodromeAcrossPole (double pole, const ::terra::GeodeticCoordinates& to) throws();

    /**
     * The last waypoint.
     * @pre assert(!_needMoveTo)
     * @return the last waypoint
     */
  private:
    const WayPoint& lastWayPoint() const throws();
    
    /** True if we need a move to */
  private:
    bool _needMoveTo;

    /** The current path */
  private:
    mutable ::timber::Pointer<Path> _path;

    /** The size of the data structure */
  private:
    size_t _size;

    /** The capacity */
  private:
    size_t _capacity;

    /** The path points */
  private:
    WayPoint* _waypoints;

  };
}
#endif
