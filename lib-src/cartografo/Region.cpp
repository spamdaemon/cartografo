#include <cartografo/Region.h>
#include <cartografo/Area.h>
#include <cartografo/AreaBuilder.h>

#include <cartografo/projector/IdentityProjector2D.h>
#include <cartografo/Projection.h>

#include <terra/Extent.h>
#include <terra/Bounds.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Spheroid.h>

#include <tikal/Region2D.h>
#include <tikal/Area2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Point2D.h>
#include <tikal/VertexOrder.h>

#include <timber/logging.h>

#include <vector>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::tikal;
using namespace ::terra;

using namespace ::cartografo::projector;

namespace cartografo {
   namespace {
      static Log logger()
      {
         return Log("cartografo.Region");
      }

      struct ExtentRegion : public Region
      {
            ExtentRegion(const Extent& e) :
               _extent(e)
            {
               _extent.shiftTowards(Extent());
            }

            ~ExtentRegion()throws()
            {}

            bool contains(double lat, double lon) const throws()
            {
               Extent ext(lat,lon);
               return ext.shiftTowards(_extent);
            }

            void project(Projector& p) const
            {
               if (classifyExtent(_extent) == OUTSIDE) {
                  return;
               }

               if (!p.beginOutsidePath(true, &_extent)) {
                  return;
               }
               p.moveTo(_extent.north(), _extent.west(), 0, 0);
               Projector::Arc arc;
               arc.endAlt = 0;
               arc.endLat = _extent.north();
               arc.endLon = _extent.east();
               p.moveAlongArcs(1, &arc, Projector::LOXODROME, 0);
               arc.endLat = _extent.south();
               arc.endLon = _extent.east();
               p.moveAlongArcs(1, &arc, Projector::LOXODROME, 0);
               arc.endLat = _extent.south();
               arc.endLon = _extent.west();
               p.moveAlongArcs(1, &arc, Projector::LOXODROME, 0);
               arc.endLat = _extent.north();
               arc.endLon = _extent.west();
               p.moveAlongArcs(1, &arc, Projector::LOXODROME, 0);
            }

            Extent extent() const throws() {return _extent;}

         private:
            Extent _extent;
      };

      struct Region2DRegion : public Region
      {
            Region2DRegion(const Reference< Region2D>& r) :
               _region(r)
            {
               BoundingBox2D box(r->bounds());
               _extent = Extent(box.miny(), box.minx(), box.maxy(), box.maxx());
            }

            ~Region2DRegion()throws()
            {}

            bool contains(double lat, double lon) const throws()
            {
               Extent ext(_extent);

               // move the point so it fits into the extent
               if (lon < ext.west()) {
                  do {
                     lon += 2*PI;
                  }while(lon < ext.west());
               }
               else if (lon > ext.east()) {
                  do {
                     lon -= 2*PI;
                  }while(lon>ext.east());
               }

               if (!ext.contains(lat,lon)) {
                  return false;
               }

               return _region->contains(lon,lat);
            }

            void project(Projector& p) const
            {
               if (classifyExtent(_extent) == OUTSIDE) {
                  return;
               }
               for (size_t i = 0, rsz = _region->areaCount(); i < rsz; ++i) {
                  Reference< Area2D> a(_region->area(i));
                  bool ok = projectContour2D(p, a->outsideContour(), true);
                  if (ok) {
                     for (size_t j = 0, asz = a->contourCount(); j < asz; ++j) {
                        projectContour2D(p, a->insideContour(j), false);
                     }
                  }
               }
            }

            Extent extent() const throws() {return _extent;}

         private:
            bool projectContour2D(Projector& p, const Reference< Contour2D>& c, bool outside) const
            {
               if (c->maxDegree() != 1) {
                  return false;
               }
               bool ok;
               if (outside) {
                  ok = p.beginOutsidePath(c->vertexOrder().isCW(), 0);
               }
               else {
                  ok = p.beginInsidePath(c->vertexOrder().isCW(), 0);
               }
               if (!ok) {
                  return false;
               }
               Point2D pt[2];
               Projector::Arc arc;
               arc.endAlt = 0;
               arc.arcType = Projector::LOXODROME;
               size_t dim = c->segment(0, pt);
               assert(dim == 1);
               p.moveTo(pt[0].y(), pt[0].x(), 0, 0);
               for (size_t i = 0, sz = c->segmentCount(); i < sz; ++i) {
                  dim = c->segment(i, pt);
                  assert(dim == 1);
                  arc.endLat = pt[1].y();
                  arc.endLon = pt[1].x();
                  p.moveAlongArcs(1, &arc, Projector::LOXODROME, 0);
               };
               return true;
            }

         private:
            Extent _extent;

         private:
            Reference< Region2D> _region;
      };

      struct MultiRegion : public Region
      {
            /** Create a new region */
         public:
            MultiRegion(const ::std::vector< Reference< Area> >& regs)
            {
               for (size_t i = 0, sz = regs.size(); i < sz; ++i) {
                  _regions.push_back(regs[i]);
                  if (_regions.empty()) {
                     _extent = regs[i]->extent();
                  }
                  else {
                     _extent.merge(regs[i]->extent());
                  }
               }
            }

            MultiRegion(const ::std::vector< Reference< Region> >& regs)
            {
               for (size_t i = 0, sz = regs.size(); i < sz; ++i) {
                  _regions.push_back(regs[i]);
                  if (_regions.empty()) {
                     _extent = regs[i]->extent();
                  }
                  else {
                     _extent.merge(regs[i]->extent());
                  }
               }
            }

            ~MultiRegion()throws()
            {}

            bool contains(double x, double y) const throws()
            {
               for (::std::vector<Reference<Region> >::const_iterator i=_regions.begin();i!=_regions.end();++i) {
                  if ( (*i)->contains(x,y)) {
                     return true;
                  }
               }
               return false;
            }

            void project(Projector& p) const
            {
               if (classifyExtent(_extent) == OUTSIDE) {
                  return;
               }

               for (::std::vector< Reference< Region> >::const_iterator i = _regions.begin(); i != _regions.end(); ++i) {
                  (*i)->project(p);
               }
            }

            Extent extent() const throws() {return _extent;}

            Classification classifyExtent(const Extent& e) const throws()
            {
               for (size_t i=0;i<_regions.size();++i) {
                  switch(_regions[i]->classifyExtent(e)) {
                     case OUTSIDE:
                     // try the next region
                     break;
                     case CONTAINED:
                     return CONTAINED;
                     case POSSIBLE_OVERLAP:
                     return POSSIBLE_OVERLAP;
                  }
               }
               return OUTSIDE;
            }

            /** The regions */
         private:
            ::std::vector< Reference< Region> > _regions;

            /** The total extent */
         private:
            Extent _extent;
      };
   }

   Region::Region()
   throws()
   {}

   Region::~Region()
   throws()
   {}

   Reference< Region> Region::createExtentRegion(const ::terra::Extent& e)
   throws()
   {
      return new ExtentRegion(e);
   }

   Reference< Region> Region::createSpheroidRegion(const ::timber::Reference< GeodeticDatum>& s)
   throws()
   {  return AreaBuilder::createBoundsArea(Bounds(s));}

   Reference< Region> Region::createBoundsRegion(const Bounds& b)
   throws()
   {  return AreaBuilder::createBoundsArea(b);}

   Pointer< Region> Region::createRegion(const ::std::vector< Reference< Area> >& v)
   throws()
   {
      if (v.empty()) {return Pointer<Region>();}
      if (v.size()==1) {return v[0];}

      return new MultiRegion(v);
   }

   Pointer< Region> Region::createRegion(const ::std::vector< Reference< Region> >& v)
   throws()
   {
      if (v.empty()) {return Pointer<Region>();}
      if (v.size()==1) {return v[0];}

      return new MultiRegion(v);
   }

   Pointer< Region> Region::intersect(const Pointer< Region>& a, const Pointer< Region>& b)
   throws()
   {
      if (a==b || !b) {return a;}
      if (!a) {return b;}

      Reference<Projector2D> ip = new IdentityProjector2D(GeodeticDatum::create());

      Pointer< Region2D> r1 = Projector2D::projectRegion(*ip,*a);
      Pointer< Region2D> r2 = Projector2D::projectRegion(*ip,*b);
      Pointer< Region2D> r = r1->intersect(r2);

      Pointer<Region> res;
      if (r) {
         res = new Region2DRegion(r);
      }
      return res;
   }

   Pointer< Region> Region::merge(const Pointer< Region>& a, const Pointer< Region>& b)
   throws()
   {
      if (a==b || !b) {return a;}
      if (!a) {return b;}

      Reference<Projector2D> ip = new IdentityProjector2D(GeodeticDatum::create());
      Pointer<Region2D> r1 = Projector2D::projectRegion(*ip,*a);
      Pointer<Region2D> r2 = Projector2D::projectRegion(*ip,*b);
      Pointer<Region2D> r = r1->merge(r2);

      Pointer<Region> res;
      if (r) {
         res = new Region2DRegion(r);
      }
      return res;
   }

   Pointer< Region> Region::subtract(const Reference< Region>& a, const Pointer< Region>& b)
   throws()
   {
      if (a==b) {return Pointer<Region>();}
      if (!b) {return a;}

      Reference<Projector2D> ip = new IdentityProjector2D(GeodeticDatum::create());
      Pointer< Region2D> r1 = Projector2D::projectRegion(*ip,*a);
      Pointer< Region2D> r2 = Projector2D::projectRegion(*ip,*b);
      Pointer< Region2D> r = r1->subtract(r2);

      Pointer<Region> res;
      if (r) {
         res = new Region2DRegion(r);
      }
      return res;
   }

   Pointer< Region> Region::exclusiveOR(const Pointer< Region>& a, const Pointer< Region>& b)
   throws()
   {
      if (a==b) {return Pointer<Region>();}
      if (!a) {return b;}
      if (!b) {return a;}

      Reference<Projector2D> ip = new IdentityProjector2D(GeodeticDatum::create());
      Pointer< Region2D> r1 = Projector2D::projectRegion(*ip,*a);
      Pointer< Region2D> r2 = Projector2D::projectRegion(*ip,*b);
      Pointer< Region2D> r = r1->exclusiveOR(r2);

      Pointer<Region> res;
      if (r) {
         res = new Region2DRegion(r);
      }
      return res;
   }

   Pointer< Region> Region::normalize() const
   throws()
   {
      Reference<IdentityProjector2D> ip = new IdentityProjector2D(GeodeticDatum::create());
      ip->setExpectSelfIntersections(true);
      Pointer< Region2D> r = Projector2D::projectRegion(*ip,*this);
      Pointer<Region> res;
      if (r) {
         res = new Region2DRegion(r);
      }
      return res;
   }

   bool Region::contains(double lat, double lon) const
   throws()
   {
      Extent ext(extent());

      if (lon < ext.west()) {
         do {
            lon += 2*PI;
         }while(lon < ext.west());
      }
      else if (lon > ext.east()) {
         do {
            lon -= 2*PI;
         }while(lon>ext.east());
      }

      if (!ext.contains(lat,lon)) {
         return false;
      }

      Reference<IdentityProjector2D> ip = new IdentityProjector2D(GeodeticDatum::create());
      ProjectionMapping pmap;
      ip->projection()->project(lat,lon,pmap);
      if (pmap.type() != ProjectionMapping::POINT) {
         return false;
      }

      Pointer< Region2D> r = Projector2D::projectRegion(*ip,*this);

      // since the projector is an identity projector, the projection will just echo back the args.
      return r && r->contains(pmap.point());
   }

   Region::Classification Region::classifyExtent(const Extent& e) const
   throws()
   {
      Extent ext(extent());
      bool shifted = ext.shiftTowards(e);
      return shifted ? POSSIBLE_OVERLAP : OUTSIDE;
   }

   Region::Classification Region::classifyRegion(const Region& r) const
throws()
{
   return classifyExtent(r.extent());
}

}
