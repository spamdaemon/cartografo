#include <cartografo/AbstractProjector2D.h>

#include <terra/Extent.h>

#include <tikal/Path2D.h>
#include <tikal/Region2D.h>

namespace cartografo {
  AbstractProjector2D::AbstractProjector2D() throws()
  {}
  AbstractProjector2D::~AbstractProjector2D() throws()
  {}
    
  ::timber::Pointer< ::tikal::Path2D> AbstractProjector2D::getPath() throws()
  { return ::timber::Pointer< ::tikal::Path2D>(); }
    
  ::timber::Pointer< ::tikal::Region2D> AbstractProjector2D::getRegion() throws()
  { return ::timber::Pointer< ::tikal::Region2D>(); }
    
  bool AbstractProjector2D::beginOutsidePath(bool,const ::terra::Extent* )
  { return false; }
    
  bool AbstractProjector2D::beginInsidePath(bool,const ::terra::Extent* )
  { return false; }
    
  bool AbstractProjector2D::beginPath(const ::terra::Extent* )
  { return false; }
    
  bool AbstractProjector2D::moveTo(double, double , double , const ::terra::Extent* )
  { return false; }
    
  void AbstractProjector2D::moveAlongArcs (size_t , const Arc*, ArcType,const ::terra::Extent* )
  {}
}
