#ifndef _CARTOGRAFO_EXTENTPYRAMID_H
#define _CARTOGRAFO_EXTENTPYRAMID_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _CARTOGRAFO_REGION_H
#include <cartografo/Region.h>
#endif

#include <vector>

namespace cartografo {
  /**
   * The extent pyramid is used to approximate regions and paths are increasingly smaller
   * levels of detail. At each level, the region is approximated by a set of extents. This
   * class can be used to rasterize regions, but its primary purpose is to allow for quick
   * region-region or region-extent classification.<br>
   * The top-level of the pyramid is entire spheroid. At each level, the extent at the previous
   * level is divided into four equal sized regions. If a region contains part of the region
   * or path, then it is further subdivided, otherwise it subdivision stops.
   */
  class ExtentPyramid : public ::timber::Counted {
    
    /**
     * Default constructor
     */
  protected:
    ExtentPyramid() throws();

    /** Destructor */
  public:
    ~ExtentPyramid() throws();

    /**
     * Create a new extent pyramid. At each level, an extent is divided by 
     * @param r a region
     * @param levels the number of pyramid levels.
     * @return an extent pyramid
     */
  public:
    static ::timber::Reference<ExtentPyramid> createRegionPyramid(const ::timber::Reference<Region>& r, size_t nLevels) throws();

    /**
     * Classify the specified extent with respect to this pyramid.
     * @param ext an extent
     * @return a region classification
     */
  public:
    virtual Region::Classification classifyExtent (const ::terra::Extent& e) const throws() = 0;

    /**
     * Get the leaf extents of this pyramid. This function is primarily used for debugging
     * @param v a vector to which the extents will be appended!
     */
  public:
    virtual void gatherLeafExtents(::std::vector< ::terra::Extent>& v) const throws() = 0;
  };

}
#endif
