#ifndef _CARTOGRAFO_PROJECTION_H
#define _CARTOGRAFO_PROJECTION_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif 

#ifndef _CARTOGRAFO_PROJECTIONMAPPING_H
#include <cartografo/ProjectionMapping.h>
#endif 

#ifndef _TERRA_PROJECTEDCRS_H
#include <terra/ProjectedCRS.h>
#endif

namespace tikal {
   class Point2D;
}

namespace terra {
   class GeodeticCoordinates;
}

namespace cartografo {

   /**
    * @name Forward declarations
    * @{
    */
   class ProjectionRequest;
   class ProjectionDomain;
   /*@}*/

   namespace projection {
      /** Forward declaration */
      class ProjectionImpl;
   }

   /**
    * The projection class allows Location objects to be
    * projected into a  Cartesian coordinate system.
    */
   class Projection : public ::terra::ProjectedCRS
   {

         /** Create a default projection.  */
      protected:
         Projection()throws();

         /** Destructor */
      public:
         ~Projection()throws();

         /**
          * Create a projection from a ProjectedCRS.
          * @param crs a projectedCRS
          * @param boundary the projection domain or boundary
          * @return a projection
          * @throws ::std::invalid_argument if the datum of the crs and the boundary are different
          */
      public:
         static ::timber::Reference<Projection> create (const ::timber::Reference< ::terra::ProjectedCRS>& crs,const ::timber::Reference< ProjectionDomain>& boundary) throws(::std::invalid_argument);

         /**
          * Create an identity projection. This projection simply
          * maps each latitude and longitude into x and y in degrees. The
          * identity projection enable inverse projection as well. The center
          * of the projection is that point for which the projection will return
          * the single point (0,0).
          * @param c the center of the projection
          * @return a projection that maps a geodetic coordinates (lat,lon,height) into (lat,lon,height).
          */
      public:
         static ::timber::Reference< Projection> createIdentityProjection(const ::terra::GeodeticCoordinates& c)throws();

         /**
          * Get the projection domain.
          * @return the domain of this projection
          */
      public:
         virtual ::timber::Reference< ProjectionDomain> domain() const throws() = 0;

         /**
          * Project coordinates fast, but without sanity checks. For
          * each point, x() will be used as longitude in radians, and y() will be used as latitude()
          * in radians(). Upon projection, each point is modified to contain the closest
          * projection point.
          * @note Implementations are not required to actually run faster than they would by
          * calling project() for each point.
          * @note If projection uses actually altitude, then it must use an implied altitude of 0.
          * @param n the number of coordinates to be projected
          * @param lonlat the points to be projected and the result of the projection
          * @return the number of points that have been projected
          */
      public:
         virtual size_t projectUnsafe(size_t n, tikal::Point2D* lonlat) const throws() = 0;

         /**
          * Perform an inverse projection. Some projections will not
          * admit an inverse project.
          * @param x coordinate of a point
          * @param y coordinate of a point
          * @return the geodetic location corresponding to the given point.
          * @throws ::std::exception if the point could not be inversely projected
          */
      public:
         virtual ::terra::GeodeticCoordinates projectInverse(double x, double y) const
         throws( ::std::exception) = 0;

         /**
          * Test if this projection admits an inverse projection. If
          * this method returns false, then projectInverse will always throw
          * an exception.
          * @return true if projectInverse is supported
          */
      public:
         virtual bool isInverseProjectionEnabled() const throws() = 0;

         /**
          * Service multiple projection requests.
          * @param n the number of projection requests
          * @param req the projection requests
          * @return the number of the first n completed projections
          */
      public:
         virtual size_t project(size_t n, ProjectionRequest* req) const throws() = 0;

         /**
          * Service a projection requests.
          * @param req the projection request
          * @return true if the projection was successful, false otherwise
          */
      public:
         inline bool project(ProjectionRequest& req) const throws()
         {  return project(1,&req)==1;}

         /**
          * Project an individual point forward.
          * @param lat latitude in radians
          * @param lon a longitude in radians
          * @param map a projection mapping
          * @return map
          */
      public:
         inline ProjectionMapping& project(double lat, double lon, ProjectionMapping& map) const throws()
         {  return project(lat,lon,0,map);}

         /**
          * Project an individual point forward.
          * @param lat latitude in radians
          * @param lon a longitude in radians
          * @param alt the altitude
          * @param map a projection mapping
          * @return map
          */
      public:
         ProjectionMapping& project(double lat, double lon, double alt, ProjectionMapping& map) const throws();

         /**
          * Project an individual point forward.
          * @param pt a geodetic coordinates object
          * @param map a projection mapping
          * @return map
          */
      public:
         ProjectionMapping& project(const ::terra::GeodeticCoordinates& pt, ProjectionMapping& map) const throws();

         /**
          * Project an individual point forward.
          * @param pt a geodetic coordinates object
          * @param map a projection mapping
          * @return map
          */
      public:
         ProjectionMapping project(const ::terra::GeodeticCoordinates& pt) const throws();

         /**
          * Project an individual point forward.
          * @param lat latitude in radians
          * @param lon a longitude in radians
          * @param map a projection mapping
          * @return map
          */
      public:
         inline ProjectionMapping project(double lat, double lon) const throws()
         {  return project(lat,lon,0);}

         /**
          * Project an individual point forward.
          * @param lat latitude in radians
          * @param lon a longitude in radians
          * @param alt the altitude
          * @return map
          */
      public:
         ProjectionMapping project(double lat, double lon, double alt) const throws();

      public:
         ::timber::Reference< ::terra::Datum> datum() const  throws();
         size_t coordinateCount() const throws();
         bool toEPSG4979(const double* coords, EPSG4979Coordinates& epsg4979) const throws();
         bool fromEPSG4979(const EPSG4979Coordinates& epsg4979, double* coords) const throws();
         bool equals(const ::terra::CoordinateReferenceSystem& frame) const throws();
         ::terra::EpsgCode epsgCode() const throws();
         double distance (const double* p1, const double* p2) const throws();
   };

}

#endif
