#include <cartografo/projection/AzimuthalProjection.h>
#include <cartografo/ProjectionDomain.h>
#include <cartografo/ProjectionRequest.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/GeodeticDatum.h>
#include <tikal/Point2D.h>

using namespace cartografo;
using namespace cartografo::projection;
using namespace tikal;
using namespace terra;
using namespace timber;

static void testProjection (Reference<Projection> p)
{
  ProjectionMapping map;
  const GeodeticCoordinates pot(p->domain()->center());
  ProjectionRequest req(pot.latitude(),pot.longitude(),pot.height());
  p->project(1,&req);
  assert (req.map.point().sqrDistance(Point2D(0,0)) < .00625);
}

void utest_1()
{
  const Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createSphere(6380000.0,0,0,0));
  
  GeodeticCoordinates pot=GeodeticCoordinates::fromDegrees(45,90,0,e);
  Reference<Projection> vertical = AzimuthalProjection::createVerticalPerspectiveProjection(pot,100000);
  Reference<Projection> tilted = AzimuthalProjection::createTiltedPerspectiveProjection(pot);
  Reference<Projection> gnomonic = AzimuthalProjection::createGnomonicProjection(pot);
  Reference<Projection> stereographic = AzimuthalProjection::createStereographicProjection(pot);
  Reference<Projection> lambert = AzimuthalProjection::createLambertEqualAreaProjection(pot);
  Reference<Projection> equidistant = AzimuthalProjection::createEquidistantProjection(pot);
  Reference<Projection> ortho = AzimuthalProjection::createOrthographicProjection(pot);
  
  testProjection(gnomonic);
  testProjection(stereographic);
  testProjection(lambert);
  testProjection(equidistant);
  testProjection(ortho);
  testProjection(vertical);
  //@todo  testProjection(tilted);
}

