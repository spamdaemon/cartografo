#include <cartografo/PathBuilder.h>
#include <cartografo/Path.h>
#include <cartografo/PathSegment.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Spheroid.h>

#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;
using namespace ::cartografo;

static void printPath(const Reference<Path>& path)
{
  for (size_t i=0,sz=path->segmentCount();i<sz;++i) {
    const PathSegment& seg = path->segment(i);
    switch(seg.type()) {
    case PathSegment::MOVE:
      cerr << "Move-to " << seg.endPoint() << endl; break;
    case PathSegment::LOXODROME:
      cerr << "Loxodrome-to " << seg.endPoint() << endl; break;
    case PathSegment::GEODESIC:
      cerr << "Geodesic-to " << seg.endPoint() << endl; break;
    default:
      assert(false);
    };
  }
}

void utest_1()
{
  Spheroid::setDefaultSpheroid(Spheroid::createSphere(6378135.0,0,0,0));
  GeodeticCoordinates coords[]= {
    GeodeticCoordinates::fromDegrees(0,0),
    GeodeticCoordinates::fromDegrees(10,0),
    GeodeticCoordinates::fromDegrees(10,10),
    GeodeticCoordinates::fromDegrees(0,10),
  };
  GeodeticCoordinates x;
  
  {
    cerr << "LOXODROMIC PATH" << endl;
    Reference<Path> path (PathBuilder::createLoxodromicPath(4,coords,true));
    printPath(path);
    printPath(path->toLoxodromicPath(10));
  }

  {
    cerr << "GEODESIC PATH" << endl;
    Reference<Path> path (PathBuilder::createGeodesicPath(4,coords,true));
    printPath(path);
    cerr << "GEODESIC->LOXODROMIC PATH" << endl;
    printPath(path->toLoxodromicPath(40));
  }
}

void utest_2()
{
  Spheroid::setDefaultSpheroid(Spheroid::createSphere(6378135.0,0,0,0));
  GeodeticCoordinates coords[]= {
    GeodeticCoordinates::fromDegrees(90,0),
    GeodeticCoordinates::fromDegrees(0,0),
    GeodeticCoordinates::fromDegrees(0,20),
    GeodeticCoordinates::fromDegrees(90,60),
  };
  GeodeticCoordinates x;
  
  {
    cerr << "LOXODROMIC PATH" << endl;
    Reference<Path> path (PathBuilder::createLoxodromicPath(4,coords,true));
    printPath(path);
    printPath(path->toLoxodromicPath(10));
  }

  {
    cerr << "GEODESIC PATH" << endl;
    Reference<Path> path (PathBuilder::createGeodesicPath(4,coords,true));
    printPath(path);
    cerr << "GEODESIC->LOXODROMIC PATH" << endl;
    printPath(path->toLoxodromicPath(40));
  }
}

