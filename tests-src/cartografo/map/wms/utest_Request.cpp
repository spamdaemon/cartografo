#include <cartografo/map/wms/Request.h>
#include <timber/ContentType.h>
#include <timber/w3c/URI.h>


using namespace ::std;
using namespace ::timber;
using namespace ::timber::w3c;
using namespace ::cartografo::map::wms;

void utest_GetCapabilities()
{
   Request req;

   req.setUrl("http://localhost:8080/geoserver/wms");

   req.setVersion("1.1.1");
   req.setService("WMS");
   req.setRequest("GetCapabilities");
   req.setUpdateSequence("1");

   auto uri = req.getRequestURI();
   assert(uri && "Could not create request URI");
}


void utest_GetMap()
{
   Request req;

   req.setUrl("http://localhost:8080/geoserver/wms");

   req.setVersion("1.1.1");
   req.setService("WMS");
   req.setRequest("GetMap");

   req.addLayer("layer 1");
   req.setCRS("EPSG:2414");
   req.setBBox(Request::BBox(-1,-1,1,1));
   req.setBgColor(Request::Color(8,8,8));
   req.setWidth(10);
   req.setHeight(12);
   req.setFormat(ContentType::parse("image/png"));


   auto  uri = req.getRequestURI();
   assert(uri && "Could not create request URI");
}
