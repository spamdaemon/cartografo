#include <cartografo/map/Map.h>
#include <cartografo/map/Layer.h>
#include <cartografo/geo/nodes.h>
#include <indigo/nodes.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::cartografo::map;
using namespace ::cartografo::geo;

namespace {
  
  static Log mapLogger() { return Log("cartografo.map.Map"); }
  static Log layerLogger() { return Log("cartografo.map.Layer"); }

  struct MyLayer : public Layer {
    MyLayer() throws() 
    {
      setRootNode(new Group());
    }

    ~MyLayer() throws() {}


  public:
    void fireLayerChanged() throws()
    {
      Layer::fireLayerChanged();
    }

  public:
    void setMaterials()
    {
      layerLogger().info("Adding materials node");
      Reference<Group> g = rootNode();
      g->add(new IndigoNode(new ::indigo::SolidFillNode()));
    }
  };
}

void utest_1()
{
  Reference<Layer> top = new MyLayer();
  Reference<Layer> middle = new MyLayer();
  Reference<Layer> bottom = new MyLayer();

  Reference<Map> map = new Map();
  map->addLayer(bottom);
  map->insertLayer(top,map->layerCount());
  map->insertLayer(middle,1);

  assert(map->layerCount()==3);
  assert(map->layer(0) == bottom);
  assert(map->layer(1) == middle);
  assert(map->layer(2) == top);

  map->moveLayer(bottom,2*map->layerCount());
  assert(map->layer(2) == bottom);
  assert(map->layer(0) == middle);
  assert(map->layer(1) == top);

  map->moveLayer(top,-2*map->layerCount());
  assert(map->layer(2) == bottom);
  assert(map->layer(1) == middle);
  assert(map->layer(0) == top);

  map->moveLayer(top,1);
  assert(map->layer(2) == bottom);
  assert(map->layer(0) == middle);
  assert(map->layer(1) == top);

  map->moveLayer(bottom,-1);
  assert(map->layer(1) == bottom);
  assert(map->layer(0) == middle);
  assert(map->layer(2) == top);

  map->moveLayer(middle,1);
  assert(map->layer(0) == bottom);
  assert(map->layer(1) == middle);
  assert(map->layer(2) == top);
}

void utest_2()
{
  // check notification paths from nodes to layer to map
  mapLogger().setLevel(Level::ALL);
  layerLogger().setLevel(Level::ALL);

  Reference<MyLayer> top = new MyLayer();
  Reference<MyLayer> middle = new MyLayer();
  Reference<MyLayer> bottom = new MyLayer();

  Reference<Map> map = new Map();
  map->addLayer(bottom);
  map->addLayer(middle);
  map->addLayer(top);
  
  bottom->fireLayerChanged();
  bottom->setMaterials();

  //FIXME: assert something here
}

void utest_3()
{
  // check that a layer cannot be added to two different maps
  mapLogger().setLevel(Level::ALL);
  layerLogger().setLevel(Level::ALL);
  
  Reference<MyLayer> layer = new MyLayer();

  Reference<Map> map1 = new Map();
  Reference<Map> map2 = new Map();
  map1->addLayer(layer);
  try {
    map2->addLayer(layer);
    assert(false);
  }
  catch (...) {
  }
}
