#include <cartografo/map/MapRenderer.h>
#include <cartografo/projector/IdentityProjector2D.h>
#include <cartografo/geo/nodes.h>
#include <terra/Spheroid.h>
#include <indigo/nodes.h>

using namespace ::timber;
using namespace ::terra;
using namespace ::cartografo;
using namespace ::cartografo::projector;
using namespace ::cartografo::map;
using namespace ::cartografo::geo;

static Reference<Spheroid> createSpheroid()
{
  Reference<Spheroid> S(Spheroid::create(Spheroid::WGS_84));
  return Spheroid::createSphere((S->majorAxisLength()+S->minorAxisLength())/2,0,0,0);
}


static Reference<Projector2D> createProjector()
{ return new IdentityProjector2D(GeodeticDatum::create(createSpheroid())); }

void utest_constructor()
{
  Reference<Group> root(new Group());
  root->add(new Group());
  root->add(new Group());
  
  MapRenderer sg;
  sg.setRootNode(root);
  sg.setProjector(createProjector());
  
  Pointer< ::indigo::Node> res = sg.renderMap();
  assert(res);
}


