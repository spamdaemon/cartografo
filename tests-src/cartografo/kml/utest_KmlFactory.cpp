#include <cartografo/kml/KmlFactory.h>
#include <cartografo/kml/KmlNode.h>
#include <terra/GeodeticDatum.h>
#include <timber/logging.h>
#include <iostream>
#include <sstream>

using namespace std;
using namespace timber;
using namespace timber::logging;
using namespace cartografo::kml;

static Reference< ::terra::GeodeticDatum> datum()
{
   return ::terra::GeodeticDatum::createWGS84();
}
static Pointer< KmlFactory> newFactory()
{
   Pointer< KmlFactory> factory;
   try {
      return KmlFactory::newFactory();
   }
   catch (const ::std::exception& e) {
      cerr << "Could not get a default KmlFactory; perhaps undefined : " << e.what() << endl;
   }
   return Pointer< KmlFactory>();
}


static ::timber::SharedRef< ::timber::w3c::URI> getURI()
{
   return ::timber::w3c::URI::create("file:/foo.kml");
}
static unique_ptr< istream> getStream(const string& str)
{
   return unique_ptr < istream > (new istringstream(str));
}
static Reference< KmlNode> createNode(const string& str)
{
   auto node = newFactory()->createNode(*getStream(str),getURI(),datum());
   return node;
}

// the KML document from the libkml site
static const char* KML_DOCUMENT = "<kml xmlns='http://www.opengis.net/kml/2.2'>"
      "<Placemark>"
      "<name>hi</name>"
      "<Point>"
      "<coordinates>1,2,3</coordinates>"
      "</Point>"
      "</Placemark>"
      "</kml>";

void utest_createValidKmlNode()
{
   if (!newFactory()) {
      return;
   }


   try {
      Log("cartografo.kml.KmlNode").setLevel(Level::ALL);
      createNode(KML_DOCUMENT);
   }
   catch (const ::std::exception& e) {
      cerr << "Exception " << e.what() << endl;
      assert(false);
   }
}

// this test creates a SIGSEGV if we ParseAtom from libkml
void utest_invalidXmlDocument()
{
   static const char* INVALID_KML_DOCUMENT = "<kml xmlns='http://www.opengis.net/kml/2.2'>"
         "<Placemark>"
         "<name>hi</name>"
         "<Point>"
         "<coordinates>1,2,3</coordinates2>"
         "</Point>"
         "</Placemark>"
         "</kml>";
   if (!newFactory()) {
      return;
   }

   try {
     createNode(INVALID_KML_DOCUMENT);
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {
      cerr << "exception caught " << e.what() << endl;
   }

}
// this test creates a SIGSEGV if we ParseAtom from libkml
void utest_invalidKmlDocument()
{
   static const char* INVALID_KML_DOCUMENT = "<kml2 xmlns='http://www.opengis.net/kml/2.2'>"
         "<Placemark>"
         "<name>hi</name>"
         "<Point>"
         "<coordinates>1,2,3</coordinates>"
         "</Point>"
         "</Placemark>"
         "</kml2>";
   if (!newFactory()) {
      return;
   }
   try {
      createNode(INVALID_KML_DOCUMENT);
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {
      cerr << "exception caught " << e.what() << endl;
   }

}
// this test creates a SIGSEGV if we ParseAtom from libkml
void utest_invalidKmlNamespace()
{
#if HAVE_LIBKML == 1
   cerr << "No result because google kml is used";
#else
   static const char* INVALID_KML_DOCUMENT = "<kml xmlns='http://www.opengis.net/kml/2.21' >"
   "<Placemark>"
   "<name>hi</name>"
   "<Point>"
   "<coordinates>1,2,3</coordinates>"
   "</Point>"
   "</Placemark>"
   "</kml>";
   if (!newFactory()) {
      return;
   }

   try {
      createNode(INVALID_KML_DOCUMENT);
      assert(false);
   }
   catch (const ::std::invalid_argument& e) {
      cerr << "exception caught " << e.what() << endl;
   }
#endif
}
